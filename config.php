<?php
namespace config;

use krest\Arr;
use krest\Db;
use krest\SocNet;

date_default_timezone_set('UTC');
mb_internal_encoding('UTF-8');

list($currentSetName, $currentSet) = getConfigSet();
return array(
    '_common' => array(
        'current_config_set' => $currentSetName,
        'work_dir' => getcwd(),
        'data_dir' => $currentSet['data_dir'],
        'config_sets_dir' => $currentSet['config_sets_dir'],
    ),
    '_logger' => array(
        'logs_dir' => $currentSet['data_dir'].'/.logs',
        'loggers' => array(
            'error.program' => array(
                'appenders' => array('program_errors'),
                'level' => 'ERROR',
            ),
            'error.common' => array(
                'appenders' => array('common_errors'),
                'level' => 'ERROR',
            ),
            'error.sql' => array(
                'appenders' => array('sql_errors'),
                'level' => 'ERROR',
            ),
            'error.http' => array(
                'appenders' => array('http_errors'),
                'level' => 'ERROR',
            ),
            'error.http.4xx' => array(
                'appenders' => array('http_warnings'),
                'level' => 'WARN',
            ),
            'error.external_request' => array(
                'appenders' => array('external_requests'),
                'level' => 'ERROR',
            ),
            'error.client' => array(
                'appenders' => array('client_errors'),
                'level' => 'ERROR',
            ),
            'watch' => array(
                'appenders' => array('watch'),
                'level' => 'TRACE',
            ),
        ),
        'appenders' => array(
            'default' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/default.log',
                    'append' => true
                )
            ),
            'program_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/error.log',
                    'append' => true
                )
            ),
            'common_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/common_exceptions.log',
                    'append' => true
                )
            ),
            'sql_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/sql_db_exceptions.log',
                    'append' => true
                )
            ),
            'http_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/http_exceptions.log',
                    'append' => true
                )
            ),
            'http_warnings' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/http_warnings.log',
                    'append' => true
                )
            ),
            'external_requests' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/external_request_exceptions.log',
                    'append' => true
                )
            ),
            'client_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/client_errors.log',
                    'append' => true
                )
            ),
            'watch' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => $currentSet['data_dir'].'/.logs/watch.log',
                    'append' => true
                )
            ),
        )
    ),
    'action' => array(
        'arena' => array(
            'enemies' => array(
                'Action' => array(
                    'doppelganger_probability' => 0.3,
                ),
            ),
        ),
    ),
    'cli' => array(
        'DailyReset' => array(
            'time_to_late' => 1209600,
        ),
        'SendNotification' => array(
            'border_time' => 1209600,
        ),
    ),
    'krest' => array(
        'socnet' => $currentSet['soc_net'],
        'api' => array(
            'ExceptionHandler' => array(
                'always_code_200' => true,
            ),
        ),
        'Cache' => array(
            'default_provider' => $currentSet['cache_provider'],
            'default_ttl' => 86400,
            'emulate' => false,
        ),
        'Db' => $currentSet['sql_db'],
        'SocNet' => array(
            'token_salt' => 'ded_maksim',
        ),
    ),
    'lib' => array(
        'TkEventBroker' => array(
            'status_namespace' => 'tomato_kombat_events:status',
            'status_timeout' => 120,
            'host' => $currentSet['tk_event_broker']['host'],
            'port' => $currentSet['tk_event_broker']['port'],
            'channels' => array(
                'vk.com' => 'tomato_kombat_events:vk.com',
                'odnoklassniki.ru' => 'tomato_kombat_events:odnoklassniki.ru',
                'my.mail.ru' => 'tomato_kombat_events:my.mail.ru',
            ),
        ),
    ),
    'model' => array(
        'Bank' => array(
            'payments_testing' => $currentSet['payments_testing'],
            'currency_rates' => array(
                SocNet::VK_COM => array(
                    1 => array(
                        'id' => 1,
                        'currency' => 'f',
                        'count' => 3,
                        'price' => 1,
                    ),
                    array(
                        'id' => 2,
                        'currency' => 'f',
                        'count' => 10,
                        'price' => 3,
                    ),
                    array(
                        'id' => 3,
                        'currency' => 'f',
                        'count' => 50,
                        'price' => 14,
                    ),
                    array(
                        'id' => 4,
                        'currency' => 'f',
                        'count' => 100,
                        'price' => 25,
                    ),
                    array(
                        'id' => 5,
                        'currency' => 'f',
                        'count' => 300,
                        'price' => 70,
                    ),
                    array(
                        'id' => 6,
                        'currency' => 'f',
                        'count' => 1000,
                        'price' => 179,
                        'best_price' => 1,
                    ),
                ),
                SocNet::OK_RU => array(
                    1 => array(
                        'id' => 1,
                        'currency' => 'f',
                        'count' => 3,
                        'price' => 8,
                    ),
                    array(
                        'id' => 2,
                        'currency' => 'f',
                        'count' => 10,
                        'price' => 25,
                    ),
                    array(
                        'id' => 3,
                        'currency' => 'f',
                        'count' => 50,
                        'price' => 45,
                    ),
                    array(
                        'id' => 4,
                        'currency' => 'f',
                        'count' => 100,
                        'price' => 70,
                    ),
                    array(
                        'id' => 5,
                        'currency' => 'f',
                        'count' => 300,
                        'price' => 200,
                    ),
                    array(
                        'id' => 6,
                        'currency' => 'f',
                        'count' => 1000,
                        'price' => 500,
                        'best_price' => 1,
                    ),
                ),
            ),
        ),
        'Clans' => array(
            'creation_price' => 40,
            'rename_price' => 10,
            'max_members_count' => 50,
        ),
        'ClansClash' => array(
            'season_duration' => '3D',
            'season_prize' => 800,
            'max_user_prize' => 70,
        ),
        'Gifts' => array(
            'improved_ferros_price' => 10,
            'open_gift_ferros_price' => 2,
        ),
        'Gains' => array(
            'prices' => array(
                'experience' => array(
                    1 => 5,
                    6 => 30,
                    24 => 60,
                    48 => 120,
                ),
                'glory' => array(
                    1 => 5,
                    6 => 30,
                    24 => 60,
                    48 => 120,
                ),
                'energy' => array(
                    1 => 5,
                    6 => 30,
                    24 => 60,
                    48 => 120,
                ),
                'damage' => array(
                    1 => 8,
                    6 => 48,
                    24 => 96,
                    48 => 192,
                ),
                'armor' => array(
                    1 => 8,
                    6 => 48,
                    24 => 96,
                    48 => 192,
                ),
                'fight_tomatos' => array(
                    1 => 8,
                    6 => 48,
                    24 => 96,
                    48 => 192,
                ),
            ),
        ),
        'Rivals' => array(
            'rivals_ferros_price' => 6,
        ),
        'User' => array(
            'friends_news' => false,
            'energy_period' => 60,
            'top_limit' => 51,
            'points_learning_on_level' => 10,
            'energy_regen_on_level' => 30,
            'hp_on_level' => 50,
            'energy_recover_ferros_price' => 5,
            'energy_for_fight' => 10,
            'energy_on_level' => 1,
            'login_price' => 10,
            'reset_skills_price' => 30,
            'ease_enemy_price' => 5,
            'soc_slots_price' => 100,
            'item_slot_price' => 15,
            'ring_slot_price' => 30,
            'level_awards_step' => 15,
            'level_awards_ferros' => 1,
            'vegetable_price' => array(
                't' => 5000,
                'f' => 5,
            ),
            'day_bonuses' => array(
                'day_tomatos' => 10,
                'item_days' => array(
                    3 => array(
                        'variant' => 'drink',
                        'count' => 4,
                    ),
                    7 => array(
                        'variant' => 'grenade',
                        'count' => 6,
                    ),
                ),
            ),
            'start_bonuses' => array(
                array(
                    'type' => 'tomatos',
                    'count' => 30,
                    'item_id' => 0,
                    'name_ru' => 'Томатосы',
                ),
                array(
                    'type' => 'medicine',
                    'count' => 3,
                    'item_id' => 83,
                    'name_ru' => 'Здоровье',
                ),
                array(
                    'type' => 'grenade',
                    'count' => 3,
                    'item_id' => 90,
                    'name_ru' => 'Гранаты',
                ),
                array(
                    'type' => 'energy',
                    'count' => 2,
                    'item_id' => 86,
                    'name_ru' => 'Энергия',
                ),
                array(
                    'type' => 'ferros',
                    'count' => 2,
                    'item_id' => 0,
                    'name_ru' => 'Ферросы',
                ),
            ),
        ),
    ),
);

function getConfigSet()
{
    $configSets = require('config_sets.php');
    if (php_sapi_name() === "cli") {
        $currentSetName = Arr::get($_SERVER, 'CONFIG_SET');
        if ($currentSetName === null) {
            $currentSetName = (isset($_SERVER["DEVELOPMENT_MACHINE"])) ? "local_dev" : "production_server";
        }
    }
    else {
        $currentSetName = Arr::get($_SERVER, 'CONFIG_SET', 'production_server');
    }
    $currentSet = Arr::req($configSets, $currentSetName);

    $extends = Arr::get($currentSet, 'extends');
    if ($extends) {
        $parentSet = Arr::req($configSets, $extends);
        $currentSet = array_merge($parentSet, $currentSet);
    }

    return array($currentSetName, $currentSet);
}
