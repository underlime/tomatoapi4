<?php
namespace cli;

use krest\base\CliCommand;
use krest\Db;

class WeeklyReset extends CliCommand
{
    public function execute()
    {
        $dbIds = Db::getInstIds();
        foreach ($dbIds as $instId) {
            $db = Db::instance($instId);
            $db->exec('LOCK TABLES users_fight_data WRITE');
            $db->exec("UPDATE users_fight_data SET week_glory=0");
            $db->exec('UNLOCK TABLES');
            $this->_print("Weekly reset for '$instId' is completed");
        }
    }
}
