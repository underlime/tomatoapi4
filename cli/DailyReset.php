<?php
namespace cli;

use krest\api\Config;
use krest\Arr;
use krest\base\CliCommand;
use krest\Db;
use lib\RediskaFactory;
use model\ClanRequests;
use model\ClansClash;
use model\Rivals;
use model\UserNews;
use model\UsersJournal;

class DailyReset extends CliCommand
{
    /**
     * @var \krest\Db
     */
    private $_db;

    public function execute()
    {
        $config = Config::instance()->getForClass(__CLASS__);
        $dbIds = Db::getInstIds();
        foreach ($dbIds as $instId) {
            $this->_db = Db::instance($instId);
            $this->_db->exec('LOCK TABLES users_fight_data WRITE, clans WRITE');
            $this->_db->beginTransaction();
            Db::setDefaultName($instId);
            RediskaFactory::setDefaultNamespace($instId);
            try {
                $this->_resetValues();
                $this->_cleanJournal($config);
                $this->_cleanRivals();
                $this->_cleanClanRequests();
                $this->_deleteNews();
                $this->_checkClashSeason($instId);
            }
            catch (\Exception $e) {
                $this->_db->rollBack();
                $this->_db->exec('UNLOCK TABLES');
                throw $e;
            }
            $this->_db->commit();
            $this->_db->exec('UNLOCK TABLES');
        }
    }

    private function _resetValues()
    {
        $this->_db->exec("UPDATE users_fight_data SET day_glory=0");
        $this->_print("Daily reset of values is completed for {$this->_db->instId}");
    }

    private function _cleanJournal($config)
    {
        $usersJournal = new UsersJournal();
        $timeToLate = Arr::req($config, "time_to_late");

        $time = time() - $timeToLate;
        $affectedRows = $usersJournal->deleteMessagesEarlierThanTime($time);

        $this->_print("Journal messages deleted: $affectedRows; for {$this->_db->instId}");
    }

    private function _cleanRivals()
    {
        $rivals = new Rivals();
        $affectedRows = $rivals->cleanOutdated();
        $this->_print("Rivals deleted: $affectedRows; for {$this->_db->instId}");
    }

    private function _cleanClanRequests()
    {
        $clanRequests = new ClanRequests();
        $affectedRows = $clanRequests->clearOutdated();
        $this->_print("Clan requests deleted: $affectedRows; for {$this->_db->instId}");
    }

    private function _deleteNews()
    {
        $news = new UserNews();
        $affectedRows = $news->deleteExpired();
        $this->_print("Expired news deleted: $affectedRows; for {$this->_db->instId}");
    }

    private function _checkClashSeason($instId)
    {
        $clansClash = new ClansClash($instId);
        $seasonInfo = $clansClash->getSeasonInfo();
        $strEndTime = Arr::get($seasonInfo, "season_end");
        if ($strEndTime === null) {
            $this->_print("There is no season started in $instId, start now");
            $clansClash->startSeason();
        }
        else {
            $endTime = \DateTime::createFromFormat("Y-m-d H:i:s", $strEndTime);
            if ($endTime === null) {
                throw new \Exception("Invalid end season format");
            }
            $now = new \DateTime("now", new \DateTimeZone("UTC"));
            $diff = $endTime->diff($now);
            if ($diff->days == 0 && $diff->h < 12 || $diff->invert == 0) {
                $this->_print("It's time for a new season in $instId, start now");
                $clansClash->startSeason();
            }
            else {
                $this->_print("It's not a time for a new season in $instId, don't start");
            }
        }
    }
}
