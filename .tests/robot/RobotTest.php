<?php

use fight\robot\RobotFactory;

require_once __DIR__.'/../TomatoDbTestCase.php';

class RobotTest extends TomatoDbTestCase
{
    /**
     * Returns the test dataset.
     *
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__.'/../data/fixtures/general_database.xml');
    }

    /**
     * @covers \robot\RobotFactory::getRobot
     * @covers \robot\Doppelganger::_makeRobotInfo
     */
    public function test_doppelganger()
    {
        $userId = 1;
        $userInfo = new \fight\robot\UserInfo();

        $user = new \model\User();
        $items = new \model\Items();
        $specials = new \model\Specials();

        $userInfo->userInfo = $user->getUserInfoByUserId($userId);
        $userInfo->userFightData = $user->getUserFightData($userId);
        $userInfo->userLevelParams = $user->getUserLevelParams($userId);
        $userInfo->userInventory = $items->getUsersInventory($userId);
        $userInfo->userSpecials = $specials->getUsersSpecials($userId);

        $doppelganger = RobotFactory::getRobot(RobotFactory::DOPPELGANGER, $userInfo);

        $robotInfo = $doppelganger->getRobotInfo();

        $this->assertEquals('Doppelganger', $robotInfo->userInfo['login']);
        $this->assertEquals(-1, $robotInfo->userInfo['user_id']);

        $this->assertEquals($robotInfo->userLevelParams, $userInfo->userLevelParams);
        $this->assertEquals($robotInfo->userFightData, $userInfo->userFightData);
        $this->assertEquals($robotInfo->userInventory, $userInfo->userInventory);
        $this->assertEquals($robotInfo->userSpecials, $userInfo->userSpecials);
    }

    /**
     * @covers \robot\RobotFactory::getRobot
     * @covers \robot\Soup_NightBot::_makeRobotInfo
     */
    public function test_soup_nightbot()
    {
        $userId = 1;
        $userInfo = new \fight\robot\UserInfo();

        $user = new \model\User();
        $items = new \model\Items();
        $specials = new \model\Specials();

        $userInfo->userInfo = $user->getUserInfoByUserId($userId);
        $userInfo->userFightData = $user->getUserFightData($userId);
        $userInfo->userLevelParams = $user->getUserLevelParams($userId);
        $userInfo->userInventory = $items->getUsersInventory($userId);
        $userInfo->userSpecials = $specials->getUsersSpecials($userId);

        $doppelganger = RobotFactory::getRobot(RobotFactory::SOUP_NIGHTBOT, $userInfo);

        $robotInfo = $doppelganger->getRobotInfo();

        $this->assertEquals('Soup_NightBot', $robotInfo->userInfo['login']);
        $this->assertEquals(-2, $robotInfo->userInfo['user_id']);
    }
}
