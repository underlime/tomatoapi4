<?php
require_once __DIR__.'/../ActionTestCase.php';

class SpecialsTest extends ActionTestCase
{
    public function testSpecials()
    {
        $this->_pdo->exec('UPDATE users_level_params SET points_learning=5000');
        $data = $this->_getActionResult('academy/train.skills', array(
                                                                     'intellect' => '5000',
                                                                ));

        $expectedStructure = ['user_level_params', 'user_fight_data', 'user_specials', 'events'];
        foreach ($expectedStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }

        $specials = [];
        foreach ($data->events as $event) {
            $this->assertObjectHasAttribute('type', $event);
            if ($event->type == 'new_special') {
                $specials[] = $event->code;
            }
        }

        $expectedCodes = ['double_kick', 'xray', 'rough_skin', 'RAGE_STRIKES', 'DefenceNovice', 'valera', 'stone_skin',
                          'AnatomyExpert', 'stalagmit', 'AccurateCalculation', 'adamant_skin', 'force_blow', 'Wall',
                          'Assasin', 'Telekinez', 'Defensive', 'MonoliteSkin', 'DemonSkin',];

        foreach ($expectedCodes as $code) {
            $this->assertTrue(in_array($code, $specials), "No special code {$code}");
        }
    }
}
