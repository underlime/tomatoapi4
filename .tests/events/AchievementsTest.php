<?php
require_once __DIR__.'/../ActionTestCase.php';

class AchievementsTest extends ActionTestCase
{
    public function testMoney()
    {
        $data = $this->_getActionResult('items/buy.item', array(
                                                               'item_id' => '1',
                                                               'count' => '1',
                                                               'currency' => 'f',
                                                          ));

        $expectedStructure = ['user_fight_data', 'user_inventory', 'events', 'user_achievements'];
        foreach ($expectedStructure as $attr) {
            $this->assertObjectHasAttribute($attr, $data);
        }


        $achievements = [];
        foreach ($data->events as $eventData) {
            $this->assertObjectHasAttribute('achievement_code', $eventData);
            $achievements[] = $eventData->achievement_code;
        }

        $expectedCodes = ['3', '4'];
        foreach ($expectedCodes as $code) {
            $this->assertTrue(in_array($code, $achievements), "No achievement code {$code}");
        }
    }

    public function testVisits()
    {
        $data = $this->_getActionResult('user/get.info');

        $resultStructure = [
            'user_info',
            'user_level_params',
            'user_fight_data',
            'user_stat',
            'user_bonuses',
            'user_opt_info',
            'user_inventory',
            'config_prices',
            'gains',
            'clan_info',
            'events'
        ];
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }

        $achievements = [];
        $everydayBonus = false;
        foreach ($data->events as $eventData) {
            $this->assertObjectHasAttribute('type', $eventData);
            /** @noinspection SpellCheckingInspection */
            if ($eventData->type == 'achievment') {
                $achievements[] = $eventData->achievement_code;
            }
            elseif ($eventData->type == 'everyday_bonus') {
                $everydayBonus = true;
            }
        }

        $this->assertTrue($everydayBonus, 'No everyday bonus');
        $this->assertTrue(in_array('5', $achievements), 'No visits achievement');
    }

    public function testLevelParams()
    {
        $this->_pdo->exec('UPDATE users_level_params SET points_learning=5000');
        $data = $this->_getActionResult('academy/train.skills', array(
                                                                     'strength' => '201',
                                                                     'agility' => '201',
                                                                     'intellect' => '201',
                                                                ));

        $expectedStructure = ['user_level_params', 'user_fight_data', 'user_specials', 'events'];
        foreach ($expectedStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }

        $achievements = [];
        foreach ($data->events as $event) {
            $this->assertObjectHasAttribute('type', $event);
            /** @noinspection SpellCheckingInspection */
            if ($event->type == 'achievment') {
                $achievements[] = $event->achievement_code;
            }
        }

        $expectedCodes = ['level_5', 'level_15', 'level_30', 'SwordShieldUnlock', 'PikeUnlock', 'strength_200',
                          'HeavyUnlock', 'agility_200', 'intellect_200'];
        foreach ($expectedCodes as $code) {
            $this->assertTrue(in_array($code, $achievements), "No achievement code {$code}");
        }
    }
}
