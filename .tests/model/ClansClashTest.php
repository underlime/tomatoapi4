<?php
use model\Clans;
use model\ClansClash;

require_once __DIR__.'/../ModelTestCase.php';

class ClansClashTest extends ModelTestCase
{
    /**
     * @var \model\ClansClash
     */
    private $_object;

    protected function setUp()
    {
        parent::setUp();
        $this->_object = new ClansClash();
    }

    /**
     * @covers \model\ClansClash::startSeason
     * @covers \model\ClansClash::getSeasonInfo
     */
    public function testStartSeason()
    {
        $this->_object->startSeason();
        $info = $this->_object->getSeasonInfo();

        $this->assertTrue(isset($info['id']) && is_int($info['id']));
        $this->assertTrue(isset($info['prize']) && is_int($info['prize']));
        $this->assertTrue(isset($info['season_end']) && is_string($info['season_end']));
        $this->assertTrue(isset($info['season_end_timestamp']) && is_int($info['season_end_timestamp']));
    }

    /**
     * @covers \model\ClansClash::startSeason
     * @covers \model\Clans::resetGlory
     */
    public function testResetGlory()
    {
        $this->_pdo->exec('UPDATE clans SET glory=500');

        $this->_object->startSeason();

        $connection = $this->getConnection();
        $this->assertEquals($connection->getRowCount('clans'), $connection->getRowCount('clans', 'glory=0'));
    }

    /**
     * @covers \model\ClansClash::startSeason
     * @covers \model\Clans::writePreviousPlaces
     */
    public function testWritePreviousPlaces()
    {
        $this->_pdo->exec('UPDATE clans SET glory=100 WHERE id=1');

        $this->_object->startSeason();

        $clans = new Clans();
        $info = $clans->getInfo(1);
        $this->assertEquals(1, $info['previous_place_in_top']);
    }
}
