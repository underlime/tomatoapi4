<?php
use model\Clans;
use model\User;

require_once __DIR__.'/../ModelTestCase.php';

class UserFightDataTest extends ModelTestCase
{
    /**
     * @covers \model\User::updateGlory
     * @covers \model\Clans::addGlory
     */
    public function testUpdateGlory()
    {
        $user = new User();
        $clans = new Clans();
        $user->updateGlory(1, 100);

        $fightData = $user->getUserFightData(1);
        $clanInfo = $clans->getInfo(1);

        $this->assertEquals(100, $fightData['glory']);
        $this->assertEquals(50, $fightData['season_glory']);
        $this->assertEquals(50, $clanInfo['glory']);
    }
}
