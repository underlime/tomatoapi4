<?php

use krest\exceptions\InvalidDataException;
use model\Clans;

require_once __DIR__.'/../ModelTestCase.php';

class ClansTest extends ModelTestCase
{
    /**
     * @var \model\Clans
     */
    private $_object;

    protected function setUp()
    {
        parent::setUp();
        $this->_object = new Clans();
    }

    /**
     * @covers \model\Clans::cleanName
     */
    public function testCleanName()
    {
        $rawName = '    ----тестовое        ---- name____    ';
        $cleanName = $this->_object->cleanName($rawName);
        $this->assertEquals('тестовое - name', $cleanName);
    }

    /**
     * @covers \model\Clans::validateName
     */
    public function testValidateName()
    {
        $correctFreeName = 'Тестеры';
        $correctExistingName = 'Тестовый клан';
        $incorrectName = "'drop database";

        $correctFreeResult = $this->_object->validateName($correctFreeName);
        $correctExistingResult = $this->_object->validateName($correctExistingName);
        $incorrectResult = $this->_object->validateName($incorrectName);

        $this->assertEquals(array('correct' => true, 'exists' => false), $correctFreeResult);
        $this->assertEquals(array('correct' => true, 'exists' => true), $correctExistingResult);
        $this->assertEquals(array('correct' => false, 'exists' => false), $incorrectResult);
    }

    /**
     * @covers \model\Clans::create
     */
    public function testSuccessfulCreate()
    {
        $clanId = $this->_object->create(2, 'Тестовый 2');

        $connection = $this->getConnection();
        $expectedDb = $this->createXMLDataSet(__DIR__.'/../data/fixtures/clans_test/test_successful_create.xml');
        $clansTable = $connection->createQueryTable('clans', 'SELECT `name`, leader_id FROM clans');
        $clanMembersTable = $connection->createQueryTable('clan_members',
                                                          'SELECT user_id FROM clan_members WHERE user_id=2');
        $fightDataTable = $connection->createQueryTable('users_fight_data',
                                                        'SELECT user_id, ferros FROM users_fight_data WHERE user_id=2');

        $this->assertTrue(is_int($clanId));
        $this->assertTablesEqual($expectedDb->getTable('clans'), $clansTable);
        $this->assertTablesEqual($expectedDb->getTable('clan_members'), $clanMembersTable);
        $this->assertTablesEqual($expectedDb->getTable('users_fight_data'), $fightDataTable);
    }

    /**
     * @covers \model\Clans::create
     */
    public function testCreateLeaderError()
    {
        try {
            $this->_object->create(1, 'Test clan');
            $this->fail('Leader duplication');
        }
        catch (\krest\exceptions\AlreadyExistsException $e) {
            $this->assertEquals('User 1 has already created a clan', $e->getMessage());
        }
    }

    /**
     * @covers \model\Clans::create
     * @covers \model\Clans::addClanMember
     */
    public function testCreateNameError()
    {
        $clanName = '  ---тестовый    клан--___';
        try {
            $this->_object->create(2, $clanName);
            $this->fail('Name duplication');
        }
        catch (\krest\exceptions\AlreadyExistsException $e) {
            $this->assertEquals("Clan with the name '$clanName' is already exists", $e->getMessage());
        }
    }

    /**
     * @covers \model\Clans::rename
     */
    public function testSuccessfulRename()
    {
        $userId = 1;
        $query = $this->_pdo->prepare('SELECT id FROM clans WHERE leader_id=:userId');
        $query->execute(array(':userId' => $userId));
        $clanId = $query->fetchColumn(0);
        $this->_object->rename($userId, $clanId, 'Новое имя тестового клана');

        $connection = $this->getConnection();
        $expectedDb = $this->createXMLDataSet(__DIR__.'/../data/fixtures/clans_test/test_successful_rename.xml');
        $clansTable = $connection->createQueryTable('clans', 'SELECT `name`, leader_id FROM clans');
        $fightDataTable = $connection->createQueryTable('users_fight_data',
                                                        'SELECT user_id, ferros FROM users_fight_data WHERE user_id=1');

        $this->assertTablesEqual($expectedDb->getTable('clans'), $clansTable);
        $this->assertTablesEqual($expectedDb->getTable('users_fight_data'), $fightDataTable);
    }

    /**
     * @covers \model\Clans::rename
     */
    public function testRenameLeaderError()
    {
        $query = $this->_pdo->prepare('SELECT id FROM clans WHERE leader_id=:userId');
        $query->execute(array(':userId' => 1));
        $clanId = $query->fetchColumn(0);

        try {
            $this->_object->rename(2, $clanId, 'Новое имя тестового клана');
            $this->fail('Access to clan fail');
        }
        catch (\krest\exceptions\InvalidDataException $e) {
            $this->assertEquals('User 2 is not a clan leader', $e->getMessage());
        }
    }

    /**
     * @covers \model\Clans::delete
     */
    public function testDelete()
    {
        $connection = $this->getConnection();

        $beginCount = $connection->getRowCount('clans', 'id=1');
        $this->_object->delete(1, 1);
        $endCount = $connection->getRowCount('clans', 'id=1');

        $this->assertEquals(1, $beginCount);
        $this->assertEquals(0, $endCount);
    }

    /**
     * @covers \model\Clans::getInfo
     */
    public function testGetInfo()
    {
        $info = $this->_object->getInfo(1);

        $expectedInfo = array(
            'id' => '1',
            'name' => 'Тестовый клан',
            'clan_name' => 'Тестовый клан',
            'leader_id' => '1',
            'glory' => '0',
            'previous_place_in_top' => '0',
            'victories' => '0',
            'picture' => '',
        );
        $this->assertEquals($expectedInfo, $info);
    }

    /**
     * @covers \model\Clans::checkClanExists
     */
    public function testCheckClanExistsRight()
    {
        $this->_object->checkClanExists(1);
    }

    /**
     * @covers \model\Clans::checkClanExists
     */
    public function testCheckClanExistsWrong()
    {
        try {
            $this->_object->checkClanExists(1000);
            $this->fail('Clan is exists');
        }
        catch (\krest\exceptions\NotFoundException $e) {
            $this->assertEquals("Clan 1000 is not found", $e->getMessage());
        }
    }

    /**
     * @covers \model\Clans::getClanMembers
     */
    public function testGetClanMembers()
    {
        $data = $this->_object->getClanMembers(1);

        $this->assertEquals(array(1), $data);
    }

    /**
     * @covers \model\Clans::isClanMember
     */
    public function testIsClanMember()
    {
        $this->assertTrue($this->_object->isClanMember(1, 1));
    }

    /**
     * @covers \model\Clans::isSomeClanMember
     */
    public function testIsSomeClanMember()
    {
        $this->assertTrue($this->_object->isSomeClanMember(1));
    }

    /**
     * @covers \model\Clans::getClanMembersCount
     */
    public function testGetClanMembersCount()
    {
        $count = $this->_object->getClanMembersCount(1);
        $this->assertEquals(1, $count);
    }

    /**
     * @covers \model\Clans::delClanMember
     */
    public function testDelClanMember()
    {
        $this->_object->addClanMember(1, 2);
        $clanMembers0 = $this->_object->getClanMembers(1);

        $this->_object->delClanMember(1, 2);
        $clanMembers1 = $this->_object->getClanMembers(1);

        sort($clanMembers0);
        $this->assertEquals(array(1, 2), $clanMembers0);
        $this->assertEquals(array(1), $clanMembers1);
    }

    /**
     * @covers \model\Clans::delClanMember
     */
    public function testDelClanMemberLeaderError()
    {
        try {
            $this->_object->delClanMember(1, 1);
            $this->fail();
        }
        catch (InvalidDataException $e) {
            $this->assertEquals('User is the clan leader', $e->getMessage());
        }
    }

    /**
     * @covers \model\Clans::addGlory
     */
    public function testAddGlory()
    {
        $newGlory = $this->_object->addGlory(1, 100);
        $info = $this->_object->getInfo(1);

        $this->assertEquals(100, $newGlory);
        $this->assertEquals($newGlory, $info['glory']);
    }

    /**
     * @covers \model\Clans::incVictories
     */
    public function testIncVictories()
    {
        $newVictories = $this->_object->incVictories(1);
        $info = $this->_object->getInfo(1);

        $this->assertEquals(1, $newVictories);
        $this->assertEquals($newVictories, $info['victories']);
    }
}
