<?php

use model\ClanRequests;
use model\Clans;

require_once __DIR__.'/../ModelTestCase.php';

class ClanRequestsTest extends ModelTestCase
{
    /**
     * @var \model\ClanRequests
     */
    private $_object;

    protected function setUp()
    {
        parent::setUp();
        $this->_object = new ClanRequests();
    }

    /**
     * @covers \model\ClanRequests::create
     * @covers \model\ClanRequests::get
     */
    public function testAddGetRequests()
    {
        $this->_object->create(1, 2);
        $this->_object->create(1, 3);

        $requests = $this->_object->get(1);

        $requestIds = array_map(function ($rec) {
            return $rec["user_id"];
        }, $requests);
        sort($requestIds);
        $this->assertEquals(array(2, 3), $requestIds);
    }

    /**
     * @covers \model\ClanRequests::create
     */
    public function testAddUserAlreadyInClan()
    {
        try {
            $this->_object->create(1, 1);
            $this->fail();
        }
        catch (\krest\exceptions\InvalidDataException $e) {
            $this->assertEquals('User is already clan member', $e->getMessage());
        }
    }

    /**
     * @covers \model\ClanRequests::create
     */
    public function testAddUserAlreadyAddedRequest()
    {
        $this->_object->create(1, 2);
        try {
            $this->_object->create(1, 2);
            $this->fail();
        }
        catch (\krest\exceptions\InvalidDataException $e) {
            $this->assertEquals('User 2 has already added request', $e->getMessage());
        }
    }

    /**
     * @covers \model\ClanRequests::accept
     */
    public function testAcceptSuccess()
    {
        $clans = new Clans();
        $this->_object->create(1, 2);
        $this->_object->create(1, 3);

        $this->_object->accept(1, 2);
        $requests = $this->_object->get(1);
        $members = $clans->getClanMembers(1);

        $requestIds = array_map(function ($rec) {
            return $rec["user_id"];
        }, $requests);

        $this->assertEquals(array(3), $requestIds);
        $this->assertEquals(array(1, 2), $members);
    }

    /**
     * @covers \model\ClanRequests::reject
     */
    public function testReject()
    {
        $this->_object->create(1, 2);
        $this->_object->reject(1, 2);
        $requests = $this->_object->get(1);

        $this->assertEmpty($requests);
    }
}
