<?php

use krest\mock\ClientRequest;
use krest\Request;
use krest\SocNet;
use krest\api\Application;
use krest\View;

require_once dirname(__FILE__).'/TomatoDbTestCase.php';

abstract class ActionTestCase extends TomatoDbTestCase
{
    /**
     * @var \krest\mock\ClientRequest
     */
    protected $clientRequest;

    /**
     * Returns the test dataset.
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__.'/data/fixtures/general_database.xml');
    }

    protected function setUp()
    {
        parent::setUp();
        SocNet::clear();
        SocNet::setSocNetName(SocNet::VK_COM);
        $this->clientRequest = new ClientRequest();
        $this->clientRequest->setMethod(Request::POST);
    }

    protected function tearDown()
    {
        $this->clientRequest = null;
        parent::tearDown();
    }

    protected function _getActionResult($action, array $params = array())
    {
        $actionUrl = "/vk.com/$action";
        $this->clientRequest->setUri($actionUrl);

        $post = array_merge(array('sid' => '2', 'viewer_id' => '3081515'), $params);
        $apiId = '2893571';
        $apiSecret = 'TmmJet3LZ0WnNItJpWic';
        $post['auth_key'] = md5($baseString = "{$apiId}_{$post['viewer_id']}_{$apiSecret}");

        $this->clientRequest->setPostArray($post);
        $post['token'] = SocNet::instance()->makeToken();
        $this->clientRequest->setPostArray($post);

        $answerBody = $this->_execute();
        $data = json_decode($answerBody);
        return $data;
    }

    protected function _execute()
    {
        Request::clear();
        $action = Application::factory('')
                  ->execute();
        return View::factory($action->getAnswer())
               ->render();
    }
}
