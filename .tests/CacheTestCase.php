<?php

use base\Model;
use krest\Cache;
use krest\EventDispatcher;

require_once __DIR__."/TomatoDbTestCase.php";

class CacheTestCase extends TomatoDbTestCase
{
    /**
     * @var \krest\Cache
     */
    protected $_cache;
    protected $_krestDisableCache = false;
    protected $_cacheActions = [];

    /**
     * Returns the test dataset.
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__."/data/fixtures/general_database.xml");
    }

    protected function setUp()
    {
        parent::setUp();
        Cache::setPrefix('tomato_api4_');
        $this->_cache = Cache::instance();
        $this->_cache->flushAll();
        $this->_cacheActions = [];

        $eventDispatcher = EventDispatcher::instance();
        $eventDispatcher->addEventListener(Model::SET_TO_CACHE, [$this, "pushSet"]);
        $eventDispatcher->addEventListener(Model::GET_FROM_CACHE, [$this, "pushGet"]);
        $eventDispatcher->addEventListener(Model::UPDATE_IN_CACHE, [$this, "pushUpdate"]);
        $eventDispatcher->addEventListener(Model::DELETE_FROM_CACHE, [$this, "pushDelete"]);
    }

    public function pushSet($data)
    {
        $this->_cacheActions[] = array_merge(["set"], $data);
    }

    public function pushGet($data)
    {
        $this->_cacheActions[] = array_merge(["get"], $data);
    }

    public function pushUpdate($data)
    {
        $this->_cacheActions[] = array_merge(["update"], $data);
    }

    public function pushDelete($data)
    {
        $this->_cacheActions[] = array_merge(["delete"], $data);
    }
}
