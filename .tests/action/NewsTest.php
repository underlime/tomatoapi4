<?php
require_once __DIR__.'/../ActionTestCase.php';

class NewsTest extends ActionTestCase
{
    /**
     * @covers \action\news\get_news::post
     */
    public function testGetNews()
    {
        $data = $this->_getActionResult('news/get.news');
        $resultStructure = array('user_news');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\news\set_news_read::post
     */
    public function testSetNewsRead()
    {
        $data = $this->_getActionResult('news/set.news.read', array(
                                                                   'news_id' => 1,
                                                                   'is_shared' => 1
                                                              ));
        $resultStructure = array('user_news');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
