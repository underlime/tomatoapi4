<?php
require_once __DIR__.'/../ActionTestCase.php';

class UserGetInfoTest extends ActionTestCase
{
    /**
     * @covers \action\user\get_info::post
     */
    public function testGetBasicInfo()
    {
        $data = $this->_getActionResult('user/get.info');

        $resultStructure = array(
            'user_info',
            'user_level_params',
            'user_fight_data',
            'user_stat',
            'user_bonuses',
            'user_opt_info',
            'user_inventory',
            'config_prices',
            'game_constants',
            'gains',
            'clan_info',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\get_info::post
     */
    public function testGetAnotherUserInfo()
    {
        $data = $this->_getActionResult('user/get.info', array('soc_net_id' => '1'));

        $resultStructure = array(
            'user_info',
            'user_level_params',
            'user_fight_data',
            'user_stat',
            'user_bonuses',
            'user_inventory',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }

        $wrongFields = array(
            'user_opt_info',
            'config_prices',
            'gains',
        );
        foreach ($wrongFields as $attribute) {
            $this->assertObjectNotHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\get_list_info::post
     */
    public function testGetListInfo()
    {
        $data = $this->_getActionResult('user/get.list.info', array('soc_net_ids' => array(1, 3081515)));
        $resultStructure = array('users_list_info');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\get_money::post
     */
    public function testGetMoney()
    {
        $data = $this->_getActionResult('user/get.money');
        $resultStructure = array('user_fight_data');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\renew_energy::post
     */
    public function testRenewEnergy()
    {
        $data = $this->_getActionResult('user/renew.energy');
        $resultStructure = array('user_fight_data');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\check_login::post
     */
    public function testCheckLogin()
    {
        $data = $this->_getActionResult('user/check.login', array('login' => 'test'));
        $resultStructure = array('login_allowable');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\get_achievements::post
     */
    public function testGetAchievements()
    {
        $data = $this->_getActionResult('user/get.achievements', array('user_id' => '1'));
        $resultStructure = array(
            'user_achievements_info',
            'user_achievements',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\get_specials::post
     */
    public function testGetSpecials()
    {
        $data = $this->_getActionResult('user/get.specials');
        $resultStructure = array(
            'user_specials_info',
            'user_specials',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\get_journal::post
     */
    public function testGetJournal()
    {
        $data = $this->_getActionResult('user/get.journal');
        $resultStructure = array('user_journal');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
