<?php
require_once __DIR__.'/../ActionTestCase.php';

class CitadelTest extends ActionTestCase
{
    public function testGetPrices()
    {
        $data = $this->_getActionResult('citadel/get.prices');
        $resultStructure = array('citadel_prices');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    public function testSetGain()
    {
        $data = $this->_getActionResult('citadel/set.gain', array(
                                                                 'gain_object' => 'glory',
                                                                 'hours' => 6,
                                                            ));
        $resultStructure = array('gains', 'user_fight_data');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    public function testGetGains()
    {
        $data = $this->_getActionResult('citadel/get.gains');
        $resultStructure = array('gains');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
