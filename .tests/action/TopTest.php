<?php
require_once __DIR__.'/../ActionTestCase.php';

class TopTest extends ActionTestCase
{
    /**
     * @covers \action\top\fight_data\get_experience_top::post
     */
    public function testGetExperienceTop()
    {
        $data = $this->_getActionResult('top/fight.data/get.experience.top');
        $resultStructure = array('top_experience_common_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_by_city::post
     */
    public function testGetByCityGlobal()
    {
        $data = $this->_getActionResult('top/glory/get.by.city', array(
                                                                      'city_code' => '722',
                                                                      'interval' => 'all',
                                                                 ));
        $resultStructure = array('top_glory_by_city_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_by_city::post
     */
    public function testGetByCityWeek()
    {
        $data = $this->_getActionResult('top/glory/get.by.city', array(
                                                                      'city_code' => '722',
                                                                      'interval' => 'week',
                                                                 ));
        $resultStructure = array('top_glory_by_city_week');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_by_city::post
     */
    public function testGetByCityDay()
    {
        $data = $this->_getActionResult('top/glory/get.by.city', array(
                                                                      'city_code' => '722',
                                                                      'interval' => 'day',
                                                                 ));
        $resultStructure = array('top_glory_by_city_day');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_friends_top::post
     */
    public function testGetFriendsGlobal()
    {
        $data = $this->_getActionResult('top/glory/get.friends.top', array('interval' => 'all'));
        $resultStructure = array('top_glory_friends_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_friends_top::post
     */
    public function testGetFriendsWeek()
    {
        $data = $this->_getActionResult('top/glory/get.friends.top', array('interval' => 'week'));
        $resultStructure = array('top_glory_friends_week');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_friends_top::post
     */
    public function testGetFriendsDay()
    {
        $data = $this->_getActionResult('top/glory/get.friends.top', array('interval' => 'day'));
        $resultStructure = array('top_glory_friends_day');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_global::post
     */
    public function testGetCommonGlobal()
    {
        $data = $this->_getActionResult('top/glory/get.global', array('interval' => 'all'));
        $resultStructure = array('top_glory_common_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_global::post
     */
    public function testGetCommonWeek()
    {
        $data = $this->_getActionResult('top/glory/get.global', array('interval' => 'week'));
        $resultStructure = array('top_glory_common_week');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\glory\get_global::post
     */
    public function testGetCommonDay()
    {
        $data = $this->_getActionResult('top/glory/get.global', array('interval' => 'day'));
        $resultStructure = array('top_glory_common_day');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\level_params\get_agility_top::post
     */
    public function testGetAgilityTop()
    {
        $data = $this->_getActionResult('top/level.params/get.agility.top');
        $resultStructure = array('top_agility_common_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\level_params\get_intellect_top::post
     */
    public function testIntellectTop()
    {
        $data = $this->_getActionResult('top/level.params/get.intellect.top');
        $resultStructure = array('top_intellect_common_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\top\level_params\get_strength_top::post
     */
    public function testStrengthTop()
    {
        $data = $this->_getActionResult('top/level.params/get.strength.top');
        $resultStructure = array('top_strength_common_global');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
