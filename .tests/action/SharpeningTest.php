<?php
require_once __DIR__.'/../ActionTestCase.php';

class SharpeningTest extends ActionTestCase
{
    /**
     * @covers \action\sharpening\get_sharpening_params::post
     */
    public function testGetSharpeningParams()
    {
        $data = $this->_getActionResult('sharpening/get.sharpening.params', array(
                                                                                 'item_id' => '19',
                                                                                 'level' => '1',
                                                                            ));
        $resultStructure = array(
            'sharpening_params',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\sharpening\sharpen_weapon::post
     */
    public function testSharpenWeapon()
    {
        $data = $this->_getActionResult('sharpening/sharpen_weapon', array(
                                                                          'item_id' => '19',
                                                                          'currency' => 'f',
                                                                     ));
        $resultStructure = array(
            'sharpen_weapon_result',
            'user_fight_data',
            'user_bonuses',
            'user_inventory',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}