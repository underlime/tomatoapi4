<?php
require_once __DIR__.'/../ActionTestCase.php';

class BankTest extends ActionTestCase
{
    /**
     * @covers \action\bank\get_currency_rates::post
     */
    public function testGetCurrencyRates()
    {
        $data = $this->_getActionResult('bank/get.currency.rates', array('identity' => '1'));
        $resultStructure = array('currency_rates');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    //TODO: реализовать тест колбэка
}
