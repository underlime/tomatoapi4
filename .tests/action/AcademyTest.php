<?php
require_once __DIR__.'/../ActionTestCase.php';

class AcademyTest extends ActionTestCase
{
    /**
     * @covers \action\academy\change_details::post
     */
    public function testChangeDetails()
    {
        $data = $this->_getActionResult('academy/change.details', array(
                                                                       'body' => 'tBody2',
                                                                       'eyes' => 'eyes1',
                                                                       'mouth' => 'mouth1',
                                                                       'hair' => 'tHair1',
                                                                       'currency' => 't',
                                                                  ));
        $resultStructure = array(
            'user_info',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\academy\change_login::post
     */
    public function testChangeLogin()
    {
        $data = $this->_getActionResult('academy/change.login', array('login' => 'tBody2'));

        $resultStructure = array(
            'user_info',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\academy\change_vegetable::post
     */
    public function testChangeVegetable()
    {
        $data = $this->_getActionResult('academy/change.vegetable', array('vegetable' => 'beta'));

        $resultStructure = array(
            'user_info',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\academy\get_char_details::post
     */
    public function testGetCharDetails()
    {
        $data = $this->_getActionResult('academy/get.char.details', array('vegetable' => 'all'));

        $resultStructure = array('char_details');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\academy\reset_skills::post
     */
    public function testResetSkills()
    {
        $data = $this->_getActionResult('academy/reset.skills');

        $resultStructure = array(
            'user_level_params',
            'user_fight_data',
            'user_specials',
            'user_inventory',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\academy\train_skills::post
     */
    public function testTrainSkills()
    {
        $data = $this->_getActionResult('academy/train.skills', array(
                                                                     'strength' => '10',
                                                                     'agility' => '10',
                                                                     'intellect' => '10',
                                                                ));

        $resultStructure = array(
            'user_level_params',
            'user_fight_data',
            'user_specials',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
