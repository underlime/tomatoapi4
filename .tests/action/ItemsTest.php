<?php
require_once __DIR__.'/../ActionTestCase.php';

class ItemsTest extends ActionTestCase
{
    /**
     * @covers \action\items\apply_item::post
     */
    public function testApplyItem()
    {
        $data = $this->_getActionResult('items/apply.item', array('item_id' => '86'));
        $resultStructure = array(
            'user_fight_data',
            'user_inventory',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\items\get_market_list::post
     */
    public function testGetMarketList()
    {
        $data = $this->_getActionResult('items/get.market.list');
        $resultStructure = array('market_items_list');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\items\buy_item::post
     */
    public function testBuyItem()
    {
        $data = $this->_getActionResult('items/buy.item', array(
                                                                'item_id' => '1',
                                                                'count' => '1',
                                                                'currency' => 'f',
                                                           ));
        $resultStructure = array(
            'user_inventory',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\items\sell_item::post
     */
    public function testSellItem()
    {
        $data = $this->_getActionResult('items/sell.item', array(
                                                                'item_id' => '86',
                                                                'count' => '2',
                                                           ));
        $resultStructure = array(
            'user_inventory',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
