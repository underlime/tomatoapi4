<?php
require_once __DIR__.'/../ActionTestCase.php';

class ArenaTest extends ActionTestCase
{
    /**
     * @covers \action\arena\enemies\get_by_identity::post
     */
    public function testGetEnemiesByIdentity()
    {
        $data = $this->_getActionResult('arena/enemies/get.by.identity', array('identity' => '1'));
        $resultStructure = array('enemies_list');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\arena\enemies\get_by_level::post
     */
    public function testGetEnemiesByLevel()
    {
        $data = $this->_getActionResult('arena/enemies/get.by.level', array('level' => '1'));
        $resultStructure = array('enemies_list');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\arena\enemies\fight::post
     */
    public function testFight()
    {
        $data = $this->_getActionResult('arena/fight', array(
                                                            'enemy_id' => '1',
                                                            'ease_enemy' => '1',
                                                       ));
        $resultStructure = array(
            'enemy_eased',
            'user_level_params',
            'user_fight_data',
            'user_inventory',
            'battle_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
