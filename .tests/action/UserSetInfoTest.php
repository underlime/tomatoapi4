<?php
require_once __DIR__.'/../ActionTestCase.php';

class UserSetInfoTest extends ActionTestCase
{
    /**
     * @covers \action\user\register::post
     */
    public function testRegistration()
    {
        $data = $this->_getActionResult('user/register', array(
                                                              'viewer_id' => '487',
                                                              'login' => 'test_487',
                                                              'vegetable' => 'tomato',
                                                              'body' => 'tBody1',
                                                              'hands' => 'tArm1',
                                                              'eyes' => 'eyes1',
                                                              'mouth' => 'mouth1',
                                                              'hair' => 'tHair1',
                                                              'referrer_type' => 'test',
                                                              'referrer' => '3081515',
                                                              'city' => '722',
                                                         ));
        $resultStructure = array(
            'user_info',
            'user_level_params',
            'user_fight_data',
            'user_stat',
            'user_bonuses',
            'user_opt_info',
            'user_inventory',
            'config_prices',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\buy_item_slot::post
     */
    public function testBuyItemSlot()
    {
        $data = $this->_getActionResult('user/buy.item.slot');

        $resultStructure = array(
            'user_level_params',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\buy_ring_slot::post
     */
    public function testBuyRingSlot()
    {
        $data = $this->_getActionResult('user/buy.ring.slot');

        $resultStructure = array(
            'user_level_params',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\give_friends_prizes::post
     */
    public function testGiveFriendsPrizes()
    {
        $data = $this->_getActionResult('user/give.friends.prizes', array('bonus_type' => 'tomatos'));

        $resultStructure = array(
            'user_info',
            'friends_ids_list',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\user\set_album_id::post
     */
    public function testSetAlbumId()
    {
        $data = $this->_getActionResult('user/set.album.id', array('album_id' => '1'));

        $resultStructure = array(
            'user_info'
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
