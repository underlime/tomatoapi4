<?php
require_once __DIR__.'/../ActionTestCase.php';

class RivalsTest extends ActionTestCase
{
    /**
     * @covers \action\rivals\add_message::post
     */
    public function testAddMessage()
    {
        $data = $this->_getActionResult('rivals/add.message', array('message' => 'Нападайте!'));
        $resultStructure = array(
            'rivals_list',
            'user_fight_data',
        );
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\rivals\add_message::post
     */
    public function testGetList()
    {
        $data = $this->_getActionResult('rivals/get.list');
        $resultStructure = array('rivals_list');
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
