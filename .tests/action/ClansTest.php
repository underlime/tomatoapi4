<?php

use krest\mock\ClientRequest;
use krest\Request;
use krest\SocNet;
use model\ClanRequests;
use model\Clans;

require_once __DIR__.'/../ActionTestCase.php';

class ClansTest extends ActionTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->_flushRedis();
    }

    /**
     * @covers \action\clans\validate_name::post
     */
    public function testValidateName()
    {
        $data = $this->_getActionResult('clans/validate.name', ['name' => 'Тестовое имя']);

        $resultStructure = ['clan_name_validation_results'];
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\create::post
     */
    public function testCreate()
    {
        $this->_pdo->exec('DELETE FROM clans');

        $data = $this->_getActionResult('clans/create', ['name' => 'Тестовое имя']);
        $resultStructure = ['clan_info'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\change_name::post
     */
    public function testChangeName()
    {
        $data = $this->_getActionResult('clans/change.name', ['name' => 'Другое имя']);
        $resultStructure = ['user_fight_data', 'clan_info'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\delete::post
     */
    public function testDelete()
    {
        $data = $this->_getActionResult('clans/delete', ['clan_id' => '1']);
        $resultStructure = ['clan_deletion_result'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\get_info::post
     */
    public function testGetInfo()
    {
        $data = $this->_getActionResult('clans/get.info');
        $resultStructure = ['clan_info'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\del_member::post
     */
    public function testDelMember()
    {
        $clans = new Clans();
        $clans->addClanMember(1, 3);

        $data = $this->_getActionResult('clans/del.member', ['user_id' => 3]);
        $this->assertObjectHasAttribute('delete_clan_member_result', $data);
    }

    /**
     * @covers \action\clans\get_members::post
     */
    public function testGetMembers()
    {
        $data = $this->_getActionResult('clans/get.members', ['clan_id' => 1]);
        $resultStructure = ['clan_members'];
        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\requests\create::post
     */
    public function testRequestsCreate()
    {
        $data = $this->_getActionResult('clans/requests/create', [
                                                                 'viewer_id' => '1',
                                                                 'clan_id' => '1',
                                                                 ]);
        $resultStructure = ['clan_request_creating_status'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\requests\get::post
     */
    public function testRequestsGet()
    {
        $data = $this->_getActionResult('clans/requests/get');
        $resultStructure = ['clan_requests'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\requests\reject::post
     */
    public function testRequestsReject()
    {
        $requests = new ClanRequests();
        $requests->create(1, 2);

        $data = $this->_getActionResult('clans/requests/reject', ['user_id' => 2]);
        $resultStructure = ['clan_requests'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }

    /**
     * @covers \action\clans\requests\accept::post
     */
    public function testRequestsAccept()
    {
        $requests = new ClanRequests();
        $requests->create(1, 2);

        $data = $this->_getActionResult('clans/requests/accept', ['user_id' => 2]);
        $resultStructure = ['clan_info', 'clan_requests'];

        foreach ($resultStructure as $attribute) {
            $this->assertObjectHasAttribute($attribute, $data);
        }
    }
}
