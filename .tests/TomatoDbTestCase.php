<?php
require_once dirname(__FILE__).'/../krest/.tests/DbTestCase.php';

abstract class TomatoDbTestCase extends DbTestCase
{
    protected $_cachePrefix;
    protected $_krestDisableCache = true;

    protected function setUp()
    {
        parent::setUp();
        defined("KREST_DISABLE_CACHE") or define("KREST_DISABLE_CACHE", $this->_krestDisableCache);
        $this->_cachePrefix = \base\Model::getCachePrefix();
    }
}
