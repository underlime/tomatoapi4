<?php

use krest\SocNet;
use lib\RediskaFactory;

require_once dirname(__FILE__).'/TomatoDbTestCase.php';

abstract class ModelTestCase extends TomatoDbTestCase
{
    /**
     * @var \Rediska
     */
    protected $_rediska;

    function __construct()
    {
        parent::__construct();
        $this->_rediska = RediskaFactory::getInstance();
    }

    /**
     * Returns the test dataset.
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__.'/data/fixtures/general_database.xml');
    }

    protected function setUp()
    {
        parent::setUp();
        SocNet::clear();
        SocNet::setSocNetName(SocNet::VK_COM);
        $this->_rediska->flushDb();
    }
}
