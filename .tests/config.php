<?php

use krest\SocNet;

date_default_timezone_set('UTC');
mb_internal_encoding('UTF-8');

return array(
    '_logger' => array(
        'logs_dir' => __DIR__.'/test_data/logs',
        'loggers' => array(
            'error.program' => array(
                'appenders' => array('program_errors'),
                'level' => 'ERROR',
            ),
            'error.common' => array(
                'appenders' => array('common_errors'),
                'level' => 'ERROR',
            ),
            'error.sql' => array(
                'appenders' => array('sql_errors'),
                'level' => 'ERROR',
            ),
            'error.http' => array(
                'appenders' => array('http_errors'),
                'level' => 'ERROR',
            ),
            'error.http.4xx' => array(
                'appenders' => array('http_warnings'),
                'level' => 'WARN',
            ),
            'error.external_request' => array(
                'appenders' => array('external_requests'),
                'level' => 'ERROR',
            ),
        ),
        'appenders' => array(
            'default' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/default.log',
                    'append' => true
                )
            ),
            'program_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/data/logs/error.log',
                    'append' => true
                )
            ),
            'common_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/data/logs/common_exceptions.log',
                    'append' => true
                )
            ),
            'sql_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/data/logs/sql_db_exceptions.log',
                    'append' => true
                )
            ),
            'http_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/data/logs/http_exceptions.log',
                    'append' => true
                )
            ),
            'http_warnings' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/data/logs/http_warnings.log',
                    'append' => true
                )
            ),
            'external_requests' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/data/logs/external_request_exceptions.log',
                    'append' => true
                )
            ),
        )
    ),
    'action' => array(
        'arena' => array(
            'enemies' => array(
                'Action' => array(
                    'doppelganger_probability' => 1,
                ),
            ),
        ),
    ),
    'cli' => array(
        'DecGlory' => array(
            'dlt_glory' => 3,
            'time_to_late' => 1209600,
        ),
        'ClearUsersJournals' => array(
            'time_to_late' => 1209600,
        ),
        'SendNotification' => array(
            'border_time' => 1209600,
        ),
    ),
    'krest' => array(
        'socnet' => array(
            'VkCom' => array(
                'api_id' => 2893571,
                'api_secret' => 'TmmJet3LZ0WnNItJpWic',
                'send_at_once_notifications' => 100,
                'notifications_timeout' => 1,
            ),
            'OkRu' => array(
                'api_id' => 1,
                'api_key' => 'key',
                'api_secret' => 'secret',
            ),
            'MmRu' => array(
                'api_id' => 1,
                'api_key' => 'key',
                'api_secret' => 'secret',
            ),
        ),
        'api' => array(
            'ExceptionHandler' => array(
                'always_code_200' => false,
            ),
        ),
        'Cache' => array(
            'default_provider' => \krest\Cache::FAKE,
            'default_ttl' => 86400,
            'emulate' => false,
        ),
        'Db' => array(
            'default' => array(
                //'unix_socket' => '/var/run/mysql.sock',
                'host' => isset($_SERVER['DEVELOPMENT_MACHINE']) ? '192.168.2.10' : 'localhost',
                'db_name' => 'tomato_test',
                'user' => 'root',
                'password' => isset($_SERVER['DEVELOPMENT_MACHINE']) ? 'password' : '1qaz3wasql',
                'soc_net' => 'vk.com',
            ),
            'ok_ru' => array(
                'host' => 'localhost',
                'db_name' => 'tomato_kombat_ok',
                'user' => 'root',
                'password' => 'password',
                'soc_net' => 'odnoklassniki.ru',
            ),
        ),
        'SocNet' => array(
            'token_salt' => 'ded_maksim',
        ),
    ),
    'lib' => array(
        'TkEventBroker' => array(
            'status_namespace' => 'tomato_kombat_events:status',
            'status_timeout' => 120,
            'host' => 'localhost',
            'port' => isset($_SERVER['DEVELOPMENT_MACHINE']) ? '8000' : '80',
            'channels' => array(
                'vk.com' => 'tomato_kombat_events:vk.com',
                'odnoklassniki.ru' => 'tomato_kombat_events:odnoklassniki.ru',
                'my.mail.ru' => 'tomato_kombat_events:my.mail.ru',
            ),
        ),
    ),
    'model' => array(
        'Bank' => array(
            'currency_rates' => array(
                SocNet::VK_COM => array(
                    1 => array(
                        'id' => 1,
                        'currency' => 'f',
                        'count' => 3,
                        'price' => 1,
                    ),
                    array(
                        'id' => 2,
                        'currency' => 'f',
                        'count' => 10,
                        'price' => 3,
                    ),
                    array(
                        'id' => 3,
                        'currency' => 'f',
                        'count' => 50,
                        'price' => 17,
                    ),
                    array(
                        'id' => 4,
                        'currency' => 'f',
                        'count' => 100,
                        'price' => 33,
                    ),
                    array(
                        'id' => 5,
                        'currency' => 'f',
                        'count' => 250,
                        'price' => 83,
                    ),
                    array(
                        'id' => 6,
                        'currency' => 'f',
                        'count' => 500,
                        'price' => 167,
                    ),
                ),
            ),
        ),
        'Gains' => array(
            'prices' => array(
                'experience' => array(
                    1 => 1,
                    6 => 2,
                    24 => 3,
                    48 => 4,
                ),
                'glory' => array(
                    1 => 1,
                    6 => 2,
                    24 => 3,
                    48 => 4,
                ),
                'energy' => array(
                    1 => 1,
                    6 => 2,
                    24 => 3,
                    48 => 4,
                ),
                'damage' => array(
                    1 => 1,
                    6 => 2,
                    24 => 3,
                    48 => 4,
                ),
                'armor' => array(
                    1 => 1,
                    6 => 2,
                    24 => 3,
                    48 => 4,
                ),
                'fight_tomatos' => array(
                    1 => 1,
                    6 => 2,
                    24 => 3,
                    48 => 4,
                ),
            ),
        ),
        'Clans' => array(
            'creation_price' => 1,
            'rename_price' => 1,
            'max_members_count' => 50,
        ),
        'ClansClash' => array(
            'season_duration' => '7D',
            'season_prize' => 1500,
            'clans_to_award' => 3,
            'max_user_prize' => 100,
        ),
        'Gifts' => array(
            'improved_ferros_price' => 10,
            'open_gift_ferros_price' => 2,
        ),
        'Rivals' => array(
            'rivals_ferros_price' => 6,
        ),
        'User' => array(
            'friends_news' => false,
            'energy_period' => 60,
            'top_limit' => 51,
            'points_learning_on_level' => 10,
            'energy_regen_on_level' => 30,
            'hp_on_level' => 50,
            'energy_for_fight' => 10,
            'energy_recover_ferros_price' => 5,
            'energy_on_level' => 1,
            'login_price' => 10,
            'reset_skills_price' => 30,
            'ease_enemy_price' => 5,
            'soc_slots_price' => 100,
            'item_slot_price' => 5,
            'ring_slot_price' => 5,
            'level_awards_step' => 15,
            'level_awards_ferros' => 1,
            'vegetable_price' => array(
                't' => 5000,
                'f' => 5,
            ),
            'day_bonuses' => array(
                'day_tomatos' => 10,
                'item_days' => array(
                    3 => array(
                        'variant' => 'drink',
                        'count' => 4,
                    ),
                    7 => array(
                        'variant' => 'grenade',
                        'count' => 6,
                    ),
                ),
            ),
            'start_bonuses' => array(
                array(
                    'type' => 'tomatos',
                    'count' => 30,
                    'item_id' => 0,
                    'name_ru' => 'Томатосы',
                ),
                array(
                    'type' => 'medicine',
                    'count' => 3,
                    'item_id' => 83,
                    'name_ru' => 'Здоровье',
                ),
                array(
                    'type' => 'grenade',
                    'count' => 3,
                    'item_id' => 90,
                    'name_ru' => 'Гранаты',
                ),
                array(
                    'type' => 'energy',
                    'count' => 2,
                    'item_id' => 86,
                    'name_ru' => 'Энергия',
                ),
                array(
                    'type' => 'ferros',
                    'count' => 2,
                    'item_id' => 0,
                    'name_ru' => 'Ферросы',
                ),
            ),
        ),
    ),
);
