<?php
namespace bootstrap;

use krest\api\AutoLoader;
use krest\api\Config;

define('ROOT_DIR', realpath(__DIR__.'/..'));
define('LIB_DIR', realpath(__DIR__.'/../lib'));
define('PHPUNIT_TESTING', 1);

require ROOT_DIR.'/krest/api/AutoLoader.php';
$autoLoader = new AutoLoader(ROOT_DIR);
spl_autoload_register([$autoLoader, 'load']);
require ROOT_DIR.'/vendor/autoload.php';

ini_set('display_errors', 1);
error_reporting(-1);
mb_internal_encoding('UTF-8');

Config::setDefaultConfigFile(__DIR__.'/config.php');
\Logger::configure(Config::instance()->getConfig()['_logger']);
