<?php
namespace base;

use krest\base\Struct;

class EventData extends Struct
{
    public $type, $text_ru;
}
