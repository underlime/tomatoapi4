<?php
namespace base;

use krest\base\Action;
use krest\SocNet;
use model\User;

abstract class ServerCallBack extends Action
{
    /**
     * @var \krest\SocNet
     */
    protected $socNet;

    protected function __construct($id)
    {
        parent::__construct($id);
        $this->socNet = SocNet::instance();
    }

    abstract public function run();

    public function get()
    {
        $this->run();
    }

    public function post()
    {
        $this->run();
    }
}
