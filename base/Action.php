<?php
namespace base;

use krest\api\Config;
use krest\Arr;
use krest\EventDispatcher;
use krest\exceptions\HttpException;
use krest\exceptions\NotFoundException;
use krest\Request;
use krest\SocNet;
use model\Achievements;
use model\achievements\AchievementEventData;
use model\spy\Spy;
use model\User;

abstract class Action extends \krest\base\Action
{
    /**
     * @var SocNet
     */
    protected $socNet;

    /**
     * @var \model\User
     */
    protected $user;

    protected $eventsList = [];
    protected $wasAchievement = false;
    protected $needUserInfo = true;
    protected $socNetId;
    protected $userInfo;
    protected $request;

    protected function __construct($id)
    {
        parent::__construct($id);
        Spy::instance()->pushActionData(__FILE__, __METHOD__, __LINE__, 'Инициализация', get_class($this));

        $this->_checkSocNet();
        $this->user = new User();
        if ($this->needUserInfo) {
            $this->_getUserInfo();
            Spy::instance()->setUserId($this->userInfo['soc_net_id']);
        } else {
            Spy::instance()->setIsTarget(false);
        }

        $this->request = Request::instance();
        $this->_handleEventData();
    }

    private function _checkSocNet()
    {
        $this->socNet = SocNet::instance();
        $this->socNet->checkAuth();

        $rightToken = $this->socNet->makeToken();
        $requestParams = Request::instance()->getRequestBodyParams();
        $token = Arr::get($requestParams, 'token');

        if ($token === null) {
            throw new HttpException(400, 'Missing request id');
        }

        if ($token !== $rightToken) {
            throw new HttpException(400, 'Wrong request id');
        }

        $this->socNetId = $this->socNet->getSocNetId();
    }

    private function _getUserInfo()
    {
        try {
            $this->userInfo = $this->user->getUserInfo($this->socNetId);
            Spy::instance()->pushActionData(__FILE__, __METHOD__, __LINE__, 'Извлечение информации о пользователе');
        } catch (NotFoundException $e) {
            Spy::instance()->pushActionData(__FILE__, __METHOD__, __LINE__, 'Получено NotFoundException');
            throw new HttpException(401, $e->getMessage());
        }

        if ($this->userInfo['ban_level']) {
            Spy::instance()->pushActionData(__FILE__, __METHOD__, __LINE__, 'Пользователь в бане');
            throw new HttpException(403, $this->userInfo['ban_reason']);
        }
    }

    private function _handleEventData()
    {
        $eventDispatcher = EventDispatcher::instance();
        $eventDispatcher->addEventListener(\Events::EVENT_DATA, array($this, 'handleEventData'));
    }

    /**
     * Обработчик для события Events::EVENT_DATA,
     * добавляет в список событие для вывода в произвольном экшене
     * @param $eventData
     */
    public function handleEventData($eventData)
    {
        $this->eventsList[] = $eventData;
        $this->wasAchievement = ($this->wasAchievement || ($eventData instanceof AchievementEventData));
    }

    /**
     * Извлечь параметр soc_net_id
     * @param bool $required
     * @throws \krest\exceptions\HttpException
     * @return mixed
     */
    protected function _getSocNetId($required = false)
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $socNetId = Arr::get($requestParams, 'soc_net_id');
        if ($required && $socNetId === null) {
            throw new HttpException(400, 'Wrong soc_net_id');
        } elseif ($socNetId === null) {
            $socNetId = $this->socNetId;
        }

        return $socNetId;
    }

    /**
     * Извлечь параметр user_id
     * @param bool $required
     * @throws \krest\exceptions\HttpException
     * @return mixed
     */
    protected function _getUserId($required = false)
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $userId = Arr::get($requestParams, 'user_id');
        if ($required && $userId === null) {
            throw new HttpException(400, 'Wrong user id');
        } elseif ($userId === null) {
            $userId = Arr::get($this->userInfo, 'user_id');
        }

        return $userId;
    }

    /**
     * Извлечь ответ с добавленными событиями
     * @return array
     */
    public function getAnswer()
    {
        if ($this->eventsList) {
            $this->_setEventsAnswer();
        }
        $answer = parent::getAnswer();
        $spy = Spy::instance();
        if ($spy->getIsTarget()) {
            $spyActionData = [
                'action' => get_class($this),
                'params' => $this->request->getRequestBodyParams(),
                'response' => $answer,
            ];
            $spy->pushActionData(__FILE__, __METHOD__, __LINE__, 'Получен ответ', $spyActionData);
        }
        return $answer;
    }

    private function _setEventsAnswer()
    {
        $data = array('events' => $this->eventsList);
        if ($this->wasAchievement) {
            $achievements = new Achievements();
            $data['user_achievements'] = array(
                '_init' => $achievements->getUserAchievements($this->userInfo['user_id']),
            );
        }
        $this->_addAnswerArr($data);
        $this->eventsList = [];
    }
}
