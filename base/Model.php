<?php
namespace base;

use krest\base\MagicAccessors;
use krest\base\Model as BaseModel;
use krest\Cache;
use krest\EventDispatcher;

abstract class Model extends BaseModel
{
    use MagicAccessors;

    const GET_FROM_CACHE = "Model::getFromCache";
    const SET_TO_CACHE = "Model::setToCache";
    const DELETE_FROM_CACHE = "Model::deleteFromCache";
    const UPDATE_IN_CACHE = "Model::updateInCache";

    private $_isPhpunit;

    public function __construct()
    {
        $this->_isPhpunit = defined("PHPUNIT_TESTING");
    }

    /**
     * @static
     * @return string
     */
    public static function getCachePrefix()
    {
        return Cache::getPrefix();
    }

    /**
     * @static
     * @param string $prefix
     */
    public static function setCachePrefix($prefix)
    {
        Cache::setPrefix($prefix);
    }

    /**
     * Обработчик кэша
     * @param callable $func
     * @param string $cacheKey
     * @param null|int $ttl
     * @return mixed
     */
    public function getCached($func, $cacheKey, $ttl = null)
    {
        $data = $this->_getDataFromCache($cacheKey);
        if ($data === null) {
            $data = call_user_func($func);
            $this->_setDataIntoCache($cacheKey, $data, $ttl);
        }
        return $data;
    }

    protected function _getDataFromCache($cacheKey)
    {
        if ($this->_isPhpunit) {
            EventDispatcher::instance()->dispatchEvent(self::GET_FROM_CACHE, [Cache::getPrefix().$cacheKey]);
        }
        return parent::_getDataFromCache($cacheKey);
    }

    protected function _setDataIntoCache($cacheKey, $data, $time = null)
    {
        if ($this->_isPhpunit) {
            EventDispatcher::instance()->dispatchEvent(self::SET_TO_CACHE, [Cache::getPrefix().$cacheKey]);
        }
        return parent::_setDataIntoCache($cacheKey, $data, $time);
    }

    protected function _updateCachedData($cacheKey, $data, $time = null)
    {
        if ($this->_isPhpunit) {
            EventDispatcher::instance()->dispatchEvent(self::UPDATE_IN_CACHE, [Cache::getPrefix().$cacheKey]);
        }
        return parent::_updateCachedData($cacheKey, $data, $time);
    }

    protected function _deleteDataFromCache($cacheKey)
    {
        if ($this->_isPhpunit) {
            EventDispatcher::instance()->dispatchEvent(self::DELETE_FROM_CACHE, [Cache::getPrefix().$cacheKey]);
        }
        return parent::_deleteDataFromCache($cacheKey);
    }
}
