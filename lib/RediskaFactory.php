<?php
namespace lib;

class RediskaFactory
{
    private static $_defaultNamespace = 'tomato_kombat:undefined:';
    private static $_cache = [];

    /**
     * @param string $userNamespace
     * @return \Rediska
     */
    public static function getInstance($userNamespace=null)
    {
        if ($userNamespace === null) {
            $namespace = self::$_defaultNamespace;
        }
        else {
            $namespace = $userNamespace;
        }

        if (isset(self::$_cache[$namespace])) {
            $instance = self::$_cache[$namespace];
        }
        else {
            $dev = isset($_SERVER['DEVELOPMENT_MACHINE']);
            $localTesting = $dev && defined('PHPUNIT_TESTING');
            $options = array(
                'namespace' => $namespace,
                'servers' => array(
                    array(
                        'host' => ($localTesting || $dev) ? '192.168.2.10' : 'localhost',
                        'db' => $localTesting ? '15' : '0',
                    ),
                ),
            );
            $instance = new \Rediska($options);
            self::$_cache[$namespace] = $instance;
        }
        return $instance;
    }

    /**
     * @return mixed
     */
    public static function getDefaultNamespace()
    {
        return self::$_defaultNamespace;
    }

    /**
     * @param mixed $namespace
     */
    public static function setDefaultNamespace($namespace)
    {
        self::$_defaultNamespace = "tomato_kombat:$namespace:";
    }
}
