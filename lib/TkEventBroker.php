<?php
namespace lib;

use krest\api\Config;
use krest\Arr;
use krest\SocNet;

class TkEventBroker
{
    /**
     * @var TkEventBroker
     */
    private static $_instance;

    /**
     * @var \Rediska
     */
    private $_redis;

    private $_config;

    /**
     * @return TkEventBroker
     */
    public static function instance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function __construct()
    {
        $this->_redis = RediskaFactory::getInstance('');
        $this->_config = Config::instance()->getForClass(__CLASS__);
    }

    protected function __clone()
    {
    }

    protected function __wakeup()
    {
    }

    /**
     * Время последней установки статуса
     * @return \DateTime|null
     */
    public function getLastStatusTime()
    {
        $strDate = $this->_redis->get($this->_config['status_namespace'] . ':status');
        if ($strDate === null) {
            $dateTime = null;
        } else {
            $dateTime = new \DateTime($strDate);
        }
        return $dateTime;
    }

    /**
     * Жив ли сервис?
     * @return bool
     */
    public function getServiceStatus()
    {
        $lastTime = $this->getLastStatusTime();
        if ($lastTime === null) {
            $alive = false;
        } else {
            $nowTime = new \DateTime('now', $lastTime->getTimezone());
            $interval = $nowTime->diff($lastTime);
            $seconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;
            $alive = ($seconds <= $this->_config['status_timeout']);
        }
        return $alive;
    }

    /**
     * Извлечь канал для данной соц. сети
     * @return string|null
     */
    public function getChannel()
    {
        $alive = $this->getServiceStatus();
        if ($alive) {
            $channel = Arr::get($this->_config['channels'], SocNet::instance()->getSocNetName());
            $enabledChannelsStr = $this->_redis->get($this->_config['status_namespace'] . ':enabled_channels');
            if ($enabledChannelsStr) {
                $enabledChannels = json_decode($enabledChannelsStr);
                if (!in_array($channel, $enabledChannels)) {
                    $channel = null;
                }
            } else {
                $channel = null;
            }
        } else {
            $channel = null;
        }
        return $channel;
    }

    /**
     * Извлечь хост для подключения
     * @return string
     */
    public function getHost()
    {
        return $this->_config['host'];
    }

    /**
     * Извлечь порт для подключения
     * @return int
     */
    public function getPort()
    {
        return $this->_config['port'];
    }

    /**
     * Отправить широковещательное событие
     * @param string $eventName
     * @param mixed $eventData
     */
    public function dispatchBroadcastEvent($eventName, $eventData = null)
    {
        $channel = $this->getChannel();
        if ($channel) {
            $message = [
                'type' => 'broadcast',
                'message' => json_encode(
                    [
                    'type' => $eventName,
                    'data' => $eventData,
                    ]
                ),
            ];
            $this->_redis->publish($channel, json_encode($message));
        }
    }

    /**
     * Отправить событие пользователю
     * @param string $socNetId
     * @param string $eventName
     * @param mixed $eventData
     */
    public function dispatchPrivateEvent($socNetId, $eventName, $eventData = null)
    {
        $channel = $this->getChannel();
        if ($channel) {
            $message = [
                'type' => 'private',
                'userId' => $socNetId,
                'message' => json_encode(
                    [
                    'type' => $eventName,
                    'data' => $eventData,
                    ]
                ),
            ];
            $this->_redis->publish($channel, json_encode($message));
        }
    }
}
