<?php

/* @var $actionsList array */
use krest\Arr;

/* @var $action string */
/* @var $socNet string */
/* @var $socNetId int */
/* @var $format string */

if ($action)
    $actionData = Arr::req($actionsList, $action);
else
    $actionData = NULL;
?>

<h2>Параметры запроса</h2>

<form id="params-form">
    <table class="fields-table">
        <tr>
            <td colspan="2">
                <label for="soc_net">Социальная сеть:</label>
                <select name="soc_net" id="soc_net">
                    <option value="vk.com">vk.com</option>
                    <option value="odnoklassniki.ru">odnoklassniki.ru</option>
                    <option value="my.mail.ru">my.mail.ru</option>
                </select>
                <script>
                    (function() {
                        var item = document.querySelector("#soc_net option[value='<?=$socNet?>']");
                        if (item) {
                            item.setAttribute("selected", "selected");
                        }
                    })();
                </script>
            </td>
        </tr>
        <tr>
            <td>
                <label for="soc_net_id">soc_net_id:</label>
            </td>
            <td>
                <input type="text" name="soc_net_id" id="soc_net_id" value="<?=$socNetId?>" />
            </td>
        </tr>
        <tr>
            <td>
                <label for="format">Формат:</label>
            </td>
            <td>
                <select name="format" id="format">
                    <? $selected = ($format == 'JSON') ? 'selected="selected"' : '' ?>
                    <option value="JSON" <?=$selected?>>JSON</option>
                    <? $selected = ($format == 'XML') ? 'selected="selected"' : '' ?>
                    <option value="XML" <?=$selected?>>XML</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label for="action">Метод:</label>
            </td>
            <td>
                <select name="action" id="action">
                    <? foreach ($actionsList as $actionName => $params) { ?>
                        <? $selected = ($action == $actionName) ? 'selected="selected"' : '' ?>
                        <option value="<?=$actionName?>" <?=$selected?>><?=$actionName?></option>
                    <? } ?>
                </select>
            </td>
        </tr>
    </table>
    <script>
        (function() {
            document.getElementById('soc_net').addEventListener('change', submitForm);
            document.getElementById('format').addEventListener('change', submitForm);
            document.getElementById('action').addEventListener('change', submitForm);

            function submitForm() {
                document.getElementById('params-form').submit();
            }
        })();
    </script>
    <button type="submit">Выбрать</button>
</form>

<? if ($action) { ?>
    <? $sentParams = Arr::get($_POST, 'params'); ?>
    <form method="post">
        <div id="action-params">
            <input type="hidden" name="params[execute]" value="1" />
            <table class="fields-table">
                <? foreach ($actionData as $name => $defaultValue) { ?>
                <tr>
                    <td>
                        <label for="param-<?=$name?>"><?=$name?>:</label>
                    </td>
                    <td>
                        <? $value = Arr::get($sentParams, $name, $defaultValue) ?>
                        <input type="text" name="params[<?=$name?>]" id="param-<?=$name?>" value="<?=$value?>" />
                    </td>
                </tr>
                <? } ?>
            </table>
            <button type="submit">Выполнить</button>
        </div>
    </form>
<? } ?>
