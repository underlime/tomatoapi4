<?php
/* @var $paramsHtml string */
/* @var $actionHtml string */
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Тестовая среда</title>
        <meta charset="UTF-8" />
        <link rel="stylesheet" href="test-request/styles.css" />
    </head>
    <body>
        <div id="params">
            <?=$paramsHtml?>
        </div>
        <div id="action-data">
            <?=$actionHtml?>
        </div>
    </body>
</html>
