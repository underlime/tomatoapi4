<?php
/* @var $text string */
/* @var $httpCode int */
/* @var $contentType string */
/* @var $errno int */
/* @var $error string */
/* @var $url string */
/* @var $token string */
/* @var $params array */
?>

<h2>Ответ сервера</h2>

<h3>Запрос</h3>

<div class="action-data-param">
    <span class="action-data-label">URL:</span>
    <span class="action-data-value"><?=$url?></span>
</div>

<div class="action-data-param">
    <span class="action-data-label">Токен:</span>
    <span class="action-data-value"><?=$token?></span>
</div>

<div class="action-data-param">
    <span class="action-data-label">Параметры:</span>
    <? foreach ($params as $name=>$value) { ?>
        <div class="action-data-value"><?="$name&nbsp;&rarr;&nbsp;$value"?></div>
    <? } ?>
</div>

<h3>Ответ</h3>

<div class="action-data-param">
    <span class="action-data-label">HTTP код:</span>
    <span class="action-data-value"><?=$httpCode?></span>
</div>

<div class="action-data-param">
    <span class="action-data-label">Content-type:</span>
    <span class="action-data-value"><?=$contentType?></span>
</div>

<div class="action-data-param">
    <span class="action-data-label">Размер, Кб:</span>
    <span class="action-data-value"><?=round(strlen($text)/1024, 3)?></span>
</div>

<? if (substr_count($contentType, 'json')) { ?>
    <? $arrayText = json_decode($text) ?>
    <div class="action-data-param">
        <span class="action-data-label">print_r ответа:</span>
        <br />
        <span class="action-data-value">
            <textarea rows="15" cols="100" readonly="readonly"><?print_r($arrayText)?></textarea>
        </span>
    </div>
<? } ?>


<div class="action-data-param">
    <span class="action-data-label">Текст ответа:</span>
    <br />
    <span class="action-data-value">
        <textarea rows="10" cols="100" readonly="readonly"><?=htmlspecialchars($text, ENT_QUOTES)?></textarea>
    </span>
</div>
