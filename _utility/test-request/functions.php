<?php

use krest\Arr;
use krest\SocNet;

function renderView($viewName, array $data = array())
{
    foreach ($data as $name => $value)
        ${$name} = $value;

    ob_start();
    require __DIR__.'/views/'.$viewName.'.php';
    return ob_get_clean();
}

function executeActionRequest($action, $socNet, $socNetId, $format, array $params)
{
    $url = _getUrl($action, $socNet);
    $params = createRequestParams($socNet, $socNetId, $format, $params);
    $_POST = array_merge($_POST, $params);

    $urlParts = parse_url($url);
    $_SERVER['REQUEST_URI'] = $urlParts['path'];
    SocNet::setSocNetName($socNet);
    $token = SocNet::instance()->makeToken();

    return _makeRequest($url, $token, $params);
}

function createRequestParams($socNet, $socNetId, $format, array $params)
{
    if ($socNet == SocNet::VK_COM) {
        $config = \krest\api\Config::instance()->getForClass('\krest\socnet\VkCom');
        $authKey = md5("{$config['api_id']}_{$socNetId}_{$config['api_secret']}");

        $params['soc_net'] = $socNet;
        $params['auth_key'] = $authKey;
        $params['sid'] = '1';
        $params['viewer_id'] = $socNetId;
        $params['format'] = $format;
    }
    elseif ($socNet == SocNet::OK_RU) {
        $config = \krest\api\Config::instance()->getForClass('\krest\socnet\OkRu');
        $baseParams = [
            "logged_user_id" => $socNetId,
            "application_key" => "123",
            "session_key" => "123",
            "api_server" => "http://api.odnoklassniki.ru/",
        ];

        ksort($baseParams);
        $sigBase = "";
        foreach ($baseParams as $name => $value) {
            $sigBase .= "{$name}={$value}";
        }
        $sigBase .= Arr::req($config, "api_secret");
        $baseParams["sig"] = md5($sigBase);

        $params["query_params"] = json_encode($baseParams);
    }
    elseif ($socNet == SocNet::MM_RU) {
        $config = \krest\api\Config::instance()->getForClass('\krest\socnet\MmRu');
        $baseParams = [
            "vid" => $socNetId,
            "session_key" => "123",
        ];

        ksort($baseParams);
        $sigBase = "";
        foreach ($baseParams as $name => $value) {
            $sigBase .= "{$name}={$value}";
        }
        $sigBase .= Arr::req($config, "api_secret");
        $baseParams["sig"] = md5($sigBase);

        $params["query_params"] = json_encode($baseParams);
    }
    else {
        throw new InvalidArgumentException("Wrong social network id: $socNet");
    }

    return $params;
}

function _getUrl($action, $socNet)
{
    $baseUrl = _getBaseUrl();
    $path = "http://localhost{$baseUrl}{$socNet}/{$action}";
    $url = str_replace(DIRECTORY_SEPARATOR, '/', $path);

    return $url;
}

function _getBaseUrl()
{
    $noDirSep = (DIRECTORY_SEPARATOR == '/') ? '\\' : '/';

    $docRoot = str_replace($noDirSep, DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT']);
    $dir = realpath(__DIR__.'/../..').'/';
    $baseUrl = str_replace($docRoot, '', $dir);

    return $baseUrl;
}

function _makeRequest($url, $token, array $params)
{
    $params['token'] = $token;

    $post = array();
    foreach ($params as $name => $val)
        if ($val)
            $post[$name] = $val;

    $ch = curl_init($url);
    curl_setopt_array($ch, array(
                                CURLOPT_POST => true,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POSTFIELDS => http_build_query($post),
                                CURLOPT_HTTPHEADER => array(
                                    'Host: '.Arr::get($_SERVER, 'HTTP_HOST', 'localhost'),
                                ),
                           ));

    $dataParams = $params;
    unset($dataParams['execute']);

//
//    $memcache = new \Memcache();
//    if (@$memcache->connect('127.0.0.1')) {
//        $memcache->flush();
//        $memcache->close();
//    }

    $data = array(
        'text' => curl_exec($ch),
        'httpCode' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        'contentType' => curl_getinfo($ch, CURLINFO_CONTENT_TYPE),
        'errno' => curl_errno($ch),
        'error' => curl_error($ch),
        'url' => $url,
        'token' => $token,
        'params' => $dataParams,
    );

    curl_close($ch);
    return $data;
}
