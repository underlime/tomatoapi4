<?php
$actionsList = [
    //academy
    "academy/change.details" => [
        "body" => "tBody1",
        "eyes" => "eyes1",
        "mouth" => "mouth1",
        "hair" => "tHair1",
        "currency" => "t",
    ],
    "academy/change.vegetable" => [
        "vegetable" => "tomato",
    ],
    "academy/change.login" => [
        "login" => "al-tomat",
    ],
    "academy/get.char.details" => [
        "vegetable" => "all",
    ],
    "academy/get.login.price" => [],
    "academy/get.vegetable.price" => [],
    "academy/train.skills" => [
        "strength" => null,
        "agility" => null,
        "intellect" => null,
    ],
    "academy/reset.skills" => [],

    //arena
    "arena/enemies/get.by.identity" => [
        "identity" => "1",
    ],
    "arena/enemies/get.by.level" => [
        "level" => "1",
    ],
    "arena/fight" => [
        "enemy_id" => "2",
        "ease_enemy" => "0",
    ],

    //bank
    "bank/get.currency.rates" => [],

    //citadel
    "citadel/get.prices" => [],
    "citadel/set.gain" => [
        "gain_object" => "experience",
        "hours" => "1",
    ],
    "citadel/get.gains" => [],

    //clans
    "clans/validate.name" => [
        "name" => "Тестовое имя"
    ],
    "clans/create" => [
        "name" => "Тестовое имя"
    ],
    "clans/change.name" => [
        "name" => "Другое имя"
    ],
    "clans/delete" => [
        "clan_id" => "0"
    ],
    "clans/get.info" => [
        "clan_id" => "1"
    ],
    "clans/get.members" => [
        "clan_id" => "1",
    ],
    "clans/del.member" => [
        "user_id" => "",
    ],
    "clans/get.top" => [],
    "clans/requests/create" => [
        "clan_id" => "1",
    ],
    "clans/requests/get" => [],
    "clans/requests/reject" => [
        "user_id" => "",
    ],
    "clans/requests/accept" => [
        "user_id" => "",
    ],

    //gifts
    "gifts/get.gifts.info" => [],
    "gifts/get.random.users" => [],
    "gifts/get.user.gifts.list" => [],
    "gifts/send.gifts.to.friends" => [],
    "gifts/send.gifts.to.random.users" => [],
    "gifts/send.gift.to.user" => [
        "user_id" => null,
    ],
    "gifts/open.gift.as.usual" => [
        "gift_id" => 0,
    ],
    "gifts/open.all.gifts.as.usual" => [],
    "gifts/open.gift.as.improved" => [
        "gift_id" => 0,
    ],

    //items
    "items/apply.item" => [
        "item_id" => 0,
    ],
    "items/buy.item" => [
        "item_id" => 0,
        "count" => null,
        "currency" => null,
    ],
    "items/get.market.list" => [],
    "items/sell.item" => [
        "item_id" => 0,
        "count" => null,
    ],
    "items/transfer" => [
        "item_id" => 1,
        "count" => 1,
        "to_user_id" => 0,
    ],

    //news
    "news/get.news" => [],
    "news/set.news.read" => [
        "news_id" => 0,
        "is_shared" => 1,
    ],

    //rivals
    "rivals/add.message" => [
        "message" => "Нападайте!",
    ],
    "rivals/get.list" => [],
    "rivals/get.price" => [],

    //sharpening
    "sharpening/get.sharpening.params" => [
        "item_id" => "19",
        "level" => "1",
    ],
    "sharpening/sharpen.weapon" => [
        "item_id" => "19",
        "currency" => "t",
    ],

    //top
    "top/glory/get.by.city" => [
        "city_code" => 722,
        "interval" => "all",
    ],
    "top/glory/get.global" => [
        "interval" => "all",
    ],
    "top/glory/get.friends.top" => [
        "interval" => "all",
    ],
    "top/level.params/get.strength.top" => [],
    "top/level.params/get.agility.top" => [],
    "top/level.params/get.intellect.top" => [],
    "top/fight.data/get.experience.top" => [],

    //user
    "user/change.settings" => [
        "graphics_quality" => null,
        "blood_enabled" => null,
        "blood_color" => null,
        "cartoon_stroke" => null,
        "sounds_enabled" => null,
    ],
    "user/check.login" => [
        "login" => "test",
    ],
    "user/get.achievements" => [
        "user_id" => null,
    ],
    "user/get.info" => [
        "soc_net_id" => null,
    ],
    "user/get.list.info" => [
        "soc_net_ids" => "3081515",
    ],
    "user/get.journal" => [],
    "user/get.specials" => [],
    "user/register" => [
        "login" => "test",
        "vegetable" => "tomato",
        "body" => "tBody1",
        "hands" => "tArm1",
        "eyes" => "eyes1",
        "mouth" => "mouth1",
        "hair" => "tHair1",
        "referrer_type" => "test",
        "referrer" => 3081515,
        "city" => 722,
    ],
    "user/renew.energy" => [],
    "user/get.money" => [],
    "user/recover.energy" => [],
    "user/buy.social.slots" => [],
    "user/buy.item.slot" => [],
    "user/buy.ring.slot" => [],
    "user/give.friends.prizes" => ["bonus_type" => "tomatos"],
    "user/set.album.id" => ["album_id" => 0],
    "user/test" => [],

    "system/log" => [
        "error_info" => "test"
    ],
    "triggers/on.tutorial.complete" => [],
];

ksort($actionsList);
return $actionsList;
