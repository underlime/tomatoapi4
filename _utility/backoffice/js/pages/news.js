//noinspection JSUnusedGlobalSymbols
function NewsFormController($scope) {
    "use strict";
    $scope.onValuesInit = function () {
        handleAdditionalData();
        handleCommonNews();
    };

    function handleAdditionalData() {
        if ($scope["UserNews_additional_data"]) {
            var value = JSON.parse($scope["UserNews_additional_data"]);
            switch ($scope["UserNews_news_type"]) {
                case "0":
                    $scope["header_ru"] = value["h_ru"];
                    $scope["header_en"] = value["h_en"];
                    $scope["text_ru"] = value["ru"];
                    $scope["text_en"] = value["en"];
                    break;
            }
        }
    }

    function handleCommonNews() {
        var fields = ["header_ru", "header_en", "text_ru", "text_en"];
        var value = {"h_ru": "", "ru": "", "h_en": "", "en": ""};
        fields.forEach(function (model) {
            var key = model
                .replace("header_", "h_")
                .replace("text_", "");
            $scope.$watch(model, function () {
                value[key] = $scope[model];
                $scope["UserNews_additional_data"] = JSON.stringify(value);
            });
        });
    }
}
