<?php
return array(
	array( // row #0
		'item_id' => 1,
		'description' => '<p>Неплохой выбор, если бюджет ограничен!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+5</span>   ЖЗН:  <span class=\'green\'>+10</span></p>\n',
	),
	array( // row #1
		'item_id' => 2,
		'description' => '<p>В ней невыносимо тянет купаться в фонтанах!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+10</span>   ЖЗН:  <span class=\'green\'>+15</span></p>\n',
	),
	array( // row #2
		'item_id' => 3,
		'description' => '<p>Истинный фанат преодолеет все преграды!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+15</span>   ЖЗН:  <span class=\'green\'>+15</span></p>\n',
	),
	array( // row #3
		'item_id' => 4,
		'description' => '<p>Самое то для начинающих Робин Гудов!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>125</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+30</span>   СКО:  <span class=\'green\'>+10</span>   ЖЗН:  <span class=\'green\'>+40</span></p>\n',
	),
	array( // row #4
		'item_id' => 5,
		'description' => '<p>Приятно облегает тело, защитит от колюще-режущих ран!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>131</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+35</span>   СКО:  <span class=\'green\'>+10</span>   ЖЗН:  <span class=\'green\'>+40</span></p>\n',
	),
	array( // row #5
		'item_id' => 6,
		'description' => '<p>Броник выпилен лобзиком на уроке труда, нерадивым учеником!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>111</span>   ЛОВ:  <span class=\'red\'>126</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+45</span>   ЖЗН:  <span class=\'green\'>+60</span></p>\n',
	),
	array( // row #6
		'item_id' => 7,
		'description' => '<p>Из косточек абрикоса, ручная работа!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>131</span>   ЛОВ:  <span class=\'red\'>126</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+50</span>   ЖЗН:  <span class=\'green\'>+70</span></p>\n',
	),
	array( // row #7
		'item_id' => 8,
		'description' => '<p>Такие носят медбратья, повышают шансы на выживание!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>131</span>   ЛОВ:  <span class=\'red\'>126</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+60</span>   ЖЗН:  <span class=\'green\'>+100</span></p>\n',
	),
	array( // row #8
		'item_id' => 9,
		'description' => '<p>С ним Вам не страшны удары врага, битвы превращаются в избиение младенцев!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>143</span>   ЛОВ:  <span class=\'red\'>143</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   ЗАЩ:  <span class=\'green\'>+80</span>   ЖЗН:  <span class=\'green\'>+150</span></p>\n',
	),
	array( // row #9
		'item_id' => 10,
		'description' => '<p>Лучше, чем голые руки.</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>110</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #10
		'item_id' => 11,
		'description' => '<p>Идеальный вариант для начинающих бойцов. Имеется автограф от братьев Плечко!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>110</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+5</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #11
		'item_id' => 12,
		'description' => '<p>Крепкий дубовый щит</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>120</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #12
		'item_id' => 13,
		'description' => '<p>Недорогой металлический щит</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>130</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+15</span></p>\r\n',
	),
	array( // row #13
		'item_id' => 14,
		'description' => '<p>Моток бинта, битое стекло и клей, теперь Вы король подпольных боев!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>125</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+15</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #14
		'item_id' => 15,
		'description' => '<p>Хорошая защита от любой вороны</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>140</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #15
		'item_id' => 16,
		'description' => '<p>Пацаны на районе оценят. Незаменимый инструмент для удаления зубов.</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>130</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+30</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #16
		'item_id' => 17,
		'description' => '<p>Щит из драгоценного железа</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+25</span></p>\r\n',
	),
	array( // row #17
		'item_id' => 18,
		'description' => '<p>Очень прочный щит с шипами</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>160</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+30</span></p>\r\n',
	),
	array( // row #18
		'item_id' => 19,
		'description' => '<p>Один удар - три дырки! Для тех кто предпочитает жёсткий стиль!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>135</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+40</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #19
		'item_id' => 20,
		'description' => '<p>Все что осталось от древнего братства наёмных убийц!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>155</span>   ЛОВ:  <span class=\'red\'>165</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+90</span>   СКО:  <span class=\'green\'>+70</span></p>\r\n',
	),
	array( // row #20
		'item_id' => 21,
		'description' => '<p>Прилетели из космоса, или из будущего. Происхождение - тайна за семью печатями!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>140</span>   ЛОВ:  <span class=\'red\'>175</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+120</span>   СКО:  <span class=\'green\'>+70</span></p>\r\n',
	),
	array( // row #21
		'item_id' => 22,
		'description' => '<p>Новомодный аргумент в споре!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>130</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+30</span>   СКО:  <span class=\'green\'>+10</span></p>\n',
	),
	array( // row #22
		'item_id' => 23,
		'description' => '<p>Орудие пра-пра-пра-пра-прадеда, с которым он ходил на охоту долгими, зимними вечерами!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>135</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+40</span>   СКО:  <span class=\'green\'>+10</span></p>\n',
	),
	array( // row #23
		'item_id' => 24,
		'description' => '<p>Четвёртая база обеспечена!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>140</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+12</span></p>\n',
	),
	array( // row #24
		'item_id' => 25,
		'description' => '<p>Недорогое оружие для борьбы с инопланетными захватчиками!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>145</span>   ЛОВ:  <span class=\'red\'>110</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+60</span>   ЗАЩ:  <span class=\'green\'>+10</span>   СКО:  <span class=\'green\'>+15</span></p>\r\n',
	),
	array( // row #25
		'item_id' => 26,
		'description' => '<p>Одноручный, короткий меч!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>115</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+70</span>   СКО:  <span class=\'green\'>+17</span></p>\r\n',
	),
	array( // row #26
		'item_id' => 27,
		'description' => '<p>Энергетический меч, который бережет природу!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>155</span>   ЛОВ:  <span class=\'red\'>120</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+80</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #27
		'item_id' => 28,
		'description' => '<p>Этот топор предназначен для рубки овощей!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>160</span>   ЛОВ:  <span class=\'red\'>125</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+90</span>   СКО:  <span class=\'green\'>+25</span></p>\r\n',
	),
	array( // row #28
		'item_id' => 29,
		'description' => '<p>Диджей-лишь-владеть-мечом-может-этим...</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>165</span>   ЛОВ:  <span class=\'red\'>130</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+100</span>   СКО:  <span class=\'green\'>+30</span></p>\r\n',
	),
	array( // row #29
		'item_id' => 30,
		'description' => '<p>Аладдин мечтал о таком!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>170</span>   ЛОВ:  <span class=\'red\'>135</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+110</span>   СКО:  <span class=\'green\'>+35</span></p>\r\n',
	),
	array( // row #30
		'item_id' => 31,
		'description' => '<p>Этим мечом можно очень сильно порезаться!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>175</span>   ЛОВ:  <span class=\'red\'>140</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+120</span>   СКО:  <span class=\'green\'>+40</span></p>\r\n',
	),
	array( // row #31
		'item_id' => 32,
		'description' => '<p>Очень крепкий меч, с ним можно валить снежных троллей!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>180</span>   ЛОВ:  <span class=\'red\'>145</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+130</span>   СКО:  <span class=\'green\'>+45</span></p>\r\n',
	),
	array( // row #32
		'item_id' => 33,
		'description' => '<p>Работает от ярости хозяина!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+140</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #33
		'item_id' => 34,
		'description' => '<p>Защищает от сырости около прудов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+5</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #34
		'item_id' => 35,
		'description' => '<p>Добротный посох, можно пасти овец, можно крушить качаны!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>160</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+80</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #35
		'item_id' => 36,
		'description' => '<p>Хороша против придавливания танком, проверена Микитой Нихалковым</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+10</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #36
		'item_id' => 37,
		'description' => '<p>Сшит из останков Тэндальфа Серого</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>110</span>   ЛОВ:  <span class=\'red\'>110</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+15</span>   СКО:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #37
		'item_id' => 38,
		'description' => '<p>Плащ настоящего барда. Привезен с грушинского фестиваля!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>120</span>   ЛОВ:  <span class=\'red\'>120</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   СКО:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #38
		'item_id' => 39,
		'description' => '<p>Плащ формата А1, отбит у героя по имени Ватман</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>130</span>   ЛОВ:  <span class=\'red\'>130</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+25</span>   СКО:  <span class=\'green\'>+15</span></p>\r\n',
	),
	array( // row #39
		'item_id' => 40,
		'description' => '<p>Развивает большую скорость и силу</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>220</span>   ЛОВ:  <span class=\'red\'>250</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+120</span>   СКО:  <span class=\'green\'>+80</span></p>\r\n',
	),
	array( // row #40
		'item_id' => 41,
		'description' => '<p>Не слишком хорошо летает, но можно немного потерять в весе</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+5</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #41
		'item_id' => 42,
		'description' => '<p>Шлем, благодаря которому салат Цезарь соблазнил Клеопатру</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>110</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #42
		'item_id' => 43,
		'description' => '<p>Шляпа заядлого искателя приключений</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>115</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+15</span></p>\r\n',
	),
	array( // row #43
		'item_id' => 44,
		'description' => '<p>Для копейщиков-нищебродов!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>170</span>   ЛОВ:  <span class=\'red\'>115</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+100</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #44
		'item_id' => 45,
		'description' => '<p>Сшита Из останков Тэндальфа Серого</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>120</span>   ЛОВ:  <span class=\'red\'>110</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #45
		'item_id' => 46,
		'description' => '<p>Тот самый шлем, о котором писала Марина Семкина в не самых лучших рассказах</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>125</span>   ЛОВ:  <span class=\'red\'>115</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+25</span></p>\r\n',
	),
	array( // row #46
		'item_id' => 47,
		'description' => '<p>Наследие предков</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>180</span>   ЛОВ:  <span class=\'red\'>125</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+120</span>   СКО:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #47
		'item_id' => 48,
		'description' => '<p>Форменный шлем солдат рабоче-крестьянской армии</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>130</span>   ЛОВ:  <span class=\'red\'>115</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+30</span></p>\r\n',
	),
	array( // row #48
		'item_id' => 49,
		'description' => '<p>Незаменимая вещь в сельском хозяйстве</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>190</span>   ЛОВ:  <span class=\'red\'>135</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+140</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #49
		'item_id' => 50,
		'description' => '<p>Шлем для самых упрямых</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>135</span>   ЛОВ:  <span class=\'red\'>120</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+35</span></p>\r\n',
	),
	array( // row #50
		'item_id' => 51,
		'description' => '<p>Можно косить траву и врагов!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>140</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+160</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #51
		'item_id' => 52,
		'description' => '<p>Внушает страх в сердца врагов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>130</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+60</span>   ЖЗН:  <span class=\'green\'>+100</span></p>\r\n',
	),
	array( // row #52
		'item_id' => 53,
		'description' => '<p>Пародия на настоящие рыцарские пики!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>210</span>   ЛОВ:  <span class=\'red\'>140</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+180</span>   СКО:  <span class=\'green\'>+30</span></p>\r\n',
	),
	array( // row #53
		'item_id' => 54,
		'description' => '<p>Легкое и крепкое, все помрут от зависти!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>220</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+200</span>   СКО:  <span class=\'green\'>+30</span></p>\r\n',
	),
	array( // row #54
		'item_id' => 55,
		'description' => '<p>Топорище и древко из обсидиана, редкое и прочное оружие!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>230</span>   ЛОВ:  <span class=\'red\'>160</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+240</span>   СКО:  <span class=\'green\'>+40</span></p>\r\n',
	),
	array( // row #55
		'item_id' => 56,
		'description' => '<p>Мощнейшее оружие в сильных руках!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>205</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+120</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #56
		'item_id' => 57,
		'description' => '<p>Все шары улетят в правильном направлении!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>225</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+150</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #57
		'item_id' => 58,
		'description' => '<p>Орудие кройки!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+180</span>   СКО:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #58
		'item_id' => 59,
		'description' => '<p>Лёгкое в обращении оружие, потому что на нем всего четыре струны!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>270</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+210</span>   СКО:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #59
		'item_id' => 60,
		'description' => '<p>Этим мечом ушатали самому Саворону, пока тот спал!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>290</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+240</span>   СКО:  <span class=\'green\'>+15</span></p>\r\n',
	),
	array( // row #60
		'item_id' => 61,
		'description' => '<p>Оружие введено в моду тем, кто носил на голове кухонную вытяжку!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>310</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+280</span>   СКО:  <span class=\'green\'>+15</span></p>\r\n',
	),
	array( // row #61
		'item_id' => 62,
		'description' => '<p>Никто не знает, кто он был, но меч у него классный!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>330</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+320</span>   СКО:  <span class=\'green\'>+15</span></p>',
	),
	array( // row #62
		'item_id' => 63,
		'description' => '<p>Найдено в руинах древней Овощной Империи!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>350</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+360</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #63
		'item_id' => 64,
		'description' => '<p>Модные очки, помогают пройти фейс-контроль</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+5</span>   ЖЗН:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #64
		'item_id' => 65,
		'description' => '<p>Такие очки помогли герою Роан Джоллинг победить Черного Властелина</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+10</span>   ЖЗН:  <span class=\'green\'>+75</span></p>\r\n',
	),
	array( // row #65
		'item_id' => 66,
		'description' => '<p>Очки родом с рынка, с китайскими линзами</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>110</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+15</span>   ЖЗН:  <span class=\'green\'>+100</span></p>\r\n',
	),
	array( // row #66
		'item_id' => 67,
		'description' => '<p>Очки, в которых Стальвестор Стуллоне был невыразимо крут</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>120</span>   ЛОВ:  <span class=\'red\'>110</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+125</span></p>\r\n',
	),
	array( // row #67
		'item_id' => 68,
		'description' => '<p>Мастерская маскировка</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>125</span>   ЛОВ:  <span class=\'red\'>115</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+150</span></p>\r\n',
	),
	array( // row #68
		'item_id' => 69,
		'description' => '<p>Недорогое кольцо, которое все же поднимает самооценку!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   ЗАЩ:  <span class=\'green\'>+10</span>   ЖЗН:  <span class=\'green\'>+50</span></p>\n',
	),
	array( // row #69
		'item_id' => 70,
		'description' => '<p>Помогают видеть мир не таким плоским</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>130</span>   ЛОВ:  <span class=\'red\'>115</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+175</span></p>\r\n',
	),
	array( // row #70
		'item_id' => 71,
		'description' => '<p>К чему слова? Просто классные очки. Хипстеры гарантируют это.</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>135</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>145</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #71
		'item_id' => 72,
		'description' => '<p>Кольцо дает невиданную крутизну! Однажды Саворон прошляпил целый контейнер таких!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>140</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>105</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+350</span>   ЗАЩ:  <span class=\'green\'>+80</span>   СКО:  <span class=\'green\'>+500</span><br>ЖЗН:  <span class=\'green\'>+500</span></p>\r\n',
	),
	array( // row #72
		'item_id' => 73,
		'description' => '<p>Его уже нет с нами, но очки хранят былую силу</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>140</span>   ЛОВ:  <span class=\'red\'>120</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+225</span></p>\r\n',
	),
	array( // row #73
		'item_id' => 74,
		'description' => '<p>Почувствуй себя солдатом будущего!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+26</span>   ЖЗН:  <span class=\'green\'>+250</span></p>\r\n',
	),
	array( // row #74
		'item_id' => 75,
		'description' => '<p>Поднимает боевой дух воспоминанием о славной победе!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+30</span>   ЗАЩ:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #75
		'item_id' => 76,
		'description' => '<p>Волшебным образом повышает удачу в бою!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЖЗН:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #76
		'item_id' => 77,
		'description' => '<p>Маска, которую нужно одевать очень быстро</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>105</span>   ЛОВ:  <span class=\'red\'>105</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+5</span>   ЗАЩ:  <span class=\'green\'>+5</span>   СКО:  <span class=\'green\'>+5</span><br>ЖЗН:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #77
		'item_id' => 78,
		'description' => '<p>В такой маске работают бензопилой</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>120</span>   ЛОВ:  <span class=\'red\'>120</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+10</span>   ЗАЩ:  <span class=\'green\'>+10</span>   СКО:  <span class=\'green\'>+10</span><br>ЖЗН:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #78
		'item_id' => 79,
		'description' => '<p>В такой ходят по лабораториям</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>140</span>   ЛОВ:  <span class=\'red\'>140</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+20</span>   СКО:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #79
		'item_id' => 80,
		'description' => '<p>Не позволяет забыть 5 ноября</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>170</span>   ЛОВ:  <span class=\'red\'>170</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+30</span>   ЗАЩ:  <span class=\'green\'>+30</span>   СКО:  <span class=\'green\'>+30</span><br>ЖЗН:  <span class=\'green\'>+28</span></p>\r\n',
	),
	array( // row #80
		'item_id' => 81,
		'description' => '<p>Только эрудит Грузь знает состав этого варева.<br>Восстанавливает <span class=\'green\'>5% здоровья</span> за счет веры!</p>',
	),
	array( // row #81
		'item_id' => 82,
		'description' => '<p>Восстанавливает <span class=\'green\'>15% здоровья</span> и лечит кашель!</p>',
	),
	array( // row #82
		'item_id' => 83,
		'description' => '<p>Эликсир с прекрасным вкусом.<br>Восстанавливает <span class=\'green\'>50% здоровья!</span></p>',
	),
	array( // row #83
		'item_id' => 84,
		'description' => '<p>Чудо фармакологии, <span class=\'green\'>полностью исцеляет</span> бойца!</p>',
	),
	array( // row #84
		'item_id' => 85,
		'description' => '<p>Стимулирует организм на подвиги! Вливает <span class=\'green\'>10 пунктов энергии!</span></p>',
	),
	array( // row #85
		'item_id' => 86,
		'description' => '<p>Хороший мотиватор подняться и идти дальше, восстанавливает <span class=\'green\'>30 пунктов энергии!</span></p>',
	),
	array( // row #86
		'item_id' => 87,
		'description' => '<p>Адский напиток, вяжет рот и восстанавливает <span class=\'green\'>60 пунктов энергии!</span></p>',
	),
	array( // row #87
		'item_id' => 88,
		'description' => '<p>Мобилизует скрытые резервы организма, восстанавливает <span class=\'green\'>150 пунктов энергии!</span></p>',
	),
	array( // row #88
		'item_id' => 89,
		'description' => '<p>Обжигает противника холодом! Не дает шанса отразить атаку.<br><span class=\'red\'>Отнимает 15% HP</span></p>',
	),
	array( // row #89
		'item_id' => 90,
		'description' => '<p>Опаляет противника огнем! Не дает шанса отразить атаку.<br><span class=\'red\'>Отнимает 22% HP</span></p>',
	),
	array( // row #90
		'item_id' => 91,
		'description' => '<p>Шарашит электричеством! Не дает шанса отразить атаку.<br><span class=\'red\'>Отнимает 30% HP</span></p>',
	),
	array( // row #91
		'item_id' => 95,
		'description' => '<p>В ящике 6 колбочек с микстурой. Экономия - 10%</p>',
	),
	array( // row #92
		'item_id' => 96,
		'description' => '<p>В ящике 6 пузырьков эликсира здоровья. Экономия - 10%</p>',
	),
	array( // row #93
		'item_id' => 97,
		'description' => '<p>В ящике 6 бутылок с панацеей. Экономия - 10%</p>',
	),
	array( // row #94
		'item_id' => 98,
		'description' => '<p>В ящике 6 баночек «Грин бага». Экономия - 10%</p>',
	),
	array( // row #95
		'item_id' => 99,
		'description' => '<p>В ящике 6 баночек «Витамин краша». Экономия - 10%</p>',
	),
	array( // row #96
		'item_id' => 100,
		'description' => '<p>В ящике 6 баночек «Тёрна». Экономия - 10%</p>',
	),
	array( // row #97
		'item_id' => 101,
		'description' => '<p>В ящике 6 бутылок «Свит лимонэйда». Экономия - 10%</p>',
	),
	array( // row #98
		'item_id' => 102,
		'description' => '<p>В ящике 6 новеньких ледяных гранат. Экономия - 10%</p>',
	),
	array( // row #99
		'item_id' => 103,
		'description' => '<p>В ящике 6 новеньких огненных гранат. Экономия - 10%</p>',
	),
	array( // row #100
		'item_id' => 104,
		'description' => '<p>В ящике 6 новеньких гранат Тесла. Экономия - 10%</p>',
	),
	array( // row #101
		'item_id' => 105,
		'description' => '<p>В ящике 18 колбочек с микстурой. Экономия - 20%</p>',
	),
	array( // row #102
		'item_id' => 106,
		'description' => '<p>В ящике 18 пузырьков эликсира здоровья. Экономия - 20%</p>',
	),
	array( // row #103
		'item_id' => 107,
		'description' => '<p>В ящике 18 бутылок с панацеей. Экономия - 20%</p>',
	),
	array( // row #104
		'item_id' => 108,
		'description' => '<p>В ящике 18 баночек «Грин бага». Экономия - 20%</p>',
	),
	array( // row #105
		'item_id' => 109,
		'description' => '<p>В ящике 18 баночек «Витамин краша». Экономия - 20%</p>',
	),
	array( // row #106
		'item_id' => 110,
		'description' => '<p>В ящике 18 баночек «Тёрна». Экономия - 20%</p>',
	),
	array( // row #107
		'item_id' => 111,
		'description' => '<p>В ящике 18 бутылок «Свит лимонэйда». Экономия - 20%</p>',
	),
	array( // row #108
		'item_id' => 112,
		'description' => '<p>В ящике 18 гранат. Экономия - 20%</p>',
	),
	array( // row #109
		'item_id' => 113,
		'description' => '<p>В ящике 18 гранат. <b>Экономия 20%</b></p>',
	),
	array( // row #110
		'item_id' => 114,
		'description' => '<p>В ящике 18 гранат. Экономия - 20%</p>',
	),
	array( // row #111
		'item_id' => 115,
		'description' => '<p>В ящике 36 пузырьков микстуры. Экономия - 30%</p>',
	),
	array( // row #112
		'item_id' => 116,
		'description' => '<p>В ящике 36 бутылок с эликсиром здоровья. Экономия - 30%</p>',
	),
	array( // row #113
		'item_id' => 117,
		'description' => '<p>В ящике 36 бутылок с панацеей. Экономия - 30%</p>',
	),
	array( // row #114
		'item_id' => 118,
		'description' => '<p>В ящике 36 баночек «Грин бага». Экономия - 30%</p>',
	),
	array( // row #115
		'item_id' => 119,
		'description' => '<p>в ящике 36 баночек «Витамин краша». Экономия - 30%</p>',
	),
	array( // row #116
		'item_id' => 120,
		'description' => '<p>В ящике 36 баночек «Тёрна». Экономия - 30%</p>',
	),
	array( // row #117
		'item_id' => 121,
		'description' => '<p>в ящике 36 бутылок «Свит лимонэйда». Экономия - 30%</p>',
	),
	array( // row #118
		'item_id' => 122,
		'description' => '<p>В ящике 36 гранат. Экономия - 30%</p>',
	),
	array( // row #119
		'item_id' => 123,
		'description' => '<p>В ящике 36 гранат. Экономия - 30%</p>',
	),
	array( // row #120
		'item_id' => 124,
		'description' => '<p>В ящике 36 мощнейших гранат. Экономия - 30%</p>',
	),
	array( // row #121
		'item_id' => 125,
		'description' => '<p>Можно уколоться!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+70</span>   ЗАЩ:  <span class=\'green\'>+10</span>   ЖЗН:  <span class=\'green\'>+100</span></p>\r\n',
	),
	array( // row #122
		'item_id' => 126,
		'description' => '<p>Красивое колечко, увеличивает запас сил!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+30</span>   СКО:  <span class=\'green\'>+50</span>   ЖЗН:  <span class=\'green\'>+300</span></p>',
	),
	array( // row #123
		'item_id' => 127,
		'description' => '<p>Кто знает что скрывает тьма этого кольца…</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>100</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+80</span>   ЗАЩ: &#160;<span class=\'green\'>+20</span>   СКО: &#160;<span class=\'green\'>+50</span><br>ЖЗН: &#160;<span class=\'green\'>+300</span></p>\n',
	),
	array( // row #124
		'item_id' => 128,
		'description' => '<p>Превращает ярость из сердца владельца, в неудержимую силу!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+200</span>   ЗАЩ:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+50</span><br>ЖЗН:  <span class=\'green\'>+300</span></p>\r\n',
	),
	array( // row #125
		'item_id' => 129,
		'description' => '<p>Кому-то могут помочь!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   ЗАЩ:  <span class=\'green\'>+30</span></p>\r\n',
	),
	array( // row #126
		'item_id' => 130,
		'description' => '<p>Лапа смертельного врага овощей!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+30</span>   ЖЗН:  <span class=\'green\'>+196</span></p>',
	),
	array( // row #127
		'item_id' => 131,
		'description' => '<p>Сегодня Вам повезет! (Примечание: не принимать внутрь!)</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+30</span>   ЗАЩ:  <span class=\'green\'>+30</span>   СКО:  <span class=\'green\'>+30</span><br>ЖЗН:  <span class=\'green\'>+100</span></p>\n',
	),
	array( // row #128
		'item_id' => 132,
		'description' => '<p>Предсказывает куда будет нанесен следующий удар!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+50</span><br>ЖЗН:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #129
		'item_id' => 133,
		'description' => '<p>Поднимает боевой дух!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+50</span>   ЖЗН:  <span class=\'green\'>+200</span></p>',
	),
	array( // row #130
		'item_id' => 134,
		'description' => '<p>Первый выпуск. Тема номера: как грамотно вломить?</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+70</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #131
		'item_id' => 135,
		'description' => '<p>Увеличивает силу!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+90</span>   ЖЗН:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #132
		'item_id' => 136,
		'description' => '<p>Ускоряет Ваши рефлексы!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>СКО:  <span class=\'green\'>+350</span>   ЖЗН:  <span class=\'green\'>+50</span></p>',
	),
	array( // row #133
		'item_id' => 137,
		'description' => '<p>Теперь Вам тяжело ушатать!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+90</span>   ЖЗН:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #134
		'item_id' => 138,
		'description' => '<p>Для поклонников ретро-футуризма!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>120</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+40</span>   СКО:  <span class=\'green\'>+20</span>   ЖЗН:  <span class=\'green\'>+300</span></p>',
	),
	array( // row #135
		'item_id' => 139,
		'description' => '<p>С ней Вы здоровы, как бык!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>ЖЗН:  <span class=\'green\'>+350</span></p>\r\n',
	),
	array( // row #136
		'item_id' => 140,
		'description' => '<p>Один за всех, и все за одного!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+200</span>   ЖЗН:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #137
		'item_id' => 141,
		'description' => '<p>Один за всех, и все за одного!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+200</span>   ЖЗН:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #138
		'item_id' => 142,
		'description' => '<p>Можно пугать друзей!</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>180</span>   ЛОВ: &#160;<span class=\'red\'>170</span>   ИНТ: &#160;<span class=\'red\'>120</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+40</span>   ЗАЩ: &#160;<span class=\'green\'>+40</span>   СКО: &#160;<span class=\'green\'>+40</span><br>ЖЗН: &#160;<span class=\'green\'>+40</span></p>\n',
	),
	array( // row #139
		'item_id' => 143,
		'description' => '<p>Вводит противника в заблуждение, относительно вашего IQ</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>190</span>   ЛОВ: &#160;<span class=\'red\'>170</span>   ИНТ: &#160;<span class=\'red\'>120</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+50</span>   ЗАЩ: &#160;<span class=\'green\'>+50</span>   СКО: &#160;<span class=\'green\'>+50</span><br>ЖЗН: &#160;<span class=\'green\'>+50</span></p>\n',
	),
	array( // row #140
		'item_id' => 144,
		'description' => '<p>Из коры хищного медведерева!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>265</span>   ЛОВ:  <span class=\'red\'>155</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+230</span>   ЖЗН:  <span class=\'green\'>+280</span></p>\r\n',
	),
	array( // row #141
		'item_id' => 145,
		'description' => '<p>Позволяет выглядеть брутально, желательно носить с длинными черными волосами</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>190</span>   ЛОВ:  <span class=\'red\'>180</span>   ИНТ:  <span class=\'red\'>120</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+60</span>   ЗАЩ:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+60</span><br>ЖЗН:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #142
		'item_id' => 146,
		'description' => '<p>Холера, настало твое время!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>190</span>   ЛОВ:  <span class=\'red\'>190</span>   ИНТ:  <span class=\'red\'>120</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+70</span>   ЗАЩ:  <span class=\'green\'>+50</span>   СКО:  <span class=\'green\'>+70</span><br>ЖЗН:  <span class=\'green\'>+70</span></p>\r\n',
	),
	array( // row #143
		'item_id' => 147,
		'description' => '<p>Вызывает у врага приступ эпилепсии!</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>200</span>   ЛОВ: &#160;<span class=\'red\'>200</span>   ИНТ: &#160;<span class=\'red\'>150</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+100</span>   ЗАЩ: &#160;<span class=\'green\'>+50</span>   СКО: &#160;<span class=\'green\'>+120</span><br>ЖЗН: &#160;<span class=\'green\'>+140</span></p>\n',
	),
	array( // row #144
		'item_id' => 148,
		'description' => '<p>Ни один зверь не уйдёт!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>265</span>   ЛОВ:  <span class=\'red\'>155</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+230</span>   ЖЗН:  <span class=\'green\'>+280</span></p>\r\n',
	),
	array( // row #145
		'item_id' => 149,
		'description' => '<p>Вас могут принять за робота!</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>200</span>   ЛОВ: &#160;<span class=\'red\'>200</span>   ИНТ: &#160;<span class=\'red\'>170</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+110</span>   ЗАЩ: &#160;<span class=\'green\'>+60</span>   СКО: &#160;<span class=\'green\'>+120</span><br>ЖЗН: &#160;<span class=\'green\'>+160</span></p>\n',
	),
	array( // row #146
		'item_id' => 150,
		'description' => '<p>Доктор уже выехал!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>280</span>   ЛОВ:  <span class=\'red\'>160</span>   ИНТ:  <span class=\'red\'>99</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+260</span>   ЖЗН:  <span class=\'green\'>+300</span></p>\r\n',
	),
	array( // row #147
		'item_id' => 151,
		'description' => '<p>В цехе сборки жгли резину!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>295</span>   ЛОВ:  <span class=\'red\'>165</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   ЗАЩ:  <span class=\'green\'>+290</span>   ЖЗН:  <span class=\'green\'>+320</span></p>\r\n',
	),
	array( // row #148
		'item_id' => 152,
		'description' => '<p>Можно собирать рис и отрабатывать удары!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>310</span>   ЛОВ:  <span class=\'red\'>170</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+330</span>   ЖЗН:  <span class=\'green\'>+350</span></p>\r\n',
	),
	array( // row #149
		'item_id' => 153,
		'description' => '<p>Прочная и пластичная, из лунного серебра!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>325</span>   ЛОВ:  <span class=\'red\'>175</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+60</span>   ЗАЩ:  <span class=\'green\'>+360</span>   ЖЗН:  <span class=\'green\'>+370</span></p>\r\n',
	),
	array( // row #150
		'item_id' => 154,
		'description' => '<p>Вулканизированная резина, все что нужно для победы!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>340</span>   ЛОВ:  <span class=\'red\'>180</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+60</span>   ЗАЩ:  <span class=\'green\'>+390</span>   ЖЗН:  <span class=\'green\'>+390</span></p>\r\n',
	),
	array( // row #151
		'item_id' => 155,
		'description' => '<p>Крутой многослойный доспех!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>360</span>   ЛОВ:  <span class=\'red\'>190</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+60</span>   ЗАЩ:  <span class=\'green\'>+420</span>   ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #152
		'item_id' => 156,
		'description' => '<p>Один за всех, и все за одного!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>200</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+130</span>   ЗАЩ:  <span class=\'green\'>+10</span></p>\r\n',
	),
	array( // row #153
		'item_id' => 157,
		'description' => '<p>Один за всех, и все за одного!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>200</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+130</span>   СКО:  <span class=\'green\'>+80</span></p>\r\n',
	),
	array( // row #154
		'item_id' => 158,
		'description' => '<p>Позволяет быстро двигаться и сильно бить</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>250</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+140</span>   СКО:  <span class=\'green\'>+100</span></p>\r\n',
	),
	array( // row #155
		'item_id' => 159,
		'description' => '<p>Позволяет быть выносливым и сильным</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>250</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+140</span>   ЖЗН:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #156
		'item_id' => 160,
		'description' => '<p>Хорошо держит удар!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>160</span>   ЛОВ:  <span class=\'red\'>135</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+80</span></p>\r\n',
	),
	array( // row #157
		'item_id' => 161,
		'description' => '<p>Внушает страх в сердца врагов!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>170</span>   ЛОВ:  <span class=\'red\'>140</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+90</span></p>\r\n',
	),
	array( // row #158
		'item_id' => 162,
		'description' => '<p>В корзину можно класть рис или наносить ею удары</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>250</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+160</span></p>\r\n',
	),
	array( // row #159
		'item_id' => 163,
		'description' => '<p>Тысяча чертей!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>180</span>   ЛОВ:  <span class=\'red\'>145</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+100</span></p>\r\n',
	),
	array( // row #160
		'item_id' => 164,
		'description' => '<p>Защищает лекаря от напастей</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>250</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+170</span></p>\r\n',
	),
	array( // row #161
		'item_id' => 165,
		'description' => '<p>Каналья!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>190</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+110</span></p>\r\n',
	),
	array( // row #162
		'item_id' => 166,
		'description' => '<p>Защищает от палящего солнца!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>155</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+120</span></p>\r\n',
	),
	array( // row #163
		'item_id' => 167,
		'description' => '<p>Выдает в своем владельце, победителя медведерева, или состоятельного парня!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>210</span>   ЛОВ:  <span class=\'red\'>160</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+130</span></p>\r\n',
	),
	array( // row #164
		'item_id' => 168,
		'description' => '<p>Обладателя этого шлема видно издалека!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>220</span>   ЛОВ:  <span class=\'red\'>165</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+140</span></p>\r\n',
	),
	array( // row #165
		'item_id' => 169,
		'description' => '<p>Внушает надежду в сердце больного</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>230</span>   ЛОВ:  <span class=\'red\'>170</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+150</span></p>\r\n',
	),
	array( // row #166
		'item_id' => 170,
		'description' => '<p>Столетний дуб не устоит!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+160</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #167
		'item_id' => 171,
		'description' => '<p>Защищайтесь, кисуля! (sic)</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+170</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #168
		'item_id' => 172,
		'description' => '<p>Можно не только колоть, но и рубить!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>205</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+180</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #169
		'item_id' => 173,
		'description' => '<p>Позволяет донести истину до окружающих!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>205</span>   ЛОВ:  <span class=\'red\'>150</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+190</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #170
		'item_id' => 174,
		'description' => '<p>Копье с тремя лезвиями</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>240</span>   ЛОВ:  <span class=\'red\'>170</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+280</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #171
		'item_id' => 175,
		'description' => '<p>Копье от модного кузнеца-огурца по фамилии Безье</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>180</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+320</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #172
		'item_id' => 176,
		'description' => '<p>Копье экстра-класса, с отметкой казначейства</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>260</span>   ЛОВ:  <span class=\'red\'>190</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+360</span>   СКО:  <span class=\'green\'>+70</span></p>\r\n',
	),
	array( // row #173
		'item_id' => 177,
		'description' => '<p>Отличная секира, разрубает всё!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>380</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+400</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #174
		'item_id' => 178,
		'description' => '<p>Раны после этого меча заживают очень долго</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>440</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+440</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #175
		'item_id' => 179,
		'description' => '<p>Чудо кузнечного дела! </p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>487</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+487</span>   СКО:  <span class=\'green\'>+20</span></p>\r\n',
	),
	array( // row #176
		'item_id' => 180,
		'description' => '<p>Меч из другого мира!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1200</span>   ЛОВ:  <span class=\'red\'>300</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1126</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #177
		'item_id' => 181,
		'description' => '<p>В помощь угнетенным!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>2200</span>   ЛОВ:  <span class=\'red\'>700</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+3887</span>   СКО:  <span class=\'green\'>+999</span></p>',
	),
	array( // row #178
		'item_id' => 182,
		'description' => '<p></p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>700</span>   ЛОВ:  <span class=\'red\'>600</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+800</span>   СКО:  <span class=\'green\'>+120</span></p>\r\n',
	),
	array( // row #179
		'item_id' => 183,
		'description' => '<p>Кастет оставляет очень глубокие раны </p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>230</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+140</span>   СКО:  <span class=\'green\'>+150</span></p>\r\n',
	),
	array( // row #180
		'item_id' => 184,
		'description' => '<p>Пробуждает жажду убийства!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>650</span>   ЛОВ:  <span class=\'red\'>650</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+600</span>   СКО:  <span class=\'green\'>+90</span></p>\r\n',
	),
	array( // row #181
		'item_id' => 185,
		'description' => '<p>Незаменимый инструмент для игры в "обнимашки"</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>150</span>   ЛОВ:  <span class=\'red\'>290</span>   ИНТ:  <span class=\'red\'>0</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+195</span>   СКО:  <span class=\'green\'>+250</span></p>\r\n',
	),
	array( // row #182
		'item_id' => 186,
		'description' => '<p>Для серьезных парней </p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>170</span>   ЛОВ:  <span class=\'red\'>360</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+260</span>   СКО:  <span class=\'green\'>+350</span></p>\r\n',
	),
	array( // row #183
		'item_id' => 187,
		'description' => '<p>Можно порезаться, держать подальше от детей </p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>190</span>   ЛОВ:  <span class=\'red\'>440</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+360</span>   СКО:  <span class=\'green\'>+450</span></p>\r\n',
	),
	array( // row #184
		'item_id' => 188,
		'description' => '<p>Из панциря Жука-Гаргантюа </p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>210</span>   ЛОВ:  <span class=\'red\'>530</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+460</span>   СКО:  <span class=\'green\'>+600</span></p>\r\n',
	),
	array( // row #185
		'item_id' => 189,
		'description' => '<p>Острые и смертоносные </p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>240</span>   ЛОВ:  <span class=\'red\'>630</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+560</span>   СКО:  <span class=\'green\'>+777</span></p>\r\n',
	),
	array( // row #186
		'item_id' => 190,
		'description' => '<p>Собраны из деталей для бомбы из лагеря жуков</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>280</span>   ЛОВ:  <span class=\'red\'>750</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+777</span>   СКО:  <span class=\'green\'>+999</span></p>\r\n',
	),
	array( // row #187
		'item_id' => 191,
		'description' => '<p>Ослепи врага крутизной!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>400</span>   ЛОВ:  <span class=\'red\'>200</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+70</span>   ЗАЩ:  <span class=\'green\'>+450</span>   ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #188
		'item_id' => 192,
		'description' => '<p>Костюм обнаружен на замороженном, но патриотичном овоще</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>450</span>   ЛОВ:  <span class=\'red\'>210</span>   ИНТ:  <span class=\'red\'>0</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+70</span>   ЗАЩ:  <span class=\'green\'>+460</span>   ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #189
		'item_id' => 193,
		'description' => '<p>Любите готовить на пару? Этот доспех для вас.</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>510</span>   ЛОВ:  <span class=\'red\'>220</span>   ИНТ:  <span class=\'red\'>0</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+80</span>   ЗАЩ:  <span class=\'green\'>+500</span>   ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #190
		'item_id' => 194,
		'description' => '<p>Меч никогда не тупится</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1300</span>   ЛОВ:  <span class=\'red\'>350</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1350</span>   СКО:  <span class=\'green\'>+100</span></p>\r\n',
	),
	array( // row #191
		'item_id' => 195,
		'description' => '<p>Собран из деталей для бомбы из лагеря жуков</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>580</span>   ЛОВ:  <span class=\'red\'>229</span>   ИНТ:  <span class=\'red\'>0</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+80</span>   ЗАЩ:  <span class=\'green\'>+540</span>   ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #192
		'item_id' => 196,
		'description' => '<p>Воин, надевший это кимоно получает великую силу, но должен понести и проклятье</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>670</span>   ЛОВ:  <span class=\'red\'>240</span>   ИНТ:  <span class=\'red\'>0</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+90</span>   ЗАЩ:  <span class=\'green\'>+587</span>   ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #193
		'item_id' => 197,
		'description' => '<p>Всегда голоден</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1500</span>   ЛОВ:  <span class=\'red\'>350</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1600</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #194
		'item_id' => 198,
		'description' => '<p>Кошмарное порождение тьмы</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1700</span>   ЛОВ:  <span class=\'red\'>375</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+2000</span>   СКО:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #195
		'item_id' => 199,
		'description' => '<p>Шарашит молнией, лечит, пробивает силовой щит, камера 1.3Mpx</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>220</span>   ЛОВ:  <span class=\'red\'>160</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+230</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #196
		'item_id' => 200,
		'description' => '<p>Отражает любые атаки (почти)</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+50</span></p>\r\n',
	),
	array( // row #197
		'item_id' => 201,
		'description' => '<p>Пригодится, когда надвигается буря</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>170</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+270</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #198
		'item_id' => 202,
		'description' => '<p>Для опасных парней</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>280</span>   ЛОВ:  <span class=\'red\'>180</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+340</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #199
		'item_id' => 203,
		'description' => '<p>Прочный и стильный щит</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>300</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #200
		'item_id' => 204,
		'description' => '<p>Воплощенное проклятье</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>310</span>   ЛОВ:  <span class=\'red\'>190</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+400</span>   СКО:  <span class=\'green\'>+60</span></p>\r\n',
	),
	array( // row #201
		'item_id' => 205,
		'description' => '<p>От молнии не спасает</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>300</span>   ЛОВ:  <span class=\'red\'>300</span>   ИНТ:  <span class=\'red\'>150</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+180</span></p>\r\n',
	),
	array( // row #202
		'item_id' => 206,
		'description' => '<p>Проклятье на вашу голову</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>320</span>   ЛОВ:  <span class=\'red\'>300</span>   ИНТ:  <span class=\'red\'>150</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+200</span></p>\r\n',
	),
	array( // row #203
		'item_id' => 207,
		'description' => '<p>Ночью можно читать с его помощью!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>250</span>   ЛОВ:  <span class=\'red\'>180</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+190</span></p>\r\n',
	),
	array( // row #204
		'item_id' => 208,
		'description' => '<p>Можно использовать как самовар</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>300</span>   ЛОВ:  <span class=\'red\'>200</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+190</span></p>\r\n',
	),
	array( // row #205
		'item_id' => 209,
		'description' => '<p>Вечеринка перемещается к вам</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>350</span>   ЛОВ:  <span class=\'red\'>320</span>   ИНТ:  <span class=\'red\'>150</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+230</span></p>\r\n',
	),
	array( // row #206
		'item_id' => 210,
		'description' => '<p>Вызывает приступ патриотизма</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>350</span>   ЛОВ:  <span class=\'red\'>220</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+210</span></p>\r\n',
	),
	array( // row #207
		'item_id' => 211,
		'description' => '<p>Добавляет много лошадиных сил</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>380</span>   ЛОВ:  <span class=\'red\'>350</span>   ИНТ:  <span class=\'red\'>150</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+265</span></p>\r\n',
	),
	array( // row #208
		'item_id' => 212,
		'description' => '<p>Для огородных патриотов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>200</span>   ЛОВ:  <span class=\'red\'>200</span>   ИНТ:  <span class=\'red\'>120</span></p><br><p class=\'header\'>Бонусы:</p><p>ЗАЩ:  <span class=\'green\'>+60</span>   СКО:  <span class=\'green\'>+30</span><br>ЖЗН:  <span class=\'green\'>+350</span></p>\r\n',
	),
	array( // row #209
		'item_id' => 213,
		'description' => '<p>Получи силу бога смерти!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>300</span>   ЛОВ:  <span class=\'red\'>240</span>   ИНТ:  <span class=\'red\'>170</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+150</span>   ЗАЩ:  <span class=\'green\'>+70</span>   СКО:  <span class=\'green\'>+150</span><br>ЖЗН:  <span class=\'green\'>+180</span></p>\r\n',
	),
	array( // row #210
		'item_id' => 214,
		'description' => '<p>Собрана… ну, вы знаете из каких запчастей</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>340</span>   ЛОВ: &#160;<span class=\'red\'>270</span>   ИНТ: &#160;<span class=\'red\'>170</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+190</span>   ЗАЩ: &#160;<span class=\'green\'>+80</span>   СКО: &#160;<span class=\'green\'>+150</span><br>ЖЗН: &#160;<span class=\'green\'>+180</span></p>\n',
	),
	array( // row #211
		'item_id' => 215,
		'description' => '<p>Позволяют почувствовать себя крабом на галерах</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>300</span>   ЛОВ:  <span class=\'red\'>850</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+888</span>   СКО:  <span class=\'green\'>+999</span></p>\r\n',
	),
	array( // row #212
		'item_id' => 216,
		'description' => '<p>Чудо инженерной мысли. Стаханов рекомендует!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1800</span>   ЛОВ:  <span class=\'red\'>400</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+2400</span>   ЗАЩ:  <span class=\'green\'>+0</span>   СКО:  <span class=\'green\'>+50</span><br>ЖЗН:  <span class=\'green\'>+0</span></p>\r\n',
	),
	array( // row #213
		'item_id' => 217,
		'description' => '<p>Изменение размеров владельца, не способно порвать эти штаны!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>800</span>   ЛОВ:  <span class=\'red\'>250</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+100</span>   ЗАЩ:  <span class=\'green\'>+650</span>  <br>ЖЗН:  <span class=\'green\'>+420</span></p>\r\n',
	),
	array( // row #214
		'item_id' => 218,
		'description' => '<p>Один удар -- три пореза!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>950</span>   ЛОВ:  <span class=\'red\'>800</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1000</span>  СКО:  <span class=\'green\'>+250</span><br>   \r\n',
	),
	array( // row #215
		'item_id' => 219,
		'description' => '<p>Можно сильно ущипнуть врага</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>450</span>   ЛОВ:  <span class=\'red\'>1050</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1100</span> СКО:  <span class=\'green\'>+999</span><br>\r\n',
	),
	array( // row #216
		'item_id' => 220,
		'description' => '<p>Резко повышают самооценку и веру в свои силы!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1500</span>   ЛОВ:  <span class=\'red\'>1000</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+487</span>   ЗАЩ:  <span class=\'green\'>+999</span>   СКО:  <span class=\'green\'>+100</span><br>ЖЗН:  <span class=\'green\'>+999</span></p>\r\n',
	),
	array( // row #217
		'item_id' => 221,
		'description' => '<p>Смотрит на всех, как солдат на вошь.</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>0</span>   ЛОВ:  <span class=\'red\'>0</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+150</span>   ЗАЩ:  <span class=\'green\'>+50</span>   <br>ЖЗН:  <span class=\'green\'>+500</span></p>\r\n',
	),
	array( // row #218
		'item_id' => 222,
		'description' => '<p>С ним вы дольше считаете себя живым!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>0</span>   ЛОВ:  <span class=\'red\'>0</span>   ИНТ:  <span class=\'red\'>200</span></p><br><p class=\'header\'>Бонусы:</p><p>ЖЗН:  <span class=\'green\'>+3500</span></p>\r\n',
	),
	array( // row #219
		'item_id' => 223,
		'description' => '<p>Можно устроить свой маленький армагеддон</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1950</span>   ЛОВ:  <span class=\'red\'>400</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+2666</span>  СКО:  <span class=\'green\'>+400</span></p>\r\n',
	),
	array( // row #220
		'item_id' => 224,
		'description' => '<p>Наконечник смазан смертельно-опасным ядом!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1150</span>   ЛОВ:  <span class=\'red\'>950</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1250</span> СКО:  <span class=\'green\'>+400</span></p>\r\n',
	),
	array( // row #221
		'item_id' => 225,
		'description' => '<p>Традиционный меч вишневых островов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>450</span>   ЛОВ:  <span class=\'red\'>450</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+500</span>  СКО:  <span class=\'green\'>+210</span></p>\r\n',
	),
	array( // row #222
		'item_id' => 226,
		'description' => '<p>Йо-хо-хо! Якорь мне в глотку!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>400</span>   ЛОВ:  <span class=\'red\'>300</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p>ЗАЩ:  <span class=\'green\'>+230</span> </p>',
	),
	array( // row #223
		'item_id' => 227,
		'description' => '<p>Шлем воина вишневых островов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>450</span>   ЛОВ:  <span class=\'red\'>350</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p>ЗАЩ: <span class=\'green\'>+260</span></p>\r\n',
	),
	array( // row #224
		'item_id' => 228,
		'description' => '<p>Кусок стены, найденный в древних руинах</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1000</span>   ЛОВ:  <span class=\'red\'>200</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+50</span>   ЗАЩ:  <span class=\'green\'>+120</span></p>\r\n',
	),
	array( // row #225
		'item_id' => 229,
		'description' => '<p>Вы неутомимы, как буйвол. Восстановление <span class=\'green\'>300 ед. энергии</span></p>',
	),
	array( // row #226
		'item_id' => 230,
		'description' => '<p>Каждая клетка Вашего овощного тела переполняется энергией. Восстановление <span class=\'green\'>500 пунктов энергии!</span></p>',
	),
	array( // row #227
		'item_id' => 231,
		'description' => '<p>Заводит не по-детски. Восстанавливает <span class=\'green\'>800 ед. энергии</span></p>',
	),
	array( // row #228
		'item_id' => 232,
		'description' => '<p>Традиционные доспехи жителей вишневых островов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1100</span>   ЛОВ:  <span class=\'red\'>600</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+120</span>   ЗАЩ:  <span class=\'green\'>+800</span> <br>  ЖЗН:  <span class=\'green\'>+600</span></p>\r\n',
	),
	array( // row #229
		'item_id' => 233,
		'description' => '<p>Одинаково хорошо пилит дрова и врагов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>2000</span>   ЛОВ:  <span class=\'red\'>900</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+3887</span> СКО:  <span class=\'green\'>+1200</span><br>',
	),
	array( // row #230
		'item_id' => 234,
		'description' => '<p>За ним удобно прятаться от вандалов</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1200</span>   ЛОВ:  <span class=\'red\'>300</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>  ЗАЩ:  <span class=\'green\'>+220</span> </p>',
	),
	array( // row #231
		'item_id' => 235,
		'description' => '<p>Оно жаждет томатного сока…</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1700</span>   ЛОВ:  <span class=\'red\'>1100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+3887</span> СКО:  <span class=\'green\'>+1200</span></p>\r\n',
	),
	array( // row #232
		'item_id' => 236,
		'description' => '<p>Простой и надежный</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>1600</span>   ЛОВ:  <span class=\'red\'>1200</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>  ЗАЩ:  <span class=\'green\'>+1234</span>   <br>ЖЗН:  <span class=\'green\'>+1111</span></p>\r\n',
	),
	array( // row #233
		'item_id' => 237,
		'description' => '<p>6 банок. Восстанавливается 300 ед. энергии. Экономия - 10%</p>\r\n',
	),
	array( // row #234
		'item_id' => 238,
		'description' => '<p>18 банок. Восстанавливается 300 ед. энергии. Экономия - 20%!</p>',
	),
	array( // row #235
		'item_id' => 239,
		'description' => '<p>36 банок. Восстанавливается 300 ед. энергии. Экономия - 30%!</p>',
	),
	array( // row #236
		'item_id' => 240,
		'description' => '<p>Незаменимый инструмент администрации</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>100</span>   ЛОВ:  <span class=\'red\'>100</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+99999</span>   СКО:  <span class=\'green\'>+9999</span><br>ЖЗН:  <span class=\'green\'>+10000</span></p>\r\n',
	),
	array( // row #237
		'item_id' => 241,
		'description' => '<p>Никого не интересовало, кто я такой, пока я не одел маску…</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>380</span>   ЛОВ:  <span class=\'red\'>300</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+240</span>   ЗАЩ:  <span class=\'green\'>+90</span>   СКО:  <span class=\'green\'>+200</span><br>ЖЗН:  <span class=\'green\'>+180</span></p>\r\n',
	),
	array( // row #238
		'item_id' => 242,
		'description' => '<p>Враг не поймет, что его поразило</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>550</span>   ЛОВ:  <span class=\'red\'>1500</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+1600</span>     СКО:  <span class=\'green\'>+700</span>\r\n',
	),
	array( // row #239
		'item_id' => 243,
		'description' => '<p>Производят неизгладимое впечатление</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>410</span>   ЛОВ: &#160;<span class=\'red\'>380</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+300</span>   ЗАЩ: &#160;<span class=\'green\'>+10</span>   СКО: &#160;<span class=\'green\'>+100</span><br>ЖЗН: &#160;<span class=\'green\'>+10</span></p>\n',
	),
	array( // row #240
		'item_id' => 244,
		'description' => '<p>6 банок. Восстанавливается 500 ед. энергии. Экономия - 10%!</p>',
	),
	array( // row #241
		'item_id' => 245,
		'description' => '<p>18 банок. Восстанавливается 500 ед. энергии. Экономия - 20%!</p>',
	),
	array( // row #242
		'item_id' => 246,
		'description' => '<p>36 банок. Восстанавливается 500 ед. энергии. Экономия - 30%!</p>',
	),
	array( // row #243
		'item_id' => 247,
		'description' => '<p>Устройство для превращения в мачо</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>470</span>   ЛОВ: &#160;<span class=\'red\'>360</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+10</span>   ЗАЩ: &#160;<span class=\'green\'>+280</span>   СКО: &#160;<span class=\'green\'>+10</span><br>ЖЗН: &#160;<span class=\'green\'>+10</span></p>\n',
	),
	array( // row #244
		'item_id' => 248,
		'description' => '<p>Позволяет стать еще брутальнее</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>440</span>   ЛОВ:  <span class=\'red\'>330</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+280</span>   ЗАЩ:  <span class=\'green\'>+110</span>   СКО:  <span class=\'green\'>+150</span><br>ЖЗН:  <span class=\'green\'>+180</span></p>\r\n',
	),
	array( // row #245
		'item_id' => 249,
		'description' => '<p>Высокотехнологичное устройство...</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>440</span>   ЛОВ: &#160;<span class=\'red\'>410</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+330</span>   ЗАЩ: &#160;<span class=\'green\'>+5</span>   СКО: &#160;<span class=\'green\'>+130</span><br>ЖЗН: &#160;<span class=\'green\'>+50</span></p>\n',
	),
	array( // row #246
		'item_id' => 250,
		'description' => '<p>Внушают неописуемый ужас!</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>600</span>   ЛОВ: &#160;<span class=\'red\'>2000</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+3887</span>   ЗАЩ: &#160;<span class=\'green\'>+10</span>   СКО: &#160;<span class=\'green\'>+3333</span><br>ЖЗН: &#160;<span class=\'green\'>+200</span></p>\n',
	),
	array( // row #247
		'item_id' => 251,
		'description' => '<p>Меч красный от томатного сока...</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>550</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+560</span>   ЗАЩ: &#160;<span class=\'green\'>+5</span>   СКО: &#160;<span class=\'green\'>+5</span><br>ЖЗН: &#160;<span class=\'green\'>+5</span></p>\n',
	),
	array( // row #248
		'item_id' => 252,
		'description' => '<p>Вселяет в Вас мрачное спокойствие…</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>100</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+200</span>   ЗАЩ: &#160;<span class=\'green\'>+80</span>   СКО: &#160;<span class=\'green\'>+300</span><br>ЖЗН: &#160;<span class=\'green\'>+400</span></p>\n',
	),
	array( // row #249
		'item_id' => 253,
		'description' => '<p>Друзья-шахтеры признают вас за своего</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>340</span>   ЛОВ:  <span class=\'red\'>210</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+430</span>   ЗАЩ:  <span class=\'green\'>+2</span>   СКО:  <span class=\'green\'>+100</span><br>ЖЗН:  <span class=\'green\'>+3</span></p>\r\n',
	),
	array( // row #250
		'item_id' => 254,
		'description' => '<p>Нарушители порядка будут наказаны!</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>390</span>   ЛОВ:  <span class=\'red\'>240</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+470</span>   ЗАЩ:  <span class=\'green\'>+2</span>   СКО:  <span class=\'green\'>+70</span><br>ЖЗН:  <span class=\'green\'>+5</span></p>\r\n',
	),
	array( // row #251
		'item_id' => 255,
		'description' => '<p>Маска, найденная в водостоке</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>480</span>   ЛОВ:  <span class=\'red\'>340</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+290</span>   ЗАЩ:  <span class=\'green\'>+120</span>   СКО:  <span class=\'green\'>+150</span><br>ЖЗН:  <span class=\'green\'>+300</span></p>\r\n',
	),
	array( // row #252
		'item_id' => 256,
		'description' => '<p>Мечта всех завоевателей</p><br><p class=\'header\'>Требования:</p><p>СИЛ:  <span class=\'red\'>490</span>   ЛОВ:  <span class=\'red\'>370</span>   ИНТ:  <span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК:  <span class=\'green\'>+3</span>   ЗАЩ:  <span class=\'green\'>+300</span>   СКО:  <span class=\'green\'>+3</span><br>ЖЗН:  <span class=\'green\'>+3</span></p>\r\n',
	),
	array( // row #253
		'item_id' => 257,
		'description' => '<p>Редкий лоб выдержит удар этого молота </p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>600</span>   ЛОВ: &#160;<span class=\'red\'>130</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+610</span>   ЗАЩ: &#160;<span class=\'green\'>+10</span>   СКО: &#160;<span class=\'green\'>+10</span><br>ЖЗН: &#160;<span class=\'green\'>+10</span></p>\n',
	),
	array( // row #254
		'item_id' => 258,
		'description' => '<p>Полезная вещь в хозяйстве</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>300</span>   ЛОВ: &#160;<span class=\'red\'>220</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+450</span>   ЗАЩ: &#160;<span class=\'green\'>+5</span>   СКО: &#160;<span class=\'green\'>+120</span><br>ЖЗН: &#160;<span class=\'green\'>+5</span></p>\n',
	),
	array( // row #255
		'item_id' => 259,
		'description' => '<p>Обладатель сего артефакта -- абсолютный победитель Комиксиады ТК. Ура, товарищи!</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>100</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+800</span>   ЗАЩ: &#160;<span class=\'green\'>+500</span>   СКО: &#160;<span class=\'green\'>+1500</span><br>ЖЗН: &#160;<span class=\'green\'>+3000</span></p>\n',
	),
	array( // row #256
		'item_id' => 260,
		'description' => '<p>Обладатель сего артефакта -- занял первое место в голосовании Комиксиады ТК</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>100</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+500</span>   ЗАЩ: &#160;<span class=\'green\'>+400</span>   СКО: &#160;<span class=\'green\'>+1000</span><br>ЖЗН: &#160;<span class=\'green\'>+2000</span></p>\n',
	),
	array( // row #257
		'item_id' => 261,
		'description' => '<p>Обладатель сего артефакта -- занял второе место в голосовании Комиксиады ТК</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>100</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+300</span>   ЗАЩ: &#160;<span class=\'green\'>+300</span>   СКО: &#160;<span class=\'green\'>+1000</span><br>ЖЗН: &#160;<span class=\'green\'>+1000</span></p>\n',
	),
	array( // row #258
		'item_id' => 262,
		'description' => '<p>Обладатель сего артефакта -- занял третье место в голосовании Комиксиады ТК</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>100</span>   ЛОВ: &#160;<span class=\'red\'>100</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+200</span>   ЗАЩ: &#160;<span class=\'green\'>+200</span>   СКО: &#160;<span class=\'green\'>+900</span><br>ЖЗН: &#160;<span class=\'green\'>+900</span></p>\n',
	),
	array( // row #259
		'item_id' => 263,
		'description' => '<p>Пришло время пилить камни</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>2000</span>   ЛОВ: &#160;<span class=\'red\'>200</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+3000</span>   ЗАЩ: &#160;<span class=\'green\'>+10</span>   СКО: &#160;<span class=\'green\'>+500</span><br>ЖЗН: &#160;<span class=\'green\'>+10</span></p>\n',
	),
	array( // row #260
		'item_id' => 264,
		'description' => '<p>Кажется, пора убегать!</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>410</span>   ЛОВ: &#160;<span class=\'red\'>410</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+550</span>   ЗАЩ: &#160;<span class=\'green\'>+10</span>   СКО: &#160;<span class=\'green\'>+300</span><br>ЖЗН: &#160;<span class=\'green\'>+10</span></p>\n',
	),
	array( // row #261
		'item_id' => 265,
		'description' => '<p>Маска для игры в покер</p><br><p class=\'header\'>Требования:</p><p>СИЛ: &#160;<span class=\'red\'>520</span>   ЛОВ: &#160;<span class=\'red\'>350</span>   ИНТ: &#160;<span class=\'red\'>100</span></p><br><p class=\'header\'>Бонусы:</p><p>АТК: &#160;<span class=\'green\'>+300</span>   ЗАЩ: &#160;<span class=\'green\'>+200</span>   СКО: &#160;<span class=\'green\'>+200</span><br>ЖЗН: &#160;<span class=\'green\'>+300</span></p>\n',
	),
);
