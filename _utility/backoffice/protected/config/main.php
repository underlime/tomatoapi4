<?php
$common = require('_common.inc.php');
return array(
    'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name' => 'Tomato API',
    'language' => 'ru',
    'preload' => array('log'),
    'import' => $common['import'],
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'password',
            'ipFilters' => array('127.0.0.1', '::1', '192.168.*.*'),
            'generatorPaths' => array(
                'application.gii.generators',
            ),
        ),
    ),
    'components' => array_merge(
        array(
             'urlManager' => array(
                 'class' => 'application.components.UrlManager',
                 'urlFormat' => 'get',
             ),
             'user' => array(
                 'allowAutoLogin' => true,
             ),
             'errorHandler' => array(
                 'errorAction' => 'site/error',
             ),
             'log' => array(
                 'class' => 'CLogRouter',
                 'routes' => array(
                     array(
                         'class' => 'CFileLogRoute',
                         'levels' => 'error, warning',
                     ),
                 ),
             ),
        ),
        $common['components']
    ),
    'params' => array(),
);
