<?php
$common = require('_common.inc.php');
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Tomato API',
    'import' => $common['import'],
    'components' => $common['components'],
);
