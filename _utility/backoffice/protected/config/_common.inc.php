<?php
define('ROOT_PATH', realpath(__DIR__.'/../../../..'));
define('BACKOFFICE_PATH', realpath(__DIR__.'/../..'));
define('DOCUMENT_ROOT', isset($_SERVER['DOCUMENT_ROOT']) ? $_SERVER['DOCUMENT_ROOT'] : ROOT_PATH);
define('BASE_URL', str_replace(array('\\', DOCUMENT_ROOT), array('/', ''), BACKOFFICE_PATH));
Yii::setPathOfAlias('root', ROOT_PATH);

$dbUser = 'root';
$dbName = 'tomato_kombat';
if (isset($_SERVER["DEVELOPMENT_MACHINE"]) || isset($_SERVER["DEBUG_SERVER"])) {
    $dbHost = '192.168.2.10';
    $dbPassword = 'password';
}
else {
    $dbHost = 'localhost';
    $dbPassword = '1qaz3wasql';
}

return array(
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.classes.*',
    ),
    'components' => array(
        'db' => array(
            'connectionString' => "mysql:host={$dbHost};dbname={$dbName}",
            'emulatePrepare' => true,
            'username' => $dbUser,
            'password' => $dbPassword,
            'charset' => 'utf8',
        ),
        'db_ok' => array(
            'class' => 'CDbConnection',
            'connectionString' => "mysql:host={$dbHost};dbname={$dbName}_ok",
            'emulatePrepare' => true,
            'username' => $dbUser,
            'password' => $dbPassword,
            'charset' => 'utf8',
        ),
        'db_mm' => array(
            'class' => 'CDbConnection',
            'connectionString' => "mysql:host={$dbHost};dbname={$dbName}_mm",
            'emulatePrepare' => true,
            'username' => $dbUser,
            'password' => $dbPassword,
            'charset' => 'utf8',
        ),
        'db_test' => array(
            'class' => 'CDbConnection',
            'connectionString' => "mysql:host={$dbHost};dbname=tomato_test",
            'emulatePrepare' => true,
            'username' => $dbUser,
            'password' => $dbPassword,
            'charset' => 'utf8',
        ),
        'db_production_vk' => array(
            'class' => 'CDbConnection',
            'connectionString' => "mysql:unix_socket=/var/run/mysqld/mysqld.sock;dbname=tomato_kombat",
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '1qaz3wasql',
            'charset' => 'utf8',
        ),
        'db_production_ok' => array(
            'class' => 'CDbConnection',
            'connectionString' => "mysql:unix_socket=/var/run/mysqld/mysqld.sock;dbname=tomato_kombat_ok",
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '1qaz3wasql',
            'charset' => 'utf8',
        ),
        'db_production_mm' => array(
            'class' => 'CDbConnection',
            'connectionString' => "mysql:unix_socket=/var/run/mysqld/mysqld.sock;dbname=tomato_kombat_mm",
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '1qaz3wasql',
            'charset' => 'utf8',
        ),
    )
);
