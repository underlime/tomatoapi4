<?php
/* @var $this StatusController */
use php_rutils\RUtils;

/* @var $fileInfo SplFileInfo */
/* @var $fileContent string */
?>

<h1>Файл <?=$fileInfo->getBasename()?></h1>

<p>Изменен: <?=date('Y-m-d H:i:s', $fileInfo->getCTime())?></p>
<p>Размер: <?=RUtils::formatNumber($fileInfo->getSize())?> б</p>

<p>
    <label for="lod-content">Сообщения:</label>
    <textarea rows="10" cols="10" readonly="readonly" id="lod-content" class="log-content"><?=$fileContent?></textarea>
</p>

<form method="post">
    <input type="hidden" name="clear_log" value="1" />
    <button type="submit">Очистить лог</button>
</form>
