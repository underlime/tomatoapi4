<?php
/* @var $this StatusController */
$loggerConfig = $this->config['_logger'];
?>

<h1>Директории и файлы</h1>
<?php
showFileInfo($loggerConfig['logs_dir'], 'Директория логов');
showFileInfo($loggerConfig['logs_dir'].'/error.log', 'Лог программных ошибок');
showFileInfo($loggerConfig['logs_dir'].'/http_exceptions.log', 'Лог ошибок HTTP');
showFileInfo($loggerConfig['logs_dir'].'/http_warnings.log', 'Лог предупреждений HTTP');
showFileInfo($loggerConfig['logs_dir'].'/sql_db_exceptions.log', 'Лог ошибок SQL');
showFileInfo($loggerConfig['logs_dir'].'/external_request_exceptions.log', 'Лог ошибок сторонних ресурсов');
showFileInfo($loggerConfig['logs_dir'].'/common_exceptions.log', 'Лог прочих исключений');
showFileInfo($loggerConfig['logs_dir'].'/client_errors.log', 'Лог ошибок клиента');
showFileInfo($loggerConfig['logs_dir'].'/watch.log', 'Лог наблюдений');

function showFileInfo($path, $title)
{
    ?>
    <p>
        <span style="font-weight: bold;"><?= $title ?>: <code><?= $path ?></code></span>
        <br />
        Существует: <?= (file_exists($path) ? 'Да' : 'Нет') ?>
        <? if (file_exists($path)) { ?>
            <br />
            Чтение: <?= (is_readable($path) ? 'Да' : 'Нет') ?>
            <br />
            Запись: <?= (is_writable($path) ? 'Да' : 'Нет') ?>
            <br />
            Изменен: <?= date('Y-m-d H:i:s', filectime($path)) ?>
            <? if (!is_dir($path)) { ?>
                <br />
                Размер: <?= \php_rutils\RUtils::formatNumber(filesize($path)) ?> б
            <? } ?>
        <? } ?>
    </p>
<?
};
?>
