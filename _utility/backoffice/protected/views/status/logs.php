<?php
/* @var $this StatusController */
/* @var $handlerConfig array */
/* @var $fileInfo SplFileInfo */
$dirIterator = new DirectoryIterator($this->config['_logger']['logs_dir']);
$fileNames = [];
foreach ($dirIterator as $fileInfo) {
    if ($fileInfo->isFile()) {
        $fileNames[] = $fileInfo->getFilename();
    }
}
sort($fileNames);
?>

<h1>Логи</h1>
<ul>
    <? foreach ($fileNames as $fileName) { ?>
        <li>
            <?php
            if (strrpos($fileName, 'log') === strlen($fileName) - 3) {
                echo CHtml::link($fileName, array('logDetail', 'file' => $fileName));
            }
            else {
                echo $fileName;
            }
            ?>
        </li>
    <? } ?>
</ul>
