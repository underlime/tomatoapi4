<?php
/* @var $this StatusController */

use krest\Arr;
use krest\Db;

$hidePaths = [
    'krest/socnet/VkCom/api_id',
    'krest/socnet/VkCom/api_secret',
    'krest/socnet/OkRu/api_id',
    'krest/socnet/OkRu/api_key',
    'krest/socnet/OkRu/api_secret',
    'krest/socnet/MmRu/api_id',
    'krest/socnet/MmRu/api_key',
    'krest/socnet/MmRu/api_secret',

];
foreach (Db::getInstIds() as $dbId) {
    $hidePaths[] = "krest/Db/$dbId/password";
}
foreach ($hidePaths as $path) {
    Arr::setByPath($this->config, $path, '***', '/');
}
?>

<h1>Конфигурация</h1>
<div class="code"><?=\krest\Arr::getArrayAsList($this->config)?></div>
