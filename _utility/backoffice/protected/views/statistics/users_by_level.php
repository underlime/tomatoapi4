<?php

/* @var $this Controller */
use php_rutils\RUtils;

/* @var $data array */

$this->breadcrumbs[] = 'Статистика игроков по уровням';
?>

<h1>Статистика игроков по уровням</h1>

<table class="marked-table">
    <thead>
        <tr>
            <th>Уровень</th>
            <th>Количество игроков</th>
        </tr>
    </thead>
    <? foreach ($data as $record) { ?>
        <tr>
            <td><?=RUtils::formatNumber($record['level'])?></td>
            <td><?=RUtils::formatNumber($record['count'])?></td>
        </tr>
    <? } ?>
</table>
