<?php
/* @var $this StatisticsController */
/* @var $data array */
/* @var $ferrosPlotData array */
/* @var $tomatosPlotData array */
/* @var $ferrosData array */

$this->breadcrumbs[] = 'Статистика банка';
?>

<h1>Статистика банка</h1>
<? $this->widget('application.widgets.dates_range_form.DatesRangeForm', array(
                                                                             'startDate' => $this->startDate,
                                                                             'stopDate' => $this->stopDate,
                                                                        )) ?>

<h2>Статистика ферросов</h2>
<?php
$this->widget('application.widgets.bars_diagram.BarsDiagram', array(
                                                             'data' => array($ferrosData),
                                                        ))
?>

<h2 style="margin-top: 40px;">График покупок</h2>
<?php
$this->widget('application.widgets.stat_plot.StatPlot', array(
                                                             'data' => array(
                                                                 array('label' => 'Ферросы', 'data' => $ferrosPlotData),
                                                                 array('label' => 'Томатосы', 'data' => $tomatosPlotData),
                                                             ),
                                                        ))
?>

<table class="marked-table" style="margin-top: 40px;">
    <thead>
        <tr>
            <th>Дата</th>
            <th>Ферросы</th>
            <th>Томатосы</th>
        </tr>
    </thead>
    <? $revData = array_reverse($data) ?>
    <? foreach ($revData as $record) { ?>
        <tr>
            <td><?=\php_rutils\RUtils::dt()->ruStrFTime(array('date' => $record['date'], 'format' => 'd F Y', 'monthInflected' => true))?></td>
            <td><?=\php_rutils\RUtils::formatNumber($record['ferros'])?></td>
            <td><?=\php_rutils\RUtils::formatNumber($record['tomatos'])?></td>
        </tr>
    <? } ?>
</table>


