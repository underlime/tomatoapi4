<?php

/* @var $this Controller */
use php_rutils\RUtils;

/* @var $date string */
/* @var $usersData array */

$this->breadcrumbs[] = 'Давно не заходившие игроки';
?>

<h1>Давно не заходившие игроки</h1>

<div class="form">
    <form method="POST">
        <div class="row">
            <?= CHtml::label('Граничная дата', 'border_date') ?>
            <?$this->widget(
                'zii.widgets.jui.CJuiDatePicker', array(
                                                       'name' => 'border_date',
                                                       'value' => $date,
                                                       'options' => array(
                                                           'dateFormat' => 'yy-mm-dd',
                                                       ),
                                                  )
            )?>
        </div>
        <div class="row buttons">
            <?=CHtml::submitButton('Отправить')?>
        </div>
    </form>
</div>

<? if ($usersData) { ?>
    <div style="margin-top: 20px">
        Всего <b><?=RUtils::formatNumber($usersData['all_count'])?></b>
        <?=RUtils::numeral()->choosePlural($usersData['all_count'], array('игрок', 'игрока', 'игроков'))?>
        <br />
        Последний раз заходивших раньше
        <?=RUtils::dt()->ruStrFTime(array('date' => $date, 'format' => 'd F Y', 'monthInflected' => true))?> года:
        <b><?=RUtils::formatNumber($usersData['lost_count'])?></b> (<?=$usersData['lost_percent']?>%).
    </div>
<? } ?>
