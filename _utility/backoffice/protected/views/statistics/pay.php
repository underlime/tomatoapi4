<?php
/* @var $payFerrosData array */
use php_rutils\RUtils;

/* @var $payTomatosData array */
?>

<h1>Цели расходов</h1>
<? $this->widget('application.widgets.dates_range_form.DatesRangeForm', array(
                                                                             'startDate' => $this->startDate,
                                                                             'stopDate' => $this->stopDate,
                                                                        )) ?>

<h2>Ферросы</h2>
<?php
$this->widget('application.widgets.bars_diagram.BarsDiagram', array(
                                                                   'data' => array($payFerrosData),
                                                              ))
?>

<table class="marked-table" style="margin-top: 20px;">
    <thead>
        <tr>
            <th>Цель</th>
            <th>Ферросов потрачено</th>
        </tr>
    </thead>
    <? foreach ($payFerrosData as $record) { ?>
        <tr>
            <td><?=$record[0]?></td>
            <td><?=RUtils::formatNumber($record[1])?></td>
        </tr>
    <? } ?>
</table>

<h2 style="margin-top: 40px;">Томатосы</h2>
<?php
$this->widget('application.widgets.bars_diagram.BarsDiagram', array(
                                                                   'data' => array($payTomatosData),
                                                              ))
?>

<table class="marked-table" style="margin-top: 20px;">
    <thead>
        <tr>
            <th>Цель</th>
            <th>Томатосов потрачено</th>
        </tr>
    </thead>
    <? foreach ($payTomatosData as $record) { ?>
        <tr>
            <td><?=$record[0]?></td>
            <td><?=RUtils::formatNumber($record[1])?></td>
        </tr>
    <? } ?>
</table>
