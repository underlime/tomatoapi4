<?php
/* @var $this StatisticsController */
use php_rutils\RUtils;

/* @var $itemsRatingData array */
/* @var $currencyData array */
?>

<h1>Статистика рынка</h1>
<? $this->widget('application.widgets.dates_range_form.DatesRangeForm', array(
                                                                             'startDate' => $this->startDate,
                                                                             'stopDate' => $this->stopDate,
                                                                        )) ?>

<h2>Рейтинг валют</h2>
<?php
$this->widget('application.widgets.bars_diagram.BarsDiagram', array(
                                                                   'data' => array($currencyData),
                                                              ))
?>

<? if (sizeof($currencyData) == 2) { ?>
    <table class="marked-table" style="margin-top: 20px">
        <thead>
            <tr>
                <th>Валюта</th>
                <th>Количество покупок</th>
            </tr>
        </thead>
        <tr>
            <td>Ферросы</td>
            <td><?=RUtils::formatNumber($currencyData[0][1])?></td>
        </tr>
        <tr>
            <td>Томатосы</td>
            <td><?=RUtils::formatNumber($currencyData[1][1])?></td>
        </tr>
    </table>
<? } ?>


<h2 style="margin-top: 40px;">Рейтинг предметов</h2>
<table class="marked-table">
    <thead>
        <tr>
            <th>Предмет</th>
            <th>Количество покупок</th>
        </tr>
    </thead>
    <? foreach ($itemsRatingData as $record) { ?>
        <tr>
            <td><?=CHtml::link($record['name_ru'], array('items/view', 'id' => $record['item_id']))?></td>
            <td><?=RUtils::formatNumber($record['count'])?></td>
        </tr>
    <? } ?>
</table>
