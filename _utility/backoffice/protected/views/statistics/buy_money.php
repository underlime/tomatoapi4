<?php
/* @var $this Controller */
use php_rutils\RUtils;

/* @var $buyFerrosData array */
/* @var $buyTomatosData array */
?>

<h1>Источники доходов</h1>

<? $this->widget('application.widgets.dates_range_form.DatesRangeForm', array(
                                                                             'startDate' => $this->startDate,
                                                                             'stopDate' => $this->stopDate,
                                                                        )) ?>

<h2>Ферросы</h2>
<?php
$this->widget('application.widgets.bars_diagram.BarsDiagram', array(
                                                                   'data' => array($buyFerrosData),
                                                              ))
?>

<table class="marked-table" style="margin-top: 20px;">
    <thead>
        <tr>
            <th>Источник</th>
            <th>Ферросов получено</th>
        </tr>
    </thead>
    <? foreach ($buyFerrosData as $record) { ?>
        <tr>
            <td><?=$record[0]?></td>
            <td><?=RUtils::formatNumber($record[1])?></td>
        </tr>
    <? } ?>
</table>


<h2 style="margin-top: 40px;">Томатосы</h2>
<?php
$this->widget('application.widgets.bars_diagram.BarsDiagram', array(
                                                                   'data' => array($buyTomatosData),
                                                              ))
?>

<table class="marked-table" style="margin-top: 20px;">
    <thead>
        <tr>
            <th>Источник</th>
            <th>Томатосов получено</th>
        </tr>
    </thead>
    <? foreach ($buyTomatosData as $record) { ?>
        <tr>
            <td><?=$record[0]?></td>
            <td><?=RUtils::formatNumber($record[1])?></td>
        </tr>
    <? } ?>
</table>
