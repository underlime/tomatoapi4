<?php
/* @var $this Controller */
use php_rutils\RUtils;

/* @var $data array */

$this->breadcrumbs[] = 'Проценты получения достижений';
?>

<h1>Проценты получения достижений</h1>

<table class="marked-table">
    <thead>
        <tr>
            <th>Достижение</th>
            <th>Количество игроков</th>
            <th>Процент игроков</th>
        </tr>
    </thead>
    <? foreach ($data as $record) { ?>
        <tr>
            <td><?=CHtml::link($record['name_ru'], array('achievements/view', 'id' => $record['achievement_id']))?></td>
            <td><?=RUtils::formatNumber($record['count'])?></td>
            <td><?=$record['percent']?>%</td>
        </tr>
    <? } ?>
</table>
