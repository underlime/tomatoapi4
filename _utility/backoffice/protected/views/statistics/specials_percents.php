<?php
/* @var $this Controller */
use php_rutils\RUtils;

/* @var $data array */

$this->breadcrumbs[] = 'Проценты получения навыков';
?>

<h1>Проценты получения навыков</h1>

<table class="marked-table">
    <thead>
        <tr>
            <th>Навык</th>
            <th>Количество игроков</th>
            <th>Процент игроков</th>
        </tr>
    </thead>
    <? foreach ($data as $record) { ?>
        <tr>
            <td><?=CHtml::link($record['name_ru'], array('specials/view', 'id' => $record['special_id']))?></td>
            <td><?=RUtils::formatNumber($record['count'])?></td>
            <td><?=$record['percent']?>%</td>
        </tr>
    <? } ?>
</table>
