<?php
/* @var $this AchievementsController */
/* @var $model Achievements */
/* @var $form CActiveForm */
?>

<div class="form">
    <? $form = $this->beginWidget('CActiveForm', array(
                                                      'id' => 'achievements-form',
                                                      'enableAjaxValidation' => false,
                                                 )) ?>

    <p class="note"><span class="required">*</span> &mdash; обязательные поля</p>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <?= $form->labelEx($model, 'code') ?>
        <?= $form->textField($model, 'code', array('size' => 32, 'maxlength' => 32)) ?>
        <?= $form->error($model, 'code') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'name_ru') ?>
        <?= $form->textField($model, 'name_ru', array('size' => 60, 'maxlength' => 64)) ?>
        <?= $form->error($model, 'name_ru') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'text_ru') ?>
        <?= $form->textField($model, 'text_ru', array('size' => 60, 'maxlength' => 512)) ?>
        <?= $form->error($model, 'text_ru') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'pic') ?>
        <?= $form->textField($model, 'pic', array('size' => 60, 'maxlength' => 64)) ?>
        <?= $form->error($model, 'pic') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'wins_level') ?>
        <?= $form->textField($model, 'wins_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'wins_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'fails_level') ?>
        <?= $form->textField($model, 'fails_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'fails_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'ferros_payed') ?>
        <?= $form->textField($model, 'ferros_payed', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'ferros_payed') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'tomatos_payed') ?>
        <?= $form->textField($model, 'tomatos_payed', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'tomatos_payed') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'visits_level') ?>
        <?= $form->textField($model, 'visits_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'visits_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'level_reached') ?>
        <?= $form->textField($model, 'level_reached', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'level_reached') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'strength_level') ?>
        <?= $form->textField($model, 'strength_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'strength_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'agility_level') ?>
        <?= $form->textField($model, 'agility_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'agility_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'intellect_level') ?>
        <?= $form->textField($model, 'intellect_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'intellect_level') ?>
    </div>

    <div class="row buttons">
        <?= CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить') ?>
    </div>

    <? $this->endWidget() ?>

</div><!-- form -->
