<?php
/* @var $this AchievementsController */
/* @var $model Achievements */

$this->breadcrumbs = array(
	'Achievements' => array('index'),
	'Создать',
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Создать</h1>

<? $this->renderPartial('_form', array('model' => $model)) ?>