<?php
/* @var $this AchievementsController */
/* @var $model Achievements */

$this->breadcrumbs = array(
	'Achievements' => array('index'),
	'Управление',
);

$this->menu = array(
	array('label' => 'Список', 'url'=>array('index')),
	array('label' => 'Создать', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#achievements-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление</h1>

<p>
Вы можете использовать в поиске операторы сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале поискового запроса для уточнения условий.
</p>

<?=CHtml::link('Расширенный поиск','#',array('class'=>'search-button'))?>
<div class="search-form" style="display:none">
<? $this->renderPartial('_search',array(
	'model'=>$model,
)) ?>
</div><!-- search-form -->

<?  $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'achievements-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'achievement_id',
		'code',
		'name_ru',
		'text_ru',
		'pic',
		array(
			'class' => 'CButtonColumn',
		),
	),
)) ?>
