<?php
/* @var $this AchievementsController */
/* @var $model Achievements */

$this->breadcrumbs = array(
	'Achievements' => array('index'),
	$model->achievement_id => array('view','id' => $model->achievement_id),
	'Изменить',
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Свойства', 'url' => array('view', 'id' => $model->achievement_id)),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Изменить</h1>

<? $this->renderPartial('_form', array('model'=>$model)) ?>