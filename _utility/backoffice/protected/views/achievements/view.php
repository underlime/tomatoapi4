<?php
/* @var $this AchievementsController */
/* @var $model Achievements */

$this->breadcrumbs = array(
	'Achievements' => array('index'),
	$model->achievement_id,
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Изменить', 'url' => array('update', 'id'=>$model->achievement_id)),
	array('label' => 'Удалить', 'url' => '#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->achievement_id),'confirm'=>'Вы уверены, что хотите удалить объект?')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Свойства Achievements #<?php echo $model->achievement_id; ?></h1>

<?  $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
		'achievement_id',
		'code',
		'name_ru',
		'text_ru',
		'pic',
		'wins_level',
		'fails_level',
		'ferros_payed',
		'tomatos_payed',
		'visits_level',
		'level_reached',
		'strength_level',
		'agility_level',
		'intellect_level',
	),
)) ?>
