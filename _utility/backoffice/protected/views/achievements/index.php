<?php
/* @var $this AchievementsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Achievements',
);

$this->menu = array(
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Achievements</h1>

<?  $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)) ?>
