<?php
/* @var $this AchievementsController */
/* @var $data Achievements */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('achievement_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->achievement_id), array('view', 'id'=>$data->achievement_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_ru')); ?>:</b>
	<?php echo CHtml::encode($data->text_ru); ?>
</div>