<?php

/* @var $this ConfigController */
use krest\Arr;
use krest\SocNet;
use php_rutils\RUtils;

/* @var $config array */
/* @var $bankRates array */
/* @var $clansClashParams array */
/* @var $form CActiveForm */

$this->breadcrumbs = ['Конфиг'];

switch ($this->socNetCode) {
    case SocNet::VK_COM:
        $socCurrency = ['голос', 'голоса', 'голосов'];
        break;
    case SocNet::OK_RU:
        $socCurrency = ['OK', 'OK', 'OK'];
        break;
    default:
        $socCurrency = ['ХЗ', 'ХЗ', 'ХЗ'];
}
?>

<h1>Конфиг</h1>

<section>
    <header>
        <h2>Курсы банка</h2>
    </header>
    <? $form = $this->beginWidget('CActiveForm') ?>
    <table style="width: auto; margin-bottom: 5px">
        <?php
        foreach ($bankRates as $id => $data) {
            echo '<tr>';
            $name = "bank_rates[$id]";
            echo '<td>', $data['count'], ' ', strtoupper($data['currency']), '</td>';
            echo '<td>', CHtml::numberField($name, $data['price']), '</td>';
            echo '<td>', RUtils::numeral()->choosePlural($data['price'], $socCurrency), '</td>';
            echo '</tr>';
        }
        ?>
    </table>
    <?= CHtml::submitButton('Сохранить') ?>
    <? $this->endWidget() ?>
</section>

<section style="margin-top: 50px">
    <header>
        <h2>Клановый турнир</h2>
    </header>
    <? $form = $this->beginWidget('CActiveForm') ?>
    <div class="form">
        <div class="row">
            <?= CHtml::label('Дней в сезоне', 'ClansClash_season_duration') ?>
            <?= CHtml::numberField('ClansClash[season_duration]', intval($clansClashParams['season_duration'])) ?>
        </div>
        <div class="row">
            <?= CHtml::label('Приз', 'ClansClash_season_prize') ?>
            <?= CHtml::numberField('ClansClash[season_prize]', $clansClashParams['season_prize']) ?>
        </div>
        <div class="row">
            <?= CHtml::label('Макс. приз для игрока', 'ClansClash_max_user_prize') ?>
            <?= CHtml::numberField('ClansClash[max_user_prize]', $clansClashParams['max_user_prize']) ?>
        </div>
        <div class="row buttons">
            <?= CHtml::submitButton('Сохранить') ?>
        </div>
    </div>
    <? $this->endWidget() ?>
</section>
