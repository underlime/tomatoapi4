<?php
/* @var $this SpecialsController */
/* @var $data Specials */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('special_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->special_id), array('view', 'id'=>$data->special_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_ru')); ?>:</b>
	<?php echo CHtml::encode($data->description_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
	<?php echo CHtml::encode($data->picture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('animation')); ?>:</b>
	<?php echo CHtml::encode($data->animation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
</div>