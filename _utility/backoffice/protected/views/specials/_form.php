<?php
/* @var $this SpecialsController */
/* @var $model Specials */
/* @var $form CActiveForm */
?>

<div class="form">
    <? $form = $this->beginWidget('CActiveForm', array(
        'id' => 'specials-form',
        'enableAjaxValidation' => FALSE,
    )) ?>

    <p class="note"><span class="required">*</span> &mdash; обязательные поля</p>

    <?=$form->errorSummary($model) ?>
    
        <div class="row">
            <?=$form->labelEx($model,'code')?>
            <?=$form->textField($model,'code',array('size'=>32,'maxlength'=>32))?>
            <?=$form->error($model,'code')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'name_ru')?>
            <?=$form->textField($model,'name_ru',array('size'=>32,'maxlength'=>32))?>
            <?=$form->error($model,'name_ru')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'description_ru')?>
            <?=$form->textField($model,'description_ru',array('size'=>60,'maxlength'=>140))?>
            <?=$form->error($model,'description_ru')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'picture')?>
            <?=$form->textField($model,'picture',array('size'=>32,'maxlength'=>32))?>
            <?=$form->error($model,'picture')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'animation')?>
            <?=$form->textField($model,'animation',array('size'=>32,'maxlength'=>32))?>
            <?=$form->error($model,'animation')?>
        </div>
    
        <div class="row checkbox-row">
            <?=$form->checkBox($model,'active')?>
            <?=$form->labelEx($model,'active')?>
            <?=$form->error($model,'active')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'bonuses')?>
            <?=$form->textArea($model,'bonuses',array('rows'=>6, 'cols'=>50))?>
            <?=$form->error($model,'bonuses')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'k_damage')?>
            <?=$form->textField($model,'k_damage')?>
            <?=$form->error($model,'k_damage')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'steps')?>
            <?=$form->textField($model,'steps',array('size'=>10,'maxlength'=>10))?>
            <?=$form->error($model,'steps')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'additional_data')?>
            <?=$form->textField($model,'additional_data',array('size'=>60,'maxlength'=>64))?>
            <?=$form->error($model,'additional_data')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'intellect_level')?>
            <?=$form->textField($model,'intellect_level',array('size'=>10,'maxlength'=>10))?>
            <?=$form->error($model,'intellect_level')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'level_intellect_step')?>
            <?=$form->textField($model,'level_intellect_step',array('size'=>10,'maxlength'=>10))?>
            <?=$form->error($model,'level_intellect_step')?>
        </div>
    
        <div class="row">
            <?=$form->labelEx($model,'level_k_bonuse')?>
            <?=$form->textField($model,'level_k_bonuse')?>
            <?=$form->error($model,'level_k_bonuse')?>
        </div>
    
        <div class="row buttons">
            <?=CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить')?>
        </div>

    <?$this->endWidget()?>

</div><!-- form -->
