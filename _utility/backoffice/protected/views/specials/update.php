<?php
/* @var $this SpecialsController */
/* @var $model Specials */

$this->breadcrumbs = array(
	'Specials' => array('index'),
	$model->special_id => array('view','id' => $model->special_id),
	'Изменить',
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Свойства', 'url' => array('view', 'id' => $model->special_id)),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Изменить</h1>

<? $this->renderPartial('_form', array('model'=>$model)) ?>