<?php
/* @var $this SpecialsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'Specials',
);

$this->menu = array(
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Specials</h1>

<?  $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)) ?>
