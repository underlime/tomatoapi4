<?php
/* @var $this SpecialsController */
/* @var $model Specials */

$this->breadcrumbs = array(
	'Specials' => array('index'),
	$model->special_id,
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Изменить', 'url' => array('update', 'id'=>$model->special_id)),
	array('label' => 'Удалить', 'url' => '#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->special_id),'confirm'=>'Вы уверены, что хотите удалить объект?')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Свойства Specials #<?php echo $model->special_id; ?></h1>

<?  $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
		'special_id',
		'code',
		'name_ru',
		'description_ru',
		'picture',
		'animation',
		'active',
		'bonuses',
		'k_damage',
		'steps',
		'additional_data',
		'intellect_level',
		'level_intellect_step',
		'level_k_bonuse',
	),
)) ?>
