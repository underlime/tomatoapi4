<?php
/* @var $this UsersController */
/* @var $model UserMoneyForm */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Пользователи' => array('users/index'),
    'Дать/забрать денег'
);
?>

<h1>Дать/забрать денег</h1>

<p>Отрицательные значения, чтобы забирать</p>

<?php
$form = $this->beginWidget('CActiveForm');
?>

<div class="form">
    <div class="row">
        <?= $form->labelEx($model, 'soc_net_id') ?>
        <?= $form->textField($model, 'soc_net_id') ?>
        <?= $form->error($model, 'soc_net_id') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'ferros') ?>
        <?= $form->textField($model, 'ferros') ?>
        <?= $form->error($model, 'ferros') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'tomatos') ?>
        <?= $form->textField($model, 'tomatos') ?>
        <?= $form->error($model, 'tomatos') ?>
    </div>

    <div class="row buttons">
        <?=CHtml::submitButton('Отправить')?>
    </div>
</div>

<? $this->endWidget() ?>
