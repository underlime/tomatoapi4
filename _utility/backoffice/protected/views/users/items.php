<?php
/* @var $this UsersController */
/* @var $model UserMoneyForm */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Пользователи' => array('users/index'),
    'Подарить предмет'
);
?>

<h1>Подарить предмет</h1>

<?php
$form = $this->beginWidget('CActiveForm');
?>

<div class="form">
    <div class="row">
        <?= $form->labelEx($model, 'soc_net_id') ?>
        <?= $form->textField($model, 'soc_net_id') ?>
        <?= $form->error($model, 'soc_net_id') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'item_id') ?>
        <?= $form->textField($model, 'item_id') ?>
        <?= $form->error($model, 'item_id') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'count') ?>
        <?= $form->textField($model, 'count') ?>
        <?= $form->error($model, 'count') ?>
    </div>

    <div class="row buttons">
        <?=CHtml::submitButton('Отправить')?>
    </div>
</div>

<? $this->endWidget() ?>
