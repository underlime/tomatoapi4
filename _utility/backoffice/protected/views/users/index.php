<?php

/* @var $this UsersController */
use php_rutils\RUtils;

/* @var $usersCount int */

$this->breadcrumbs = array(
    'Пользователи'
);
?>

<h1>Пользователи</h1>

<p>
    В системе
    <?=
    RUtils::numeral()->getPlural(
        $usersCount,
        array('пользователь', 'пользователя', 'пользователей')
    )?>.
</p>
