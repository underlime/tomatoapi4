<?php
/* @var $this UsersController */
/* @var $model UserMoneyForm */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Пользователи' => array('users/index'),
    'Забанить/разбанить'
);
?>

<h1>Забанить/разбанить</h1>

<p>Чтобы разбанить, введите уровень бана 0</p>

<?php
$form = $this->beginWidget('CActiveForm');
?>

<div class="form">
    <div class="row">
        <?= $form->labelEx($model, 'soc_net_id') ?>
        <?= $form->textField($model, 'soc_net_id') ?>
        <?= $form->error($model, 'soc_net_id') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'ban_level') ?>
        <?= $form->textField($model, 'ban_level') ?>
        <?= $form->error($model, 'ban_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'ban_reason') ?>
        <?= $form->textField($model, 'ban_reason', array('size' => 60, 'maxlength' => 140)) ?>
        <?= $form->error($model, 'ban_reason') ?>
    </div>

    <div class="row buttons">
        <?=CHtml::submitButton('Отправить')?>
    </div>
</div>

<? $this->endWidget() ?>
