<?php
/* @var $this CController */
$controllerId = $this->id;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
              media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
              media="print" />

        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
              media="screen, projection" />
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body>
        <div class="container" id="page">
            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
                <? if (!Yii::app()->user->isGuest) { ?>
                    <div id="db-switch-wrapper">
                        <? $form = $this->beginWidget("CActiveForm", array("method" => "get")) ?>
                        <?=CHtml::label("База данных:", "database")?>
                        <?=CHtml::dropDownList("database", $this->dbCode, DbData::getPairs())?>
                        <?php
                        $script = <<<JS
                            $("#database").change(function() {
                                $(this).closest("form").submit();
                            });
JS;
                        Yii::app()->clientScript->registerScript("change-db", $script, CClientScript::POS_READY);
                        ?>
                        <? $this->endWidget() ?>
                    </div>
                <? } ?>
            </div>
            <!-- header -->
            <div id="mainmenu">
                <?php $this->widget('zii.widgets.CMenu', array(
                                                              'items' => array(
                                                                  createMenuItem('Старт', 'site/index'),
                                                                  createMenuItem('Пользователи', 'users/index'),
                                                                  createMenuItem('Магазин', 'items/index'),
                                                                  createMenuItem('Достижения', 'achievements/index'),
                                                                  createMenuItem('Навыки', 'specials/index'),
                                                                  createMenuItem('Новости', 'userNews/index'),
                                                                  createMenuItem('Шпионаж', 'spy/index'),
                                                                  createMenuItem('Конфиг', 'config/index'),
                                                                  createMenuItem('Статус', 'status/index'),
                                                                  createMenuItem('Статистика', 'statistics/index'),
                                                                  createMenuItem('Вход', 'site/login', null, true),
                                                                  createMenuItem('Выход ('.Yii::app()->user->name.')',
                                                                                 'site/logout', false),
                                                              ),
                                                         )); ?>
            </div>
            <!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links'=>$this->breadcrumbs,
                )); ?><!-- breadcrumbs -->
            <?php endif ?>
            <?php echo $content; ?>
            <div class="clear"></div>
            <div id="footer">
                <?=Yii::powered()?>
                <? if (!Yii::app()->user->isGuest) { ?>
                    <br />
                    Используется соединение: <?=ActiveRecord::getConnectionName()?>
                <? } ?>
            </div>
            <!-- footer -->
        </div>
        <!-- page -->
    </body>
</html>
<?php
function createMenuItem($label, $urlPattern, $active = null, $forGuest = false)
{
    $controllerId = Yii::app()->controller->id;
    $actionId = Yii::app()->controller->action->id;

    if ($active === null && !($controllerId == 'site' && $actionId == 'error')) {
        $parts = explode('/', $urlPattern, 2);
        $active = ($parts && $parts[0] == $controllerId);
    }

    $item = array(
        'label' => $label,
        'url' => array($urlPattern),
        'active' => $active
    );

    if ($forGuest) {
        $item['visible'] = Yii::app()->user->isGuest;
    }
    else {
        $item['visible'] = !Yii::app()->user->isGuest;
    }

    return $item;
}
?>
