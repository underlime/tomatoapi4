<?php
/* @var $this UserNewsController */
/* @var $model UserNews */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'time'); ?>
		<?php echo $form->textField($model,'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'expires_time'); ?>
		<?php echo $form->textField($model,'expires_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'news_type'); ?>
		<?php echo $form->textField($model,'news_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'additional_data'); ?>
		<?php echo $form->textArea($model,'additional_data',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'like_reward_type'); ?>
		<?php echo $form->textField($model,'like_reward_type',array('size'=>7,'maxlength'=>7)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'like_reward_count'); ?>
		<?php echo $form->textField($model,'like_reward_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'like_reward_data'); ?>
		<?php echo $form->textField($model,'like_reward_data',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->