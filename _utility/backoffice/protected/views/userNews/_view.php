<?php

/* @var $this UserNewsController */
use krest\Arr;

/* @var $data UserNews */
$newsData = json_decode($data->additional_data, true);
?>

<div class="view">
    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('time')); ?>:</b>
    <?php echo CHtml::encode($data->time); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('news_type')); ?>:</b>
    <?php echo CHtml::encode(Arr::get(UserNews::$NEWS_TYPES, $data->news_type)); ?>
    <br />

    <? if ($data->news_type == 0) { ?>
        <b>Заголовок:</b>
        <?php echo CHtml::encode(isset($newsData['h_ru']) ? $newsData['h_ru'] : ''); ?>
        <br />
    <? } ?>

    <b><?php echo CHtml::encode($data->getAttributeLabel('like_reward_type')); ?>:</b>
    <?php echo CHtml::encode($data->like_reward_type); ?>
</div>
