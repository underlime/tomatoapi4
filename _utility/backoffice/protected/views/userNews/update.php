<?php
/* @var $this UserNewsController */
/* @var $model UserNews */

$this->breadcrumbs = array(
	'User News' => array('index'),
	$model->id => array('view','id' => $model->id),
	'Изменить',
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Свойства', 'url' => array('view', 'id' => $model->id)),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Изменить</h1>

<? $this->renderPartial('_form', array('model'=>$model)) ?>