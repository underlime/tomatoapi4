<?php
/* @var $this UserNewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
	'User News',
);

$this->menu = array(
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>User News</h1>

<?  $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)) ?>
