<?php
/* @var $this UserNewsController */
/* @var $model UserNews */

$this->breadcrumbs = array(
	'User News' => array('index'),
	'Создать',
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Создать</h1>

<? $this->renderPartial('_form', array('model' => $model)) ?>