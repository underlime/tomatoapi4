<?php
/* @var $this UserNewsController */
/* @var $model UserNews */
/* @var $form NgActiveForm */
?>


<div class="form" data-ng-app="" data-ng-controller="NewsFormController">
    <?php
    $attributes = $model->getAttributes();
    foreach ($attributes as $name => $value) {
        $value = CHtml::encode($value);
        $htmlOptions = [];
        CHtml::resolveNameID($model, $name, $htmlOptions);
        $ngModel = $htmlOptions['id'];
        echo "<input type=\"hidden\" data-ng-init=\"{$ngModel} = '$value'\" />", PHP_EOL;
    }
    ?>
    <input type="hidden" data-ng-init="onValuesInit()" />

    <? $form = $this->beginWidget('NgActiveForm', array(
                                                       'id' => 'user-news-form',
                                                       'enableAjaxValidation' => false,
                                                  )) ?>

    <p class="note"><span class="required">*</span> &mdash; обязательные поля</p>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <?= $form->labelEx($model, 'news_type') ?>
        <?= $form->dropDownList($model, 'news_type', UserNews::$NEWS_TYPES) ?>
        <?= $form->error($model, 'news_type') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'user_id') ?>
        <?= $form->textField($model, 'user_id', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'user_id') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'time') ?>
        <?= $form->textField($model, 'time', ['placeholder' => 'Y-m-d H:i:s']) ?>
        <?= $form->error($model, 'time') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'expires_time') ?>
        <?= $form->textField($model, 'expires_time', ['placeholder' => 'Y-m-d H:i:s']) ?>
        <?= $form->error($model, 'expires_time') ?>
    </div>

    <div class="row" class="data-row">
        <?= $form->hiddenField($model, 'additional_data', ['value' => '{{UserNews_additional_data}}'])?>
        <?= $form->error($model, 'additional_data') ?>
        <div class="common-news-form" data-ng-show="UserNews_news_type == 0">
            <div class="row">
                <label for="common-news-header-ru">Заголовок (RU)</label>
                <input type="text" id="common-news-header-ru" data-ng-model="header_ru" data-ng-maxlength="64" size="32" />
            </div>
            <div class="row">
                <label for="common-news-text-ru">Текст (RU)</label>
                <input type="text" id="common-news-text-ru" data-ng-model="text_ru" data-ng-maxlength="140" size="64" />
            </div>
            <div class="row">
                <label for="common-news-header-en">Заголовок (EN)</label>
                <input type="text" id="common-news-header-en" data-ng-model="header_en" data-ng-maxlength="64" size="32" />
            </div>
            <div class="row">
                <label for="common-news-text-en">Текст (EN)</label>
                <input type="text" id="common-news-text-en" data-ng-model="text_en" data-ng-maxlength="140" size="64" />
            </div>
        </div>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'like_reward_type') ?>
        <?= $form->dropDownList($model, 'like_reward_type', UserNews::$LIKE_REWARDS) ?>
        <?= $form->error($model, 'like_reward_type') ?>
    </div>

    <div class="row" data-ng-show="UserNews_like_reward_type">
        <?= $form->labelEx($model, 'like_reward_count') ?>
        <?= $form->textField($model, 'like_reward_count') ?>
        <?= $form->error($model, 'like_reward_count') ?>
    </div>

    <div class="row" data-ng-show="UserNews_like_reward_type == 'item'">
        <?= $form->labelEx($model, 'like_reward_data') ?>
        <?= $form->textField($model, 'like_reward_data', array('size' => 32, 'maxlength' => 32)) ?>
        <?= $form->error($model, 'like_reward_data') ?>
    </div>

    <div class="row buttons">
        <?= CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить') ?>
    </div>

    <? $this->endWidget() ?>

</div><!-- form -->
