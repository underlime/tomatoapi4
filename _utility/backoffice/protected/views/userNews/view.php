<?php
/* @var $this UserNewsController */
/* @var $model UserNews */

$this->breadcrumbs = array(
	'User News' => array('index'),
	$model->id,
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Изменить', 'url' => array('update', 'id'=>$model->id)),
	array('label' => 'Удалить', 'url' => '#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены, что хотите удалить объект?')),
	array('label' => 'Управление', 'url' => array('admin')),
);

$newsData = json_decode($model->additional_data);
?>

<h1>Свойства UserNews #<?php echo $model->id; ?></h1>

<?  $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'time',
		'expires_time',
		'news_type',
		'like_reward_type',
		'like_reward_count',
		'like_reward_data',
	),
)) ?>

<pre><? print_r($newsData) ?></pre>
