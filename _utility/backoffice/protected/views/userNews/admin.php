<?php
/* @var $this UserNewsController */
/* @var $model UserNews */

$this->breadcrumbs = array(
	'User News' => array('index'),
	'Управление',
);

$this->menu = array(
	array('label' => 'Список', 'url'=>array('index')),
	array('label' => 'Создать', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление</h1>

<p>
Вы можете использовать в поиске операторы сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) в начале поискового запроса для уточнения условий.
</p>

<?=CHtml::link('Расширенный поиск','#',array('class'=>'search-button'))?>
<div class="search-form" style="display:none">
<? $this->renderPartial('_search',array(
	'model'=>$model,
)) ?>
</div><!-- search-form -->

<?  $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'user-news-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'user_id',
		'time',
		'expires_time',
		'news_type',
		'additional_data',
		/*
		'like_reward_type',
		'like_reward_count',
		'like_reward_data',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)) ?>
