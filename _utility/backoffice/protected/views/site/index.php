<?php
/* @var $this SiteController */
/* @var $cacheFlushStatus bool */
$this->pageTitle = Yii::app()->name;
?>

<h1>Старт</h1>
<p>Томато Комбат &mdash; крутая игра. А отсюда она управляется.</p>

<h2>Обслуживание</h2>
<div>
    <form method="post">
        <input type="hidden" name="clear_all_cache" value="1" />
        <button type="submit">Очистить кэш</button>
    </form>
</div>

<div class="messages-area">
    <? if ($cacheFlushStatus) { ?>
        <p>Кэш полностью очищен</p>
    <? } ?>
</div>
