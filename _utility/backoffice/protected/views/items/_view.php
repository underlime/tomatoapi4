<?php
/* @var $this ItemsController */
/* @var $data Items */
?>

<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('item_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->item_id), array('view', 'id'=>$data->item_id)); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
    <?php echo CHtml::encode($data->name_ru); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rubric_code')); ?>:</b>
	<?php echo CHtml::encode($data->rubric_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('buy_another')); ?>:</b>
	<?php echo CHtml::encode($data->buy_another); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apply_in_fight')); ?>:</b>
	<?php echo CHtml::encode($data->apply_in_fight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('new')); ?>:</b>
	<?php echo CHtml::encode($data->new); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sale')); ?>:</b>
	<?php echo CHtml::encode($data->sale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('show_in_market')); ?>:</b>
	<?php echo CHtml::encode($data->show_in_market); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sort')); ?>:</b>
	<?php echo CHtml::encode($data->sort); ?>
</div>