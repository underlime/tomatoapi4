<?php
/* @var $this ItemsController */
/* @var $model Items */

$this->breadcrumbs = array(
	'Items' => array('index'),
	$model->item_id => array('view','id' => $model->item_id),
	'Изменить',
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Свойства', 'url' => array('view', 'id' => $model->item_id)),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Изменить</h1>

<? $this->renderPartial('_form', array('model'=>$model)) ?>