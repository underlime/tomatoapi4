<?php
/* @var $this ItemsController */
/* @var $model Items */

$this->breadcrumbs = array(
	'Items' => array('index'),
	$model->item_id,
);

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Изменить', 'url' => array('update', 'id'=>$model->item_id)),
	array('label' => 'Удалить', 'url' => '#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->item_id),'confirm'=>'Вы уверены, что хотите удалить объект?')),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Свойства Items #<?php echo $model->item_id; ?></h1>

<? $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
		'item_id',
        'name_ru',
        'arrangement_variant',
        'rubric_code',
		'picture',
		'price_ferros',
		'price_tomatos',
		'sell_price_ferros',
		'sell_price_tomatos',
		'buy_another',
		'buy_another_at_once',
		'apply_in_fight',
		'fight_damage',
		'heal_percents',
		'max_hp',
		'no_wear',
		'new',
		'sale',
		'show_in_market',
		'sort',
		'gift_type',
		'required_level',
	),
)) ?>

<div style="margin-top: 30px">
    <b>Бонусы:</b>
    <pre><?=CHtml::encode($model->bonuses)?></pre>
</div>

<div style="margin-top: 30px; overflow: auto">
    <b>Описание (RU):</b>
    <pre><?=CHtml::encode($model->description_ru)?></pre>
</div>
