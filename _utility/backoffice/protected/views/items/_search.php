<?php
/* @var $this ItemsController */
/* @var $model Items */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'item_id'); ?>
		<?php echo $form->textField($model,'item_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'arrangement_variant'); ?>
		<?php echo $form->textField($model,'arrangement_variant',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rubric_code'); ?>
		<?php echo $form->textField($model,'rubric_code',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name_ru'); ?>
		<?php echo $form->textField($model,'name_ru',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'picture'); ?>
		<?php echo $form->textField($model,'picture',array('size'=>32,'maxlength'=>32)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_ferros'); ?>
		<?php echo $form->textField($model,'price_ferros',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price_tomatos'); ?>
		<?php echo $form->textField($model,'price_tomatos',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sell_price_ferros'); ?>
		<?php echo $form->textField($model,'sell_price_ferros',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sell_price_tomatos'); ?>
		<?php echo $form->textField($model,'sell_price_tomatos',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'buy_another'); ?>
		<?php echo $form->textField($model,'buy_another',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'buy_another_at_once'); ?>
		<?php echo $form->textField($model,'buy_another_at_once',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bonuses'); ?>
		<?php echo $form->textArea($model,'bonuses',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apply_in_fight'); ?>
		<?php echo $form->textField($model,'apply_in_fight',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fight_damage'); ?>
		<?php echo $form->textField($model,'fight_damage',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'heal_percents'); ?>
		<?php echo $form->textField($model,'heal_percents'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'max_hp'); ?>
		<?php echo $form->textField($model,'max_hp',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_wear'); ?>
		<?php echo $form->textField($model,'no_wear'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'new'); ?>
		<?php echo $form->textField($model,'new'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sale'); ?>
		<?php echo $form->textField($model,'sale'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'show_in_market'); ?>
		<?php echo $form->textField($model,'show_in_market'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sort'); ?>
		<?php echo $form->textField($model,'sort'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gift_type'); ?>
		<?php echo $form->textField($model,'gift_type',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'required_level'); ?>
		<?php echo $form->textField($model,'required_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_ru'); ?>
		<?php echo $form->textArea($model,'description_ru',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Поиск'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->