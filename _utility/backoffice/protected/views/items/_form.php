<?php
/* @var $this ItemsController */
/* @var $model Items */
/* @var $form CActiveForm */
?>

<div class="form">
    <? $form = $this->beginWidget('CActiveForm', array(
                                                      'id' => 'items-form',
                                                      'enableAjaxValidation' => FALSE,
                                                 )) ?>

    <p class="note"><span class="required">*</span> &mdash; обязательные поля</p>

    <?= $form->errorSummary($model) ?>

    <div class="row">
        <?= $form->labelEx($model, 'name_ru') ?>
        <?= $form->textField($model, 'name_ru', array('size' => 40, 'maxlength' => 40)) ?>
        <?= $form->error($model, 'name_ru') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'arrangement_variant') ?>
        <?= $form->dropDownList($model, 'arrangement_variant', $this->arrangementVariants) ?>
        <?= $form->error($model, 'arrangement_variant') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'rubric_code') ?>
        <?= $form->dropDownList($model, 'rubric_code', $this->rubricCodes) ?>
        <?= $form->error($model, 'rubric_code') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'picture') ?>
        <?= $form->textField($model, 'picture', array('size' => 32, 'maxlength' => 32)) ?>
        <?= $form->error($model, 'picture') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'price_ferros') ?>
        <?= $form->textField($model, 'price_ferros', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'price_ferros') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'price_tomatos') ?>
        <?= $form->textField($model, 'price_tomatos', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'price_tomatos') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'sell_price_tomatos') ?>
        <?= $form->textField($model, 'sell_price_tomatos', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'sell_price_tomatos') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'required_level') ?>
        <?= $form->textField($model, 'required_level', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'required_level') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'description_ru') ?>
        <?= $form->textArea($model, 'description_ru', array('rows' => 6, 'cols' => 50)) ?>
        <?= $form->error($model, 'description_ru') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'sort') ?>
        <?= $form->textField($model, 'sort') ?>
        <?= $form->error($model, 'sort') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'buy_another') ?>
        <?= $form->textField($model, 'buy_another', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'buy_another') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'buy_another_at_once') ?>
        <?= $form->textField($model, 'buy_another_at_once', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'buy_another_at_once') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'bonuses') ?>
        <?= $form->textArea($model, 'bonuses', array('rows' => 6, 'cols' => 50)) ?>
        <?= $form->error($model, 'bonuses') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'apply_in_fight') ?>
        <?= $form->dropDownList($model, 'apply_in_fight', $this->applyInFightVariants) ?>
        <?= $form->error($model, 'apply_in_fight') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'fight_damage') ?>
        <?= $form->textField($model, 'fight_damage', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'fight_damage') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'heal_percents') ?>
        <?= $form->textField($model, 'heal_percents', array('size' => 10, 'maxlength' => 10)) ?>
        <?= $form->error($model, 'heal_percents') ?>
    </div>

    <div class="row checkbox-row">
        <?= $form->checkBox($model, 'new') ?>
        <?= $form->labelEx($model, 'new') ?>
        <?= $form->error($model, 'new') ?>
    </div>

    <div class="row checkbox-row">
        <?= $form->checkBox($model, 'sale') ?>
        <?= $form->labelEx($model, 'sale') ?>
        <?= $form->error($model, 'sale') ?>
    </div>

    <div class="row checkbox-row">
        <?= $form->checkBox($model, 'show_in_market') ?>
        <?= $form->labelEx($model, 'show_in_market') ?>
        <?= $form->error($model, 'show_in_market') ?>
    </div>

    <div class="row">
        <?= $form->labelEx($model, 'gift_type') ?>
        <?= $form->dropDownList($model, 'gift_type', $this->giftTypeVariants) ?>
        <?= $form->error($model, 'gift_type') ?>
    </div>

    <div class="row buttons">
        <?= CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить') ?>
    </div>

    <? $this->endWidget() ?>

</div><!-- form -->
