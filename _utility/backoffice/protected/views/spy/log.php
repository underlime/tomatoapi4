<?php
/* @var $this Controller */
/* @var $model ARSpyData */
$this->breadcrumbs = [
    'Шпионаж' => ['index'],
    'Журнал'
];
$this->menu = [
    ['label' => 'Пользователи', 'url' => ['index']]
];
?>

<h1>Журнал шпионажа</h1>
<?php
$this->widget(
    'zii.widgets.grid.CGridView',
    array(
         'id' => 'items-grid',
         'dataProvider' => $model->search(),
         'filter' => $model,
         'columns' => array(
             'id',
             'user_id',
             'time',
             'method',
             'line',
             'description',
             array(
                 'class' => 'CButtonColumn',
                 'template' => '{view}',
             ),
         ),
    )
);
