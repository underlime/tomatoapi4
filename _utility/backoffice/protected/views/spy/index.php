<?php
/* @var $this StatusController */
/* @var $idsString array */
$this->breadcrumbs[] = 'Шпионаж';
$this->menu = [
    ['label' => 'Журнал', 'url' => ['log']]
];
?>

<h1>Шпионаж</h1>

<?=CHtml::beginForm()?>
<div class="form">
    <div class="row">
        <?=CHtml::label('Социальные ID целей', 'Spy_soc_net_ids')?>
        <div>Разделяйте ID запятыми, пробелами или переносами строк</div>
        <?=CHtml::textArea('SpyData[soc_net_ids]', $idsString, ['rows' => 10, 'cols' => 30])?>
    </div>
    <div class="row buttons">
        <?=CHtml::submitButton('Сохранить')?>
    </div>
</div>
<?=CHtml::endForm()?>
