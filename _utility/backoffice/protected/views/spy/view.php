<?php
/* @var $this Controller */
/* @var $model ARSpyData */

$this->breadcrumbs = [
    'Шпионаж' => ['index'],
    'Журнал' => ['log'],
    $model->id,
];
$this->menu = [
    ['label' => 'Пользователи', 'url' => ['index']],
    ['label' => 'Журнал', 'url' => ['log']],
];
$fileReference = str_replace(ROOT_PATH, '', $model->file);
?>

    <h1>Данные шпионажа #<?= $model->id ?></h1>

<?php
$this->widget(
    'zii.widgets.CDetailView',
    [
         'data' => $model,
         'attributes' => ['id', 'user_id', 'method', 'file', 'line', 'description'],
    ]
);
?>

<h4 style="margin-top: 40px">Подробности</h4>
<div>
    Код (последний релиз):
    <a href="https://bitbucket.org/underlime/tomatoapi4/src/HEAD<?=$fileReference?>?at=release#cl-<?=$model->line?>"
       target="_blank">
        <?=$fileReference?>: <?=$model->line?>
    </a>
</div>
<div style="margin-top: 10px">
    Значение:
    <pre><? print_r(json_decode($model->data, true)) ?></pre>
</div>
