<?php
/* @var $this DatesRangeForm */
/* @var $requestUrl string */
/* @var $requestParams array */
?>

<div class="dates-range" id="<?= $this->id ?>" style="margin-bottom: 40px">
    <h2>Временной интервал</h2>

    <?= CHtml::beginForm($requestUrl, 'GET') ?>
    <? foreach ($requestParams as $name => $val) {
        if ($name != 'start_date' && $name != 'stop_date')
            echo CHtml::hiddenField($name, $val);
    } ?>

    <span class="from">От</span>
    <span class="start">
        <? $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name' => 'start_date',
                                                                'value' => $this->startDate,
                                                                'options' => array(
                                                                    'dateFormat' => 'yy-mm-dd',
                                                                ),
                                                           )) ?>
    </span>
    <span class="to">до</span>
    <span class="stop">
        <? $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                                                'name' => 'stop_date',
                                                                'value' => preg_replace('/^(\d{4}-\d{2}-\d{2}).*$/', '$1', $this->stopDate),
                                                                'options' => array(
                                                                    'dateFormat' => 'yy-mm-dd',
                                                                ),
                                                           )) ?>
    </span>
    <span class="submit">
        <button type="submit">Изменить</button>
    </span>
    <?= CHtml::endForm() ?>
</div>
