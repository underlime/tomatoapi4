<?php
class DatesRangeForm extends CWidget
{
    public $startDate;
    public $stopDate;

    public function run()
    {
        $requestUrl = Yii::app()->baseUrl . '/index.php';
        $requestParams = empty($_GET) ? array() : $_GET;
        $this->render('view', array(
                                   'requestUrl' => $requestUrl,
                                   'requestParams' => $requestParams
                              ));
    }
}
