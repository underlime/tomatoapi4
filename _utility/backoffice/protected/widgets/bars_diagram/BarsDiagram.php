<?php
class BarsDiagram extends CWidget
{
    public $data;

    public function init()
    {
        parent::init();
        $baseUtl = Yii::app()->request->baseUrl;
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile($baseUtl.'/static-libs/flot/jquery.flot.min.js');
        Yii::app()->clientScript->registerScriptFile($baseUtl.'/static-libs/flot/jquery.flot.categories.min.js');
    }

    public function run()
    {
        $this->render('view');
    }
}
