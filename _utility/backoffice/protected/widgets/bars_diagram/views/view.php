<?php
/* @var $this BarsDiagram */
?>

<div id="plot-<?=$this->id?>" style="height: 300px"></div>
<div id="tooltip-<?=$this->id?>"></div>

<script>
    (function() {
        var data = (<?=json_encode($this->data)?>);
        var options = {
            'series': {
                'bars': {
                    'show': true,
                    'barWidth': 0.6,
                    'align': "center"
                }
            },
            'xaxis': {
                'mode': "categories",
                'tickLength': 0
            }
        };
        $.plot('#plot-<?=$this->id?>', data, options);
    })();
</script>

