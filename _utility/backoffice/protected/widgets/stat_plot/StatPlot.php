<?php
class StatPlot extends CWidget
{
    public $data;
    public $timeRange = 1209600000;

    public $minTime;
    public $trueMinTime;
    public $maxTime;

    public $maxValue;

    public function init()
    {
        parent::init();
        $baseUtl = Yii::app()->request->baseUrl;
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile($baseUtl.'/static-libs/flot/jquery.flot.min.js');
        Yii::app()->clientScript->registerScriptFile($baseUtl.'/static-libs/flot/jquery.flot.navigate.min.js');
        Yii::app()->clientScript->registerScriptFile($baseUtl.'/static-libs/flot/jquery.flot.time.min.js');
    }

    public function run()
    {
        foreach ($this->data as $dataSet) {
            $data = isset($dataSet['data']) ? $dataSet['data'] : $dataSet;
            foreach ($data as $record) {
                if ($this->minTime === null || $record[0] < $this->minTime)
                    $this->minTime = $record[0];

                if ($this->maxTime === null || $record[0] > $this->maxTime)
                    $this->maxTime = $record[0];

                if ($this->maxValue === null || $record[1] > $this->maxValue)
                    $this->maxValue = $record[1];
            }
        }

        $this->trueMinTime = $this->minTime;
        if ($this->maxTime - $this->minTime > $this->timeRange)
            $this->minTime = $this->maxTime - $this->timeRange;

        $this->maxValue += 500;
        $this->render('view');
    }
}
