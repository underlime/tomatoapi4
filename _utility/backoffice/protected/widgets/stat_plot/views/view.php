<?php
/* @var $this StatPlot */
?>

<div id="plot-<?=$this->id?>" style="height: 300px"></div>
<div id="tooltip-<?=$this->id?>"></div>

<script>
    (function() {
        var data = (<?=json_encode($this->data)?>);
        var options = {
            'series': {
                'lines': {
                    'show': true
                },
                'points': {
                    'show': true
                }
            },
            'xaxis': {
                'mode': 'time',
                'minTickSize': [1, 'day'],
                'timeformat': '%d.%m.%Y',
                'min': (<?=$this->minTime?>),
                'max': (<?=$this->maxTime?>),
                'panRange': [(<?=$this->trueMinTime?>), (<?=$this->maxTime?>)]
            },
            'yaxis': {
                'min': 0,
                'max': (<?=$this->maxValue?>),
                'panRange': [0, (<?=$this->maxValue?>)],
                'zoomRange': [0, 1]
            },
            'grid': {
                'hoverable': true
            },
            'zoom': {
                'interactive': true
            },
            'pan': {
                'interactive': true
            }
        };
        $.plot('#plot-<?=$this->id?>', data, options);

        $("#plot-<?=$this->id?>").bind("plothover", function (event, pos, item) {
            if (item) {
                var tooltip = $('#tooltip-<?=$this->id?>');
                tooltip.css({'position': 'absolute', 'top': item.pageY, 'left': item.pageX});
                tooltip.html(item.datapoint[1]);
            }
        });
    })();
</script>
