<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $parentId int */
/* @var $parentModel CActiveRecord */

//TODO: заменить на рабочий код
<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = array(
    'Родительский раздел' => array('ctrl/index'),
    \$parentModel->name => array('ctrl/view', 'id' => \$parentModel->id),
	'$label' => array('index', 'parent_id' => \$parentId),
	\$model->{$nameColumn} => array('view','id' => \$model->{$this->tableSchema->primaryKey}),
	'Изменить',
);\n";
?>

$this->menu = array(
	array('label' => 'Список', 'url' => array('index', 'parent_id' => $parentId)),
	array('label' => 'Создать', 'url' => array('create', 'parent_id' => $parentId)),
	array('label' => 'Свойства', 'url' => array('view', 'id' => $model-><?php echo $this->tableSchema->primaryKey; ?>)),
);
?>

<h1>Изменить</h1>

<?php echo "<? \$this->renderPartial('_form', array('model'=>\$model, 'parentModel' => \$parentModel)) ?>"; ?>
