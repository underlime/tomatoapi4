<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $parentId int */
/* @var $parentModel CActiveRecord */

//TODO: заменить на рабочий код
<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = array(
    'Родительский раздел' => array('ctrl/index'),
    \$parentModel->name => array('ctrl/view', 'id' => \$parentModel->id),
	'$label' => array('index', 'parent_id' => \$parentId),
	'Создать',
);\n";
?>

$this->menu = array(
	array('label' => 'Список', 'url' => array('index', 'parent_id' => $parentId)),
);
?>

<h1>Создать</h1>

<?php echo "<? \$this->renderPartial('_form', array('model' => \$model, 'parentModel' => \$parentModel)) ?>"; ?>
