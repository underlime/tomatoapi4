<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $parentId int */
/* @var $parentModel CActiveRecord */

//TODO: заменить на рабочий код
<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = array(
    'Родительский раздел' => array('ctrl/index'),
    \$parentModel->name => array('ctrl/view', 'id' => \$parentModel->id),
	'$label' => array('index', 'parent_id' => \$parentId),
	\$model->{$nameColumn},
);\n";
?>

$this->menu = array(
	array('label' => 'Список', 'url' => array('index', 'parent_id' => $parentId)),
	array('label' => 'Создать', 'url' => array('create', 'parent_id' => $parentId)),
	array('label' => 'Изменить', 'url' => array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label' => 'Удалить', 'url' => '#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Вы уверены, что хотите удалить объект?')),
);
?>

<h1>Свойства <?php echo $this->modelClass." #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<? "; ?> $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
<?php
foreach($this->tableSchema->columns as $column)
	echo "\t\t'".$column->name."',\n";
?>
	),
)) ?>
