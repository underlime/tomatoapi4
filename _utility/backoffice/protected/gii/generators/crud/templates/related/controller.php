<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
	public $layout = '//layouts/column2';

    private $parentId;

    /**
    * @var CActiveRecord
    */
    private $parentModel;

    /**
    * Lists all models.
    */
    public function actionIndex()
    {
        $this->_getParentModelFromRequest();

        //TODO: заменить поля
        $params = array(
            'criteria' => array(
                'condition' => 'parent_id=:parent_id',
                'params' => array(
                    ':parent_id' => $this->parentId,
                ),
            ),
        );
        $dataProvider = new CActiveDataProvider('<?php echo $this->modelClass; ?>', $params);
        $this->render('index',array(
            'dataProvider' => $dataProvider,
            'parentId' => $this->parentId,
            'parentModel' => $this->parentModel,
        ));
    }

    private function _getParentModelFromRequest()
    {
        //TODO: заменить на рабочий код
        $this->parentId = Yii::app()->request->getQuery('parent_id');
        if (!$this->parentId)
            throw new CHttpException(400, 'Необходимо указать parent_id');
        $this->parentModel = ParentModel::model()->findByPk($this->parentId);
        if (!$this->parentModel)
            throw new CHttpException(404, 'Родительский элемент не найден');
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        $this->_getParentModelFromModel($model);

		$this->render('view',array(
			'model' => $model,
            'parentId' => $this->parentId,
            'parentModel' => $this->parentModel,
		));
	}

    private function _getParentModelFromModel(CActiveRecord $model)
    {
        //TODO: заменить на рабочий код
        $this->parentId = $model->parent_id;
        if (!$this->parentId)
            throw new CHttpException(500, 'Необходимо указать parent_id');
        $this->parentModel = $model->parentModel;
        if (!$this->parentModel)
            throw new CHttpException(500, 'Родительский элемент не найден');
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->_getParentModelFromRequest();

		$model = new <?php echo $this->modelClass; ?>();
        //TODO: заменить на рабочий код
        $model->parent_id = $this->parentId;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>'])) {
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
			if($model->save())
				$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
		}

		$this->render('create',array(
			'model' => $model,
            'parentId' => $this->parentId,
            'parentModel' => $this->parentModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $this->_getParentModelFromModel($model);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>'])) {
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];
			if($model->save())
				$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
		}

		$this->render('update',array(
			'model' => $model,
            'parentId' => $this->parentId,
            'parentModel' => $this->parentModel,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        //TODO: заменить на рабочий код
        $parentId = $model->parent_id;
        $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('index', 'parent_id' => $parentId));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return <?php echo $this->modelClass; ?> the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = <?php echo $this->modelClass; ?>::model()->findByPk($id);
		if($model === NULL)
			throw new CHttpException(404, 'The requested page does not exist');

        return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param <?php echo $this->modelClass; ?> $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='<?php echo $this->class2id($this->modelClass); ?>-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
