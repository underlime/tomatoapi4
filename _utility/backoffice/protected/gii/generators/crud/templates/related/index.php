<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $dataProvider CActiveDataProvider */
/* @var $parentId int */
/* @var $parentModel CActiveRecord */

//TODO: заменить на рабочий код
<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = array(
    'Родительский раздел' => array('ctrl/index'),
    \$parentModel->name => array('ctrl/view', 'id' => \$parentModel->id),
	'$label',
);\n";
?>

$this->menu = array(
	array('label' => 'Создать', 'url' => array('create', 'parent_id' => $parentId)),
);
?>

<h1><?php echo $label; ?></h1>

<?php echo "<? "; ?> $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)) ?>
