<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs = array(
	'$label' => array('index'),
	\$model->{$nameColumn} => array('view','id' => \$model->{$this->tableSchema->primaryKey}),
	'Изменить',
);\n";
?>

$this->menu = array(
	array('label' => 'Список', 'url' => array('index')),
	array('label' => 'Создать', 'url' => array('create')),
	array('label' => 'Свойства', 'url' => array('view', 'id' => $model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label' => 'Управление', 'url' => array('admin')),
);
?>

<h1>Изменить</h1>

<?php echo "<? \$this->renderPartial('_form', array('model'=>\$model)) ?>"; ?>
