<?php

/**
 * This is the model class for table "users_level_params".
 *
 * The followings are the available columns in table 'users_level_params':
 * @property string $user_id
 * @property string $strength
 * @property string $agility
 * @property string $intellect
 * @property string $max_hp
 * @property string $level
 * @property string $max_level
 * @property string $points_learning
 * @property string $items_slots_avaible
 * @property integer $social_slots_avaible
 * @property integer $skill_resets_count
 * @property integer $ring_slots_avaible
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class UsersLevelParams extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_level_params';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('social_slots_avaible, skill_resets_count, ring_slots_avaible', 'numerical', 'integerOnly'=>true),
			array('user_id, strength, agility, intellect, max_hp, level, max_level, points_learning, items_slots_avaible', 'length', 'max'=>10),
			// The following rule is used by search().
			array('user_id, strength, agility, intellect, max_hp, level, max_level, points_learning, items_slots_avaible, social_slots_avaible, skill_resets_count, ring_slots_avaible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'strength' => 'Strength',
			'agility' => 'Agility',
			'intellect' => 'Intellect',
			'max_hp' => 'Max Hp',
			'level' => 'Level',
			'max_level' => 'Max Level',
			'points_learning' => 'Points Learning',
			'items_slots_avaible' => 'Items Slots Avaible',
			'social_slots_avaible' => 'Social Slots Avaible',
			'skill_resets_count' => 'Skill Resets Count',
			'ring_slots_avaible' => 'Ring Slots Avaible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('strength',$this->strength,true);
		$criteria->compare('agility',$this->agility,true);
		$criteria->compare('intellect',$this->intellect,true);
		$criteria->compare('max_hp',$this->max_hp,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('max_level',$this->max_level,true);
		$criteria->compare('points_learning',$this->points_learning,true);
		$criteria->compare('items_slots_avaible',$this->items_slots_avaible,true);
		$criteria->compare('social_slots_avaible',$this->social_slots_avaible);
		$criteria->compare('skill_resets_count',$this->skill_resets_count);
		$criteria->compare('ring_slots_avaible',$this->ring_slots_avaible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersLevelParams the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
