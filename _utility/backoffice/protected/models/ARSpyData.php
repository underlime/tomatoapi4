<?php

/**
 * This is the model class for table "spy_data".
 *
 * The followings are the available columns in table 'spy_data':
 * @property integer $id
 * @property string $user_id
 * @property string $file
 * @property string $method
 * @property integer $line
 * @property string $description
 * @property string $data
 * @property string $time
 */
class ARSpyData extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'spy_data';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('line', 'numerical', 'integerOnly' => true),
            array('user_id, description', 'length', 'max' => 255),
            array('file', 'length', 'max' => 128),
            array('method', 'length', 'max' => 32),
            array('data, time', 'safe'),
            // The following rule is used by search().
            array('id, user_id, file, method, line, description, data, time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'file' => 'File',
            'method' => 'Method',
            'line' => 'Line',
            'description' => 'Description',
            'data' => 'Data',
            'time' => 'Time',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id, false);
        $criteria->compare('file', $this->file, true);
        $criteria->compare('method', $this->method, true);
        $criteria->compare('line', $this->line);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('data', $this->data, true);
        $criteria->compare('time', $this->time, true);
        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, [
                                              'criteria' => $criteria,
                                              'pagination' => [
                                                  'pageSize' => 500,
                                              ],
                                              ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ARSpyData the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
