<?php

/**
 * This is the model class for table "items_inventory".
 *
 * The followings are the available columns in table 'items_inventory':
 * @property string $user_id
 * @property string $item_id
 * @property string $count
 * @property string $broken_count
 * @property string $put
 * @property string $social_put
 * @property string $hp
 *
 * @property \Items $item
 * @property \Users $user
 */
class ItemsInventory extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ItemsInventory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'items_inventory';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, item_id', 'required'),
            array('user_id, item_id, count, broken_count, put, social_put, hp', 'length', 'max' => 10),
            array('user_id, item_id, count, broken_count, put, social_put, hp', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Items', 'item_id'),
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'User',
            'item_id' => 'Item',
            'count' => 'Count',
            'broken_count' => 'Broken Count',
            'put' => 'Put',
            'social_put' => 'Social Put',
            'hp' => 'Hp',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('item_id', $this->item_id, true);
        $criteria->compare('count', $this->count, true);
        $criteria->compare('broken_count', $this->broken_count, true);
        $criteria->compare('put', $this->put, true);
        $criteria->compare('social_put', $this->social_put, true);
        $criteria->compare('hp', $this->hp, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }
}
