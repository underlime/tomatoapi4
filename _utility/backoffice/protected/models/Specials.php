<?php

/**
 * This is the model class for table "specials".
 *
 * The followings are the available columns in table 'specials':
 * @property string $special_id
 * @property string $code
 * @property string $name_ru
 * @property string $description_ru
 * @property string $picture
 * @property string $animation
 * @property integer $active
 * @property string $bonuses
 * @property double $k_damage
 * @property string $steps
 * @property string $additional_data
 * @property string $intellect_level
 * @property string $level_intellect_step
 * @property double $level_k_bonuse
 *
 * The followings are the available model relations:
 * @property Users[] $users
 */
class Specials extends ActiveRecord
{
    public function __construct($scenario = 'insert')
    {
        $res = parent::__construct($scenario);
        $this->bonuses = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <damage>0</damage>
        <armor>0</armor>
        <speed>0</speed>
        <act_again>0</act_again>
        <dodge>0</dodge>
        <crit>0</crit>
        <block>0</block>
        <special>0</special>
    </fight>
    <scale>
        <hp>0</hp>
    </scale>
    <apply>
        <energy>0</energy>
        <hp>0</hp>
    </apply>
</bonuses>
XML;
        return $res;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Specials the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'specials';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('code, name_ru, description_ru, picture, animation, additional_data', 'required'),
            array('active', 'numerical', 'integerOnly' => true),
            array('k_damage, level_k_bonuse', 'numerical'),
            array('code, name_ru, picture, animation', 'length', 'max' => 32),
            array('description_ru', 'length', 'max' => 140),
            array('steps, intellect_level, level_intellect_step', 'length', 'max' => 10),
            array('additional_data', 'length', 'max' => 64),
            array('bonuses', 'safe'),
            // The following rule is used by search().
            array('special_id, code, name_ru, description_ru, picture, animation, active, bonuses, k_damage, steps, additional_data, intellect_level, level_intellect_step, level_k_bonuse',
                  'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users' => array(self::MANY_MANY, 'Users', 'specials_in_use(special_id, user_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'special_id' => 'Special',
            'code' => 'Код',
            'name_ru' => 'Название (Ru)',
            'description_ru' => 'Описание (Ru)',
            'picture' => 'Picture',
            'animation' => 'Animation',
            'active' => 'Активный',
            'bonuses' => 'Бонусы',
            'k_damage' => 'K Damage',
            'steps' => 'Кол-во шагов',
            'additional_data' => 'Additional Data',
            'intellect_level' => 'Intellect Level',
            'level_intellect_step' => 'Level Intellect Step',
            'level_k_bonuse' => 'Level K Bonuse',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('special_id', $this->special_id, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('name_ru', $this->name_ru, true);
        $criteria->compare('description_ru', $this->description_ru, true);
        $criteria->compare('picture', $this->picture, true);
        $criteria->compare('animation', $this->animation, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('bonuses', $this->bonuses, true);
        $criteria->compare('k_damage', $this->k_damage);
        $criteria->compare('steps', $this->steps, true);
        $criteria->compare('additional_data', $this->additional_data, true);
        $criteria->compare('intellect_level', $this->intellect_level, true);
        $criteria->compare('level_intellect_step', $this->level_intellect_step, true);
        $criteria->compare('level_k_bonuse', $this->level_k_bonuse);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }
}
