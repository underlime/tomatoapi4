<?php

/**
 * This is the model class for table "achievements".
 *
 * The followings are the available columns in table 'achievements':
 * @property string $achievement_id
 * @property string $code
 * @property string $name_ru
 * @property string $text_ru
 * @property string $pic
 * @property string $glory
 * @property string $wins_level
 * @property string $fails_level
 * @property string $ferros_payed
 * @property string $tomatos_payed
 * @property string $visits_level
 * @property string $level_reached
 * @property string $strength_level
 * @property string $agility_level
 * @property string $intellect_level
 *
 * The followings are the available model relations:
 * @property Users[] $users
 */
class Achievements extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Achievements the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'achievements';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('code, name_ru, text_ru, pic', 'required'),
            array('code', 'length', 'max' => 32),
            array('name_ru, pic', 'length', 'max' => 64),
            array('text_ru', 'length', 'max' => 512),
            array('glory, wins_level, fails_level, ferros_payed, tomatos_payed, visits_level, level_reached, strength_level, agility_level, intellect_level',
                  'length', 'max' => 10),
            array('achievement_id, code, name_ru, text_ru, pic, glory, wins_level, fails_level, ferros_payed, tomatos_payed, visits_level, level_reached, strength_level, agility_level, intellect_level',
                  'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'users' => array(self::MANY_MANY, 'Users', 'achievements_gotten(achievement_id, user_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'achievement_id' => 'Achievement',
            'code' => 'Код',
            'name_ru' => 'Название (Ru)',
            'text_ru' => 'Текст (Ru)',
            'pic' => 'Картинка',
            'glory' => 'Слава',
            'wins_level' => 'Wins Level',
            'fails_level' => 'Fails Level',
            'ferros_payed' => 'Ferros Payed',
            'tomatos_payed' => 'Tomatos Payed',
            'visits_level' => 'Visits Level',
            'level_reached' => 'Level Reached',
            'strength_level' => 'Strength Level',
            'agility_level' => 'Agility Level',
            'intellect_level' => 'Intellect Level',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('achievement_id', $this->achievement_id, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('name_ru', $this->name_ru, true);
        $criteria->compare('text_ru', $this->text_ru, true);
        $criteria->compare('pic', $this->pic, true);
        $criteria->compare('glory', $this->glory, true);
        $criteria->compare('wins_level', $this->wins_level, true);
        $criteria->compare('fails_level', $this->fails_level, true);
        $criteria->compare('ferros_payed', $this->ferros_payed, true);
        $criteria->compare('tomatos_payed', $this->tomatos_payed, true);
        $criteria->compare('visits_level', $this->visits_level, true);
        $criteria->compare('level_reached', $this->level_reached, true);
        $criteria->compare('strength_level', $this->strength_level, true);
        $criteria->compare('agility_level', $this->agility_level, true);
        $criteria->compare('intellect_level', $this->intellect_level, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    protected function beforeValidate()
    {
        $nullifyFields = array('wins_level', 'fails_level', 'ferros_payed', 'tomatos_payed',
                               'visits_level', 'level_reached', 'strength_level', 'agility_level',
                               'intellect_level');
        foreach ($nullifyFields as $field) {
            if (!$this->$field)
                $this->$field = null;
        }
        return parent::beforeValidate();
    }
}
