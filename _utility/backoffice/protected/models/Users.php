<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $user_id
 * @property string $soc_net_id
 * @property string $referrer
 * @property string $city
 * @property string $login
 * @property integer $ban_level
 * @property string $ban_reason
 * @property string $vegetable
 * @property string $body
 * @property string $hands
 * @property string $eyes
 * @property string $mouth
 * @property string $hair
 * @property string $reg_time
 * @property integer $was_friends_prizes_given
 * @property string $album_id
 * @property string $referrer_type
 *
 * The followings are the available model relations:
 * @property Achievements[] $achievements
 * @property Gifts[] $giftsReceived
 * @property Gifts[] $giftsSent
 * @property Items[] $items
 * @property Specials[] $specials
 * @property UserNews[] $userNews
 * @property UsersBuyMoneyLog[] $usersBuyMoneyLog
 * @property UsersPayLog[] $usersPayLog
 * @property UsersFightData $usersFightData
 * @property UsersLevelParams[] $usersLevelParams
 */
class Users extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('soc_net_id, login, vegetable, body, hands, eyes, mouth, hair, reg_time', 'required'),
            array('ban_level, was_friends_prizes_given', 'numerical', 'integerOnly' => true),
            array('soc_net_id, referrer, vegetable, body, hands, eyes, mouth, hair, album_id, referrer_type', 'length', 'max' => 32),
            array('city', 'length', 'max' => 10),
            array('login', 'length', 'max' => 20),
            array('ban_reason', 'length', 'max' => 140),
            // The following rule is used by search().
            array('user_id, soc_net_id, referrer, city, login, ban_level, ban_reason, vegetable, body, hands, eyes, mouth, hair, reg_time, was_friends_prizes_given, album_id, referrer_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'achievements' => array(self::MANY_MANY, 'Achievements', 'achievements_gotten(user_id, achievement_id)'),
            'giftsReceived' => array(self::HAS_MANY, 'Gifts', 'receiver_id'),
            'giftsSent' => array(self::HAS_MANY, 'Gifts', 'sender_id'),
            'items' => array(self::MANY_MANY, 'Items', 'items_inventory(user_id, item_id)'),
            'specials' => array(self::MANY_MANY, 'Specials', 'specials_in_use(user_id, special_id)'),
            'userNews' => array(self::MANY_MANY, 'UserNews', 'user_news_read(user_id, news_id)'),
            'usersFightData' => array(self::HAS_ONE, 'UsersFightData', 'user_id'),
            'usersJournals' => array(self::HAS_MANY, 'UsersJournal', 'user_id'),
            'usersLevelParams' => array(self::HAS_MANY, 'UsersLevelParams', 'user_id'),
            'usersBuyMoneyLog' => array(self::HAS_MANY, 'UsersBuyMoneyLog', 'user_id'),
            'usersPayLog' => array(self::HAS_MANY, 'UsersPayLog', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'User',
            'soc_net_id' => 'Soc Net',
            'referrer' => 'Referrer',
            'city' => 'City',
            'login' => 'Login',
            'ban_level' => 'Ban Level',
            'ban_reason' => 'Ban Reason',
            'vegetable' => 'Vegetable',
            'body' => 'Body',
            'hands' => 'Hands',
            'eyes' => 'Eyes',
            'mouth' => 'Mouth',
            'hair' => 'Hair',
            'reg_time' => 'Reg Time',
            'was_friends_prizes_given' => 'Was Friends Prizes Given',
            'album_id' => 'Album',
            'referrer_type' => 'Referrer Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('soc_net_id', $this->soc_net_id, true);
        $criteria->compare('referrer', $this->referrer, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('ban_level', $this->ban_level);
        $criteria->compare('ban_reason', $this->ban_reason, true);
        $criteria->compare('vegetable', $this->vegetable, true);
        $criteria->compare('body', $this->body, true);
        $criteria->compare('hands', $this->hands, true);
        $criteria->compare('eyes', $this->eyes, true);
        $criteria->compare('mouth', $this->mouth, true);
        $criteria->compare('hair', $this->hair, true);
        $criteria->compare('reg_time', $this->reg_time, true);
        $criteria->compare('was_friends_prizes_given', $this->was_friends_prizes_given);
        $criteria->compare('album_id', $this->album_id, true);
        $criteria->compare('referrer_type', $this->referrer_type, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
