<?php

/**
 * This is the model class for table "items".
 *
 * The followings are the available columns in table 'items':
 * @property string $item_id
 * @property string $arrangement_variant
 * @property string $rubric_code
 * @property string $name_ru
 * @property string $picture
 * @property string $price_ferros
 * @property string $price_tomatos
 * @property string $sell_price_ferros
 * @property string $sell_price_tomatos
 * @property string $buy_another
 * @property string $buy_another_at_once
 * @property string $bonuses
 * @property string $apply_in_fight
 * @property string $fight_damage
 * @property integer $heal_percents
 * @property string $max_hp
 * @property integer $no_wear
 * @property integer $new
 * @property integer $sale
 * @property integer $show_in_market
 * @property integer $sort
 * @property string $gift_type
 * @property integer $required_level
 * @property string $description_ru
 *
 * The followings are the available model relations:
 * @property ItemsArrangement $arrangementVariant
 * @property ItemsRubrics $rubricCode
 * @property Users[] $users
 */
class Items extends ActiveRecord
{
    public static $APPLY_IN_FIGHT = array('none', 'med_low', 'med_mid', 'med_high', 'grenade');
    public static $GIFT_TYPE = array('usual', 'improved');

    public function __construct($scenario = 'insert')
    {
        $res = parent::__construct($scenario);
        $this->bonuses = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <damage>0</damage>
        <armor>0</armor>
        <speed>0</speed>
        <act_again>0</act_again>
        <dodge>0</dodge>
        <crit>0</crit>
        <block>0</block>
        <special>0</special>
    </fight>
    <scale>
        <hp>0</hp>
    </scale>
    <apply>
        <energy>0</energy>
        <hp>0</hp>
    </apply>
</bonuses>
XML;
        return $res;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Items the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'items';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('arrangement_variant, rubric_code, name_ru, picture, bonuses, sort', 'required'),
            array('heal_percents, no_wear, new, sale, show_in_market, sort, required_level', 'numerical',
                  'integerOnly' => true),
            array('arrangement_variant, rubric_code, picture', 'length', 'max' => 32),
            array('name_ru', 'length', 'max' => 40),
            array('price_ferros, price_tomatos, sell_price_ferros, sell_price_tomatos, buy_another, buy_another_at_once, fight_damage, max_hp',
                  'length', 'max' => 10),
            array('apply_in_fight, gift_type', 'length', 'max' => 8),
            array('description_ru', 'safe'),
            array('item_id, arrangement_variant, rubric_code, name_ru, picture, price_ferros, price_tomatos, sell_price_ferros, sell_price_tomatos, buy_another, buy_another_at_once, bonuses, apply_in_fight, fight_damage, heal_percents, max_hp, no_wear, new, sale, show_in_market, sort, gift_type, required_level, description_ru',
                  'safe', 'on' => 'search'),
            array('apply_in_fight', 'in', 'range' => self::$APPLY_IN_FIGHT),
            array('apply_in_fight', 'default', 'value' => 'none'),
            array('gift_type', 'in', 'range' => self::$GIFT_TYPE, 'allowEmpty' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'arrangementVariant' => array(self::BELONGS_TO, 'ItemsArrangement', 'arrangement_variant'),
            'rubricCode' => array(self::BELONGS_TO, 'ItemsRubrics', 'rubric_code'),
            'users' => array(self::MANY_MANY, 'Users', 'items_inventory(item_id, user_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'item_id' => 'Item',
            'arrangement_variant' => 'Вариант экипировки',
            'rubric_code' => 'Рубрика',
            'name_ru' => 'Название (RU)',
            'picture' => 'Картинка',
            'price_ferros' => 'Цена, F',
            'price_tomatos' => 'Цена, T',
            'sell_price_ferros' => 'Цена продажи, F',
            'sell_price_tomatos' => 'Цена продажи, T',
            'buy_another' => 'Id другого (для ящиков)',
            'buy_another_at_once' => 'Количество другого (для ящиков)',
            'bonuses' => 'Бонусы',
            'apply_in_fight' => 'Применение в бою',
            'fight_damage' => 'Урон в бою',
            'heal_percents' => 'Лечение, %',
            'new' => 'New',
            'sale' => 'Sale',
            'show_in_market' => 'Показывать на рынке',
            'sort' => 'Сортировка',
            'gift_type' => 'Тип подарка (для подарков)',
            'required_level' => 'Необходимый уровень',
            'description_ru' => 'Описание (RU)',
            'max_hp' => 'Макс. здоровье',
            'no_wear' => 'Без износа',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('item_id', $this->item_id, true);
        $criteria->compare('arrangement_variant', $this->arrangement_variant, true);
        $criteria->compare('rubric_code', $this->rubric_code, true);
        $criteria->compare('name_ru', $this->name_ru, true);
        $criteria->compare('picture', $this->picture, true);
        $criteria->compare('price_ferros', $this->price_ferros, true);
        $criteria->compare('price_tomatos', $this->price_tomatos, true);
        $criteria->compare('sell_price_ferros', $this->sell_price_ferros, true);
        $criteria->compare('sell_price_tomatos', $this->sell_price_tomatos, true);
        $criteria->compare('buy_another', $this->buy_another, true);
        $criteria->compare('buy_another_at_once', $this->buy_another_at_once, true);
        $criteria->compare('bonuses', $this->bonuses, true);
        $criteria->compare('apply_in_fight', $this->apply_in_fight, true);
        $criteria->compare('fight_damage', $this->fight_damage, true);
        $criteria->compare('heal_percents', $this->heal_percents);
        $criteria->compare('max_hp', $this->max_hp, true);
        $criteria->compare('no_wear', $this->no_wear);
        $criteria->compare('new', $this->new);
        $criteria->compare('sale', $this->sale);
        $criteria->compare('show_in_market', $this->show_in_market);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('gift_type', $this->gift_type, true);
        $criteria->compare('required_level', $this->required_level);
        $criteria->compare('description_ru', $this->description_ru, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    protected function beforeValidate()
    {
        if (empty($this->gift_type))
            $this->gift_type = null;
        return parent::beforeValidate();
    }
}
