<?php
class UserBanForm extends CFormModel
{
    public $soc_net_id = null;
    public $ban_level = 0;
    public $ban_reason = 'Снять бан';

    public function rules()
    {
        return array(
            array('soc_net_id, ban_level', 'numerical', 'integerOnly' => true),
            array('ban_reason', 'length', 'max' => 140),
            array('soc_net_id', 'exist', 'attributeName' => 'soc_net_id', 'className' => '\Users',
                  'message' => 'Такой пользователь не зарегистрирован'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'soc_net_id' => 'Id в соц. сети',
            'ban_level' => 'Уровень бана',
            'ban_reason' => 'Причина бана',
        );
    }

    public function banUser()
    {
        if ($this->validate()) {
            $user = Users::model()->findByAttributes(array('soc_net_id' => $this->soc_net_id));
            $user->ban_level = $this->ban_level;
            $user->ban_reason = $this->ban_reason;
            $res = $user->save();
            if ($res)
                CacheRemover::clearByKey("user_info_{$this->soc_net_id}");
            return $res;
        }
        return false;
    }
}
