<?php

class UserMoneyForm extends CFormModel
{
    public $soc_net_id = null;
    public $ferros = 0;
    public $tomatos = 0;

    public function rules()
    {
        return array(
            array('soc_net_id, ferros, tomatos', 'numerical', 'integerOnly' => true),
            array('soc_net_id', 'required'),
            array('soc_net_id', 'exist', 'attributeName' => 'soc_net_id', 'className' => '\Users',
                'message' => 'Такой пользователь не зарегистрирован'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'soc_net_id' => 'Id в соц. сети',
            'ferros' => 'Ферросы',
            'tomatos' => 'Томатосы',
        );
    }

    public function updateUserMoney()
    {
        if ($this->validate()) {
            $model = Users::model()->findByAttributes(array('soc_net_id' => $this->soc_net_id));
            if ($model == null)
                throw new CHttpException(404, 'User not found');

            $model->usersFightData->ferros += $this->ferros;
            $model->usersFightData->tomatos += $this->tomatos;
            $res = $model->usersFightData->save();

            if ($res)
                CacheRemover::clearByKey("user_fight_data_{$model->user_id}");
            return $res;
        }
        return false;
    }
}
