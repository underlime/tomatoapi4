<?php

/**
 * This is the model class for table "users_stat".
 *
 * The followings are the available columns in table 'users_stat':
 * @property string $user_id
 * @property string $visits
 * @property string $last_visit
 * @property string $ferros_payed
 * @property string $tomatos_payed
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class UsersStat extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users_stat';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, last_visit', 'required'),
            array('user_id, visits, ferros_payed, tomatos_payed', 'length', 'max' => 10),
            array('user_id, visits, last_visit, ferros_payed, tomatos_payed', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'User',
            'visits' => 'Visits',
            'last_visit' => 'Last Visit',
            'ferros_payed' => 'Ferros Payed',
            'tomatos_payed' => 'Tomatos Payed',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('visits', $this->visits, true);
        $criteria->compare('last_visit', $this->last_visit, true);
        $criteria->compare('ferros_payed', $this->ferros_payed, true);
        $criteria->compare('tomatos_payed', $this->tomatos_payed, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UsersStat the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
