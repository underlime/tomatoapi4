<?php

/**
 * This is the model class for table "user_news".
 *
 * The followings are the available columns in table 'user_news':
 * @property integer $id
 * @property string $user_id
 * @property string $time
 * @property string $expires_time
 * @property integer $news_type
 * @property string $additional_data
 * @property string $like_reward_type
 * @property integer $like_reward_count
 * @property string $like_reward_data
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Users[] $users
 */
class UserNews extends ActiveRecord
{
    public static $NEWS_TYPES = array(
        0 => 'Просто новость',
        1 => 'Стартовый подарок',
        2 => 'Новый друг',
    );

    public static $LIKE_REWARDS = array(
        '' => 'Ничего',
        'tomatos' => 'Томатосы',
        'ferros' => 'Ферросы',
        'item' => 'Предмет',
    );

    public function __construct($scenario = 'insert')
    {
        $result = parent::__construct($scenario);
        $this->news_type = '0';
        $this->like_reward_type = 'tomatos';
        $this->like_reward_count = '50';
        return $result;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserNews the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('news_type, like_reward_count', 'numerical', 'integerOnly' => true),
            array('user_id', 'length', 'max' => 10),
            array('like_reward_type', 'length', 'max' => 7),
            array('like_reward_data', 'length', 'max' => 32),
            array('time, expires_time, additional_data', 'safe'),
            array(
                'id, user_id, time, expires_time, news_type, additional_data, like_reward_type, like_reward_count, like_reward_data',
                'safe', 'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
            'users' => array(self::MANY_MANY, 'Users', 'user_news_read(news_id, user_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'Id пользователя в базе',
            'time' => 'Время в UTC',
            'expires_time' => 'Время истечения срока в UTC',
            'news_type' => 'Тип новости',
            'additional_data' => 'Данные',
            'like_reward_type' => 'Награда за лайк',
            'like_reward_count' => 'Количество награды за лайк',
            'like_reward_data' => 'Данные награды за лайк',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('time', $this->time, true);
        $criteria->compare('expires_time', $this->expires_time, true);
        $criteria->compare('news_type', $this->news_type);
        $criteria->compare('additional_data', $this->additional_data, true);
        $criteria->compare('like_reward_type', $this->like_reward_type, true);
        $criteria->compare('like_reward_count', $this->like_reward_count);
        $criteria->compare('like_reward_data', $this->like_reward_data, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    protected function beforeValidate()
    {
        if (!$this->user_id) {
            $this->user_id = null;
        }
        if (!$this->time) {
            $this->time = null;
        }
        if (!$this->expires_time) {
            $this->expires_time = null;
        }
        if (!$this->like_reward_type) {
            $this->like_reward_type = null;
        }
        return parent::beforeValidate();
    }
}
