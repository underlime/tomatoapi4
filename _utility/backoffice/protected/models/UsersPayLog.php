<?php

/**
 * This is the model class for table "users_pay_log".
 *
 * The followings are the available columns in table 'users_pay_log':
 * @property string $user_id
 * @property string $pay_id
 * @property string $time
 * @property string $ferros
 * @property string $tomatos
 * @property string $comment
 * @property string $count
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class UsersPayLog extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users_pay_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, time', 'required'),
			array('user_id, ferros, tomatos, count', 'length', 'max'=>10),
			array('comment', 'length', 'max'=>140),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, pay_id, time, ferros, tomatos, comment, count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'pay_id' => 'Pay',
			'time' => 'Time',
			'ferros' => 'Ferros',
			'tomatos' => 'Tomatos',
			'comment' => 'Comment',
			'count' => 'Count',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('pay_id',$this->pay_id,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('ferros',$this->ferros,true);
		$criteria->compare('tomatos',$this->tomatos,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('count',$this->count,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersPayLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
