<?php
class DbData
{
    const DEFAULT_DB = 'db';

    public static $databases = array(
        array(
            'id' => 1,
            'name' => 'vk.com',
            'code' => 'db'
        ),
        array(
            'id' => 2,
            'name' => 'odnoklassniki.ru',
            'code' => 'db_ok'
        ),
        array(
            'id' => 3,
            'name' => 'my.mail.ru',
            'code' => 'db_mm'
        ),
    );

    /**
     * Проверить код
     * @param $code
     * @throws CHttpException
     */
    public static function validateCode($code)
    {
        $valid = false;
        foreach (self::$databases as $record) {
            if ($code == $record['code']) {
                $valid = true;
                break;
            }
        }
        if ($valid == false) {
            throw new CHttpException(400, 'Указан неправильный город');
        }
    }

    /**
     * Извлечь базу по id
     * @param int $id
     * @return array
     * @throws Exception
     */
    public static function getDbById($id)
    {
        foreach (self::$databases as $record) {
            if ($record['id'] == $id) {
                return $record;
            }
        }
        throw new InvalidArgumentException("$id not found");
    }

    /**
     * Извлечь базу по коду
     * @param string $code
     * @return array
     * @throws Exception
     */
    public static function getDbByCode($code)
    {
        foreach (self::$databases as $record) {
            if ($record['code'] == $code) {
                return $record;
            }
        }
        throw new InvalidArgumentException("$code not found");
    }

    /**
     * Вернуть массив пар код => имя
     * @return array
     */
    public static function getPairs()
    {
        $data = array();
        foreach (self::$databases as $record) {
            $data[$record["code"]] = $record["name"];
        }
        return $data;
    }
}
