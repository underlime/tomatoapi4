<?php

/**
 * This is the model class for table "users_fight_data".
 *
 * The followings are the available columns in table 'users_fight_data':
 * @property string $user_id
 * @property string $hp
 * @property string $glory
 * @property string $energy
 * @property string $max_energy
 * @property string $energy_time
 * @property string $experience
 * @property integer $ferros
 * @property integer $tomatos
 * @property string $battles_count
 * @property string $wins_count
 * @property string $day_glory
 * @property string $week_glory
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class UsersFightData extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users_fight_data';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, energy_time', 'required'),
            array('ferros, tomatos', 'numerical', 'integerOnly' => true),
            array('user_id, hp, glory, energy, max_energy, experience, battles_count, wins_count, day_glory, week_glory', 'length', 'max' => 10),
            // The following rule is used by search().
            array('user_id, hp, glory, energy, max_energy, energy_time, experience, ferros, tomatos, battles_count, wins_count, day_glory, week_glory', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'User',
            'hp' => 'Hp',
            'glory' => 'Glory',
            'energy' => 'Energy',
            'max_energy' => 'Max Energy',
            'energy_time' => 'Energy Time',
            'experience' => 'Experience',
            'ferros' => 'Ferros',
            'tomatos' => 'Tomatos',
            'battles_count' => 'Battles Count',
            'wins_count' => 'Wins Count',
            'day_glory' => 'Day Glory',
            'week_glory' => 'Week Glory',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('hp', $this->hp, true);
        $criteria->compare('glory', $this->glory, true);
        $criteria->compare('energy', $this->energy, true);
        $criteria->compare('max_energy', $this->max_energy, true);
        $criteria->compare('energy_time', $this->energy_time, true);
        $criteria->compare('experience', $this->experience, true);
        $criteria->compare('ferros', $this->ferros);
        $criteria->compare('tomatos', $this->tomatos);
        $criteria->compare('battles_count', $this->battles_count, true);
        $criteria->compare('wins_count', $this->wins_count, true);
        $criteria->compare('day_glory', $this->day_glory, true);
        $criteria->compare('week_glory', $this->week_glory, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UsersFightData the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
