<?php

/**
 * This is the model class for table "gifts".
 *
 * The followings are the available columns in table 'gifts':
 * @property string $gift_id
 * @property string $sender_id
 * @property string $receiver_id
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Users $receiver
 * @property Users $sender
 */
class Gifts extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'gifts';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sender_id, receiver_id', 'required'),
            array('sender_id, receiver_id', 'length', 'max' => 10),
            array('date', 'safe'),
            // The following rule is used by search().
            array('gift_id, sender_id, receiver_id, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'receiver' => array(self::BELONGS_TO, 'Users', 'receiver_id'),
            'sender' => array(self::BELONGS_TO, 'Users', 'sender_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'gift_id' => 'Gift',
            'sender_id' => 'Sender',
            'receiver_id' => 'Receiver',
            'date' => 'Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('gift_id', $this->gift_id, true);
        $criteria->compare('sender_id', $this->sender_id, true);
        $criteria->compare('receiver_id', $this->receiver_id, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
                                                   'criteria' => $criteria,
                                              ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Gifts the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
