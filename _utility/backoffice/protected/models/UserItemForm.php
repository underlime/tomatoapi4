<?php

class UserItemForm extends CFormModel
{
    public $soc_net_id = null;
    public $item_id = 0;
    public $count = 1;

    public function rules()
    {
        return array(
            array('soc_net_id, item_id, count', 'numerical', 'integerOnly' => true),
            array('soc_net_id, item_id, count', 'required'),
            array('soc_net_id', 'exist', 'attributeName' => 'soc_net_id', 'className' => '\Users',
                  'message' => 'Такой пользователь не зарегистрирован'),
            array('item_id', 'exist', 'attributeName' => 'item_id', 'className' => '\Items',
                  'message' => 'Такого предмета нет'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'soc_net_id' => 'Id в соц. сети',
            'item_id' => 'Id предмета',
            'count' => 'Количество',
        );
    }

    public function updateItems()
    {
        if ($this->validate()) {
            $user = Users::model()->findByAttributes(array('soc_net_id' => $this->soc_net_id));
            if (!$user)
                throw new CHttpException(400, 'User not found');

            $item = Items::model()->findByPk($this->item_id);
            if (!$item)
                throw new CHttpException(400, 'Item not found');

            if ($item->buy_another) {
                $this->addError('item_id', 'Нельзя добавить ящик');
                return false;
            }

            $invRecord = ItemsInventory::model()->findByAttributes(array(
                                                                        'user_id' => $user->user_id,
                                                                        'item_id' => $this->item_id
                                                                   ));
            if (!$invRecord) {
                $invRecord = new ItemsInventory();
                $invRecord->item_id = $this->item_id;
                $invRecord->user_id = $user->user_id;
                $invRecord->count = 0;
            }
            $invRecord->count += $this->count;
            $res = $invRecord->save();
            if ($res)
                CacheRemover::clearByKey("user_inventory_data_{$user->user_id}");
            return $res;
        }
        return false;
    }
}
