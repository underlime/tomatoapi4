<?php
class CacheRemover
{
    /**
     * @var \Memcache
     */
    private static $_connection;

    public static function StaticConstructor()
    {
        self::$_connection = new Memcache();
        self::$_connection->connect('127.0.0.1');
    }

    public static function clearAll()
    {
        self::$_connection->flush();
    }

    public static function clearByKey($key)
    {
        $fullKey = \base\Model::getCachePrefix().$key;
        self::$_connection->delete($fullKey);
    }
}

CacheRemover::StaticConstructor();
