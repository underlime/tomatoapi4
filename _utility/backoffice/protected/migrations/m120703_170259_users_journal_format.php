<?php

class m120703_170259_users_journal_format extends CDbMigration
{
	public function up()
	{
        $this->truncateTable('users_journal');

        $db = $this->getDbConnection();

        $sql = <<<SQL
            ALTER TABLE
                users_journal
            ADD COLUMN
                subject_user_id INT(10) UNSIGNED NOT NULL DEFAULT 0
            AFTER
                user_id
SQL;
        $db->createCommand($sql)
            ->execute();

        $this->alterColumn('users_journal', 'event_type', "ENUM('win','fail','gift','new_friend','friend_level')");
	}

	public function down()
	{
		echo "m120703_170259_users_journal_format does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}