<?php

class m120618_201018_boots extends CDbMigration
{
	public function up()
	{
        $params = array(
            'arrangement_variant' => 'boots',
            'description' => 'Сапоги',
        );
        $this->insert('items_arrangement', $params);
	}

	public function down()
	{
		echo "m120618_201018_boots does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}