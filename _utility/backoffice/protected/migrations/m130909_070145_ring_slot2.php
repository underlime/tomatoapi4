<?php

class m130909_070145_ring_slot2 extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users_level_params', 'ring_slots_avaible', 'tinyint default 1');
	}

	public function down()
	{
		echo "m130909_070145_ring_slot2 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}