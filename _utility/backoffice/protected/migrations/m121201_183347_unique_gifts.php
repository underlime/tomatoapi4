<?php

class m121201_183347_unique_gifts extends CDbMigration
{
	public function up()
	{
        $this->createIndex('reseiver_sender_date', 'gifts', 'sender_id, receiver_id, date', TRUE);
	}

	public function down()
	{
		echo "m121201_183347_unique_gifts does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}