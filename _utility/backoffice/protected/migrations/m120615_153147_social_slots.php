<?php

class m120615_153147_social_slots extends CDbMigration
{
	public function up()
	{
        $db = $this->getDbConnection();

        $sql = '
            ALTER TABLE
                `items_inventory`
            ADD COLUMN
                `social_put` INT UNSIGNED NOT NULL DEFAULT 0
            AFTER
                `put`
        ';
        $db->createCommand($sql)
            ->execute();
	}

	public function down()
	{
		echo "m120615_153147_social_slots does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}