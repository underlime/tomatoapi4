<?php

class m120606_133202_drop_views extends CDbMigration
{
	public function up()
	{
        $viewsList = array(
            'view_buy_money_targets',
            'view_last_users',
            'view_masters_and_pupils',
            'view_pay_ladder',
            'view_tables',
            'view_users_by_level',
            'view_users_heavy_count',
            'view_users_with_heavy',
        );

        $db = $this->getDbConnection();

        foreach ($viewsList as $viewName) {
            $sql = "DROP VIEW IF EXISTS `$viewName`";
            $db->createCommand($sql)
                ->execute();

            echo ">> $viewName dropped\n";
        }
	}

	public function down()
	{
		echo "m120606_133202_drop_views does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}