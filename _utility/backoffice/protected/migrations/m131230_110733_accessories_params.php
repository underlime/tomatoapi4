<?php
class m131230_110733_accessories_params extends CDbMigration
{
    public function safeUp()
    {
        $data = array(
            array(
                'Кольцо из латуни',
                'Кольцо с шипами',
                'Лазурное кольцо',
                'Ониксовое кольцо',
                'Кольцо ярости',
                'Зеленое кольцо',
            ),
            array(
                'Клык огородника',
                'Счастливый клевер',
                'Игральные кубики',
                'Кроличья лапка',
                'Счастливый билетик',
                'Бильярдный шар-оракул',
                'Позитивный росток',
                'Журнал Сад и огород',
                'Руна силы',
                'Руна скорости',
                'Руна защиты',
                'Руна здоровья',
                'Глаз бехольдера',
                'Обезболивающее',
            ),
        );

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <damage>{{DAMAGE}}</damage>
        <armor>{{ARMOR}}</armor>
        <speed>{{SPEED}}</speed>
    </fight>
    <scale>
        <hp>{{HEALTH}}</hp>
    </scale>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{DAMAGE}}', '{{ARMOR}}', '{{SPEED}}', '{{HEALTH}}');

        foreach ($data as $i => $group) {
            if ($i == 0) {
                $level = 2;
                $tomatos = 4000;
                $ferros = 40;

                $armor = 10;
                $health = 50;
                $damage = 90;
                $speed = 10;
            }
            else {
                $level = 2;
                $tomatos = 2000;
                $ferros = 30;

                $armor = 20;
                $health = 777;
                $damage = 10;
                $speed = 20;
            }

            foreach ($group as $itemName) {
                /* @var $model \Items */
                $model = \Items::model()->findByAttributes(array('name_ru' => $itemName));
                if (empty($model))
                    throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($itemName));

                $model->required_level = $level;
                $model->price_tomatos = $tomatos;
                $model->price_ferros = $ferros;
                $model->bonuses = str_replace($BONUSES_SEARCH, array(
                                                                    $damage,
                                                                    $armor,
                                                                    $speed,
                                                                    $health
                                                               ), $BONUSES_TEMPLATE);
                if (!$model->save())
                    throw new RuntimeException(CHtml::errorSummary($model));

                $level = round(($level*20)/128 + $level) + 3;
                $tomatos = round(((($tomatos*66)/128 + $tomatos) + 3)/10)*10;
                $ferros = round(($ferros*15)/128 + $ferros) + 3;
                $armor = round((($armor*15)/128) + $armor) + 3;
                $health = round((((($health*16)/128) + $health) + 3)/10)*10;
                $damage = round((($damage*25)/128) + $damage) + 3;
                $speed = round((($speed*8.6)/128) + $speed) + 3;
            }
        }

        $itemName = 'Кольцо Саворона';
        $model = \Items::model()->findByAttributes(array('name_ru' => $itemName));
        if (empty($model))
            throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($itemName));

        $model->required_level = 20;
        $model->price_tomatos = 0;
        $model->price_ferros = 350;
        $model->bonuses = str_replace($BONUSES_SEARCH, array(
                                                            350,
                                                            80,
                                                            500,
                                                            500
                                                       ), $BONUSES_TEMPLATE);
        if (!$model->save())
            throw new RuntimeException(CHtml::errorSummary($model));
    }

    public function safeDown()
    {
        echo "m131230_110733_accessories_params does not support migration down.\n";
        return false;
    }
}
