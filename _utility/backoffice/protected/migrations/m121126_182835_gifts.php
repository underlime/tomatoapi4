<?php

class m121126_182835_gifts extends CDbMigration
{
	public function up()
	{
        $this->createTable(
            'gifts',
            array(
                 'sender_id' => 'INT UNSIGNED NOT NULL',
                 'receiver_id' => 'INT UNSIGNED NOT NULL',
                 'time' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                 'PRIMARY KEY(sender_id, receiver_id)',
            ),
            'Engine=InnoDB'
        );

        $this->createIndex('receiver_id', 'gifts', 'receiver_id');
        $this->addForeignKey(
            'gifts_users_sender_id',
            'gifts',
            'sender_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'gifts_users_receiver_id',
            'gifts',
            'receiver_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
	}

	public function down()
	{
		echo "m121126_182835_gifts does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}