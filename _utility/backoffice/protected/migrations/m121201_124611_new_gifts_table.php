<?php

class m121201_124611_new_gifts_table extends CDbMigration
{
	public function up()
	{
        $this->dropTable('gifts');
        $this->createTable(
            'gifts',
            array(
                 'gift_id' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                 'sender_id' => 'INT UNSIGNED NOT NULL',
                 'receiver_id' => 'INT UNSIGNED NOT NULL',
                 'date' => 'date default null',
            )
        );

        $this->createIndex('sender_id_date', 'gifts', 'sender_id, date');

        $this->addForeignKey(
            'gifts_users_sender_id',
            'gifts',
            'sender_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'gifts_users_receiver_id',
            'gifts',
            'receiver_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
	}

	public function down()
	{
		echo "m121201_124611_new_gifts_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}