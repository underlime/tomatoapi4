<?php

class m130710_063820_char_details_default extends CDbMigration
{
	public function up()
	{
        $this->renameColumn('char_details', 'default', 'is_default');
	}

	public function down()
	{
		echo "m130710_063820_char_details_default does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}