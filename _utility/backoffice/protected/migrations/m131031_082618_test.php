<?php

class m131031_082618_test extends CDbMigration
{
	public function up()
	{
        echo "Test migration", PHP_EOL;
	}

	public function down()
	{
		echo "m131031_082618_test does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}