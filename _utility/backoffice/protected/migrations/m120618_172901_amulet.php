<?php

class m120618_172901_amulet extends CDbMigration
{
	public function up()
	{
        $params = array(
            'arrangement_variant' => 'amulet',
            'description' => 'Амулет',
        );
        $this->insert('items_arrangement', $params);
	}

	public function down()
	{
		echo "m120618_172901_amulet does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}