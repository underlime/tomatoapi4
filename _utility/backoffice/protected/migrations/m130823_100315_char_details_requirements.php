<?php

class m130823_100315_char_details_requirements extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('char_details', 'requirements');
	}

	public function down()
	{
		echo "m130823_100315_char_details_requirements does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}