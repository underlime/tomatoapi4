<?php

class m120620_180434_logs_counter extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users_pay_log', 'count', 'INT(10) UNSIGNED NOT NULL DEFAULT 1');
        $this->addColumn('users_buy_money_log', 'count', 'INT(10) UNSIGNED NOT NULL DEFAULT 1');
	}

	public function down()
	{
		echo "m120620_180434_logs_counter does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}