<?php
class m140126_152112_clan_memvers extends CDbMigration
{
    public function up()
    {
        $this->createTable('clan_members', array(
                                                'clan_id' => 'int',
                                                'user_id' => 'int unsigned not null',
                                                'primary key(clan_id, user_id)',
                                           ));

        $this->addForeignKey('fk__clan_members__clan_id', 'clan_members', 'clan_id',
                             'clans', 'id', 'CASCADE');
        $this->addForeignKey('fk__clan_members__user_id', 'clan_members', 'user_id',
                             'users', 'user_id', 'CASCADE');
    }

    public function down()
    {
        echo "m140126_152112_clan_memvers does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}