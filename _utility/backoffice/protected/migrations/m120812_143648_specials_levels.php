<?php

class m120812_143648_specials_levels extends CDbMigration
{
	public function up()
	{
        echo ">> Start to change specials levels";

        $sql = <<<SQL
            SELECT
                su.user_id,
                su.special_id,
                s.k_damage,
                s.intellect_level,
                s.level_intellect_step,
                s.level_k_bonuse,
                s.active,
                ulp.intellect
            FROM
                specials_in_use su
                INNER JOIN
                specials s
                USING(special_id)
                INNER JOIN
                users_level_params ulp
                USING(user_id)
            WHERE
                s.active = 1
SQL;


        $data = $this->getDbConnection()
            ->createCommand($sql)
            ->queryAll();

        foreach ($data as &$record)
            $this->_setSpecialLevel($record);

        $this->addColumn('specials_in_use', 'level', 'INT UNSIGNED NOT NULL DEFAULT 0');
        $this->addColumn('specials_in_use', 'k_damage', 'FLOAT UNSIGNED NOT NULL DEFAULT 0.0');

        $trns = $this->getDbConnection()
            ->beginTransaction();

        $sql = <<<SQL
            UPDATE
                specials_in_use
            SET
                level=:level,
                k_damage=:k_damage
            WHERE
                user_id=:user_id
                AND
                special_id=:special_id
SQL;

        $command = $this->getDbConnection()
            ->createCommand($sql);

        foreach ($data as $record) {
            $params = array(
                ':level' => $record['level'],
                ':k_damage' => $record['k_damage'],
                ':user_id' => $record['user_id'],
                ':special_id' => $record['special_id'],
            );
            $command->execute($params);
        }

        $trns->commit();
	}

    private function _setSpecialLevel(array &$record)
    {
        if ($record['active']) {
            $dIntel = $record['intellect'] - $record['intellect_level'];
            if ($dIntel > -1) {
                $record['level'] = 1 + floor($dIntel/$record['level_intellect_step']);
                $record['k_damage'] += $record['level']*$record['level_k_bonuse'];
            }
            else {
                $record['k_damage'] = 0;
                $record['level'] = 0;
            }
        }
        else {
            $record['k_damage'] = 0;
            $record['level'] = 0;
        }
    }

	public function down()
	{
		echo "m120812_143648_specials_levels does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}