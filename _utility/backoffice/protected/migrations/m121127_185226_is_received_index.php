<?php

class m121127_185226_is_received_index extends CDbMigration
{
	public function up()
	{
        $this->createIndex('sender_id_is_received', 'gifts', 'sender_id, is_received');
	}

	public function down()
	{
		echo "m121127_185226_is_received_index does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}