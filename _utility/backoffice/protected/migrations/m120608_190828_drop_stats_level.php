<?php

class m120608_190828_drop_stats_level extends CDbMigration
{
	public function up()
	{
        echo ">> Start to update level\n";
        $this->getDbConnection()
            ->createCommand('UPDATE `users_level_params` SET `level`=`stats_level`, `max_hp`=(1000+50*`stats_level`)')
            ->execute();

        $this->dropColumn('users_level_params', 'stats_level');
	}

	public function down()
	{
		echo "m120608_190828_drop_stats_level does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}