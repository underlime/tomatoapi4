<?php

class m131003_164630_items_description extends CDbMigration
{
	public function up()
	{
        $this->addColumn('items', 'description_ru', 'string');
	}

	public function down()
	{
		echo "m131003_164630_items_description does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}