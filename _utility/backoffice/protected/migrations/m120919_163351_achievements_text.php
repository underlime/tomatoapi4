<?php

class m120919_163351_achievements_text extends CDbMigration
{
	public function up()
	{
        $this->renameColumn('achievements', 'name', 'name_ru');
        $this->renameColumn('achievements', 'text', 'text_ru');
	}

	public function down()
	{
		echo "m120919_163351_achievements_text does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}