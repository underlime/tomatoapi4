<?php

class m131008_095949_news_read extends CDbMigration
{
	public function up()
	{
        $columns = array(
            'user_id' => 'int unsigned not null',
            'news_id' => 'int not null',
            'shared' => 'bool default 0',
            'PRIMARY KEY(user_id, news_id)',
        );
        $this->createTable('user_news_read', $columns, 'Engine=InnoDB');

        $this->addForeignKey(
            'fk__user_news_read__user_id',
            'user_news_read',
            'user_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk__user_news_read__news_id',
            'user_news_read',
            'news_id',
            'user_news',
            'id',
            'CASCADE',
            'CASCADE'
        );
	}

	public function down()
	{
		echo "m131008_095949_news_read does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}