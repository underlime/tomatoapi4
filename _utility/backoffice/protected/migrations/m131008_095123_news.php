<?php

class m131008_095123_news extends CDbMigration
{
	public function up()
	{
        $columns = array(
            'id' => 'pk',
            'user_id' => 'int unsigned default null',
            'time' => 'timestamp default CURRENT_TIMESTAMP',
            'expires_time' => 'datetime default null',
            'news_type' => 'tinyint default 0',
            'additional_data' => 'text default null',
        );
        $this->createTable('user_news', $columns, 'Engine=InnoDB');

        $this->addForeignKey(
            'fk__user_news__user_id',
            'user_news',
            'user_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );
	}

	public function down()
	{
		echo "m131008_095123_news does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}