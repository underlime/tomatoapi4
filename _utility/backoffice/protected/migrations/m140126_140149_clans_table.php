<?php
class m140126_140149_clans_table extends CDbMigration
{
    public function up()
    {
        $this->createTable('clans', array(
                                         'id' => 'pk',
                                         'name' => 'string UNIQUE KEY',
                                         'leader_id' => 'int unsigned not null',
                                    ));

        $this->addForeignKey('fk__clans__leader_id', 'clans', 'leader_id',
                             'users', 'user_id', 'CASCADE');
    }

    public function down()
    {
        echo "m140126_140149_clans_table does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}