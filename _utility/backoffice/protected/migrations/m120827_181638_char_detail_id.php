<?php

class m120827_181638_char_detail_id extends CDbMigration
{
	public function up()
	{
        $this->dropIndex('PRIMARY', 'char_details');
        $this->getDbConnection()
            ->createCommand('ALTER TABLE char_details ADD COLUMN char_detail_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST')
            ->execute();
	}

	public function down()
	{
		echo "m120827_181638_char_detail_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}