<?php

class m131125_102258_level_items extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
SELECT
	users_level_params.user_id,
	users_level_params.max_level,
	items.item_id,
	items.required_level
FROM
	items_inventory
	INNER JOIN items USING(item_id)
	INNER JOIN users_level_params USING(user_id)
WHERE
	items.required_level > users_level_params.max_level
	AND items_inventory.put > 0
SQL;

        $db = $this->dbConnection;
        $trns = $db->beginTransaction();
        $data = $db->createCommand($sql)->queryAll();

        $command = $db->createCommand('UPDATE items_inventory SET put=0 WHERE user_id=:userId AND item_id=:itemId');
        foreach ($data as $record) {
            $command->execute(array(
                                   ':userId' => $record['user_id'],
                                   ':itemId' => $record['item_id'],
                              ));
        }
        $trns->commit();
	}

	public function down()
	{
		echo "m131125_102258_level_items does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}