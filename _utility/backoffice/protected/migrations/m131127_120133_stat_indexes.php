<?php

class m131127_120133_stat_indexes extends CDbMigration
{
	public function up()
	{
        $this->dropForeignKey('users_buy_money_log_users_user_id', 'users_buy_money_log');
        $this->dropForeignKey('users_pay_log_users_user_id', 'users_pay_log');

        $this->dbConnection->createCommand('ALTER TABLE users_buy_money_log ENGINE=MyISAM')
            ->execute();
        $this->dbConnection->createCommand('ALTER TABLE users_pay_log ENGINE=MyISAM')
            ->execute();
	}

	public function down()
	{
		echo "m131127_120133_stat_indexes does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}