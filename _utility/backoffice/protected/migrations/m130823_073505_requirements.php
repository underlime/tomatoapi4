<?php

class m130823_073505_requirements extends CDbMigration
{
	public function safeUp()
	{
        /* @var $model Items */
        $itemsData = Items::model()->findAll();

        foreach ($itemsData as $model) {
            $requirementsData = simplexml_load_string($model->requirements);
            $userData = $requirementsData->user_data;
            $reqSum = $userData->strength + $userData->agility + $userData->intellect;
            $level = floor(($reqSum - 300)/10);
            $model->required_level = $level;
            if (!$model->save())
                throw new Exception(CHtml::errorSummary($model));
        }

        $this->dropColumn('items', 'requirements');
	}

	public function safeDown()
	{
		echo "m130823_073505_requirements does not support migration down.\n";
		return false;
	}
}