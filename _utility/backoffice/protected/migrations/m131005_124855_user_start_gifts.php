<?php

class m131005_124855_user_start_gifts extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users', 'was_friends_prizes_given', 'bool default 0');
	}

	public function down()
	{
		echo "m131005_124855_user_start_gifts does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}