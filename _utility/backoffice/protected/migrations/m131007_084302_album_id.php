<?php

class m131007_084302_album_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users', 'album_id', "varchar(32) not null default ''");
	}

	public function down()
	{
		echo "m131007_084302_album_id does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}