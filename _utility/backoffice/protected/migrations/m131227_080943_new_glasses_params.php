<?php

class m131227_080943_new_glasses_params extends CDbMigration
{
    public function safeUp()
    {
        $data = array(
            'Очки позера',
            'Противогаз',
            'Очки ботана',
            'Маска дровосека',
            'Дерзкие очки',
            'Маска химика',
            'Очки Кобры',
            'Маска Гая Фокса',
            'Очки с усами',
            'Маска Визг',
            '3D очки',
            'Маска безумца',
            'Радужные очки',
            'Маска №7',
            'Очки Джоны Леммона',
            'Маска лекаря чумы',
            'Кибернетические очки',
            'Маска Цветомузыка',
            'Стимпанк очки',
            'Маска Робот',
            'Маска Проклятого',
            'Очки Патриота',
            'Маска Железного Овоща',
            'Маска урагана',
            'Маска №8',
            'Простая маска',
            'Каменное лицо',
        );

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <damage>{{DAMAGE}}</damage>
        <armor>{{ARMOR}}</armor>
        <speed>{{SPEED}}</speed>
    </fight>
    <scale>
        <hp>{{HEALTH}}</hp>
    </scale>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{DAMAGE}}', '{{ARMOR}}', '{{SPEED}}', '{{HEALTH}}');

        $level = 1;
        $tomatos = 400;
        $ferros = 3;

        $armor = 10;
        $health = 75;
        $damage = 10;
        $speed = 10;

        foreach ($data as $itemName) {
            /* @var $model \Items */
            $model = \Items::model()->findByAttributes(array('name_ru' => $itemName));
            if (empty($model))
                throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($itemName));
            $model->required_level = $level;
            $model->price_tomatos = $tomatos;
            $model->price_ferros = $ferros;
            $model->bonuses = str_replace($BONUSES_SEARCH, array(
                                                                $damage,
                                                                $armor,
                                                                $speed,
                                                                $health
                                                           ), $BONUSES_TEMPLATE);
            if (!$model->save())
                throw new RuntimeException(CHtml::errorSummary($model));

            $level = round(($level*12)/128 + $level) + 3;
            $tomatos = round(((($tomatos*41)/128 + $tomatos) + 3)/10)*10;
            $ferros = round(($ferros*15)/128 + $ferros) + 3;
            $armor = round((($armor*5)/128) + $armor) + 3;
            $health = round((((($health*16)/128) + $health) + 3)/10)*10;
            $damage = round((($damage*10)/128) + $damage) + 3;
            $speed = round((($speed*8.6)/128) + $speed) + 3;
        }
    }

    public function safeDown()
    {
        echo "m131227_080943_new_glasses_params does not support migration down.\n";
        return false;

    }
}
