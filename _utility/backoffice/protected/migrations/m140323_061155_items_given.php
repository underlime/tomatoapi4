<?php
class m140323_061155_items_given extends CDbMigration
{
    public function up()
    {
        $this->addColumn('users_fight_data', 'items_given', 'int default 0');
    }

    public function down()
    {
        echo "m140323_061155_items_given does not support migration down.\n";
        return false;
    }
}
