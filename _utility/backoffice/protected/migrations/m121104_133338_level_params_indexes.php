<?php

class m121104_133338_level_params_indexes extends CDbMigration
{
	public function up()
	{
        $fields = array('strength', 'agility', 'intellect');
        foreach ($fields as $fieldName)
            $this->createIndex($fieldName, 'users_level_params', "$fieldName,user_id");
	}

	public function down()
	{
		echo "m121104_133338_level_params_indexes does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}