<?php
class m131222_145012_sharpening extends CDbMigration
{
    public function up()
    {
        $this->createTable('sharpening', array(
                                              'user_id' => 'int unsigned',
                                              'item_id' => 'int unsigned',
                                              'sharpening_level' => 'int',
                                              'PRIMARY KEY(user_id, item_id)'
                                         ), 'Engine=InnoDB');
        $this->addForeignKey('fk__sharpening__user_id', 'sharpening', 'user_id', 'users', 'user_id', 'CASCADE');
        $this->addForeignKey('fk__sharpening__item_id', 'sharpening', 'item_id', 'items', 'item_id', 'CASCADE');
    }

    public function down()
    {
        echo "m131222_145012_sharpening does not support migration down.\n";
        return false;
    }
}
