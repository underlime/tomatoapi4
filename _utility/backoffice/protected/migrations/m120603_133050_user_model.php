<?php

class m120603_133050_user_model extends CDbMigration
{
	public function up()
	{
        ini_set('display_errors', 1);
        error_reporting(E_ALL | E_STRICT);

        $this->_dropForeignKeys();
        $this->_createUserIdFields();
        $this->_addUserIdField();
        $this->_changeUserTrigger();
	}

    private function _dropForeignKeys()
    {
        $keysList = array(
            'achievements_gotten_ibfk_1' => 'achievements_gotten',
            'day_bonuses_shown_ibfk_1' => 'day_bonuses_shown',
            'items_inventory_ibfk_1' => 'items_inventory',
            'rivals_ibfk_1' => 'rivals',
            'specials_in_use_ibfk_1' => 'specials_in_use',
            'users_buy_money_log_ibfk_1' => 'users_buy_money_log',
            'users_fight_data_ibfk_1' => 'users_fight_data',
            'users_journal_ibfk_1' => 'users_journal',
            'users_level_params_ibfk_1' => 'users_level_params',
            'users_pay_log_ibfk_1' => 'users_pay_log',
            'users_settings_ibfk_1' => 'users_settings',
            'users_stat_ibfk_1' => 'users_stat',
        );

        foreach ($keysList as $key=>$table)
                $this->dropForeignKey($key, $table);
    }

    private function _createUserIdFields()
    {
        $this->dropIndex('PRIMARY', 'users');

        $sql = '
            ALTER TABLE
                `users`
            ADD
                `user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
            FIRST
        ';

        $this->getDbConnection()
            ->createCommand($sql)
            ->execute();

        $this->renameColumn('users', 'vk_id', 'soc_net_id');
    }

    private function _addUserIdField()
    {
        $tables = array(
            'achievements_gotten' => '`vk_id`, `achievement_id`',
            'day_bonuses_shown' => '`vk_id`, `day_bonuse_id`',
            'items_inventory' => '`vk_id`, `item_id`',
            'rivals' => NULL,
            'specials_in_use' => '`vk_id`, `special_id`',
            'users_buy_money_log' => NULL,
            'users_fight_data' => '`vk_id`',
            'users_level_params' => NULL,
            'users_journal' => NULL,
            'users_pay_log' => NULL,
            'users_settings' => '`vk_id`',
            'users_stat' => '`vk_id`',
        );

        foreach ($tables as $curTable => $key)
            $this->_cleanMissingRecords($curTable);

        $db = $this->getDbConnection();

        $sql = 'SELECT `user_id`, `soc_net_id` FROM `users`';
        $usersList = $db->createCommand($sql)
            ->queryAll();

        $this->dropIndex('vk_id_last_visit', 'users_stat');
        $this->dropIndex('last_visit', 'users_stat');

        foreach ($tables as $curTable=>$primary) {
            echo ">> Working with $curTable\n";
            if ($primary)
                $this->dropIndex('PRIMARY', $curTable);

            $sql = "
                ALTER TABLE
                    `$curTable`
                ADD
                    `user_id` INT(10) UNSIGNED NOT NULL
                FIRST
            ";

            $db->createCommand($sql)
                ->execute();

            $db->createCommand('START TRANSACTION')
                ->execute();

                echo ">> Change ids start in $curTable\n";
                foreach ($usersList as $curUser) {
                    $sql = "UPDATE `$curTable` tbl SET `user_id`='{$curUser['user_id']}' WHERE `vk_id`='{$curUser['soc_net_id']}'";
                    $db->createCommand($sql)
                        ->execute();

                    //echo "{$curUser['soc_net_id']} = {$curUser['user_id']}\n";
                }
            $db->createCommand('COMMIT')
                ->execute();

            if ($primary) {
                $fields = str_replace('`vk_id`', '`user_id`', $primary);

                $sql = "
                    ALTER TABLE
                        `$curTable`
                    ADD PRIMARY KEY ($fields)
                ";

                    $db->createCommand($sql)
                        ->execute();
            }

            $this->dropColumn($curTable, 'vk_id');
            $this->createIndex('user_id', $curTable, 'user_id');
            $this->addForeignKey("{$curTable}_users_user_id", $curTable, 'user_id', 'users', 'user_id', 'CASCADE', 'CASCADE');
        }

        $this->createIndex('user_id_last_visit', 'users_stat', 'user_id,last_visit', TRUE);
    }

    private function _cleanMissingRecords($table)
    {
        $sql = "DELETE FROM `$table` WHERE `vk_id` NOT IN (SELECT `soc_net_id` FROM `users`)";
        $rows = $this->getDbConnection()
                    ->createCommand($sql)
                    ->execute();

        echo ">> Deleted $rows missing records from $table\n";
    }

    private function _changeUserTrigger()
    {
        $db = $this->getDbConnection();

        $sql = 'DROP TRIGGER `on_create_user`';
        $db->createCommand($sql)
            ->execute();

        $sql = '
        CREATE TRIGGER `on_create_user` AFTER INSERT ON `users`
        FOR EACH ROW BEGIN
            INSERT INTO `users_fight_data` SET `user_id`=NEW.user_id;
            INSERT INTO `users_level_params` SET `user_id`=NEW.user_id;
            INSERT INTO `users_stat` SET `user_id`=NEW.user_id;
            INSERT INTO `users_settings` SET `user_id`=NEW.user_id;
        END
        ';

        $db->createCommand($sql)
            ->execute();

        echo ">> Trigger created\n";
    }

	public function down()
	{
		echo "m120603_133050_user_model does not support migration down.\n";
		return false;
	}
}