<?php

class m140331_095436_watch_table extends CDbMigration
{
    public function up()
    {
        $table = 'watch_log';
        $columns = [
            'timestamp' => 'DATETIME',
            'logger' => 'string',
            'level' => 'string',
            'message' => 'string',
            'thread' => 'string',
            'file' => 'string',
            'line' => 'string'
        ];
        $this->createTable($table, $columns, 'Engine=MyISAM');

        $this->createIndex('timestamp', $table, 'timestamp');
        $this->createIndex('logger', $table, 'logger');
        $this->createIndex('file', $table, 'file');
    }

    public function down()
    {
        echo "m140331_095436_watch_table does not support migration down.\n";
        return false;
    }
}
