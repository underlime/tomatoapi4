<?php

class m131127_121240_stat_comments extends CDbMigration
{
	public function up()
	{
        $this->delete('users_buy_money_log');
        $this->delete('users_pay_log');
	}

	public function down()
	{
		echo "m131127_121240_stat_comments does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}