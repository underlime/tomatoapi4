<?php

class m131006_140922_top_times extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('users_fight_data', 'glory_date');
	}

	public function down()
	{
		echo "m131006_140922_top_times does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}