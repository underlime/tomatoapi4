<?php

class m131003_164748_items_description_values extends CDbMigration
{
    public function safeUp()
    {
        $this->alterColumn('items', 'description_ru', 'text');

        $values = require(__DIR__ . '/../data/ItemDescriptions.php');
        foreach ($values as $itemValue) {
            $this->update(
                'items',
                array('description_ru' => $itemValue['description']),
                'item_id=:item_id',
                array(':item_id' => $itemValue['item_id'])
            );
        }
    }

    public function safeDown()
    {
        echo "m131003_164748_items_description_values does not support migration down.\n";
        return false;
    }
}