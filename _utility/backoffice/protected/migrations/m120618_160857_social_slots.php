<?php

class m120618_160857_social_slots extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users_level_params', 'social_slots_avaible', 'boolean default 0');
	}

	public function down()
	{
		echo "m120618_160857_social_slots does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}