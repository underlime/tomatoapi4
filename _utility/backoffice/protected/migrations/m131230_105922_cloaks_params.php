<?php
class m131230_105922_cloaks_params extends CDbMigration
{
    public function safeUp()
    {
        $data = array(
            'Плащ Воланда',
            'Деревянная дверь',
            'Плащ Земноземья',
            'акустическая гитара',
            'Плащ Ватмана',
            'Реактивная установка',
            'Плащ мушкетера',
            'Плащ гвардейца',
            'Плащ охотника',
            'Плащ зверолова',
            'Корзинка с рисом',
            'Плащ лекаря чумы',
            'Плащ грома',
            'Проклятый плащ',
            'Джет-пак железного овоща',
            'Паровой двигатель',
            'Механизированные лезвия',
            'Черный винт'
        );

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <speed>{{SPEED}}</speed>
    </fight>
    <scale>
        <hp>{{HEALTH}}</hp>
    </scale>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{SPEED}}', '{{HEALTH}}');

        $level = 2;
        $tomatos = 100;
        $ferros = 2;

        $health = 10;
        $speed = 75;

        foreach ($data as $name) {
            /* @var $model \Items */
            $model = \Items::model()->findByAttributes(array('name_ru' => $name));
            if (empty($model))
                throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($name));

            $model->required_level = $level;
            $model->price_tomatos = $tomatos;
            $model->price_ferros = $ferros;
            $model->bonuses = str_replace($BONUSES_SEARCH, array(
                                                                $speed,
                                                                $health
                                                           ), $BONUSES_TEMPLATE);
            if (!$model->save())
                throw new RuntimeException(CHtml::errorSummary($model));

            $level = round(($level*20)/128 + $level) + 3;
            $tomatos = round(((($tomatos*74)/128 + $tomatos) + 3)/10)*10;
            $ferros = round(($ferros*50)/128 + $ferros) + 3;

            $health = round(((($health*35)/128) + $health) + 3);
            $speed = round((((($speed*10)/128) + $speed) + 3)/10)*10;
        }
    }

    public function safeDown()
    {
        echo "m131230_105922_cloaks_params does not support migration down.\n";
        return false;
    }
}
