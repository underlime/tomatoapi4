<?php

class m120622_201735_remove_everyday_bonuse_field extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('users_stat', 'everyday_bonuse_date');
	}

	public function down()
	{
		echo "m120622_201735_remove_everyday_bonuse_field does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}