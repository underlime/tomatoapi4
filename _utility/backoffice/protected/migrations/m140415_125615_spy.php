<?php

class m140415_125615_spy extends CDbMigration
{
    public function up()
    {
        $this->createTable('spy_data', [
                                       'id' => 'pk',
                                       'user_id' => 'string',
                                       'file' => 'varchar(128)',
                                       'method' => 'varchar(32)',
                                       'line' => 'int',
                                       'description' => 'string',
                                       ], 'Engine=MyISAM');
        $this->createIndex('user_id', 'spy_data', 'user_id');
        $this->createIndex('path', 'spy_data', 'file, method, line');
        $this->createIndex('description', 'spy_data', 'description');
    }

    public function down()
    {
        echo "m140415_125615_spy does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}