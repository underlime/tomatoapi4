<?php

class m130907_144409_char_details_names extends CDbMigration
{
	public function up()
	{
        $this->addColumn('char_details', 'name_ru', "varchar(64) not null default ''");
	}

	public function down()
	{
		echo "m130907_144409_char_details_names does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}