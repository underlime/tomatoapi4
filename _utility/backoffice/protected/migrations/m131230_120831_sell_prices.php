<?php
class m131230_120831_sell_prices extends CDbMigration
{
    public function safeUp()
    {
        $items = \Items::model()->findAll();
        foreach ($items as $model) {
            /* @var $model \Items */
            if ($model->price_tomatos && $model->sell_price_tomatos) {
                $model->sell_price_tomatos = round($model->price_tomatos*0.1);
            }
            $model->sell_price_ferros = 0;

            if (!$model->save())
                throw new RuntimeException(CHtml::errorSummary($model));
        }
    }

    public function safeDown()
    {
        echo "m131230_120831_sell_prices does not support migration down.\n";
        return false;
    }
}
