<?php
class m131230_121248_equipment_level_correction extends CDbMigration
{
    public function safeUp()
    {
        $sql = <<<SQL
UPDATE
	users_level_params
	INNER JOIN items_inventory USING(user_id)
	INNER JOIN items USING(item_id)
SET
	put=0
WHERE
	`level` < required_level
	AND put > 0
SQL;
        $affectedRows = $this->dbConnection->createCommand($sql)->execute();
        var_dump("Affected rows: $affectedRows");
    }

    public function safeDown()
    {
        echo "m131230_121248_equipment_level_correction does not support migration down.\n";
        return false;
    }
}
