<?php
class m131230_122200_amulet_correction extends CDbMigration
{
    public function safeUp()
    {
        $sql = <<<SQL
SELECT
	ii.user_id,
	ii.item_id,
	ii.put
FROM
	items_inventory ii
	INNER JOIN items i USING(item_id)
WHERE
	i.arrangement_variant='amulet'
	AND ii.put
ORDER BY
	user_id ASC, item_id ASC
SQL;

        $data = $this->dbConnection->createCommand($sql)->queryAll();

        $userId = 0;
        $affectedRows = 0;
        foreach ($data as $row) {
            if ($row['user_id'] == $userId) {
                $inv = \ItemsInventory::model()->findByPk(array('user_id' => $row['user_id'], 'item_id' => $row['item_id']));
                $inv->put = 0;
                if (!$inv->save())
                    throw new RuntimeException(CHtml::errorSummary($inv));
                ++$affectedRows;
            }
            $userId = $row['user_id'];
        }

        var_dump("Affected rows: $affectedRows");
    }

    public function safeDown()
    {
        echo "m131230_122200_amulet_correction does not support migration down.\n";
        return false;
    }
}
