<?php

class m120620_184037_logs_comment extends CDbMigration
{
	public function up()
	{
        $this->alterColumn('users_buy_money_log', 'comment', 'varchar(255)');
	}

	public function down()
	{
		echo "m120620_184037_logs_comment does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}