<?php

class m131125_132651_items_description extends CDbMigration
{
	public function up()
	{
        $db = $this->dbConnection;
        $trns = $db->beginTransaction();
        $data = \Items::model()->findAll();
        foreach ($data as $record) {
            $description = preg_replace('#<p>(.+?)</p>.*#', '$1', $record['description_ru']);
            $description = strip_tags($description);
            $description = trim($description);
            \Items::model()->updateByPk($record['item_id'], array('description_ru' => $description));
        }
        $trns->commit();
	}

	public function down()
	{
		echo "m131125_132651_items_description does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}