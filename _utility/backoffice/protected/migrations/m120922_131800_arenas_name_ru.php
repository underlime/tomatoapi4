<?php

class m120922_131800_arenas_name_ru extends CDbMigration
{
	public function up()
	{
        $this->renameColumn('arenas', 'name', 'name_ru');
	}

	public function down()
	{
		echo "m120922_131800_arenas_name_ru does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}