<?php

class m130816_124026_experience_index extends CDbMigration
{
	public function up()
	{
        $this->createIndex('experience', 'users_fight_data', 'experience');
	}

	public function down()
	{
		echo "m130816_124026_experience_index does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}