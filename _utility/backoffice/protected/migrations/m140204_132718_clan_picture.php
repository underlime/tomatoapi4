<?php

class m140204_132718_clan_picture extends CDbMigration
{
    public function up()
    {
        $this->addColumn('clans', 'picture', "string default ''");
    }

    public function down()
    {
        echo "m140204_132718_clan_picture does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
