<?php

class m120608_195814_drop_money_time extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('users_stat', 'money_update_time');
	}

	public function down()
	{
		echo "m120608_195814_drop_money_time does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}