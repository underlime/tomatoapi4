<?php

class m120922_131946_remove_tables extends CDbMigration
{
	public function up()
	{
        $this->dropTable('news');
        $this->dropTable('profiler_queries');
	}

	public function down()
	{
		echo "m120922_131946_remove_tables does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}