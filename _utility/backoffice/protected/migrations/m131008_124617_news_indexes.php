<?php

class m131008_124617_news_indexes extends CDbMigration
{
	public function up()
	{
        $this->createIndex('time', 'user_news', 'time');
        $this->createIndex('expires_time', 'user_news', 'expires_time');
	}

	public function down()
	{
		echo "m131008_124617_news_indexes does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}