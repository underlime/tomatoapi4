<?php

class m130823_071133_items_level extends CDbMigration
{
	public function up()
	{
        $this->addColumn('items', 'required_level', 'int');
	}

	public function down()
	{
		echo "m130823_071133_items_level does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}