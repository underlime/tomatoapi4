<?php

class m131013_122948_news_price extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user_news', 'like_reward_type', "enum('tomatos', 'ferros', 'item') default 'tomatos'");
        $this->addColumn('user_news', 'like_reward_count', 'int default 50');
        $this->addColumn('user_news', 'like_reward_data', "varchar(32) default ''");
    }

	public function down()
	{
		echo "m131013_122948_news_price does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}