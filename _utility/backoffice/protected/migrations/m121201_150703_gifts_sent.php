<?php

class m121201_150703_gifts_sent extends CDbMigration
{
	public function up()
	{
        $this->createTable(
            'gifts_received_count',
            array(
                 'user_id' => 'INT UNSIGNED NOT NULL PRIMARY KEY',
                 'gifts_received' => 'int default 0',
                 'last_date' => 'date default null',
            )
        );
        $this->addForeignKey(
            'gifts_received_count_user_id',
            'gifts_received_count',
            'user_id',
            'users',
            'user_id',
            'CASCADE',
            'CASCADE'
        );

        $this->getDbConnection()
            ->createCommand('INSERT INTO gifts_received_count (user_id) SELECT user_id FROM users')
            ->execute();
	}

	public function down()
	{
		echo "m121201_150703_gifts_sent does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}