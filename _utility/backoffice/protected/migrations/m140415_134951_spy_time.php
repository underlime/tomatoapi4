<?php
class m140415_134951_spy_time extends CDbMigration
{
    public function up()
    {
        $this->addColumn('spy_data', 'time', 'datetime');
        $this->createIndex('user_time', 'spy_data', 'user_id, time');
    }

    public function down()
    {
        echo "m140415_134951_spy_time does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}