<?php

class m131226_120841_new_armor_params extends CDbMigration
{
    public function safeUp()
    {
        $data = array(
            'Серьёзная майка',
            'Броня ВДВ',
            'Майка фаната',
            'Кожаная броня',
            'Кольчуга',
            'Броня из красного дерева',
            'Костяной доспех',
            'Доспех крестоносца',
            'Стальной доспех',
            'Броник мушкетёра',
            'Броник гвардейца',
            'Охотничий доспех',
            'Доспех зверолова',
            'Доспех лекаря чумы',
            'Тёмная кираса',
            'Монашеское одеяние',
            'Мифриловая кольчуга',
            'Костюм из вулканита',
            'Композитный доспех',
            'Громобронь',
            'Костюм патриота',
            'Стимпанк-доспех',
            'Броня железного овоща',
            'Проклятое кимоно',
            'Нерушимые штаны',
            'Доспехи самурая',
            'Доспехи разрушителя',
            'Простой бронежилет',
        );

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <armor>{{ARMOR}}</armor>
    </fight>
    <scale>
        <hp>{{HEALTH}}</hp>
    </scale>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{ARMOR}}', '{{HEALTH}}');

        $tomatos = 200;
        $ferros = 2;
        $level = 1;
        $armor = 5;
        $health = 10;

        foreach ($data as $itemName) {
            /* @var $model \Items */
            $model = \Items::model()->findByAttributes(array('name_ru' => $itemName));
            if (empty($model))
                throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($itemName));
            $model->required_level = $level;
            $model->price_tomatos = $tomatos;
            $model->price_ferros = $ferros;
            $model->bonuses = str_replace($BONUSES_SEARCH, array($armor, $health), $BONUSES_TEMPLATE);
            if (!$model->save())
                throw new RuntimeException(CHtml::errorSummary($model));

            $level = round($level*14/128 + $level + 3);
            $ferros = round($ferros*14/128 + $ferros + 3);
            $tomatos = round(($tomatos*47/128 + $tomatos + 3)/10)*10;
            if ($armor < 9)
                $armor = round($armor*15/128 + $armor + 3);
            else
                $armor = round($armor*15/128 + $armor + 10);
            $health = round($health*27/128 + $health) + 3;
        }
    }

    public function safeDown()
    {
        echo "m131226_120841_new_armor_params does not support migration down.\n";
        return false;
    }
}