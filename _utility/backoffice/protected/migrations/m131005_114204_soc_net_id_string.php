<?php

class m131005_114204_soc_net_id_string extends CDbMigration
{
	public function up()
	{
        $this->dropIndex('soc_net_id', 'users');
        $this->alterColumn('users', 'soc_net_id', 'varchar(32) not null');
        $this->createIndex('soc_net_id', 'users', 'soc_net_id', TRUE);
	}

	public function down()
	{
		echo "m131005_114204_soc_net_id_string does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}