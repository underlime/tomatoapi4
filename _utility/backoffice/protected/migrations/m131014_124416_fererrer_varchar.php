<?php

class m131014_124416_fererrer_varchar extends CDbMigration
{
	public function up()
	{
        $this->alterColumn('users', 'referrer', "varchar(32) default '0'");
	}

	public function down()
	{
		echo "m131014_124416_fererrer_varchar does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}