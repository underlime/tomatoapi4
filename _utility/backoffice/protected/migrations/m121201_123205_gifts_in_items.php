<?php

class m121201_123205_gifts_in_items extends CDbMigration
{
	public function up()
	{
        $this->addColumn(
            'items',
            'gift_type',
            "ENUM('usual', 'improved') DEFAULT NULL"
        );

        $this->createIndex('gift_type', 'items', 'gift_type');
	}

	public function down()
	{
		echo "m121201_123205_gifts_in_items does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}