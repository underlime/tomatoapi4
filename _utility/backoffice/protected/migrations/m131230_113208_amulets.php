<?php
class m131230_113208_amulets extends CDbMigration
{
    public function safeUp()
    {
        $rings = \Items::model()->findAllByAttributes(array(
                                                           'rubric_code' => 'rings'
                                                      ));

        foreach ($rings as $model) {
            /* @var $model \Items */
            if (preg_match('/amulet/i', $model->picture)) {
                $model->arrangement_variant = 'amulet';
                if (!$model->save())
                    throw new RuntimeException(CHtml::errorSummary($model));
                var_dump(\php_rutils\RUtils::translit()->translify($model->name_ru));
            }
        }
    }

    public function safeDown()
    {
        echo "m131230_113208_amulets does not support migration down.\n";
        return false;
    }
}

