<?php

class m131103_135457_market_statistiks extends CDbMigration
{
	public function up()
	{
        $this->addColumn('market_statistics', 'currency', 'string');
	}

	public function down()
	{
		echo "m131103_135457_market_statistiks does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}