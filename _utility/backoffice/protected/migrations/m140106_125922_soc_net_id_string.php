<?php
class m140106_125922_soc_net_id_string extends CDbMigration
{
    public function up()
    {
        $this->alterColumn('bank_statistics', 'soc_net_id', "varchar(32) not null default '0'");
    }

    public function down()
    {
        echo "m140106_125922_soc_net_id_string does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}