<?php

class m131103_134341_market_statistiks extends CDbMigration
{
	public function up()
	{
        $this->createTable('market_statistics', array(
                                                     'id' => 'pk',
                                                     'item_id' => 'int',
                                                     'user_id' => 'int',
                                                     'name_ru' => 'string',
                                                     'count' => 'int',
                                                     'date' => 'date default null',
                                                ), 'Engine=MyISAM');
        $this->createIndex('date', 'market_statistics', 'date');
        $this->createIndex('user_date', 'market_statistics', 'user_id, date');
	}

	public function down()
	{
		echo "m131103_134341_market_statistiks does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}