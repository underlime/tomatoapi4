<?php

class m120914_172329_soc_net_key extends CDbMigration
{
	public function up()
	{
        $this->dropIndex('vk_id_ban_level', 'users');
        $this->createIndex('soc_net_id', 'users', 'soc_net_id', TRUE);
	}

	public function down()
	{
		echo "m120914_172329_soc_net_key does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}