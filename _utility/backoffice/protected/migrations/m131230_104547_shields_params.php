<?php
class m131230_104547_shields_params extends CDbMigration
{
    public function safeUp()
    {
        $data = array(
            'Деревянный поддон',
            'Деревянный щит',
            'Алюминиевый щит',
            'Черепашья броня',
            'Щит варвара',
            'Щит легионера',
            'Щит патриота',
            'Булатный щит',
            'Кусок стены',
            'Страж порядка'
        );

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <armor>{{ARMOR}}</armor>
    </fight>
    <scale>
        <hp>{{HEALTH}}</hp>
    </scale>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{ARMOR}}', '{{HEALTH}}');

        $level = 1;
        $tomatos = 400;
        $ferros = 3;

        $armor = 10;
        $health = 75;

        foreach ($data as $name) {
            /* @var $model \Items */
            $model = \Items::model()->findByAttributes(array('name_ru' => $name));
            if (empty($model))
                throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($name));

            $model->required_level = $level;
            $model->price_tomatos = $tomatos;
            $model->price_ferros = $ferros;
            $model->bonuses = str_replace($BONUSES_SEARCH, array(
                                                                $armor,
                                                                $health
                                                           ), $BONUSES_TEMPLATE);
            if (!$model->save())
                throw new RuntimeException(CHtml::errorSummary($model));

            $level = round(($level*70)/128 + $level) + 3;
            $tomatos = round(((($tomatos*132)/128 + $tomatos) + 3)/10)*10;
            $ferros = round(($ferros*50)/128 + $ferros) + 3;
            $armor = round((($armor*50)/128) + $armor) + 3;
            $health = round((((($health*56)/128) + $health) + 3)/10)*10;
        }
    }

    public function safeDown()
    {
        echo "m131230_104547_shields_params does not support migration down.\n";
        return false;
    }
}
