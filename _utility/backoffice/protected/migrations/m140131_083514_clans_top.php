<?php

class m140131_083514_clans_top extends CDbMigration
{
    public function up()
    {
        $this->createIndex('glory', 'clans', 'glory');
    }

    public function down()
    {
        echo "m140131_083514_clans_top does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}