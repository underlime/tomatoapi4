<?php

class m140105_112530_pay_indexes extends CDbMigration
{
    public function up()
    {
        $table = 'users_pay_log';
        $this->dropIndex('comment', $table);
        $this->dropIndex('vk_id_time', $table);
        $this->createIndex('ferros_rule', $table, 'time, ferros, comment');
        $this->createIndex('tomatos_rule', $table, 'time, tomatos, comment');
    }

    public function down()
    {
        echo "m140105_112530_pay_indexes does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}