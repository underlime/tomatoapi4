<?php

class m120919_175403_specials_text extends CDbMigration
{
	public function up()
	{
        $this->renameColumn('specials', 'name', 'name_ru');
        $this->renameColumn('specials', 'description', 'description_ru');
	}

	public function down()
	{
		echo "m120919_175403_specials_text does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}