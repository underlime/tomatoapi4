<?php

class m140131_070055_clan_glory extends CDbMigration
{
    public function up()
    {
        $this->addColumn('clans', 'glory', 'int default 0');
    }

    public function down()
    {
        echo "m140131_070055_clan_glory does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}