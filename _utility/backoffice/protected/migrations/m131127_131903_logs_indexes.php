<?php

class m131127_131903_logs_indexes extends CDbMigration
{
	public function up()
	{
        $this->dropIndex('user_money_target', 'users_buy_money_log');
        $this->createIndex('comment', 'users_buy_money_log', 'comment');

        $this->dropIndex('user_money_target', 'users_pay_log');
        $this->createIndex('comment', 'users_pay_log', 'comment');
	}

	public function down()
	{
		echo "m131127_131903_logs_indexes does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}