<?php

class m140204_124755_clan_victories extends CDbMigration
{
    public function up()
    {
        $this->addColumn('clans', 'victories', 'int default 0');
    }

    public function down()
    {
        echo "m140204_124755_clan_victories does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}