<?php

class m120610_172922_update_level_data extends CDbMigration
{
	public function up()
	{
        $sql = '
           UPDATE
                `users_level_params` lp
                INNER JOIN
                `users_fight_data` fd
                USING(`user_id`)
           SET
                `lp`.`level`=FLOOR((`lp`.`strength`+`lp`.`agility`+`lp`.`intellect`-300)/10),
                `lp`.`max_hp`=(1000+FLOOR((`lp`.`strength`+`lp`.`agility`+`lp`.`intellect`-300)/10)*50),
                `fd`.`hp`=(1000+FLOOR((`lp`.`strength`+`lp`.`agility`+`lp`.`intellect`-300)/10)*50),
                `fd`.`max_energy`=(100+FLOOR((`lp`.`strength`+`lp`.`agility`+`lp`.`intellect`-300)/10)),
                `fd`.`energy`=(100+FLOOR((`lp`.`strength`+`lp`.`agility`+`lp`.`intellect`-300)/10))
        ';

        echo ">> Start to change level and fight data\n";

        $rows = $this->getDbConnection()
            ->createCommand($sql)
            ->execute();

        echo ">> Woo hoo, rows affected: $rows\n";
	}

	public function down()
	{
		echo "m120610_172922_update_level_data does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}