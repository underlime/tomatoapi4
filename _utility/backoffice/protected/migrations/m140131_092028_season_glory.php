<?php
class m140131_092028_season_glory extends CDbMigration
{
    public function up()
    {
        $this->addColumn('users_fight_data', 'season_glory', 'int default 0');
    }

    public function down()
    {
        echo "m140131_092028_season_glory does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
