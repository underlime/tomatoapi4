<?php

class m121104_103356_glory_by_time extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users_fight_data', 'day_glory', 'INT UNSIGNED DEFAULT 1 NOT NULL');
        $this->addColumn('users_fight_data', 'week_glory', 'INT UNSIGNED DEFAULT 1 NOT NULL');
        $this->addColumn('users_fight_data', 'glory_date', 'DATE DEFAULT NULL');

        $this->createIndex('glory_date_day', 'users_fight_data', 'glory_date,day_glory,user_id');
        $this->createIndex('glory_date_week', 'users_fight_data', 'glory_date,week_glory,user_id');
	}

	public function down()
	{
		echo "m121104_103356_glory_by_time does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}