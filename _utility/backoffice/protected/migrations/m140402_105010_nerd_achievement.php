<?php
class m140402_105010_nerd_achievement extends CDbMigration
{
    public function up()
    {
        $this->insert('achievements', [
                                        'code' => 'nerd',
                                        'name_ru' => 'Ботан',
                                        'text_ru' => 'Вы многому научились сегодня!',
                                        'pic' => 'tutorial_complete',
                                        'glory' => '10'
                                      ]);
    }

    public function down()
    {
        echo "m140402_105010_nerd_achievement does not support migration down.\n";
        return false;
    }
}
