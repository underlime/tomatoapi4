<?php
class m140126_151237_clan_leader extends CDbMigration
{
    public function up()
    {
        $this->createIndex('leader_id', 'clans', 'leader_id', true);
    }

    public function down()
    {
        echo "m140126_151237_clan_leader does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}