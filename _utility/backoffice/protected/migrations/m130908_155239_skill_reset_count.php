<?php

class m130908_155239_skill_reset_count extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users_level_params', 'skill_resets_count', 'int default 0');
	}

	public function down()
	{
		echo "m130908_155239_skill_reset_count does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}