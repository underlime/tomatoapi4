<?php

class m121126_192443_gifts_received extends CDbMigration
{
	public function up()
	{
        $this->addColumn('gifts', 'is_received', 'boolean default 0');
        $this->createIndex('is_receiver_id_is_received', 'gifts', 'receiver_id, is_received');
	}

	public function down()
	{
		echo "m121126_192443_gifts_received does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}