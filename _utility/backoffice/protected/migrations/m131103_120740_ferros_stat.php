<?php

class m131103_120740_ferros_stat extends CDbMigration
{
	public function up()
	{
        $this->createTable('bank_statistics', array(
                                                   'id' => 'pk',
                                                   'date' => 'date default null',
                                                   'soc_net_id' => 'int',
                                                   'ferros' => 'int',
                                                   'tomatos' => 'int',
                                              ), 'Engine=MyISAM');
        $this->createIndex('date', 'bank_statistics', 'date');
        $this->createIndex('id_date', 'bank_statistics', 'soc_net_id, date');
	}

	public function down()
	{
		echo "m131103_120740_ferros_stat does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}