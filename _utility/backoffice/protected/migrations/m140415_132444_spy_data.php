<?php

class m140415_132444_spy_data extends CDbMigration
{
    public function up()
    {
        $this->addColumn('spy_data', 'data', 'text');
    }

    public function down()
    {
        echo "m140415_132444_spy_data does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}