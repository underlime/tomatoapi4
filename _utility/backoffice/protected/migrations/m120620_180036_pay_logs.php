<?php

class m120620_180036_pay_logs extends CDbMigration
{
    private $data;

    public function up()
    {
        $this->_handlePayData();
    }

    private function _handlePayData()
    {
        $this->_getBuyData();
        $this->truncateTable('users_pay_log');
        $this->createIndex('user_money_target', 'users_pay_log', 'user_id, comment', TRUE);
        $this->_insertBuyData();
    }

    private function _getBuyData()
    {
        $sql = '
            SELECT
                user_id,
                SUM(ferros) as ferros,
                SUM(tomatos) as tomatos,
                `comment`
            FROM
                users_pay_log
            GROUP BY
                user_id,
                `comment`
            ORDER BY
                user_id DESC,
                `comment` ASC';

        $this->data = $this->getDbConnection()
            ->createCommand($sql)
            ->queryAll();
    }

    private function _insertBuyData()
    {
        $db = $this->getDbConnection();
        $transaction = $db->beginTransaction();

        $sql = '
            INSERT INTO
                users_pay_log
            SET
                user_id=:user_id,
                ferros=:ferros,
                tomatos=:tomatos,
                `comment`=:comment';

        foreach ($this->data as $row) {
            $params = array(
                ':user_id' => $row['user_id'],
                ':ferros' => $row['ferros'],
                ':tomatos' => $row['tomatos'],
                ':comment' => $row['comment'],
            );
            $db->createCommand($sql)
                ->execute($params);
        }

        $transaction->commit();
    }

	public function down()
	{
		echo "m120620_180036_pay_logs does not support migration down.\n";
		return FALSE;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}