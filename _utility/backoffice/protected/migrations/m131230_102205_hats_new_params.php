<?php
class m131230_102205_hats_new_params extends CDbMigration
{
    public function safeUp()
    {
        $data = array(
            'Шлем дудлера',
            'Шлем Цезаря',
            'Шляпа Оклахомы Смита',
            'Шляпа Земноземья',
            'Шлем Одина',
            'Будёновка',
            'Бараний шлем',
            'Тёмный шлем',
            'Ушанка',
            'Пушистая ушанка',
            'Шляпа мушкетёра',
            'Шляпа гвардейца',
            'Шапка монаха',
            'Цветок медведерева',
            'Череп саблезуба',
            'Шляпа лекаря чумы',
            'Громошлем',
            'Стимпанк шляпа',
            'Шлем патриота',
            'Шляпа морского волка',
            'Шлем самурая',
            'Сомбреро',
            'Шлем конкистадора'
        );

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <damage>{{DAMAGE}}</damage>
        <armor>{{ARMOR}}</armor>
    </fight>
    <scale>
        <hp>{{HEALTH}}</hp>
    </scale>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{DAMAGE}}', '{{ARMOR}}', '{{HEALTH}}');

        $level = 1;
        $tomatos = 400;
        $ferros = 3;

        $armor = 10;
        $health = 75;
        $damage = 10;

        foreach ($data as $name) {
            /* @var $model \Items */
            $model = \Items::model()->findByAttributes(array('name_ru' => $name));
            if (empty($model))
                throw new RuntimeException('Empty: '.\php_rutils\RUtils::translit()->translify($name));
            $model->required_level = $level;
            $model->price_tomatos = $tomatos;
            $model->price_ferros = $ferros;
            $model->bonuses = str_replace($BONUSES_SEARCH, array(
                                                                $damage,
                                                                $armor,
                                                                $health
                                                           ), $BONUSES_TEMPLATE);
            if (!$model->save())
                throw new RuntimeException(CHtml::errorSummary($model));

            $level = round(($level*12)/128 + $level) + 3;
            $tomatos = round(((($tomatos*41)/128 + $tomatos) + 3)/10)*10;
            $ferros = round(($ferros*15)/128 + $ferros) + 3;
            $armor = round((($armor*5)/128) + $armor) + 3;
            $health = round((((($health*16)/128) + $health) + 3)/10)*10;
            $damage = round((($damage*10)/128) + $damage) + 3;
        }
    }

    public function safeDown()
    {
        echo "m131230_102205_hats_new_params does not support migration down.\n";
        return false;
    }
}
