<?php

class m120919_123910_users_journal_level extends CDbMigration
{
	public function up()
	{
        $this->alterColumn('users_journal', 'level', 'INT NOT NULL DEFAULT 0');
	}

	public function down()
	{
		echo "m120919_123910_users_journal_level does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}