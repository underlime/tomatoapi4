<?php

class m131014_115822_referrers extends CDbMigration
{
	public function up()
	{
        $this->addColumn('users', 'referrer_type', "varchar(32) default ''");
        $this->createIndex('referrer_type', 'users', 'referrer_type');
	}

	public function down()
	{
		echo "m131014_115822_referrers does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}