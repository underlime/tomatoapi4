<?php

class m120607_162006_drop_trigger extends CDbMigration
{
	public function up()
	{
        $this->getDbConnection()
            ->createCommand('DROP TRIGGER `on_create_user`')
            ->execute();
	}

	public function down()
	{
		echo "m120607_162006_drop_trigger does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}