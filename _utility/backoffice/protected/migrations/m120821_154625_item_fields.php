<?php

class m120821_154625_item_fields extends CDbMigration
{
	public function up()
	{
        $this->renameColumn('items', 'name', 'name_ru');
        $this->dropColumn('items', 'description');
	}

	public function down()
	{
		echo "m120821_154625_item_fields does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}