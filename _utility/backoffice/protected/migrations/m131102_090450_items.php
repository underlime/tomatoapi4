<?php

use php_rutils\RUtils;

class m131102_090450_items extends CDbMigration
{
    public function up()
    {
        $data = array(
            'Боксёрские перчатки',
            'Бинты с битым стеклом',
            'Классический кастет',
            'Деревянная бита',
            'Каменный топор',
            'Металлическая бита',
            'Пастуший посох',
            'Пляжный зонтик',
            'Острый камень на палке',
            'Кастет с шипами',
            'Доска с гвоздём',
            'Обычные вилы',
            'Мощное бревно',
            'Скрытые клинки',
            'Гладиус',
            'Обычная коса',
            'Полицейская дубинка',
            'Клюшка для гольфа',
            'Силовые перчатки',
            'Энергосберегающий меч',
            'Рыцарская пика',
            'Ножницы портного',
            'Бас-гитара',
            'Ужасные когти',
            'Тесак овощника',
            'Стальное копьё',
            'Меч Исильдура',
            'Катана',
            'Молот из камнедрева',
            'Лапы',
            'Меч диджея',
            'Обсидиановый бердыш',
            'Меч Вытяжкоголового',
            'Бойцовские кастеты',
            'Вычурный ятаган',
            'Явелин',
            'Меч Александра Великого',
            'Кастеты «Росомаха»',
            'Опасный меч',
            'Копьё Безье',
            'Большая глефа',
            'Обсидиановый, короткий меч',
            'Меч ярости',
            'Топор дровосека',
            'Шпага мушкетёра',
            'Рапира гвардейца',
            'Золотая марка',
            'Сокрушители',
            'Булава внушения',
            'Драконьи клыки',
            'Рубило',
            'Громолот',
            'Секира битвы',
            'Перчатки железного овоща',
            'Опасный клевец',
            'Потрошитель',
            'Голод',
            'Пиксельная кирка',
            'Надгробная плита',
            'Жало',
            'Кримсон',
            'Садовый резак',
            'Грань',
            'Мини бур',
            'Руна',
            'Пасть',
            'Разрыватель',
            'Клешни',
            'Монстр',
            'Трилистник',
            'Кастеты погибели',
            'Веном',
            'Паровой отбойный молот',
            'Рагнарек',
            'Неведомые',
            'Цепной меч Mk.2',
        );

        $K_TOMATOS = 1.13;
        $K_FERROS = 1.21;
        $K_ATTACK = 4;
        $K_SPEED = 2;
        $K_LEVEL = 1.212;

        $BONUSES_TEMPLATE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<bonuses>
    <fight>
        <damage>{{DAMAGE}}</damage>
        <speed>{{SPEED}}</speed>
    </fight>
</bonuses>
XML;

        $BONUSES_SEARCH = array('{{DAMAGE}}', '{{SPEED}}');


        $tomatos = 150;
        $ferros = 1;
        $level = 1;
        $attack = 5;
        $speed = 10;

        $trns = Items::model()->getDbConnection()->beginTransaction();
        foreach ($data as $itemName) {
            $model = Items::model()->findByAttributes(array('name_ru' => $itemName));
            if ($model) {
                $model->attributes = array(
                    'price_ferros' => $ferros,
                    'price_tomatos' => $tomatos,
                    'required_level' => $level,
                    'bonuses' => str_replace($BONUSES_SEARCH, array($attack, $speed), $BONUSES_TEMPLATE),
                );
                if (!$model->save()) {
                    $trns->rollback();
                    throw new Exception(RUtils::translit()->translify(CHtml::errorSummary($model)));
                }
            }

            $tomatos = round((($tomatos*1.25 + 2)/$K_TOMATOS + 53)/10)*10;
            $ferros = round(($ferros*1.25 + 2)/$K_FERROS);
            $level = round(($level*1.25 + 2)/$K_LEVEL);
            $attack = round($attack*(1 + $K_ATTACK/128) + 10);
            $speed = round($speed*(1 + $K_SPEED/128) + 10);
        }


        $data = array(
            'Око хаоса' => array(
                'price_ferros' => 900,
                'price_tomatos' => 0,
                'required_level' => 20,
                'bonuses' => str_replace($BONUSES_SEARCH, array(3887, 3333), $BONUSES_TEMPLATE)
            ),
            'Цепной меч рока' => array(
                'price_ferros' => 900,
                'price_tomatos' => 0,
                'required_level' => 20,
                'bonuses' => str_replace($BONUSES_SEARCH, array(3887, 1200), $BONUSES_TEMPLATE)
            ),
            'Осквернитель' => array(
                'price_ferros' => 900,
                'price_tomatos' => 0,
                'required_level' => 20,
                'bonuses' => str_replace($BONUSES_SEARCH, array(3887, 1200), $BONUSES_TEMPLATE)
            ),
            'Защитник'  => array(
                'price_ferros' => 900,
                'price_tomatos' => 0,
                'required_level' => 20,
                'bonuses' => str_replace($BONUSES_SEARCH, array(3887, 1200), $BONUSES_TEMPLATE)
            ),
        );
        foreach ($data as $itemName => $attributes) {
            $model = Items::model()->findByAttributes(array('name_ru' => $itemName));
            if ($model) {
                $model->attributes = $attributes;
                if (!$model->save()) {
                    $trns->rollback();
                    throw new Exception(RUtils::translit()->translify(CHtml::errorSummary($model)));
                }
            }
        }

        $trns->commit();
    }

    public function down()
    {
        echo "m131102_090450_items does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}