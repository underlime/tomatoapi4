<?php

class m120610_143732_max_level extends CDbMigration
{
	public function up()
	{
        $sql = <<<SQL
ALTER TABLE
  `users_level_params`
ADD COLUMN
  `max_level` INT(10) UNSIGNED DEFAULT 0 NOT NULL
AFTER
  `level`
SQL;
        $this->getDbConnection()
            ->createCommand($sql)
            ->execute();

        $sql = <<<SQL
UPDATE
  `users_level_params`
SET
  `max_level`=`level`
SQL;

        $this->getDbConnection()
            ->createCommand($sql)
            ->execute();
	}

	public function down()
	{
		echo "m120610_143732_max_level does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}