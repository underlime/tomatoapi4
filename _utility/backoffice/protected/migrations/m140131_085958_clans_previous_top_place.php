<?php

class m140131_085958_clans_previous_top_place extends CDbMigration
{
    public function up()
    {
        $this->addColumn('clans', 'previous_place_in_top', 'int default 0');
    }

    public function down()
    {
        echo "m140131_085958_clans_previous_top_place does not support migration down.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}