<?php
class NgActiveForm extends CActiveForm
{
    public $onAttributeChange;

    /**
     * @param CModel $model
     * @param string $attribute
     * @return array
     */
    protected function getElemNgAttributes($model, $attribute)
    {
        $htmlOptions = array();
        CHtml::resolveNameID($model, $attribute, $htmlOptions);
        $id = $htmlOptions['id'];
        $name = $htmlOptions['name'];
        $htmlOptions['data-ng-model'] = $id;
        if ($this->onAttributeChange !== null) {
            $htmlOptions['data-ng-change'] = "$this->onAttributeChange('$id', '$name')";
        }

        foreach ($model->getValidators($attribute) as $validator) {
            switch (get_class($validator)) {
                case 'CRequiredValidator':
                    $htmlOptions['data-ng-required'] = 'true';
                    break;
                case 'CRegularExpressionValidator':
                case 'CEmailValidator':
                case 'CUrlValidator':
                    $htmlOptions['data-ng-pattern'] = $validator->pattern;
                    break;
                case 'CStringValidator':
                    if ($validator->min !== null) {
                        $htmlOptions['data-ng-minlength'] = $validator->min;
                    }
                    if ($validator->max !== null) {
                        $htmlOptions['data-ng-maxlength'] = $validator->max;
                    }
                    break;
                case 'CNumberValidator':
                    if ($validator->integerOnly) {
                        $pattern = $validator->integerPattern;
                    }
                    else {
                        $pattern = $validator->numberPattern;
                    }
                    $htmlOptions['data-ng-pattern'] = $pattern;
                    break;
                case 'CCompareValidator':
                    if ($validator->compareValue) {
                        $htmlOptions['data-ng-pattern'] = "^{$validator->compareValue}$";
                    }
                    break;
            }
        }
        return $htmlOptions;
    }

    public function urlField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::urlField($model, $attribute, $htmlOptions);
    }

    public function emailField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::emailField($model, $attribute, $htmlOptions);
    }

    public function numberField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::numberField($model, $attribute, $htmlOptions);
    }

    public function rangeField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::rangeField($model, $attribute, $htmlOptions);
    }

    public function dateField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::dateField($model, $attribute, $htmlOptions);
    }

    public function timeField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::timeField($model, $attribute, $htmlOptions);
    }

    public function telField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::telField($model, $attribute, $htmlOptions);
    }

    public function textField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::textField($model, $attribute, $htmlOptions);
    }

    public function searchField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::searchField($model, $attribute, $htmlOptions);
    }

    public function hiddenField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::hiddenField($model, $attribute, $htmlOptions);
    }

    public function passwordField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::passwordField($model, $attribute, $htmlOptions);
    }

    public function textArea($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::textArea($model, $attribute, $htmlOptions);
    }

    public function fileField($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::fileField($model, $attribute, $htmlOptions);
    }

    public function radioButton($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::radioButton($model, $attribute, $htmlOptions);
    }

    public function checkBox($model, $attribute, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::checkBox($model, $attribute, $htmlOptions);
    }

    public function dropDownList($model, $attribute, $data, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::dropDownList($model, $attribute, $data, $htmlOptions);
    }

    public function listBox($model, $attribute, $data, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::listBox($model, $attribute, $data, $htmlOptions);
    }

    public function checkBoxList($model, $attribute, $data, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::checkBoxList($model, $attribute, $data, $htmlOptions);
    }

    public function radioButtonList($model, $attribute, $data, $htmlOptions = array())
    {
        $ngAttributes = $this->getElemNgAttributes($model, $attribute);
        $htmlOptions = array_merge($ngAttributes, $htmlOptions);
        return parent::radioButtonList($model, $attribute, $data, $htmlOptions);
    }
}
