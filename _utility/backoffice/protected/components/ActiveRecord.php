<?php
class ActiveRecord extends CActiveRecord
{
    private static $_connectionName = DbData::DEFAULT_DB;
    private static $_dbPool = array();

    private $_dev = false;

    /**
     * @return string
     */
    public static function getConnectionName()
    {
        return self::$_connectionName;
    }

    /**
     * @param string $connectionName
     */
    public static function setConnectionName($connectionName)
    {
        self::$_connectionName = $connectionName;
    }

    public function __construct($scenario = 'insert')
    {
        $this->_dev = isset($_SERVER["DEVELOPMENT_MACHINE"]) || isset($_SERVER["DEBUG_SERVER"]);
        return parent::__construct($scenario);
    }

    public function getDbConnection()
    {
        $dbName = $this->_getDbName();
        if (isset(self::$_dbPool[$dbName])) {
            $db = self::$_dbPool[$dbName];
        }
        elseif (isset(Yii::app()->$dbName)) {
            $db = Yii::app()->$dbName;
            self::$_dbPool[$dbName] = $db;
        }
        else {
            throw new RuntimeException("Wrong database component");
        }
        return $db;
    }

    private function _getDbName()
    {
        $connectionName = DbData::DEFAULT_DB;
        if (php_sapi_name() == "cli") {
            global $argv;
            foreach ($argv as $arg) {
                $res = preg_match('/.*?--connectionID=(?P<id>\w+).*?/i', $arg, $matches);
                if ($res) {
                    $connectionName = $matches['id'];
                    break;
                }
            }
        }
        else {
            $connectionName = self::$_connectionName;
            if ($this->_dev === false) {
                $connectionName = preg_replace('#^([a-z]+)_([a-z]+)$#i', '$1_production_$2', $connectionName);
            }
        }
        return $connectionName;
    }
}
