<?php
use krest\api\Config;
use krest\Db;
use krest\SocNet;
use lib\RediskaFactory;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */

    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $dbCode;
    public $socNetCode;

    protected $baseUrl;

    public function init()
    {
        parent::init();
        $this->baseUrl = Yii::app()->request->baseUrl;
        $this->_registerAssets();
        $this->_handleDb();
        $this->_setSocNetCode();

        Config::setDefaultConfigFile(ROOT_PATH.'/config.php');
        $dbId = Db::getSocNetTable()[$this->socNetCode];
        RediskaFactory::setDefaultNamespace($dbId);
        Config::setRedisClient(RediskaFactory::getInstance());
    }

    private function _registerAssets()
    {
        /* @var $clientScript CClientScript */
        $clientScript = Yii::app()->clientScript;
        $clientScript->registerCoreScript('jquery');
        $clientScript->registerScriptFile('http://code.angularjs.org/1.2.9/angular.min.js');
        $clientScript->registerScriptFile('http://code.angularjs.org/1.2.9/i18n/angular-locale_ru-ru.js');
    }

    private function _handleDb()
    {
        $this->dbCode = Yii::app()->request->getParam('database', DbData::DEFAULT_DB);
        DbData::validateCode($this->dbCode);
        UrlManager::addCommonParam('database', $this->dbCode);
        ActiveRecord::setConnectionName($this->dbCode);
    }

    private function _setSocNetCode()
    {
        switch ($this->dbCode) {
            case 'db':
            case 'db_test':
            case 'db_production_vk':
                $this->socNetCode = SocNet::VK_COM;
                break;
            case 'db_ok':
            case 'db_production_ok':
                $this->socNetCode = SocNet::OK_RU;
                break;
            case 'db_mm':
            case 'db_production_mm':
                $this->socNetCode = SocNet::MM_RU;
                break;
            default:
                throw new Exception('Invalid social network code');
        }
    }

    public function filters()
    {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('admin'),
            ),
            array(
                'deny',
            ),
        );
    }
}