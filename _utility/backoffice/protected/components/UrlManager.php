<?php
class UrlManager extends CUrlManager
{
    private static $_commonParams = array();

    /**
     * @return array
     */
    public static function getCommonParams()
    {
        return self::$_commonParams;
    }

    /**
     * @param array $commonParams
     */
    public static function setCommonParams($commonParams)
    {
        self::$_commonParams = $commonParams;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public static function addCommonParam($name, $value)
    {
        self::$_commonParams[$name] = $value;
    }

    public function createUrl($route, $params = array(), $ampersand = '&')
    {
        $params = array_merge(self::$_commonParams, $params);
        return parent::createUrl($route, $params, $ampersand);
    }
}
