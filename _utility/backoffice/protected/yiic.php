<?php
use krest\api\AutoLoader;

$yiic = dirname(__FILE__).'/../../.yii_framework/yiic.php';
$config = dirname(__FILE__).'/config/console.php';

$rootPath = realpath(__DIR__.'/../../..');
require_once $rootPath.'/krest/api/AutoLoader.php';
$autoLoader = new AutoLoader($rootPath);
spl_autoload_register(array($autoLoader, 'load'));
require $rootPath.'/vendor/autoload.php';

require_once($yiic);
