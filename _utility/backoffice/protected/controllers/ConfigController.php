<?php

use krest\api\Config;
use krest\Arr;
use lib\RediskaFactory;

class ConfigController extends Controller
{
    private $_config;
    private $_bankRates;
    private $_clansClashParams;

    private $_paths = [];

    public function init()
    {
        parent::init();
        $this->_paths['clans_clash'] = 'model/ClansClash';
        $this->_paths['bank_rates'] = "model/Bank/currency_rates/{$this->socNetCode}";

        $this->_config = Config::instance()->getConfig();
        $this->_bankRates = Arr::getByPath($this->_config, $this->_paths['bank_rates'], '/');
        $this->_clansClashParams = Arr::getByPath($this->_config, $this->_paths['clans_clash'], '/');
    }

    public function actionIndex()
    {
        if (Yii::app()->request->isPostRequest) {
            $this->_handlePost();
        }
        $this->render('index', [
                               'config' => $this->_config,
                               'bankRates' => $this->_bankRates,
                               'clansClashParams' => $this->_clansClashParams,
                               ]);
    }

    private function _handlePost()
    {
        $saveData = [];
        $bankRates = Arr::get($_POST, 'bank_rates');
        if ($bankRates) {
            $data = [];
            foreach ($bankRates as $id => $price) {
                $data[$id] = ['price' => $price];
            }
            $this->_bankRates = Arr::deepMerge($this->_bankRates, $data);
            $saveData[$this->_paths['bank_rates']] = $this->_bankRates;
        }

        $clashParams = Arr::get($_POST, 'ClansClash');
        if ($clashParams) {
            $fields = array_keys($this->_clansClashParams);
            $data = [];
            foreach ($fields as $name) {
                $value = Arr::get($clashParams, $name);
                if ($value !== null) {
                    if (!filter_var($value, FILTER_VALIDATE_INT)) {
                        throw new CHttpException(400, "Invalid $name");
                    }
                    $data[$name] = $value;
                }
            }
            if (isset($data['season_duration'])) {
                $data['season_duration'] = "{$data['season_duration']}D";
            }
            $this->_clansClashParams = array_merge($this->_clansClashParams, $data);
            $saveData[$this->_paths['clans_clash']] = $this->_clansClashParams;
        }

        if ($saveData) {
            $config = Config::instance();
            $userConfig = $config->getUserConfig();
            foreach ($saveData as $path => $value) {
                Arr::setByPath($userConfig, $path, $value, '/');
            }
            $config->setUserConfig($userConfig);
        }
    }
}
