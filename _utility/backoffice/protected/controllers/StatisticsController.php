<?php

class StatisticsController extends Controller
{
    public $layout = '//layouts/column2';
    public $startDate;
    public $stopDate;

    /**
     * @var CDbConnection
     */
    private $_db;

    public function init()
    {
        parent::init();
        $this->_db = Users::model()->dbConnection;

        $this->menu = array(
            array('label' => 'Информация', 'url' => array('index')),
            array('label' => 'Статистика банка', 'url' => array('bankSummaryStatistics')),
            array('label' => 'Статистика рынка', 'url' => array('marketStatistics')),
            array('label' => 'Источники денег', 'url' => array('buyMoneyStatistics')),
            array('label' => 'Цели расходов', 'url' => array('payStatistics')),
            array('label' => 'Игроки по уровням', 'url' => array('usersByLevel')),
            array('label' => 'Проценты достижений', 'url' => array('achievementsPercents')),
            array('label' => 'Проценты навыков', 'url' => array('specialsPercents')),
            array('label' => 'Забившие', 'url' => array('lostUsers')),
        );

        $this->breadcrumbs['Статистика'] = array('index');

        $this->_handleDates();
    }

    private function _handleDates()
    {
        $request = Yii::app()->request;
        $this->startDate = $request->getParam('start_date');
        $this->stopDate = $request->getParam('stop_date');

        if ($this->startDate === null)
            $this->startDate = date('Y-m-d', time() - 604800);
        if ($this->stopDate === null)
            $this->stopDate = date('Y-m-d');

        $this->stopDate .= ' 23:59:59';

        $datePattern = '/^\d{4}-\d{2}-\d{2}(?:\s\d{2}:\d{2}:\d{2})?$/';
        if (!(preg_match($datePattern, $this->startDate) && preg_match($datePattern, $this->stopDate)))
            throw new CHttpException(400, 'Неверный формат даты');
    }

    /**
     * Индексная страница
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    /**
     * Игроки по уровням
     */
    public function actionUsersByLevel()
    {
        $sql = 'SELECT level, COUNT(*) AS count FROM users_level_params GROUP BY level ORDER BY count DESC';
        $data = $this->_db->createCommand($sql)
                ->queryAll();
        $this->render('users_by_level', array('data' => $data));
    }

    /**
     * Процент получения каждого достижения
     */
    public function actionAchievementsPercents()
    {
        $sql = <<<SQL
            SELECT
                a.achievement_id,
                a.name_ru,
                COUNT(*) AS count
            FROM
                achievements a
                INNER JOIN
                achievements_gotten ag
                USING(achievement_id)
            GROUP BY
                ag.achievement_id
            ORDER BY
                count DESC, name_ru ASC
SQL;
        $data = $this->_db->createCommand($sql)
                ->queryAll();

        $count = Users::model()->count();
        $k = 100/$count;
        foreach ($data as &$record)
            $record['percent'] = round($record['count']*$k);

        $this->render('achievements_percents', array('data' => $data));
    }

    /**
     * Процент получения каждого навыка
     */
    public function actionSpecialsPercents()
    {
        $sql = <<<SQL
            SELECT
                s.special_id,
                s.name_ru,
                COUNT(*) AS count
            FROM
                specials s
                INNER JOIN
                specials_in_use su
                USING(special_id)
            GROUP BY
                su.special_id
            ORDER BY
                count DESC, name_ru ASC
SQL;
        $data = $this->_db->createCommand($sql)
                ->queryAll();

        $count = Users::model()->count();
        $k = 100/$count;
        foreach ($data as &$record)
            $record['percent'] = round($record['count']*$k);

        $this->render('specials_percents', array('data' => $data));
    }

    /**
     * Пользователи, которые не заходили давно
     */
    public function actionLostUsers()
    {
        $defaultDate = date('Y-m-d', strtotime('today - 7 days'));
        if (Yii::app()->request->isPostRequest) {
            $date = isset($_POST['border_date']) ? $_POST['border_date'] : $defaultDate;
            $allCount = UsersStat::model()->count();
            $lostCount = UsersStat::model()->count('last_visit < :borderDate', array(':borderDate' => $date));
            $usersData = array(
                'all_count' => $allCount,
                'lost_count' => $lostCount,
                'lost_percent' => round($lostCount*100/$allCount, 2),
            );
        }
        else {
            $date = $defaultDate;
            $usersData = null;
        }

        $this->render('lost_users', array(
                                         'date' => $date,
                                         'usersData' => $usersData,
                                    ));
    }

    public function actionBankSummaryStatistics()
    {
        $sql = <<<SQL
SELECT
    `date`,
    SUM(ferros) AS ferros,
    SUM(tomatos) AS tomatos
FROM
    bank_statistics
WHERE
    `date` BETWEEN :startDate AND :stopDate
GROUP BY `date`
ORDER BY `date` ASC
SQL;

        $summaryData = $this->_db->createCommand($sql)
                       ->queryAll(true, array(
                                             ':startDate' => $this->startDate,
                                             ':stopDate' => $this->stopDate,
                                        ));

        $ferrosPlotData = array();
        $tomatosPlotData = array();
        foreach ($summaryData as $record) {
            $time = strtotime($record['date'])*1000;
            $ferrosPlotData[] = array($time, (float)$record['ferros']);
            $tomatosPlotData[] = array($time, (float)$record['tomatos']);
        }

        $sql = 'SELECT ferros, COUNT(*) AS `count` FROM bank_statistics GROUP BY ferros ORDER BY ferros ASC';
        $ferrosData = $this->_db->createCommand($sql)
                      ->queryAll();

        $func = function ($record) {
            return array($record['ferros'], $record['count']);
        };
        $ferrosData = array_map($func, $ferrosData);

        $this->render('bank_summary_statistics', array(
                                                      'data' => $summaryData,
                                                      'ferrosPlotData' => $ferrosPlotData,
                                                      'tomatosPlotData' => $tomatosPlotData,
                                                      'ferrosData' => $ferrosData,
                                                 ));
    }

    public function actionMarketStatistics()
    {
        $this->breadcrumbs[] = 'Статистика рынка';

        $sql = <<<SQL
SELECT
    COUNT(*) AS `count`,
    currency
FROM
    market_statistics
WHERE
    `date` BETWEEN :startDate AND :stopDate
GROUP BY currency
SQL;
        $currencyData = $this->_db->createCommand($sql)
                        ->queryAll(true, array(
                                              ':startDate' => $this->startDate,
                                              ':stopDate' => $this->stopDate,
                                         ));

        $currencyData = array_map(function ($record) {
            return array($record['currency'], $record['count']);
        }, $currencyData);

        $sql = <<<SQL
SELECT
    COUNT(*) AS `count`,
    item_id,
    name_ru
FROM
    market_statistics
WHERE
    `date` BETWEEN :startDate AND :stopDate
GROUP BY item_id
ORDER BY `count` DESC
SQL;
        $itemsRatingData = $this->_db->createCommand($sql)
                           ->queryAll(true, array(
                                                 ':startDate' => $this->startDate,
                                                 ':stopDate' => $this->stopDate,
                                            ));

        $this->render('market', array(
                                     'itemsRatingData' => $itemsRatingData,
                                     'currencyData' => $currencyData,
                                ));
    }

    public function actionBuyMoneyStatistics()
    {
        $this->breadcrumbs[] = 'Статистика источников денег';

        $sql = <<<SQL
SELECT
    SUM(ferros) AS `count`,
    `comment`
FROM
    users_buy_money_log
WHERE
    `time` BETWEEN :startDate AND :stopDate
    AND ferros
GROUP BY
    `comment`
ORDER BY `count` DESC
SQL;
        $data = $this->_db->createCommand($sql)
                ->queryAll(true, array(
                                      ':startDate' => $this->startDate,
                                      ':stopDate' => $this->stopDate,
                                 ));
        $buyFerrosData = array_map(function ($record) {
            return array($record['comment'], $record['count']);
        }, $data);

        $sql = <<<SQL
SELECT
    SUM(tomatos) AS `count`,
    `comment`
FROM
    users_buy_money_log
WHERE
    `time` BETWEEN :startDate AND :stopDate
    AND tomatos
GROUP BY
    `comment`
ORDER BY `count` DESC
SQL;
        $data = $this->_db->createCommand($sql)
                ->queryAll(true, array(
                                      ':startDate' => $this->startDate,
                                      ':stopDate' => $this->stopDate,
                                 ));
        $buyTomatosData = array_map(function ($record) {
            return array($record['comment'], $record['count']);
        }, $data);

        $this->render('buy_money', array(
                                        'buyFerrosData' => $buyFerrosData,
                                        'buyTomatosData' => $buyTomatosData,
                                   ));
    }

    public function actionPayStatistics()
    {
        $this->breadcrumbs[] = 'Статистика целей расходов';

        $sql = <<<SQL
SELECT
    SUM(ferros) AS `count`,
    `comment`
FROM
    users_pay_log
WHERE
    `time` BETWEEN :startDate AND :stopDate
    AND ferros
GROUP BY `comment`
SQL;
        $payFerrosData = $this->_db->createCommand($sql)
                         ->queryAll(true, array(
                                               ':startDate' => $this->startDate,
                                               ':stopDate' => $this->stopDate,
                                          ));
        $payFerrosData = $this->_handlePayData($payFerrosData);

        $sql = <<<SQL
SELECT
    SUM(tomatos) AS `count`,
    `comment`
FROM
    users_pay_log
WHERE
    `time` BETWEEN :startDate AND :stopDate
    AND tomatos
GROUP BY `comment`
SQL;
        $payTomatosData = $this->_db->createCommand($sql)
                          ->queryAll(true, array(
                                                ':startDate' => $this->startDate,
                                                ':stopDate' => $this->stopDate,
                                           ));
        $payTomatosData = $this->_handlePayData($payTomatosData);

        $this->render('pay', array(
                                  'payFerrosData' => $payFerrosData,
                                  'payTomatosData' => $payTomatosData,
                             ));
    }

    private function _handlePayData(array $oldData)
    {
        $newData = array();
        $counts = array();
        $sharpening = 0;
        $citadel = 0;

        foreach ($oldData as $record) {
            if (preg_match('#^заточка \d+ до \d+$#ui', $record['comment'])) {
                $sharpening += $record['count'];
            }
            elseif (preg_match('#увеличение \w+ на время \d+#ui', $record['comment'])) {
                $citadel += $record['count'];
            }
            else {
                $newData[] = array($record['comment'], $record['count']);
                $counts[] = $record['count'];
            }
        }

        if ($sharpening) {
            $newData[] = array('Заточка', $sharpening);
            $counts[] = $sharpening;
        }

        if ($citadel) {
            $newData[] = array('ГМО', $citadel);
            $counts[] = $citadel;
        }

        array_multisort($counts, SORT_DESC, $newData);
        return $newData;
    }
}
