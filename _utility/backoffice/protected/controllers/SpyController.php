<?php

use krest\api\Config;
use krest\Db;
use lib\RediskaFactory;
use model\SpyData;

class SpyController extends Controller
{
    public function actionIndex()
    {
        $model = new SpyData();
        if (isset($_POST['SpyData']['soc_net_ids'])) {
            $model->setIdsFromString($_POST['SpyData']['soc_net_ids']);
        }
        $ids = $model->getSavedIds();
        $idsString = implode("\r\n", $ids);
        $this->render('index', ['idsString' => $idsString]);
    }

    public function actionLog()
    {
        $model = new ARSpyData();
        $this->render('log', ['model' => $model]);
    }

    public function actionView($id)
    {
        $model = ARSpyData::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'Not found');
        }
        $this->render('view', ['model' => $model]);
    }
}
