<?php
class SiteController extends Controller
{
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('admin'),
            ),
            array(
                'allow',
                'actions' => array('login'),
                'users' => array('?'),
            ),
            array(
                'deny',
            ),
        );
    }

    public function actionIndex()
    {
        $cacheFlushStatus = false;
        if (isset($_POST['clear_all_cache'])) {
            CacheRemover::clearAll();
            $cacheFlushStatus = true;
        }
        $this->render('index', array(
                                    'cacheFlushStatus' => $cacheFlushStatus
                               ));
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionLogin()
    {
        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        $this->render('login', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}
