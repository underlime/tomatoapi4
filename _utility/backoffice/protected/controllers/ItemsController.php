<?php

class ItemsController extends Controller
{
    public $layout = '//layouts/column2';

    public $arrangementVariants, $rubricCodes;
    public $applyInFightVariants, $giftTypeVariants;

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $params = array(
            'criteria' => array(
                'order' => 'rubric_code ASC, sort ASC',
            ),
        );
        $dataProvider = new CActiveDataProvider('Items', $params);
        $this->render('index', array(
                                    'dataProvider' => $dataProvider,
                               ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
                                   'model' => $this->loadModel($id),
                              ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->_setEnums();
        $model = new Items();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Items'])) {
            $model->attributes = $_POST['Items'];
            if ($model->save()) {
                CacheRemover::clearByKey("items_list_1");
                CacheRemover::clearByKey("items_list_0");
                $this->redirect(array('view', 'id' => $model->item_id));
            }
        }

        $this->render('create', array(
                                     'model' => $model,
                                ));
    }

    private function _setEnums()
    {
        $this->applyInFightVariants = array();
        foreach (\Items::$APPLY_IN_FIGHT as $applyVariant)
            $this->applyInFightVariants[$applyVariant] = $applyVariant;

        $this->giftTypeVariants = array(
            null => 'no gift'
        );
        foreach (\Items::$GIFT_TYPE as $giftType)
            $this->giftTypeVariants[$giftType] = $giftType;

        $this->arrangementVariants = array();
        $arrangements = \ItemsArrangement::model()->findAll(array('order' => 'description ASC'));
        foreach ($arrangements as $model)
            $this->arrangementVariants[$model->arrangement_variant] = $model->description;

        $this->rubricCodes = array();
        $rubricCodes = \ItemsRubrics::model()->findAll(array('order' => 'name ASC'));
        foreach ($rubricCodes as $model)
            $this->rubricCodes[$model->rubric_code] = $model->name;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->_setEnums();
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Items'])) {
            $model->attributes = $_POST['Items'];
            if ($model->save()) {
                CacheRemover::clearByKey("items_list_1");
                CacheRemover::clearByKey("items_list_0");
                $this->redirect(array('view', 'id' => $model->item_id));
            }
        }

        $this->render('update', array(
                                     'model' => $model,
                                ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Items('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Items']))
            $model->attributes = $_GET['Items'];

        $this->render('admin', array(
                                    'model' => $model,
                               ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Items the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Items::model()->findByPk($id);
        if ($model === NULL)
            throw new CHttpException(404, 'The requested page does not exist');

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Items $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'items-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
