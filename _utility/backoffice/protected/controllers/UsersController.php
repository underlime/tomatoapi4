<?php

class UsersController extends Controller
{
    public $layout = '//layouts/column2';

    public function init()
    {
        parent::init();
        $this->menu = array(
            array('label' => 'Статистика', 'url' => array('index')),
            array('label' => 'Дать/забрать денег', 'url' => array('money')),
            array('label' => 'Подарить предмет', 'url' => array('item')),
            array('label' => 'Забанить/разбанить', 'url' => array('ban')),
        );
    }

    public function actionIndex()
    {
        $usersCount = Users::model()->count();
        $this->render('index', array('usersCount' => $usersCount));
    }

    public function actionMoney()
    {
        $model = new UserMoneyForm();
        if (isset($_POST['UserMoneyForm']))
            $this->_handleAddMoney($model);
        $this->render('money', array('model' => $model));
    }

    private function _handleAddMoney($model)
    {
        $model->attributes = $_POST['UserMoneyForm'];
        $updateResult = $model->updateUserMoney();
        $this->_alertMessage($updateResult);
    }

    private function _alertMessage($updateResult)
    {
        $resultMessage = ($updateResult ? 'Успех' : 'Ошибка');
        $script = <<<JS
window.addEventListener('load', function() {
    var message = "$resultMessage";
    alert(message);
});
JS;
        Yii::app()->clientScript->registerScript('alert', $script);
    }

    public function actionItem()
    {
        $model = new UserItemForm();
        if (isset($_POST['UserItemForm']))
            $this->_handleItem($model);
        $this->render('items', array('model' => $model));
    }

    private function _handleItem(UserItemForm $model)
    {
        $model->attributes = $_POST['UserItemForm'];
        $updateResult = $model->updateItems();
        $this->_alertMessage($updateResult);
    }

    public function actionBan()
    {
        $model = new UserBanForm();
        if (isset($_POST['UserBanForm']))
            $this->_handleBan($model);
        $this->render('ban', array('model' => $model));
    }

    private function _handleBan(UserBanForm $model)
    {
        $model->attributes = $_POST['UserBanForm'];
        $updateResult = $model->banUser();
        $this->_alertMessage($updateResult);
    }
}
