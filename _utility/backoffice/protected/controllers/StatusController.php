<?php

use krest\api\Config;

class StatusController extends Controller
{
    /**
     * Конфигурация KREST приложения
     * @var array
     */
    public $config;

    public function init()
    {
        parent::init();
        $this->config = Config::instance()->getConfig();
        $this->breadcrumbs = array(
            'Статус' => array('index')
        );

        $this->menu = array(
            array('label' => 'Конфигурация', 'url' => array('index')),
            array('label' => 'PHP Info', 'url' => array('phpInfo')),
            array('label' => 'Директории и файлы', 'url' => array('dir')),
            array('label' => 'Логи', 'url' => array('logs')),
        );
    }

    public function actionIndex()
    {
        $this->breadcrumbs[] = 'Конфигурация';
        $this->render('index');
    }

    public function actionDir()
    {
        $this->breadcrumbs[] = 'Директории и файлы';
        $this->render('dir');
    }

    public function actionLogs()
    {
        $handlerConfig = $this->config['krest']['api']['ExceptionHandler'];
        $this->breadcrumbs[] = 'Логи';
        $this->render('logs', array(
                                   'handlerConfig' => $handlerConfig
                              ));
    }

    public function actionLogDetail($file)
    {
        $this->breadcrumbs['Логи'] = array('logs');
        $this->breadcrumbs[] = $file;

        if (strpos('..', $file) !== false) {
            throw new CHttpException(400, 'Wrong file name');
        }

        $filePath = $this->config['_logger']['logs_dir'].DIRECTORY_SEPARATOR.$file;
        if (!file_exists($filePath)) {
            throw new CHttpException(404, 'Not found');
        }

        $fileInfo = new SplFileInfo($filePath);
        if (isset($_POST['clear_log']) && $_POST['clear_log'] === '1') {
            if ($fileInfo->isWritable()) {
                file_put_contents($filePath, '');
            }
            else {
                throw new CHttpException(403, 'Permission denied');
            }
        }

        if ($fileInfo->isReadable()) {
            $fileContent = file_get_contents($filePath);
        }
        else {
            throw new CHttpException(403, 'Permission denied');
        }

        $this->render('log_detail', array(
                                         'fileInfo' => $fileInfo,
                                         'fileContent' => $fileContent,
                                    ));
    }

    public function actionPhpInfo()
    {
        $this->breadcrumbs[] = 'PHP Info';
        $this->render('php_info');
    }

    public function actionPurePhpInfo()
    {
        phpinfo();
    }
}
