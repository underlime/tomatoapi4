<?php
use krest\api\AutoLoader;

$yii = dirname(__FILE__).'/../.yii_framework/yii.php';
$config = dirname(__FILE__).'/protected/config/main.php';

defined('YII_DEBUG') or define('YII_DEBUG', isset($_SERVER['DEBUG_SERVER']));
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

$rootPath = realpath(__DIR__.'/../..');
require_once $rootPath.'/krest/api/AutoLoader.php';
$autoLoader = new AutoLoader($rootPath);
spl_autoload_register(array($autoLoader, 'load'));
require $rootPath.'/vendor/autoload.php';

require_once($yii);
Yii::createWebApplication($config)->run();
