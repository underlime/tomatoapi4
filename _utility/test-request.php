<?php
namespace test_request;

use krest\Arr;
use krest\api\AutoLoader;
use krest\api\Config;

$testRequestAllowed = isset($_SERVER['TEST_REQUEST']) && $_SERVER['TEST_REQUEST'];
if (!$testRequestAllowed)
    die('Test requests are not allowed');

require '../krest/api/AutoLoader.php';
require 'test-request/functions.php';
$actionsList = require('test-request/actions_list.php');

$dir = realpath(__DIR__.'/..');
$autoLoader = new AutoLoader($dir);
spl_autoload_register(array($autoLoader, 'load'));

$config = realpath('../config.php');
Config::setDefaultConfigFile($config);

$socNet = Arr::get($_GET, 'soc_net', 'vk.com');
$socNetId = Arr::get($_GET, 'soc_net_id', 3081515);
$action = Arr::get($_GET, 'action');
$format = Arr::get($_GET, 'format');
$postParams = Arr::get($_POST, 'params');

if(is_array($postParams)) {
    $actionAreaData = executeActionRequest($action, $socNet, $socNetId, $format, $postParams);
    $actionHtml = renderView('action_data', $actionAreaData);
}
else {
    $actionHtml = '';
}

$data = array(
    'actionsList' => $actionsList,
    'action' => $action,
    'socNet' => $socNet,
    'socNetId' => $socNetId,
    'format' => $format,
);
$paramsHtml = renderView('params', $data);

$data = array(
    'paramsHtml' => $paramsHtml,
    'actionHtml' => $actionHtml,
);
echo renderView('layout', $data);
