<?php
namespace fight\robot;

class Doppelganger extends RobotFactory
{

    protected function _makeRobotInfo()
    {
        $this->robotInfo = clone $this->userInfo;
        $this->robotInfo->userInfo['soc_net_id'] = 0;
        $this->robotInfo->userInfo['user_id'] = -1;
        $this->robotInfo->userInfo['login'] = 'Doppelganger';
    }

}
