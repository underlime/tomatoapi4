<?php
namespace fight\robot;

use krest\Arr;
use krest\exceptions\InvalidDataException;

abstract class RobotFactory
{
    const SOUP_NIGHTBOT = 'Soup_NightBot';
    const DOPPELGANGER = 'Doppelganger';

    /**
     * Список ботов
     * @var array
     */
    protected static $botTable = array(
        -1 => self::DOPPELGANGER,
        -2 => self::SOUP_NIGHTBOT,
    );

    /**
     * @var UserInfo
     */
    protected $userInfo;

    /**
     * @var UserInfo
     */
    protected $robotInfo;

    /**
     * Извлечь список ботов
     * @return array
     */
    final public static function getTable()
    {
        return self::$botTable;
    }

    /**
     * Создать бота
     * @param mixed $botType
     * @param UserInfo $userInfo
     * @return RobotFactory
     */
    public static function getRobot($botType, UserInfo $userInfo)
    {
        if (is_numeric($botType)) {
            $bot = self::_getBotByIndex($botType, $userInfo);
        }
        else {
            $bot = self::_getBotByName($botType, $userInfo);
        }

        return $bot;
    }

    private static function _getBotByIndex($botType, UserInfo $userInfo)
    {
        $botName = Arr::req(self::$botTable, $botType);
        $bot = self::_getBotByName($botName, $userInfo);

        return $bot;
    }

    private static function _getBotByName($botType, UserInfo $userInfo)
    {
        switch ($botType) {
            case self::SOUP_NIGHTBOT:
                $bot = new Soup_NightBot($userInfo);
                break;
            case self::DOPPELGANGER:
                $bot = new Doppelganger($userInfo);
                break;
            default:
                throw new InvalidDataException('Wrong robot type');
        }

        return $bot;
    }

    protected function __construct(UserInfo $userInfo)
    {
        $this->userInfo = $userInfo;
        $this->_makeRobotInfo();
    }

    /**
     * Извлечь информацию о боте
     * @return UserInfo
     */
    public function getRobotInfo()
    {
        return $this->robotInfo;
    }

    /**
     * Создать информацию о боте из информации о пользователе,
     * выполняется автоматически в конструкторе
     */
    abstract protected function _makeRobotInfo();
}
