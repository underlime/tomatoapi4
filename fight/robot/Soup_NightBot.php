<?php
namespace fight\robot;

use model\Specials;

class Soup_NightBot extends RobotFactory
{
    /**
     * @var Specials
     */
    private $specials;

    private $levelParamsProportions;

    protected function __construct(UserInfo $userInfo)
    {
        $this->specials = new Specials();
        parent::__construct($userInfo);
    }

    protected function _makeRobotInfo()
    {
        $this->robotInfo = clone $this->userInfo;
        $this->_getBotBaseInfo();
        $this->_getBotLevelParamsFightData();
        $this->_setBotSpecials();
        $this->_setBotInventory();
    }

    private function _getBotBaseInfo()
    {
        $botBaseInfo = array(
            'soc_net_id' => 0,
            'user_id' => -2,
            'login' => 'Soup_NightBot',
            'vegetable' => 'tomato',
            'body' => 'tBody90',
            'hands' => 'tArm90',
            'eyes' => 'eyes90',
            'mouth' => 'mouth90',
            'hair' => 'tHair90'
        );

        $this->robotInfo->userInfo =
            array_merge(
                $this->robotInfo->userInfo,
                $botBaseInfo
            );
    }

    private function _getBotLevelParamsFightData()
    {
        $this->robotInfo->userLevelParams['level'] = $this->userInfo->userLevelParams['level'] + 1;
        $this->robotInfo->userLevelParams['max_level'] = $this->robotInfo->userLevelParams['level'];
        $pointsLearning = $this->robotInfo->userLevelParams['level']*10;

        $this->_setUserLevelParamsProportions();
        $this->robotInfo->userLevelParams['strength'] = 200 + round($pointsLearning*$this->levelParamsProportions['strength']);
        $this->robotInfo->userLevelParams['intellect'] = 200 + round($pointsLearning*$this->levelParamsProportions['intellect']);

        $this->robotInfo->userLevelParams['agility'] =
            200
            + max(
                0,
                $pointsLearning
                - (
                    $this->robotInfo->userLevelParams['strength']
                    + $this->robotInfo->userLevelParams['intellect']
                )
            );

        $this->robotInfo->userLevelParams['max_hp'] = $this->userInfo->userLevelParams['max_hp'] + 5000;
        $this->robotInfo->userFightData['hp'] = $this->robotInfo->userLevelParams['max_hp'];
    }

    private function _setUserLevelParamsProportions()
    {
        $charSum =
            $this->userInfo->userLevelParams['strength']
            + $this->userInfo->userLevelParams['agility']
            + $this->userInfo->userLevelParams['intellect'];

        $this->levelParamsProportions = array();
        $this->levelParamsProportions['strength'] = $this->userInfo->userLevelParams['strength']/$charSum;
        $this->levelParamsProportions['intellect'] = $this->userInfo->userLevelParams['intellect']/$charSum;
        $this->levelParamsProportions['agility'] = max(0,
                                                       1 - ($this->levelParamsProportions['strength'] + $this->levelParamsProportions['intellect']));
    }

    private function _setBotSpecials()
    {
        $this->robotInfo->userSpecials
            = $this->specials->getSpecialsListByIntellect(
            $this->robotInfo->userLevelParams['intellect']
        );
    }

    private function _setBotInventory()
    {
        $this->robotInfo->userInventory = array();

        if ($this->robotInfo->userLevelParams['strength'] > $this->robotInfo->userLevelParams['agility']) {
            $damage = 560;
            $speed = 800;
        }
        else {
            $damage = 800;
            $speed = 560;
        }

        $itemInfo = array(
            'item_id' => 0,
            'arrangement_variant' => 'both_hands',
            'rubric_code' => 'weapons',
            'name_ru' => 'Модные перчатки',
            'picture' => 'gloves090',
            'description_ru' => 'Крутые перчатки',
            'apply_in_fight' => 'none',
            'max_hp' => 5000,
            'hp' => 5000,
            'no_wear' => 1,
            'count' => 1,
            'put' => 1,
            'bonuses' => "<?xml version='1.0' encoding='UTF-8'?>\n<bonuses><fight><damage>$damage</damage><speed>$speed</speed></fight></bonuses>",
        );

        $this->robotInfo->userInventory[] = $itemInfo;
    }

}
