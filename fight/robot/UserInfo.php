<?php
namespace fight\robot;

use krest\base\Struct;

class UserInfo extends Struct
{
    public
        $userInfo = array(),
        $userFightData = array(),
        $userLevelParams = array(),
        $userInventory = array(),
        $userSpecials = array();
}
