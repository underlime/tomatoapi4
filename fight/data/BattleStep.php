<?php
namespace fight\data;

use krest\base\Struct;

class BattleStep extends Struct
{
    const TYPE_SIMPLE = 'simple';
    const TYPE_SPECIAL = 'special';
    const TYPE_ACT_ITEM = 'act_item';
    const TYPE_MED_ITEM = 'med_item';

    protected $who;
    protected $who_role;
    protected $type;
    protected $damage;
    protected $special;
    protected $user_hp;
    protected $enemy_hp;
    protected $crit = false;
    protected $block = false;
    protected $dodge = false;
    protected $act_again = false;
    protected $spec_data = array();
    protected $item_data = array();

    public function __construct($who, $type, $damage = 0)
    {
        $this->who = $who;
        $this->type = $type;
        $this->damage = $damage;

        $this->special = ($type == self::TYPE_SPECIAL);
    }

    /**
     * Был критический удар
     */
    public function setCrit()
    {
        $this->crit = true;
    }

    /**
     * Был блок
     */
    public function setBlock()
    {
        $this->block = true;
    }

    /**
     * Был уворот
     */
    public function setDodge()
    {
        $this->dodge = true;
    }

    /**
     * Повторных ход
     */
    public function setActAgain()
    {
        $this->act_again = true;
    }

    /**
     * Установить уровень здоровья
     * @param float $userHp
     * @param float $enemyHp
     */
    public function setHp($userHp, $enemyHp)
    {
        $this->user_hp = round($userHp, 3);
        $this->enemy_hp = round($enemyHp, 3);
    }

    /**
     * Установить информацию о спецприеме
     * @param array $specInfo
     * @param array $steps
     */
    public function setSpecData(array $specInfo, array $steps)
    {
        $this->spec_data = array(
            'spec_info' => $specInfo,
            'spec_steps' => array("_init" => $steps)
        );
    }

    /**
     * Установить информацию о предмете
     * @param array $itemInfo
     * @internal param array $specInfo
     * @internal param array $steps
     */
    public function setItemData(array $itemInfo)
    {
        $this->item_data = $itemInfo;
    }

    /**
     * Задать роль пользователя
     * @param string $role
     */
    public function setWhoRole($role)
    {
        $this->who_role = $role;
    }

    public function getWho()
    {
        return $this->who;
    }
}
