<?php
namespace fight\data;

use krest\base\Struct;

class BattleTape extends Struct
{
    /**
     * Кто победил
     * @var int user_id победителя
     */
    public $wins;

    public $userId, $enemyId;

    public $userInfo, $enemyInfo;
    public $userLevelParams, $enemyLevelParams;
    public $userFightData, $enemyFightData;
    public $userInventory, $enemyInventory;

    public $arenaData;

    /**
     * Шаги боя
     * @var BattleStep[]
     */
    public $battle = array();
    public $stepsCount = 0;

    /**
     * Использованные предметы
     * @var array
     */
    public $usedItems = array();

    /**
     * Прибыль
     * @var array
     */
    public $profit = array();

    /**
     * Добавить шаг
     * @param \fight\data\BattleStep $step
     * @param int $userHp
     * @param int $enemyHp
     */
    public function setBattleStep(BattleStep $step, $userHp = null, $enemyHp = null)
    {
        if (!is_null($userHp) && !is_null($enemyHp)) {
            $step->setHp($userHp, $enemyHp);
        }

        $role = ($step->getWho() == $this->userId) ? 'user' : 'enemy';
        $step->setWhoRole($role);

        $this->battle[] = $step;
        ++$this->stepsCount;
    }

    /**
     * Последший шаг повлечет повторный
     */
    public function setActAgain()
    {
        $this->battle[$this->stepsCount - 1]->setActAgain();
    }
}
