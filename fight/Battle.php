<?php
namespace fight;

use fight\data\BattleStep;
use fight\data\BattleTape;
use fight\robot\RobotFactory;
use fight\robot\UserInfo;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;
use krest\Prob;
use krest\Request;
use model\Achievements;
use model\Clans;
use model\Gains;
use model\Items;
use model\Specials;
use model\User;
use model\user\Bonuses;
use model\UsersJournal;
use model\usersjournal\ProfitData;

class Battle
{
    const K_CRIT = 0.1;
    const K_BLOCK = 0.6;
    const K_DODGE = 0.35;
    const K_SPECIAL = 0.3;

    const ACTIVE_ITEM_P = 0.4;
    const GRENADES_BONUS = 0.15;
    const MEDICINES_BONUS = 0.07;

    private $arenaData;

    private $userSocNetId;
    private $userId;
    private $userInfo;
    private $userFightData;
    private $userLevelParams;
    private $userInventory;
    private $userSpecials;

    private $enemyId;
    private $easeEnemy;
    private $enemyInfo;
    private $enemyFightData;
    private $enemyLevelParams;
    private $enemyInventory;
    private $enemySpecials;

    private $usersTmpData;

    private $probs = array();
    private $acted;

    private $actItems = array();
    private $medItems = array();
    private $has = array();
    private $who;
    private $whoEnemy;

    /**
     * @var \model\user\Bonuses
     */
    private $userBonuses;

    /**
     * @var \model\user\Bonuses
     */
    private $enemyBonuses;

    /**
     * @var \model\User
     */
    private $user;

    /**
     * @var \model\Items
     */
    private $items;

    /**
     * @var \model\Specials
     */
    private $specials;

    /**
     * @var \model\UsersJournal
     */
    private $usersJournal;

    /**
     * @var \model\Achievements
     */
    private $achievements;

    /**
     * @var \fight\data\BattleTape
     */
    private $battleTape;

    private $grenadesUsed = 0;
    private $medicinesUsed = 0;

    private $userBonusHp = 0;
    private $enemyBonusHp = 0;


    function __construct()
    {
        $this->user = new User();
        $this->items = new Items();
        $this->specials = new Specials();
        $this->battleTape = new BattleTape();
        $this->usersJournal = new UsersJournal();
        $this->achievements = new Achievements();
    }

    /**
     * @param string $userSocNetId
     * @param string $enemyId
     * @param bool $easeEnemy
     * @return \fight\data\BattleTape
     */
    public function run($userSocNetId, $enemyId, $easeEnemy)
    {
        $this->userSocNetId = $userSocNetId;
        $this->enemyId = $enemyId;
        $this->easeEnemy = $easeEnemy;

        $this->_checkRobot();
        $this->_setArena();
        $this->_setUserInfo();
        $this->_setEnemyInfo();
        $this->_createUsersTmpData();
        $this->_calculateProbabilities();
        $this->_collectHasInfo();
        $this->_initBattleTape();
        $this->_makeFight();

        $this->_saveFightEffect();
        return $this->battleTape;
    }

    private function _createUsersTmpData()
    {
        $userData = array_merge(
            $this->userLevelParams,
            $this->userBonuses->fight->asArray(),
            array(
                 'work_hp' => $this->userFightData['hp'],
                 'specials' => array_values($this->userSpecials),
            )
        );

        $enemyData = array_merge(
            $this->enemyLevelParams,
            $this->enemyBonuses->fight->asArray(),
            array(
                 'work_hp' => $this->enemyFightData['hp'],
                 'specials' => array_values($this->enemySpecials),
            )
        );

        $this->usersTmpData = array(
            $this->userId => $userData,
            $this->enemyId => $enemyData,
        );
    }

    private function _checkRobot()
    {
        $hour = (int)date('H');
        if (($hour == 23 or $hour < 6) && mt_rand(1, 100) == 36) {
            $this->enemyId = -2;
        }
    }

    private function _setArena()
    {
        $sql = 'SELECT image, name_ru FROM arenas';
        $arenasData = Db::instance()->select($sql);
        if ($arenasData) {
            $arenaIndex = mt_rand(0, sizeof($arenasData) - 1);
            $this->arenaData = $arenasData[$arenaIndex];
        }

        if (!$this->arenaData or defined('HALLOWEEN_MOD')) {
            if (rand(0, 10) < 5) {
                $image = 'PumpkinOrange';
            } else {
                $image = 'PumpkinGreen';
            }

            $this->arenaData = array(
                'image' => $image,
                'name_ru' => 'Halloween',
            );
        }
    }

    private function _setUserInfo()
    {
        $this->userInfo = $this->user->getUserInfo($this->userSocNetId);
        $this->userId = $this->userInfo['user_id'];

        $this->user->disableCache();
        $this->userFightData = $this->user->getUserFightData($this->userId, true);
        if ($this->userFightData['energy'] < 10) {
            throw new InvalidDataException('Energy is not enough');
        }

        $this->userLevelParams = $this->user->getUserLevelParams($this->userId);
        $this->user->enableCache();

        $this->userBonuses = $this->user->getUserBonuses($this->userId);

        $this->userInventory = $this->_selectPutItems($this->userId);
        $this->userSpecials = $this->_selectActiveSpecials($this->userId);

        $userScale = $this->userBonuses->scale->asArray();
        $this->userBonusHp = $userScale['hp'];

        $this->userFightData['hp'] += $this->userBonusHp;
        $this->userLevelParams['max_hp'] += $this->userBonusHp;

        if (defined('HALLOWEEN_MOD')) {
            $this->_setHalloweenModParts($this->userInfo);
        }
    }

    private function _selectPutItems($userId)
    {
        $this->items->disableCache();
        $allItems = $this->items->getUsersInventory($userId);
        $this->items->enableCache();

        $putItems = array();
        foreach ($allItems as $itemId => $itemData) {
            if ($itemData['put'] or $itemData['social_put']) {
                $putItems[$itemId] = $itemData;
            }
        }

        return $putItems;
    }

    private function _selectActiveSpecials($userId)
    {
        $allSpecials = $this->specials->getUsersSpecials($userId);
        $userSpecials = array();
        foreach ($allSpecials as $specialData) {
            if ($specialData['active']) {
                $userSpecials[$specialData['special_id']] = $specialData;
            }
        }

        return $userSpecials;
    }

    private function _setHalloweenModParts(array &$userInfo)
    {
        //Общие части
        $userInfo['eyes'] = 'eyes91';
        $userInfo['mouth'] = 'mouth91';
        $userInfo['hair'] = 'tHair91';

        $r = rand(0, 30);

        //Руки
        if ($r < 10) {
            $userInfo['hands'] = 'tArm93';
        } elseif ($r < 20) {
            $userInfo['hands'] = 'tArm92';
        } else {
            $userInfo['hands'] = 'tArm91';
        }

        //Тела
        if (in_array($userInfo['vegetable'], array('tomato', 'beta', 'onion'))) {
            if ($r < 10) {
                $userInfo['body'] = 'tBody93';
            } elseif ($r < 20) {
                $userInfo['body'] = 'tBody92';
            } else {
                $userInfo['body'] = 'tBody91';
            }
        } else {
            if ($r < 10) {
                $userInfo['body'] = 'cuBody93';
            } elseif ($r < 20) {
                $userInfo['body'] = 'cuBody92';
            } else {
                $userInfo['body'] = 'cuBody91';
            }
        }
    }

    private function _setEnemyInfo()
    {
        if ($this->enemyId > 0) {
            $this->_getRealEnemyInfo();
        } elseif ($this->enemyId < 0) {
            $this->_getBotEnemyInfo();
        } else {
            throw new InvalidDataException('Wrong enemy_id');
        }
    }

    private function _getRealEnemyInfo()
    {
        $socNetId = $this->user->getSocNetIdByUserId($this->enemyId);
        $this->enemyInfo = $this->user->getUserInfo($socNetId);

        $this->user->disableCache();
        $this->enemyFightData = $this->user->getUserFightData($this->enemyId);
        $this->user->enableCache();

        $this->enemyLevelParams = $this->user->getUserLevelParams($this->enemyId);
        $this->enemyBonuses = $this->user->getUserBonuses($this->enemyId);

        $enemyScale = $this->enemyBonuses->scale->asArray();
        $this->enemyBonusHp = $enemyScale['hp'];

        $this->enemyFightData['hp'] += $this->enemyBonusHp;
        $this->enemyLevelParams['max_hp'] += $this->enemyBonusHp;

        if ($this->easeEnemy) {
            $price = $this->user->getEaseEnemyPrice();
            $this->user->pay($this->userId, $price, 0, 'Ослабить врага');
            $this->enemyFightData['hp'] = round($this->enemyFightData['hp'] * 0.75);
        }

        $this->enemyInventory = $this->_selectPutItems($this->enemyId);
        $this->enemySpecials = $this->_selectActiveSpecials($this->enemyId);
    }

    private function _getBotEnemyInfo()
    {
        $userInfo = new UserInfo();
        $userInfo->userInfo = $this->userInfo;
        $userInfo->userFightData = $this->userFightData;
        $userInfo->userLevelParams = $this->userLevelParams;
        $userInfo->userInventory = $this->userInventory;
        $userInfo->userSpecials = $this->userSpecials;

        $bot = RobotFactory::getRobot($this->enemyId, $userInfo);

        $robotInfo = $bot->getRobotInfo();
        $this->enemyInfo = $robotInfo->userInfo;
        $this->enemyFightData = $robotInfo->userFightData;
        $this->enemyLevelParams = $robotInfo->userLevelParams;
        $this->enemyInventory = $robotInfo->userInventory;

        $allSpecials = $robotInfo->userSpecials;
        $this->enemySpecials = array();
        foreach ($allSpecials as $specialData) {
            if ($specialData['active']) {
                $this->enemySpecials[$specialData['special_id']] = $specialData;
            }
        }

        $this->enemyBonuses = new Bonuses();
    }

    private function _calculateProbabilities()
    {
        $this->probs[$this->userId] = array();
        $this->probs[$this->enemyId] = array();

        $fightDataFields = array('strength', 'agility', 'intellect');
        foreach ($fightDataFields as $fieldName) {
            $userValue = $this->userLevelParams[$fieldName];
            $enemyValue = $this->userLevelParams[$fieldName];

            $sum = $userValue + $enemyValue;
            if ($sum == 0) {
                $sum = 1;
            }
            $this->probs[$this->userId][$fieldName] = $userValue / $sum;
            $this->probs[$this->enemyId][$fieldName] = 1 - $this->probs[$this->userId][$fieldName];
        }

        $fieldName = 'speed';
        $userValue = $this->userBonuses->fight->speed;
        $enemyValue = $this->enemyBonuses->fight->speed;

        $sum = $userValue + $enemyValue;
        if ($sum == 0) {
            $sum = 1;
        }
        $this->probs[$this->userId][$fieldName] = $userValue / $sum;
        $this->probs[$this->enemyId][$fieldName] = 1 - $this->probs[$this->userId][$fieldName];

        $bonusesTable = array(
            $this->userId => $this->userBonuses,
            $this->enemyId => $this->enemyBonuses,
        );

        foreach ($bonusesTable as $userId => $userBonuses) {
            $this->probs[$userId]['crit'] = $this->probs[$userId]['agility'] * self::K_CRIT + $userBonuses->fight->crit;
            $this->probs[$userId]['block'] = $this->probs[$userId]['agility'] * self::K_BLOCK + $userBonuses->fight->block;
            $this->probs[$userId]['dodge'] = $this->probs[$userId]['agility'] * self::K_DODGE + $userBonuses->fight->dodge;
            $this->probs[$userId]['special'] = $this->probs[$userId]['intellect'] * self::K_SPECIAL + $userBonuses->fight->special;
            $this->probs[$userId]['act_again'] = ($this->probs[$userId]['agility'] * $this->probs[$userId]['speed']) + $userBonuses->fight->act_again;
        }
    }

    private function _collectHasInfo()
    {
        $this->has = array();

        $this->has['specs'] = array(
            $this->userId => (bool)$this->userSpecials,
            $this->enemyId => (bool)$this->enemySpecials,
        );

        $this->has['med'] = array();
        $this->has['act'] = array();

        $this->_collectActItemsInfo($this->userId, $this->userInventory);
        $this->_collectActItemsInfo($this->enemyId, $this->enemyInventory);
    }

    private function _collectActItemsInfo($userId, array $inventory)
    {
        $this->actItems[$userId] = array(
            'grenades' => array()
        );

        $this->medItems[$userId] = array(
            'low' => array(),
            'mid' => array(),
            'high' => array(),
        );

        foreach ($inventory as $item) {
            if (!$item['put']) {
                continue;
            }

            if ($item['apply_in_fight'] == 'grenade') {
                $this->actItems[$userId]['grenades'][] = $item;
            } elseif ($item['apply_in_fight'] == 'med_low') {
                $this->medItems[$userId]['low'][] = $item;
            } elseif ($item['apply_in_fight'] == 'med_mid') {
                $this->medItems[$userId]['mid'][] = $item;
            } elseif ($item['apply_in_fight'] == 'med_high') {
                $this->medItems[$userId]['high'][] = $item;
            }
        }

        $this->has['act'][$userId] = (bool)$this->actItems[$userId]['grenades'];
        $this->_checkHasMed($userId);
    }

    private function _checkHasMed($userId)
    {
        $this->has['med'][$userId] = ($this->medItems[$userId]['low'] or $this->medItems[$userId]['mid'] or $this->medItems[$userId]['high']);
    }

    private function _initBattleTape()
    {
        $this->battleTape->userId = $this->userId;
        $this->battleTape->enemyId = $this->enemyId;

        $this->battleTape->userInfo = $this->userInfo;
        $this->battleTape->enemyInfo = $this->enemyInfo;

        $this->battleTape->userLevelParams = $this->userLevelParams;
        $this->battleTape->enemyLevelParams = $this->enemyLevelParams;

        $this->battleTape->userFightData = $this->userFightData;
        $this->battleTape->enemyFightData = $this->enemyFightData;

        $this->battleTape->userInventory = $this->userInventory;
        $this->battleTape->enemyInventory = $this->enemyInventory;

        $this->battleTape->arenaData = $this->arenaData;

        $this->battleTape->usedItems = array(
            $this->userId => array(),
            $this->enemyId => array()
        );
    }

    private function _makeFight()
    {
        $this->acted = 1;
        mt_srand(time());
        $this->_whoCourse();
        while ($this->_beat()) {
            ;
        }
    }

    private function _whoCourse()
    {
        if ($this->userLevelParams['agility'] > $this->enemyLevelParams['agility']) {
            $this->who = $this->userId;
            $this->whoEnemy = $this->enemyId;
        } else {
            $this->who = $this->enemyId;
            $this->whoEnemy = $this->userId;
        }
    }

    private function _beat()
    {
        $userCourse = ($this->who == $this->userId);
        if ($userCourse && $this->has['med'][$this->who] && ($this->usersTmpData[$this->who]['work_hp'] < $this->usersTmpData[$this->who]['max_hp'])) {
            $this->_useMedInFight();
        }

        $pSpec = $this->probs[$this->who]['special'];
        $useItem = $userCourse && $this->has['act'][$this->who] && Prob::probably(self::ACTIVE_ITEM_P);
        if ($useItem) {
            $this->_itemBeat();
        } elseif ($this->has['specs'][$this->who] && Prob::probably($pSpec)) {
            $this->_specBeat();
        } else {
            $this->_simpleBeat();
        }

        $continue = ($this->usersTmpData[$this->userId]['work_hp'] > 0 && $this->usersTmpData[$this->enemyId]['work_hp'] > 0);
        if ($continue) {
            if (!$this->_actAgain()) {
                $tmp = $this->whoEnemy;
                $this->whoEnemy = $this->who;
                $this->who = $tmp;
                $this->acted = 1;
            } else {
                ++$this->acted;
                $this->battleTape->setActAgain();
            }
        } else {
            $this->battleTape->wins = $this->who;
        }

        return $continue;
    }

    private function _useMedInFight()
    {
        $userId = $this->who;
        $hpPart = $this->usersTmpData[$userId]['work_hp'] / $this->usersTmpData[$userId]['max_hp'];

        if ($hpPart <= 0.2 && $this->medItems[$userId]['high']) {
            $this->_userMedByGroup('high');
        } elseif ($hpPart <= 0.5 && $this->medItems[$userId]['mid']) {
            $this->_userMedByGroup('mid');
        } elseif ($hpPart <= 0.8 && $this->medItems[$userId]['low']) {
            $this->_userMedByGroup('low');
        }

        $this->_checkHasMed($userId);
        ++$this->medicinesUsed;
    }

    private function _userMedByGroup($group)
    {
        $num = mt_rand(0, sizeof($this->medItems[$this->who][$group]) - 1);
        $item = $this->medItems[$this->who][$group][$num];

        if ($this->medItems[$this->who][$group][$num]['put'] > 1) {
            --$this->medItems[$this->who][$group][$num]['put'];
        } else {
            array_splice($this->medItems[$this->who][$group], $num, 1);
        }

        $this->battleTape->usedItems[$this->who][] = $item['item_id'];

        $heal = $this->usersTmpData[$this->who]['max_hp'] * ($item['heal_percents'] / 100);
        $this->usersTmpData[$this->who]['work_hp'] = min(
            $this->usersTmpData[$this->who]['max_hp'],
            $this->usersTmpData[$this->who]['work_hp'] + $heal
        );

        $step = new BattleStep($this->who, BattleStep::TYPE_MED_ITEM, 0);
        $step->setActAgain();
        $itemData = array(
            'picture' => $item['picture'],
            'heal' => $heal
        );
        $step->setItemData($itemData);
        $this->battleTape->setBattleStep(
            $step,
            $this->usersTmpData[$this->userId]['work_hp'],
            $this->usersTmpData[$this->enemyId]['work_hp']
        );
    }

    private function _itemBeat()
    {
        $num = mt_rand(0, sizeof($this->actItems[$this->who]['grenades']) - 1);
        $item = $this->actItems[$this->who]['grenades'][$num];

        if ($this->actItems[$this->who]['grenades'][$num]['put'] > 1) {
            --$this->actItems[$this->who]['grenades'][$num]['put'];
        } else {
            array_splice($this->actItems[$this->who]['grenades'], $num, 1);
        }

        $this->has['act'][$this->who] = (bool)$this->actItems[$this->who]['grenades'];
        $this->battleTape->usedItems[$this->who][] = $item['item_id'];

        $damage = $item['fight_damage'] * $this->usersTmpData[$this->whoEnemy]['max_hp'] / 100;
        $this->usersTmpData[$this->whoEnemy]['work_hp'] = max(
            0,
            $this->usersTmpData[$this->whoEnemy]['work_hp'] - $damage
        );

        $step = new BattleStep($this->who, BattleStep::TYPE_ACT_ITEM, $damage);
        $itemData = array('picture' => $item['picture']);
        $step->setItemData($itemData);
        $this->battleTape->setBattleStep(
            $step,
            $this->usersTmpData[$this->userId]['work_hp'],
            $this->usersTmpData[$this->enemyId]['work_hp']
        );
        ++$this->grenadesUsed;
    }

    private function _specBeat()
    {
        $num = mt_rand(0, sizeof($this->usersTmpData[$this->who]['specials']) - 1);
        $special = $this->usersTmpData[$this->who]['specials'][$num];

        $damage = ($this->usersTmpData[$this->who]['strength'] + $this->usersTmpData[$this->who]['damage']) * $special['k_damage'];

        $stepDamage = $damage / $special['steps'];
        $realDamage = 0;
        $steps = array();

        for ($i = 0; $i < $special['steps']; ++$i) {
            $dodge = Prob::probably($this->probs[$this->whoEnemy]['dodge']);
            if ($dodge) {
                $curDamage = 0;
            } else {
                $kd = 1 - (mt_rand() / mt_getrandmax()) * 0.4;
                $curDamage = $kd * $stepDamage;
            }

            $cleanCurDamage = round($curDamage);
            $steps[] = array(
                'dodge' => (int)$dodge,
                'damage' => $cleanCurDamage
            );

            $realDamage += $curDamage;
        }

        $hp = max(0, $this->usersTmpData[$this->whoEnemy]['work_hp'] - $realDamage);
        $this->usersTmpData[$this->whoEnemy]['work_hp'] = $hp;

        $cleanDamage = round($realDamage);
        $step = new BattleStep($this->who, BattleStep::TYPE_SPECIAL, $cleanDamage);
        $step->setSpecData($special, $steps);
        $this->battleTape->setBattleStep(
            $step,
            $this->usersTmpData[$this->userId]['work_hp'],
            $this->usersTmpData[$this->enemyId]['work_hp']
        );
    }

    private function _simpleBeat()
    {
        $dodge = Prob::probably($this->probs[$this->whoEnemy]['dodge']);
        if ($dodge) {
            $this->_simpleDodge();
        } else {
            $this->_simpleSetDamage();
        }
    }

    private function _simpleDodge()
    {
        $step = new BattleStep($this->who, BattleStep::TYPE_SIMPLE);
        $step->setDodge();
        $this->battleTape->setBattleStep(
            $step,
            $this->usersTmpData[$this->userId]['work_hp'],
            $this->usersTmpData[$this->enemyId]['work_hp']
        );
    }

    private function _simpleSetDamage()
    {
        $kd = 1 - (mt_rand() / mt_getrandmax()) * 0.4;
        $zb = $kd * ($this->usersTmpData[$this->who]['strength'] + $this->usersTmpData[$this->who]['damage']);

        $kr = 1 - (mt_rand() / mt_getrandmax()) * 0.4;
        $r = $kr * $this->usersTmpData[$this->whoEnemy]['armor'];

        $crit = Prob::probably($this->probs[$this->who]['crit']);
        $zk = 3 * ((int)$crit) * $zb;

        $za = $zb + $zk - $r;
        $damage = max(0, $za);

        $block = Prob::probably($this->probs[$this->whoEnemy]['block']);
        if ($block) {
            $damage *= $this->probs[$this->who]['strength'];
        }

        $hp = max(0, $this->usersTmpData[$this->whoEnemy]['work_hp'] - $damage);
        $this->usersTmpData[$this->whoEnemy]['work_hp'] = $hp;

        $cleanDamage = round($damage);
        $step = new BattleStep($this->who, BattleStep::TYPE_SIMPLE, $cleanDamage);
        if ($block) {
            $step->setBlock();
        }

        if ($crit) {
            $step->setCrit();
        }

        $this->battleTape->setBattleStep(
            $step,
            $this->usersTmpData[$this->userId]['work_hp'],
            $this->usersTmpData[$this->enemyId]['work_hp']
        );
    }

    private function _actAgain()
    {
        $p = (1 / $this->acted) * $this->probs[$this->who]['act_again'];
        return Prob::probably($p);
    }

    private function _saveFightEffect()
    {
        $userProfit = $this->_getUsersProfitArray();
        $this->_getEnemyProfitArray();

        $enemyWins = ($this->battleTape->wins == $this->enemyId);
        if ($enemyWins) {
            $field = 'fails_level';
            $value = $this->userFightData['battles_count'] - $this->userFightData['wins_count'];
        } else {
            $field = 'wins_level';
            $value = $this->userFightData['wins_count'];
        }

        $achvParams = array($field => $value);
        $this->achievements->checkAchievementByFields($this->userId, $achvParams);

        $this->battleTape->profit = $userProfit;
    }

    private function _getUsersProfitArray()
    {
        $userProfit = $this->_getProfitArray($this->userId);
        return $userProfit;
    }

    private function _getEnemyProfitArray()
    {
        if ($this->enemyId > 0) {
            $enemyProfit = $this->_getProfitArray($this->enemyId);
            $enemyWins = ($this->battleTape->wins == $this->enemyId);
            $this->_writeEnemyJournal($enemyProfit, $enemyWins);
        } else {
            $enemyProfit = array();
        }

        return $enemyProfit;
    }

    private function _getProfitArray($userId)
    {
        $profitArray = $this->_updateUserFightData($userId);
        $userLevelProfit = $this->_updateUserLevelData($userId);
        $profitArray = array_merge($profitArray, $userLevelProfit);

        $usedItems = Arr::get($this->battleTape->usedItems, $userId);
        if ($usedItems) {
            $profitArray['used_items'] = $this->_updateUsedItems($userId, $usedItems);
        }

        return $profitArray;
    }

    private function _writeEnemyJournal(array $enemyProfit, $enemyWins)
    {
        $eventType = ($enemyWins ? UsersJournal::WIN : UsersJournal::FAIL);
        $profitData = new ProfitData();
        $profitData->importArray($enemyProfit);

        $description = json_encode(
            array(
                 'soc_net_id' => $this->userInfo['soc_net_id'],
                 'login' => $this->userInfo['login'],
            )
        );
        $this->usersJournal->addMessage($this->enemyId, $eventType, $this->userId, $profitData, $description);
    }

    private function _updateUserFightData($userId)
    {
        $isUser = ($userId == $this->userId);
        $isWinner = ($userId == $this->battleTape->wins);

        $anotherId = ($isUser) ? $this->enemyId : $this->userId;

        if ($isUser) {
            $gains = new Gains();
            $clans = new Clans();
        } else {
            $gains = null;
            $clans = null;
        }

        $userCharsLevel = $this->usersTmpData[$userId]['strength'] + $this->usersTmpData[$userId]['intellect'];
        $enemyCharsLevel = $this->usersTmpData[$anotherId]['strength'] + $this->usersTmpData[$anotherId]['intellect'];
        if ($userCharsLevel < 1) {
            $userCharsLevel = 1;
        }
        if ($enemyCharsLevel < 1) {
            $enemyCharsLevel = 1;
        }

        $dLevel = $enemyCharsLevel - $userCharsLevel;
        if (abs($dLevel) <= 25) {
            $kLevel = 1;
        } else {
            $kLevel = $enemyCharsLevel / $userCharsLevel;
        }

        if ($isUser) {
            $fightData = $this->userFightData;
        } else {
            $fightData = $this->enemyFightData;
        }

        if ($isUser) {
            $consumablesBonusK = $this->grenadesUsed * self::GRENADES_BONUS
                + $this->medicinesUsed * self::MEDICINES_BONUS;
        } else {
            $consumablesBonusK = 0;
        }

        $newValues = array();
        $profit = array();
        $newValues['battles_count'] = $fightData['battles_count'] + 1;
        if ($isUser) {
            $dltEnergy = -10;
            //Применение ГМО к энергии
            if ($gains && $gains->hasUserGain($userId, Gains::ENERGY)) {
                $dltEnergy = floor($dltEnergy / 2);
            }
            $newValues['energy'] = $fightData['energy'] + $dltEnergy;
            if ($isWinner) {
                $dGlory = min(9, round(3 * $kLevel) * (1 + $consumablesBonusK));
                $dGlory = round($dGlory) + mt_rand(1, 3);
                //Применение ГМО к славе
                if ($gains && $gains->hasUserGain($userId, Gains::GLORY)) {
                    $dGlory *= 2;
                }
                $profit['glory'] = $dGlory;

                $newGlory = $fightData['glory'] + $dGlory;
                $this->user->updateGlory($userId, $newGlory);

                $clanId = $clans->getUsersClan($userId);
                if ($clanId !== null) {
                    $clans->incVictories($clanId);
                }
            }
        }

        if ($isWinner) {
            $newValues['wins_count'] = $fightData['wins_count'] + 1;
            $dExp = min(50, round(10 * $kLevel * (1 + $consumablesBonusK)));
            $dExp = round($dExp) + mt_rand(1, 3);
            //Применение ГМО к опыту
            if ($gains && $gains->hasUserGain($userId, Gains::EXPERIENCE)) {
                $dExp *= 2;
            }
            $newValues['experience'] = $fightData['experience'] + $dExp;
            $profit['experience'] = $dExp;
            $dTomatos = min(200, round(25 * $kLevel * (1 + $consumablesBonusK)));
            $dTomatos = round($dTomatos) + mt_rand(1, 10);
            //Применение ГМО к томатосам
            if ($gains && $gains->hasUserGain($userId, Gains::FIGHT_TOMATOS)) {
                $dTomatos *= 2;
            }
            $profit['tomatos'] = $dTomatos;
        } elseif ($isUser) {
            $newValues['experience'] = $fightData['experience'] + 1;
            $profit['experience'] = 1;
            $profit['tomatos'] = 3;
            $dTomatos = 3;
        } else {
            $profit['experience'] = 0;
            $profit['tomatos'] = 0;
            $dTomatos = 0;
        }

        $this->user->updateUserFightData($userId, $newValues);
        if ($dTomatos) {
            $this->user->addMoney($userId, 0, $dTomatos, 'Битва');
        }

        return $profit;
    }

    private function _updateUserLevelData($userId)
    {
        $profit = array();
        $maxLevel = $this->user->levelUpIfNeed($userId);

        $dLevel = $maxLevel - $this->userLevelParams['max_level'];
        $profit['level'] = $dLevel;

        return $profit;
    }

    private function _updateUsedItems($userId, array $usedItems)
    {
        $decTable = array();
        foreach ($usedItems as $itemId) {
            if (!isset($decTable[$itemId])) {
                $decTable[$itemId] = 1;
            } else {
                ++$decTable[$itemId];
            }
        }

        foreach ($decTable as $itemId => $dec) {
            $this->items->incDecItemCount($userId, $itemId, -$dec);
        }

        return $decTable;
    }
}
