<?php

use base\Model;
use krest\Arr;
use krest\Cache;
use krest\Db;
use krest\exceptions\InvalidDataException;
use krest\Request;
use krest\SocNet;
use lib\RediskaFactory;

class SocNetRouter
{
    private static $_dbTable = [
        SocNet::VK_COM => "default",
        SocNet::OK_RU => "ok_ru",
        SocNet::MM_RU => "mm_ru",
    ];

    public static function run()
    {
        $socNet = self::_getSocNetName();
        self::_setSocNet($socNet);
        self::_setCachePrefix($socNet);
        self::_setDbName($socNet);
    }

    /**
     *
     * @throws \krest\exceptions\InvalidDataException
     * @return string
     */
    private static function _getSocNetName()
    {
        $url = Arr::get($_SERVER, "REQUEST_URI");
        if (strpos($url, "/vk.com/") !== false) {
            $socNet = SocNet::VK_COM;
        }
        elseif (strpos($url, "/odnoklassniki.ru/") !== false) {
            $socNet = SocNet::OK_RU;
        }
        else {
            throw new InvalidDataException("Wrong social network code");
        }
        return $socNet;
    }

    private static function _setSocNet($socNetName)
    {
        SocNet::setSocNetName($socNetName);
    }

    /**
     * @param $socNet
     */
    private static function _setCachePrefix($socNet)
    {
        Cache::setPrefix("tomato_kombat:$socNet:");
    }

    private static function _setDbName($socNet)
    {
        $dbName = Arr::req(self::$_dbTable, $socNet);
        Db::setDefaultName($dbName);
        RediskaFactory::setDefaultNamespace($dbName);
    }
}
