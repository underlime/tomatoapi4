<?php
namespace config;

use krest\Cache;

return array(
    //По умолчанию (Development)
    'default' => array(
        'config_sets_dir' => __DIR__,
        'data_dir' => __DIR__,
        'cache_provider' => Cache::FAKE,
        'payments_testing' => true,
        'tk_event_broker' => array(
            'host' => 'localhost',
            'port' => '8000',
        ),
        'sql_db' => array(
            'default' => array(
                //'unix_socket' => '/var/run/mysql.sock',
                'host' => 'localhost',
                'db_name' => 'tomato_kombat',
                'user' => 'root',
                'password' => '1qaz3wasql',
                'soc_net' => 'vk.com',
            ),
            'ok_ru' => array(
                //'unix_socket' => '/var/run/mysql.sock',
                'host' => 'localhost',
                'db_name' => 'tomato_kombat_ok',
                'user' => 'root',
                'password' => '1qaz3wasql',
                'soc_net' => 'odnoklassniki.ru',
            ),
        ),
        'soc_net' => array(
            'VkCom' => array(
                'api_id' => 2893571,
                'api_secret' => 'TmmJet3LZ0WnNItJpWic',
                'send_at_once_notifications' => 100,
                'notifications_timeout' => 1,
            ),
            'OkRu' => array(
                'api_id' => '429224704',
                'api_key' => 'CBAEHGJKBBABABABA',
                'api_secret' => '64380B184C3581AEC38FB83E',
            ),
            'MmRu' => array(
                'api_id' => 1,
                'api_key' => 'key',
                'api_secret' => 'secret',
            ),
        ),
    ),
    'local_dev' => array(
        'extends' => 'default',
        'sql_db' => array(
            'default' => array(
                'host' => '192.168.2.10',
                'db_name' => 'tomato_kombat',
                'user' => 'root',
                'password' => 'password',
                'soc_net' => 'vk.com',
            ),
            'ok_ru' => array(
                'host' => '192.168.2.10',
                'db_name' => 'tomato_kombat_ok',
                'user' => 'root',
                'password' => 'password',
                'soc_net' => 'odnoklassniki.ru',
            ),
        ),
        'soc_net' => array(
            'VkCom' => array(
                'api_id' => 1,
                'api_secret' => 'Secret',
                'send_at_once_notifications' => 100,
                'notifications_timeout' => 1,
            ),
            'OkRu' => array(
                'api_id' => '429224704',
                'api_key' => 'CBAEHGJKBBABABABA',
                'api_secret' => '64380B184C3581AEC38FB83E',
            ),
            'MmRu' => array(
                'api_id' => 1,
                'api_key' => 'key',
                'api_secret' => 'secret',
            ),
        ),
    ),
    //Тестирование на сервере
    'server_dev' => array(
        'extends' => 'default',
        'tk_event_broker' => array(
            'host' => 'tomato-kombat.hub.underlime.net',
            'port' => '80',
        ),
        'soc_net' => array(
            'VkCom' => array(
                'api_id' => 3875155,
                'api_secret' => '2qP8gvewpm5mWvMK3vc3',
                'send_at_once_notifications' => 100,
                'notifications_timeout' => 1,
            ),
            'OkRu' => array(
                'api_id' => '429224704',
                'api_key' => 'CBAEHGJKBBABABABA',
                'api_secret' => '64380B184C3581AEC38FB83E',
            ),
            'MmRu' => array(
                'api_id' => 1,
                'api_key' => 'key',
                'api_secret' => 'secret',
            ),
        ),
    ),
    //Боевой сервер
    'production_server' => array(
        'config_sets_dir' => __DIR__,
        'data_dir' => realpath(__DIR__.'/../..'),
        'cache_provider' => Cache::MEMCACHE,
        'payments_testing' => false,
        'tk_event_broker' => array(
            'host' => 'tk-api.underlime.net',
            'port' => '80',
        ),
        'sql_db' => array(
            'default' => array(
                'host' => 'localhost',
                'db_name' => 'tomato_kombat',
                'user' => 'root',
                'password' => '1qaz3wasql',
                'soc_net' => 'vk.com',
            ),
            'ok_ru' => array(
                'host' => 'localhost',
                'db_name' => 'tomato_kombat_ok',
                'user' => 'root',
                'password' => '1qaz3wasql',
                'soc_net' => 'odnoklassniki.ru',
            ),
        ),
        'soc_net' => array(
            'VkCom' => array(
                'api_id' => 2848022,
                'api_secret' => 'sjCfUbTPIMKHDqiHIKCF',
                'send_at_once_notifications' => 100,
                'notifications_timeout' => 1,
            ),
            'OkRu' => array(
                'api_id' => '1078238720',
                'api_key' => 'CBAPJFEBEBABABABA',
                'api_secret' => '3FA9D6D9795055108BDECD4F',
            ),
        ),
    ),
);
