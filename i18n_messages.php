<?php
return [
    'ru' => [
        'add_to_clan_header' => 'Вас добавили в клан',
        'add_to_clan_notification' => 'Вас добавили в клан «{clan_name}»',
        'exile_from_clan_header' => 'Вас изгнали из клана',
        'exile_from_clan_notification' => 'Вас изгнали из клана «{clan_name}»',
        'transfer_item_header' => 'Вам передан предмет',
        'transfer_item_notification' => 'Вам предмет «{item_name}» от {user_name}',
        'clans_clash_prize_header' => 'Награда за битву кланов',
        'clans_clash_prize_message' => 'За проявленную в битве кланов доблесть вам вручается приз {prize}!',
    ],
    'en' => [],
];
