<?php
namespace index_file;

use base\Model;
use krest\api\Application;
use krest\api\AutoLoader;
use krest\api\Config;
use krest\api\FailsHandler;
use krest\api\SpySaver;
use krest\Arr;
use krest\Request;
use krest\Response;
use krest\SocNet;
use krest\utility\XHProf;
use krest\View;
use lib\RediskaFactory;
use model\spy\Spy;

require __DIR__.'/krest/api/AutoLoader.php';
$autoLoader = new AutoLoader(__DIR__);
spl_autoload_register(array($autoLoader, 'load'));
require __DIR__.'/vendor/autoload.php';

$config = realpath('config.php');
Config::setDefaultConfigFile($config);

$enableXhprof = extension_loaded('xhprof') && !empty($_COOKIE['XHPROF_ENABLE']);
if ($enableXhprof) {
    $profiler = new XHProf('tk');
    $profiler->start();
}
else {
    $profiler = null;
}

$failsHandler = new FailsHandler();
set_exception_handler([$failsHandler, 'exceptionHandler']);
set_error_handler([$failsHandler, 'errorHandler'], E_ALL | E_STRICT);

$spySaver = new SpySaver();
register_shutdown_function([$spySaver, 'saveData']);

try {
    \SocNetRouter::run();
}
catch (\Exception $e) {
    header('Content-type: text/plain; charset=UTF-8');
    header('HTTP/1.0 400 Wrong request');
    die($e->getMessage());
}

Config::setRedisClient(RediskaFactory::getInstance());
\Logger::configure(Config::instance()->getConfig()['_logger']);
$result = Application::factory(__DIR__)
          ->execute()
          ->getAnswer();

if ($enableXhprof) {
    $result['run_id'] = $profiler->stop();
}

$body = View::factory($result)
        ->render();

Response::instance()
    ->setNewBody($body)
    ->sendHeaders()
    ->echoBody();
