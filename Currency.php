<?php

use krest\exceptions\InvalidDataException;

class Currency
{
    const FERROS = 'f';
    const TOMATOS = 't';

    /**
     * Извлечь цены в обеих валютах, если указана только одна
     * @static
     * @param $price
     * @param $currency
     * @return array array($ferros, $tomatos)
     * @throws \krest\exceptions\InvalidDataException
     */
    public static function getPricesByOne($price, $currency)
    {
        if ($currency == self::FERROS or $currency == 'ferros') {
            $ferros = $price;
            $tomatos = 0;
        }
        elseif ($currency == self::TOMATOS or $currency == 'tomatos') {
            $ferros = 0;
            $tomatos = $price;
        }
        else {
            throw new InvalidDataException('Wrong currency');
        }

        return array($ferros, $tomatos);
    }

    /**
     * Извлечь цены в обеих валютах, но только одна <> 0
     * @param $ferrosPrice
     * @param $tomatosPrice
     * @param $currency
     * @return array
     * @throws \krest\exceptions\InvalidDataException
     */
    public static function getFinalPriceByItemParams($ferrosPrice, $tomatosPrice, $currency)
    {
        if ($currency == self::FERROS) {
            $ferros = $ferrosPrice;
            $tomatos = 0;
        }
        elseif ($currency == self::TOMATOS) {
            $ferros = 0;
            $tomatos = $tomatosPrice;
        }
        else {
            throw new InvalidDataException('Wrong currency');
        }

        return array($ferros, $tomatos);
    }

    /**
     * @static
     * @param $currency
     * @return string
     * @throws \krest\exceptions\InvalidDataException
     */
    public static function getFullCurrencyName($currency)
    {
        switch ($currency) {
            case self::FERROS:
                $fullCurrency = 'ferros';
                break;
            case self::TOMATOS:
                $fullCurrency = 'tomatos';
                break;
            default:
                throw new InvalidDataException("Wrong currency $currency");
        }

        return $fullCurrency;
    }
}
