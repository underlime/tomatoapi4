<?php
namespace action\news;

use base\Action;
use model\UserNews;
use model\UserNewsRead;
use model\UsersJournal;

class get_news extends Action
{
    public function post()
    {
        $news = new UserNews();
        $newsRead = new UserNewsRead();

        $userId = $this->userInfo['user_id'];
        $this->_addAnswerArr(array(
                                  'user_news' => array(
                                      'news_read_ids' => $newsRead->getNewsReadIds($userId),
                                      '_init' => $news->getNews($userId),
                                  ),
                             ));
    }
}
