<?php
namespace action\news;

use base\Action;
use krest\Arr;
use krest\Request;
use model\User;
use model\UserNews;
use model\UserNewsRead;

class set_news_read extends Action
{
    public function post()
    {
        $requestData = Request::instance()->getRequestBodyParams();

        $userId = $this->userInfo['user_id'];
        $newsId = Arr::req($requestData, 'news_id');
        $isShared = (bool)Arr::get($requestData, 'is_shared');

        $user = new User();
        $newsRead = new UserNewsRead();

        $newsRead->setNewsRead($userId, $newsId, $isShared);
        $this->_addAnswerArr(array(
                                  'user_news' => array(
                                      'news_read_ids' => $newsRead->getNewsReadIds($userId),
                                  ),
                                  'user_opt_info' => array(
                                      'unread_news_count' => $newsRead->getUnreadNewsCount($userId)
                                  ),
                                  'user_fight_data' => $user->getUserFightData($userId, true),
                             ));
    }
}
