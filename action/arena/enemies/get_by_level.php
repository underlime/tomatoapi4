<?php
namespace action\arena\enemies;

use krest\Arr;
use krest\Request;

/**
 * Извлечь противников по уровню
 * Параметр level
 * Возвращает enemies_list
 */
class get_by_level extends Action
{
    private $level;

    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->level = Arr::req($requestParams, 'level');
        $this->cacheKey = "tomato_api_4_enemies_by_level_{$this->level}";

        $this->_execute();
    }

    protected function _getEnemiesData()
    {
        return $this->user->getUsersByLevel(
            $this->level,
            $this->userInfo['user_id']
        );
    }
}
