<?php
namespace action\arena\enemies;

use krest\Arr;
use krest\Cache;
use krest\Request;

/**
 * Извлечь противников по уровню
 * Параметр identity
 * Возвращает enemies_list
 */
class get_by_identity extends Action
{
    private $identity;

    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->identity = Arr::req($requestParams, 'identity');
        $this->cacheKey = "tomato_api_4_enemies_by_identity_{$this->identity}";

        $this->_execute();
    }

    protected function _getEnemiesData()
    {
        return $this->user->getUsersByIdentity(
            $this->identity,
            $this->userInfo['user_id']
        );
    }
}
