<?php
namespace action\arena\enemies;

use fight\robot\RobotFactory;
use fight\robot\UserInfo;
use krest\Arr;
use krest\Cache;
use krest\api\Config;
use model\Items;

abstract class Action extends \base\Action
{
    const LIST_MAX_SIZE = 6;

    protected $config;
    protected $cacheKey;

    /**
     * @var \model\Items
     */
    protected $items;

    protected function _execute()
    {
        $this->config = Config::instance()->getForClass(__CLASS__);
        $this->items = new Items();

        $enemiesList = $this->_getFromCache();
        if (!$enemiesList) {
            $enemiesList = $this->_getFromModel();
        }
        $this->_handleDoppelganger($enemiesList);

        $ans = $this->_makeAnswer($enemiesList);
        $this->_addAnswerArr($ans);
    }

    private function _getFromCache()
    {
        return Cache::instance()->get($this->cacheKey);
    }

    private function _getFromModel()
    {
        $enemiesIdsList = $this->_getEnemiesData();
        $enemiesList = array();

        foreach ($enemiesIdsList as $userId) {
            $userBonus = $this->user->getUserBonuses($userId);
            $scale = $userBonus->scale->asArray();

            $enemiesList[] = array(
                'user_info' => Arr::filterFields(
                    $this->user->getUserInfoByUserId($userId),
                    array('soc_net_id', 'user_id', 'login', 'vegetable', 'body', 'hands', 'eyes', 'mouth', 'hair')
                ),
                'user_level_params' => Arr::filterFields(
                    $this->user->getUserLevelParams($userId),
                    array('level', 'strength', 'agility', 'intellect', 'max_hp', 'max_level')
                ),
                'user_fight_data' => Arr::filterFields(
                    $this->user->getUserFightData($userId, false),
                    array('hp', 'glory')
                ),
                'user_inventory' => array(
                    '_init' => $this->items->getUsersInventory($userId),
                ),
                'bonus_hp' => $scale['hp']
            );
        }

        Cache::instance()->set($this->cacheKey, $enemiesList, 600);
        return $enemiesList;
    }

    abstract protected function _getEnemiesData();

    private function _handleDoppelganger(array &$enemiesList)
    {
        if (sizeof($enemiesList) < self::LIST_MAX_SIZE) {
            $doppelgangerProbability = Arr::get($this->config, 'doppelganger_probability');
            if (mt_rand(1, 100) <= 100*$doppelgangerProbability) {
                $this->_addDoppelganger($enemiesList);
            }
        }
    }

    private function _addDoppelganger(array &$enemiesList)
    {
        $userInfo = new UserInfo();
        $userInfo->userInfo = $this->userInfo;
        $userInfo->userLevelParams = $this->user->getUserLevelParams($this->userInfo['user_id']);
        $userInfo->userFightData = $this->user->getUserFightData($this->userInfo['user_id']);
        $userInfo->userInventory = $this->items->getUsersInventory($this->userInfo['user_id']);

        $robotInfo = RobotFactory::getRobot(RobotFactory::DOPPELGANGER, $userInfo)
                     ->getRobotInfo();

        $enemiesList[] = array(
            'user_info' => $robotInfo->userInfo,
            'user_level_params' => $robotInfo->userLevelParams,
            'user_fight_data' => $robotInfo->userFightData,
            'user_inventory' => [
                '_init' => $robotInfo->userInventory
            ]
        );
    }

    private function _makeAnswer($enemiesList)
    {
        $length = sizeof($enemiesList);
        $maxIndex = $length - 1;
        $count = min(6, $length);

        $indexesList = array();
        for ($i = 0; $i < $count; ++$i) {
            $index = mt_rand(0, $maxIndex);
            while (in_array($index, $indexesList)) {
                $index = mt_rand(0, $maxIndex);
            }

            $indexesList[] = $index;
        }

        $ans = array(
            'enemies_list' => array(
                '_init' => array(),
            ),
        );
        for ($i = 0; $i < $count; ++$i) {
            $ans['enemies_list']['_init'][] = $enemiesList[$indexesList[$i]];
        }

        return $ans;
    }
}
