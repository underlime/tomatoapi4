<?php
namespace action\arena;

use base\Action;
use fight\Battle;
use krest\Arr;
use krest\base\Struct;
use krest\Request;
use model\Items;
use model\User;

/**
 * Провести бой,
 * параметры:
 * - enemy_id
 * - [ease_enemy]
 * возаращает:
 * - enemy_eased
 * - user_level_params
 * - user_fight_data
 * - user_opt_info
 * - user_inventory
 * - battle_data
 * @package action\arena
 */
class fight extends Action
{
    /**
     * @var \model\Items
     */
    private $items;

    protected function __construct($id)
    {
        parent::__construct($id);
        $this->items = new Items();
    }

    public function post()
    {
        $requestData = Request::instance()->getRequestBodyParams();
        $enemyId = Arr::req($requestData, 'enemy_id');
        $easeEnemy = (bool)Arr::get($requestData, 'ease_enemy', false);

        $battle = new Battle();
        $battleTape = $battle->run($this->userInfo['soc_net_id'], $enemyId, $easeEnemy);
        $battleSteps = Struct::createArray($battleTape->battle);

        $userId = $this->userInfo['user_id'];

        $userLevelParams = $this->user->getUserLevelParams($userId);
        $userFightData = $this->user->getUserFightData($userId);
        $inventory = $this->items->getUsersInventory($userId);

        $this->_addAnswerArr(array(
                                  'enemy_eased' => $easeEnemy,
                                  'user_level_params' => $userLevelParams,
                                  'user_fight_data' => $userFightData,
                                  'user_inventory' => array(
                                      '_init' => $inventory,
                                  ),
                                  'battle_data' => array(
                                      'enemy_info' => $battleTape->enemyInfo,
                                      'enemy_level_params' => $battleTape->enemyLevelParams,
                                      'enemy_fight_data' => $battleTape->enemyFightData,
                                      'enemy_inventory' => array(
                                          '_init' => $battleTape->enemyInventory,
                                      ),
                                      'profit' => $battleTape->profit,
                                      'arena_data' => $battleTape->arenaData,
                                      'battle_steps' => array(
                                          '_init' => $battleSteps
                                      ),
                                      'wins' => $battleTape->wins
                                  ),
                             ));
    }
}
