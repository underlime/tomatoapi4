<?php
namespace action\system;

use base\Action;
use krest\Arr;

class log extends Action
{
    public function post()
    {
        $errorInfo = Arr::req($this->request->getRequestBodyParams(), "error_info");
        $time = date('Y-m-d H:i:s');
        $logMessage = "[$time] $errorInfo";
        \Logger::getLogger('error.client')->error($logMessage);
        $this->_addAnswerArr(['log_status' => true]);
    }
}
