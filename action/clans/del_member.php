<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use krest\exceptions\HttpException;
use model\Clans;

class del_member extends Action
{
    public function post()
    {
        $curUserId = $this->userInfo['user_id'];
        $userId = Arr::get($this->request->getRequestBodyParams(), 'user_id');
        if (!$userId) {
            $userId = $curUserId;
        }

        $clans = new Clans();
        $clanId = $clans->getUsersClan($userId);
        if ($clanId === null) {
            throw new HttpException(400, "User is not a clan member");
        }

        if ($userId != $curUserId) {
            $clanInfo = $clans->getInfo($clanId);
            if ($curUserId != $clanInfo['leader_id']) {
                throw new HttpException(403, "Access denied");
            }
        }
        $clans->delClanMember($clanId, $userId);

        $this->_addAnswerArr(["delete_clan_member_result" => true]);
    }
}
