<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use model\Clans;

class get_info extends Action
{
    public function post()
    {
        $clans = new Clans();
        $clanId = Arr::get($this->request->getRequestBodyParams(), 'clan_id');
        if (!$clanId) {
            $clanId = $clans->getClanIdByLeaderId($this->userInfo['user_id']);
        }
        $info = Arr::filterFields($clans->getInfo($clanId), [
                                                            'id', 'clan_name', 'leader_id', 'glory',
                                                            'previous_place_in_top', 'victories', 'picture'
                                                            ]);
        $info["members_count"] = $clans->getClanMembersCount($clanId);
        $this->_addAnswerArr(['clan_info' => $info]);
    }
}
