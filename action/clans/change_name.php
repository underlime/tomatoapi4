<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use model\Clans;

class change_name extends Action
{
    public function post()
    {
        $name = Arr::req($this->request->getRequestBodyParams(), 'name');
        $clans = new Clans();

        $leaderId = $this->userInfo['user_id'];
        $clanId = $clans->getClanIdByLeaderId($leaderId);
        $newMoney = $clans->rename($leaderId, $clanId, $name);

        $this->_addAnswerArr([
                             'user_fight_data' => $newMoney,
                             'clan_info' => ['name' => $name],
                             ]);
    }
}
