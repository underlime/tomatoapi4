<?php
namespace action\clans;

use krest\Arr;
use model\Clans;
use model\User;

trait MembersGetter
{
    protected function _getClanMembers($clanId)
    {
        $clans = new Clans();
        $userModel = new User();
        $clanInfo = $clans->getInfo($clanId);
        $memberIds = $clans->getClanMembers($clanId);

        $data = array_map(function ($userId) use ($clanInfo, $userModel) {
            $fightData = $userModel->getUserFightData($userId);
            $info = $userModel->getUserInfoByUserId($userId);
            $isLeader = ($userId == $clanInfo['leader_id']);
            return [
                'user_id' => $userId,
                'soc_net_id' => $info['soc_net_id'],
                'season_glory' => $fightData['season_glory'],
                'items_given' => $fightData['items_given'],
                'login' => $info['login'],
                'is_leader' => $isLeader,
            ];
        }, $memberIds);

        return $data;
    }
}