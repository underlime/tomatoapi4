<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use model\Clans;

class create extends Action
{
    public function post()
    {
        $name = Arr::req($this->request->getRequestBodyParams(), 'name');
        $clans = new Clans();
        $id = $clans->create($this->userInfo['user_id'], $name);
        $this->_addAnswerArr(['clan_info' => $clans->getInfo($id)]);
    }
}
