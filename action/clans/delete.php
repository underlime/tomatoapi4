<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use model\Clans;

class delete extends Action
{
    public function post()
    {
        $clanId = Arr::req($this->request->getRequestBodyParams(), 'clan_id');
        $clans = new Clans();
        $clans->delete($this->userInfo['user_id'], $clanId);

        $this->_addAnswerArr(['clan_deletion_result' => true]);
    }
}
