<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use model\Clans;

class validate_name extends Action
{
    public function post()
    {
        $name = Arr::req($this->request->getRequestBodyParams(), 'name');
        $clans = new Clans();
        $this->_addAnswerArr(['clan_name_validation_results' => $clans->validateName($name)]);
    }
}
