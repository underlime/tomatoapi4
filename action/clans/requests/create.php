<?php
namespace action\clans\requests;

use base\Action;
use krest\Arr;
use model\ClanRequests;

/**
 * Class create
 * При выполнении действия следует ожидать ошибки с превышением количества участников клана!
 * @package action\clans\requests
 */
class create extends Action
{
    public function post()
    {
        $clanId = Arr::req($this->request->getRequestBodyParams(), 'clan_id');

        $model = new ClanRequests();
        $model->create($clanId, $this->userInfo['user_id']);

        $this->_addAnswerArr(['clan_request_creating_status' => true]);
    }
}
