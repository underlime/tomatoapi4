<?php
namespace action\clans\requests;

use action\clans\MembersGetter;
use krest\Arr;
use model\ClanRequests;
use model\Clans;

class accept extends Getter
{
    use MembersGetter;

    public function post()
    {
        $requests = new ClanRequests();
        $clans = new Clans();

        $clanId = $clans->getClanIdByLeaderId($this->userInfo['user_id']);
        $userId = Arr::req($this->request->getRequestBodyParams(), 'user_id');

        $requests->accept($clanId, $userId);
        $usersInfo = $this->_getRequests();

        $info = $clans->getInfo($clanId);
        $info["members_count"] = $clans->getClanMembersCount($clanId);
        $clanMembers = $this->_getClanMembers($clanId);
        $this->_addAnswerArr([
                             'clan_requests' => ['_init' => $usersInfo],
                             'clan_info' => $info,
                             'clan_members' => ['_init' => $clanMembers]
                             ]);
    }
}
