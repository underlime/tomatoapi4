<?php
namespace action\clans\requests;

use krest\Arr;
use model\Clans;

class get extends Getter
{
    public function post()
    {
        $usersInfo = $this->_getRequests();
        $this->_addAnswerArr(['clan_requests' => ['_init' => $usersInfo]]);
    }
}
