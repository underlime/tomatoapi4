<?php
namespace action\clans\requests;

use krest\Arr;
use model\ClanRequests;
use model\Clans;

class reject extends Getter
{
    public function post()
    {
        $requests = new ClanRequests();
        $clans = new Clans();

        $clanId = $clans->getClanIdByLeaderId($this->userInfo['user_id']);
        $userId = Arr::req($this->request->getRequestBodyParams(), 'user_id');

        $requests->reject($clanId, $userId);
        $usersInfo = $this->_getRequests();
        $this->_addAnswerArr(['clan_requests' => ['_init' => $usersInfo]]);
    }
}
