<?php
namespace action\clans\requests;

use base\Action;
use krest\Arr;
use model\ClanRequests;
use model\Clans;

abstract class Getter extends Action
{
    /**
     * @return array
     */
    protected function _getRequests()
    {
        $clans = new Clans();
        $requests = new ClanRequests();
        $user = $this->user;

        $clanId = $clans->getClanIdByLeaderId($this->userInfo['user_id']);
        $requestsData = $requests->get($clanId);
        $usersInfo = array_map(
            function ($rec) use ($user) {
                $userId = $rec["user_id"];
                $userInfo = Arr::filterFields($user->getUserInfoByUserId($userId), ['user_id', 'soc_net_id', 'login']);
                $fightData = Arr::filterFields($user->getUserFightData($userId), ['glory']);
                $levelParams = Arr::filterFields($user->getUserLevelParams($userId), ['level', 'max_level']);
                return array_merge($userInfo, $fightData, $levelParams, ["request_time" => $rec["time"]]);
            },
            $requestsData
        );
        return $usersInfo;
    }
}
