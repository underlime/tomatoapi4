<?php
namespace action\clans;

use base\Action;
use krest\Arr;
use model\Clans;
use model\User;

class get_members extends Action
{
    use MembersGetter;

    public function post()
    {
        $clanId = Arr::req($this->request->getRequestBodyParams(), 'clan_id');
        $data = $this->_getClanMembers($clanId);
        $this->_addAnswerArr(['clan_members' => ['_init' => $data]]);
    }
}
