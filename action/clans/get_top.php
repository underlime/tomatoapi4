<?php
namespace action\clans;

use base\Action;
use model\Clans;
use model\ClansClash;


class get_top extends Action
{
    public function post()
    {
        $clans = new Clans();
        $clansClash = new ClansClash();
        $seasonInfo = $clansClash->getSeasonInfo();

        $topIds = $clans->getTop();
        $clansTop = [];
        foreach ($topIds as $clanId) {
            $clanInfo = $clans->getInfo($clanId);
            $clanInfo["members_count"] = $clans->getClanMembersCount($clanId);
            $clansTop[] = $clanInfo;
        }

        $this->_addAnswerArr([
                             'clans_clash_season' => $seasonInfo,
                             'clans_top_glory' => array("_init" => $clansTop),
                             ]);
    }
}