<?php
namespace action\academy;

use base\Action;
use krest\Arr;
use krest\Request;
use model\CharDetails;

/**
 * Извлечь детали персонажа
 * Можно указать параметр [vegetable]
 * Возвращает char_details
 */
class get_char_details extends Action
{
    protected $needUserInfo = false;

    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $vegetable = Arr::get($requestParams, 'vegetable', 'all');

        $charDetails = new CharDetails();
        $data = array(
            'char_details' => array(
                '_init' => $charDetails->getDetails($vegetable)
            ),
        );
        $this->_addAnswerArr($data);
    }
}
