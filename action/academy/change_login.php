<?php
namespace action\academy;

use base\Action;
use krest\Arr;
use krest\Request;

/**
 * Изменить логин
 * Параметры:
 * - login
 * Возвращает:
 * - user_info (только логин)
 * - user_fight_data (только деньги)
 */
class change_login extends Action
{

    private $login;
    private $newMoney;

    public function post()
    {
        $this->_getParams();

        $this->newMoney = $this->user->changeLogin($this->socNetId, $this->login);
        $data = array(
            'user_info' => Arr::filterFields(
                $this->user->getUserInfo($this->socNetId),
                array('login')
            ),
            'user_fight_data' => $this->newMoney,
        );
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->login = Arr::req($requestParams, 'login');
    }
}
