<?php
namespace action\academy;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Specials;

/**
 * Прокачать навыки
 * Параметры:
 * - [strength]
 * - [agility]
 * - [intellect]
 * Нужно указать хотя бы один параметр
 * Возвращает:
 * - user_level_params (частично, см. метод)
 * - user_fight_data (частично, см. метод)
 * - [user_specials] (возвращается в случае прокачки интеллекта)
 * - [user_achievements] (возвращается в случае достижений)
 * Возможны достижения:
 * - strength_level
 * - agility_level
 * - intellect_level
 */
//TODO проработать секцию events и списки в достижениях
class train_skills extends Action
{
    //Параметры
    private $strength, $agility, $intellect;

    private $oldSpecialsIds;

    /**
     * @var Specials
     */
    private $specials;

    public function post()
    {
        $this->_getParams();

        $this->specials = new Specials();
        if ($this->intellect) {
            $this->oldSpecialsIds = array_keys(
                $this->specials->getUsersSpecials($this->userInfo['user_id'])
            );
        }

        $this->user->trainSkills(
            $this->userInfo['user_id'],
            $this->strength,
            $this->agility,
            $this->intellect
        );

        $data = array(
            'user_level_params' => Arr::filterFields(
                $this->user->getUserLevelParams($this->userInfo['user_id']),
                //список полей user_level_params
                array('strength', 'agility', 'intellect', 'max_hp', 'level', 'max_level', 'points_learning')
            ),
            'user_fight_data' => Arr::filterFields(
                $this->user->getUserFightData($this->userInfo['user_id'], true),
                //список полей user_fight_data
                array('hp', 'energy', 'max_energy', 'energy_time')
            ),
        );

        if ($this->intellect) {
            $this->_setSpecialsData($data);
        }
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->strength = Arr::get($requestParams, 'strength', 0);
        $this->agility = Arr::get($requestParams, 'agility', 0);
        $this->intellect = Arr::get($requestParams, 'intellect', 0);
    }

    private function _setSpecialsData(&$data)
    {
        $specials = new Specials();
        $specials->setUserSpecialsLevels($this->userInfo['user_id']);

        $specialsList = $specials->getUsersSpecials($this->userInfo['user_id']);
        $specialsIdsList = array_keys($specialsList);
        $newIds = array_diff($specialsIdsList, $this->oldSpecialsIds);

        if ($newIds) {
            $data['user_specials'] = array(
                '_add' => array()
            );

            foreach ($newIds as $specialId) {
                $data['user_specials']['_add'][$specialId] = Arr::req($specialsList, $specialId);
            }
        }
    }
}
