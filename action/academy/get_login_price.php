<?php
namespace action\academy;

use base\Action;

/**
 * Извлечь цену смены овоща
 * Возвращает login_ferros_price
 */
class get_login_price extends Action
{
    public function post()
    {
        $data = array(
            'config_prices' => array(
                'login_ferros_price' => $this->user->getLoginPrice(),
            ),
        );
        $this->_addAnswerArr($data);
    }
}
