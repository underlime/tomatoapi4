<?php
namespace action\academy;

use base\Action;

/**
 * Сбросить навыки пользователя
 * @package action\academy
 * возвращает:
 * - user_level_params
 * - user_fight_data
 * - user_specials
 */
class reset_skills extends Action
{
    public function post()
    {
        list($userLevelParams, $userFightData, $usersInventory)
            = $this->user->resetUserSkills($this->userInfo['user_id']);
        $this->_addAnswerArr(array(
                                  'user_level_params' => $userLevelParams,
                                  'user_fight_data' => $userFightData,
                                  'user_specials' => array(
                                      '_init' => array()
                                  ),
                                  'user_inventory' => array(
                                      '_init' => $usersInventory,
                                  ),
                             ));
    }
}
