<?php
namespace action\academy;

use base\Action;
use krest\Arr;
use krest\Request;

/**
 * Изменить тип овоща
 * Параметры:
 * - vegetable
 * - [currency]
 * Возвращает:
 * - user_info (только овощ и части овоща)
 * - user_fight_data (только деньги)
 */
class change_vegetable extends Action
{
    //Параметры
    private $vegetable;

    private $newMoney;

    public function post()
    {
        $this->_getParams();

        $this->newMoney = $this->user->changeVegetable($this->socNetId, $this->vegetable, \Currency::FERROS);
        $data = array(
            'user_info' => Arr::filterFields(
                $this->user->getUserInfo($this->socNetId),
                array('vegetable', 'body', 'hands', 'eyes', 'mouth', 'hair')
            ),
            'user_fight_data' => $this->newMoney,
        );
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->vegetable = Arr::req($requestParams, 'vegetable');
    }
}
