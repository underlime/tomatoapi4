<?php
namespace action\academy;

use base\Action;
use krest\Arr;
use krest\Request;
use model\chardetails\DetailsList;

/**
 * Изменить части овоща
 * Параметры:
 * - [body]
 * - [hair]
 * - [mouth]
 * - [eyes]
 * - [currency]
 * Обязательно нужно указать хотя бы одну часть
 * Возвращает:
 * - user_fight_data (только деньги)
 * - user_info (только детали персонажа)
 */
class change_details extends Action
{
    private $currency;

    /**
     * @var \model\chardetails\DetailsList
     */
    private $detailsList;

    public function post()
    {
        $this->_getParams();
        $newMoney = $this->user->changeCharDetails($this->socNetId, $this->currency, $this->detailsList);
        $data = array(
            'user_fight_data' => $newMoney,
            'user_info' => Arr::filterFields(
                $this->user->getUserInfo($this->socNetId),
                array('body', 'hands', 'hair', 'mouth', 'eyes')
            ),
        );
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->currency = Arr::get($requestParams, 'currency', \Currency::TOMATOS);

        $this->detailsList = new DetailsList();
        $fields = array('body', 'hair', 'mouth', 'eyes');

        foreach ($fields as $fieldName) {
            $this->detailsList->$fieldName = Arr::get($requestParams, $fieldName);
        }
    }
}
