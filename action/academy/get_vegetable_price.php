<?php
namespace action\academy;

use base\Action;

/**
 * Извлечь цену смены овоща
 * Возвращает vegetable_price
 */
class get_vegetable_price extends Action
{
    public function post()
    {
        $data = array(
            'config_prices' => array(
                'vegetable_price' => $this->user->getVegetablePrice(),
            )
        );
        $this->_addAnswerArr($data);
    }
}
