<?php
namespace action\top\glory;

/**
 * Извлечь глобальный топ по славе
 * Параметры:
 * - city_code - код города,
 * - [interval] может принимать значения all, day, week (по умолчанию all)
 * Возвращает массив пользователей:
 * top =>
 * array(
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 *      ...
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 * )
 */

use action\top\TopAction;
use krest\Arr;
use krest\Request;

class get_by_city extends TopAction
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $cityCode = Arr::req($requestParams, 'city_code');
        $this->topType = 'glory_by_city';
        $this->topId = "glory_by_city_$cityCode";

        $this->usersIdsList = $this->user->getCityGloryTopIds($cityCode, $this->interval);
        $this->_makeTop();
    }
}
