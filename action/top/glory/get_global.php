<?php
namespace action\top\glory;

use action\top\TopAction;

/**
 * Извлечь глобальный топ по славе
 * Параметр [interval] может принимать значения all, day, week (по умолчанию all)
 * Возвращает массив пользователей:
 * top =>
 * array(
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 *      ...
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 * )
 */
class get_global extends TopAction
{
    protected $topType = 'glory_common';
    protected $topId = 'glory_common';

    public function post()
    {
        $this->usersIdsList = $this->user->getGlobalGloryTopIds($this->interval);
        $this->_makeTop();
    }
}
