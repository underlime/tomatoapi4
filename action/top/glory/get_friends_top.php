<?php
namespace action\top\glory;

use action\top\TopAction;
use krest\Arr;

/**
 * Извлечь топ друзей по славе
 * Параметр [interval] может принимать значения all, day, week (по умолчанию all)
 * Возвращает массив пользователей:
 * top =>
 * array(
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 *      ...
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 * )
 */
class get_friends_top extends TopAction
{
    public function post()
    {
        $this->topType = 'glory_friends';
        $this->topId = 'glory_friends_top_'.$this->socNetId;

        $friendsList = $this->user->getFriendsInGameIdsList($this->socNetId);
        $this->usersIdsList = $this->user->getChosenGloryTopIds($friendsList, $this->interval);

        $this->_makeTop();
    }
}
