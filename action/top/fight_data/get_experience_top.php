<?php
namespace action\top\fight_data;

use action\top\TopAction;
use model\User;

/**
 * Извлечь глобальный топ по опыту
 * Возвращает массив пользователей:
 * top =>
 * array(
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 *      ...
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 * )
 */
class get_experience_top extends TopAction
{
    protected $topType = 'experience_common';
    protected $topId = 'experience_common';

    public function post()
    {
        $this->usersIdsList = $this->user->getGlobalFightDataTopIds(User::FIGHT_DATA_EXPERIENCE);
        $this->_makeTop();
    }
}
