<?php
namespace action\top;

use base\Action;
use krest\Arr;
use krest\Cache;
use krest\exceptions\HttpException;
use krest\Request;
use model\Clans;
use model\User;

abstract class TopAction extends Action
{
    protected $topType = 'other';
    protected $topId = 'etc';
    protected $cachePrefix = 'tomato_api4_';
    protected $usersIdsList = array();
    protected $interval = '';

    private $cacheKey;

    protected function __construct($id)
    {
        parent::__construct($id);
        $this->_getInterval();
    }


    protected function _makeTop()
    {
        $this->cacheKey = "{$this->cachePrefix}_users_top_{$this->topId}_{$this->interval}";
        $inCache = $this->_getFromCache();
        if (!$inCache) {
            $this->_getFromDb();
        }
    }

    private function _getInterval()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $variant = Arr::get($requestParams, 'interval', 'all');

        switch ($variant) {
            case 'all':
                $this->interval = User::INTERVAL_GLOBAL;
                break;
            case 'day':
                $this->interval = User::INTERVAL_DAY;
                break;
            case 'week':
                $this->interval = User::INTERVAL_WEEK;
                break;
            default:
                throw new HttpException(400, 'Wrong interval');
        }
    }

    private function _getFromCache()
    {
        $topData = Cache::instance()->get($this->cacheKey);

        if ($topData !== null) {
            $this->_addAnswerArr($topData);
            return true;
        }

        return false;
    }

    private function _getFromDb()
    {
        $clans = new Clans();
        $key = 'top_'.$this->topType.'_'.$this->interval;

        $data = array(
            $key => array(
                '_init' => array(),
            ),
        );

        $counter = 1;

        foreach ($this->usersIdsList as $userId) {
            $userData = array_merge(
                Arr::filterFields(
                    $this->user->getUserInfoByUserId($userId),
                    array('soc_net_id', 'user_id', 'login')
                ),
                Arr::filterFields(
                    $this->user->getUserLevelParams($userId),
                    array('level', 'strength', 'agility', 'intellect', 'max_level')
                ),
                Arr::filterFields(
                    $this->user->getUserFightData($userId, false),
                    array('glory', 'day_glory', 'week_glory')
                )
            );

            $clanId = $clans->getUsersClan($userId);
            if ($clanId) {
                $clanInfo = $clans->getInfo($clanId);
                $userData["clan_name"] = $clanInfo["name"];
            }
            $data[$key]['_init'][$counter++] = $userData;
        }

        Cache::instance()->set($this->cacheKey, $data, 600);
        $this->_addAnswerArr($data);
    }
}
