<?php
namespace action\top\level_params;

use action\top\TopAction;
use model\User;

/**
 * Извлечь глобальный топ по интеллекту
 * Возвращает массив пользователей:
 * top =>
 * array(
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 *      ...
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 * )
 */
class get_intellect_top extends TopAction
{
    protected $topType = 'intellect_common';
    protected $topId = 'intellect_common';

    public function post()
    {
        $this->usersIdsList = $this->user->getGlobalLevelParamsTopIds(User::LEVEL_PARAM_INTELLECT);
        $this->_makeTop();
    }
}
