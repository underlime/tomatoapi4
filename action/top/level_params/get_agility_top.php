<?php
namespace action\top\level_params;

use action\top\TopAction;
use model\User;

/**
 * Извлечь глобальный топ по ловкости
 * Возвращает массив пользователей:
 * top =>
 * array(
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 *      ...
 *      array(
 *          - user_info
 *          - user_level_params
 *          - user_fight_data
 *      ),
 * )
 */
class get_agility_top extends TopAction
{
    protected $topType = 'agility_common';
    protected $topId = 'agility_common';

    public function post()
    {
        $this->usersIdsList = $this->user->getGlobalLevelParamsTopIds(User::LEVEL_PARAM_AGILITY);
        $this->_makeTop();
    }
}
