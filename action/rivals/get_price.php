<?php
namespace action\rivals;

use base\Action;
use model\Rivals;

/**
 * Извлечь цену добавления в соперники
 * Возвращает rivals_ferros_price
 */
class get_price extends Action
{
    public function post()
    {
        $rivals = new Rivals();
        $data = array(
            'rivals_ferros_price' => $rivals->getRivalPrice(),
        );
        $this->_addAnswerArr($data);
    }
}
