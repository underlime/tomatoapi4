<?php
namespace action\rivals;

/**
 * Извлечь список соперников
 * Возвращает rivals_list
 */
class get_list extends RivalsAction
{
    public function post()
    {
        $this->_addRivalsListAnswer();
    }
}
