<?php
namespace action\rivals;

use base\Action;
use model\Rivals;

abstract class RivalsAction extends Action
{
    protected function _addRivalsListAnswer()
    {
        $rivals = new Rivals();

        $rivalsData = $rivals->getRivalsList();
        $rivalsList = array();
        foreach ($rivalsData as $record) {
            $rivalsList[] = array(
                'rival_data' => $record,
                'user_level_params' => $this->user->getUserLevelParams($record['user_id']),
                'user_fight_data' => $this->user->getUserFightData($record['user_id'], true),
            );
        }

        $data = array(
            'rivals_list' => array(
                '_init' => $rivalsList
            ),
        );
        $this->_addAnswerArr($data);
    }
}
