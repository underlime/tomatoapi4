<?php
namespace action\rivals;

use krest\Arr;
use krest\Request;
use model\Rivals;

/**
 * Добавить сообщение в соперники
 * Параметр message
 * Возвращает:
 * - rivals_list
 * - user_fight_data (только деньги)
 */
class add_message extends RivalsAction
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $message = Arr::req($requestParams, 'message');

        $rivals = new Rivals();
        $rivals->addRival($this->userInfo['user_id'], $message);

        $this->_addRivalsListAnswer();
        $data = array(
            'user_fight_data' => Arr::filterFields(
                $this->user->getUserFightData($this->userInfo['user_id'], false),
                array('ferros', 'tomatos')
            ),
        );
        $this->_addAnswerArr($data);
    }
}
