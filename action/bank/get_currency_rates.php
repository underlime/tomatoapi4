<?php
namespace action\bank;

use base\Action;
use model\Bank;

class get_currency_rates extends Action
{
    public function post()
    {
        $bank = new Bank();
        $data = array(
            'currency_rates' => array(
                '_init' => $bank->getCurrencyRates(),
            ),
        );
        $this->_addAnswerArr($data);
    }
}
