<?php
namespace action\bank;

use action\bank\buy_money\Buyer;
use base\ServerCallBack;
use krest\exceptions\HttpException;
use krest\Response;
use krest\SocNet;
use model\Bank;

class buy_money extends ServerCallBack
{
    public function run()
    {
        list($answerData, $statData) = Buyer::factory()->run();
        if (is_array($answerData)) {
            $this->_addAnswerArr($answerData);
        }
        else {
            Response::instance()->setHeader("Content-type", "application/xml; charset=UTF-8");
            $this->_setStringAnswer($answerData);
        }

        if ($statData) {
            $bank = new Bank();
            $bank->addStatisticsRecord($statData['soc_net_id'], $statData['ferros'], $statData['tomatos']);
        }
    }

    public function get()
    {
        if (SocNet::instance()->getSocNetName() === SocNet::OK_RU) {
            $this->run();
        }
        else {
            throw new HttpException(405, 'Method not allowed');
        }
    }
}
