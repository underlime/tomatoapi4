<?php
namespace action\bank\buy_money;

use krest\Arr;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\Request;
use krest\Response;
use krest\SocNet;
use model\Bank;
use model\User;
use php_rutils\RUtils;

class VkComBuyer extends Buyer
{
    const ERROR_GENERAL = 1;
    const ERROR_TEMPORARY = 2;
    const ERROR_SIGNATURE = 10;
    const ERROR_WRONG_REQUEST = 11;
    const ERROR_WRONG_ITEM = 20;
    const ERROR_ITEM_IS_NO_MORE = 21;
    const ERROR_USER_NOT_FOUND = 22;
    const ERROR_CUSTOM = 100;

    private static $currencyPictures = array(
        \Currency::FERROS => 'http://tk-api.underlime.net/assets/ferros.png',
        \Currency::TOMATOS => 'http://tk-api.underlime.net/assets/tomatos.png',
    );

    /**
     * @var \krest\socnet\VkCom
     */
    private $socNet;

    /**
     * @var \model\User
     */
    private $user;

    /**
     * @var \model\Bank
     */
    private $bank;

    private $receiverInfo;

    private $input;
    private $sig;
    private $userSocNetId;
    private $receiverSocNetId;
    private $actionResult;
    private $statData;

    public function run()
    {
        try {
            $this->_setInput();
            $this->_execute();
        }
        catch (InvalidDataException $e) {
            $this->_sendError($e->data);
        }
        catch (\Exception $e) {
            $this->_sendError();
        }

        return array($this->actionResult, $this->statData);
    }

    private function _throwVkException($errorCode, $message, $isCritical = 1)
    {
        $e = new InvalidDataException($message);
        $e->data = array(
            'error_code' => $errorCode,
            'error_msg' => $message,
            'critical' => $isCritical,
        );
        throw $e;
    }

    private function _setInput()
    {
        $this->input = Request::instance()->getRequestBodyParams();
        if (!isset($this->input['sig'])) {
            $this->_throwVkException(self::ERROR_WRONG_REQUEST, 'Sig is missing');
        }
        $this->sig = $this->input['sig'];
        unset($this->input['sig']);
        ksort($this->input);
    }

    private function _execute()
    {
        $this->socNet = SocNet::instance();
        $this->user = new User();
        $this->bank = new Bank();

        $this->_checkSignature();
        $this->_checkUsers();
        $this->_makeAction();
    }

    private function _checkSignature()
    {
        $baseString = '';
        foreach ($this->input as $k => $v) {
            $baseString .= $k.'='.$v;
        }

        $apiSecret = $this->socNet->getApiSecret();
        $trueSig = md5($baseString.$apiSecret);

        if ($this->sig !== $trueSig) {
            $this->_throwVkException(self::ERROR_SIGNATURE, 'Wrong sig');
        }
    }

    private function _checkUsers()
    {
        $this->userSocNetId = Arr::req($this->input, 'user_id');
        $this->receiverSocNetId = Arr::req($this->input, 'receiver_id');

        try {
            $this->user->getUserInfo($this->userSocNetId);
            $this->receiverInfo = $this->user->getUserInfo($this->receiverSocNetId);
        }
        catch (NotFoundException $e) {
            $this->_throwVkException(self::ERROR_USER_NOT_FOUND, 'Пользователя не существует');
        }
    }

    private function _makeAction()
    {
        $actionType = Arr::req($this->input, 'notification_type');
        switch ($actionType) {
            case 'get_item':
            case 'get_item_test':
                $this->_getItemInfoForRequest();
                break;
            case 'order_status_change':
            case 'order_status_change_test':
                $this->_handleOrderStatusChange();
                break;
            default:
                $this->_throwVkException(self::ERROR_WRONG_REQUEST, 'Неверный тип сообщения');
        }
    }

    private function _getItemInfoForRequest()
    {
        $itemInfo = $this->_getItemInfo();
        $this->actionResult = array('response' => $itemInfo);
    }

    private function _getItemInfo()
    {
        $bankRecord = $this->_getBankRecord();

        $itemInfo = array(
            'item_id' => $bankRecord['id'],
            'title' => $this->_getItemTitle($bankRecord['currency'], $bankRecord['count']),
            'photo_url' => self::$currencyPictures[$bankRecord['currency']],
            'price' => $bankRecord['price'],
        );

        return $itemInfo;
    }

    private function _getItemTitle($currency, $count)
    {

        if ($currency == \Currency::FERROS) {
            $variants = array('феррос', 'ферроса', 'ферросов');
        }
        elseif ($currency == \Currency::TOMATOS) {
            $variants = array('томатос', 'томатоса', 'томатосов');
        }
        else {
            $variants = array('', '', '');
        }

        return RUtils::numeral()->getPlural($count, $variants);
    }

    private function _handleOrderStatusChange()
    {
        $status = Arr::req($this->input, 'status');
        if ($status !== 'chargeable') {
            $this->_throwVkException(self::ERROR_CUSTOM, 'Неверный статус');
        }

        $orderId = Arr::req($this->input, 'order_id');
        $bankRecord = $this->_getBankRecord();
        list($ferros, $tomatos) = \Currency::getPricesByOne($bankRecord['count'], $bankRecord['currency']);
        $this->user->addMoney($this->receiverInfo['user_id'], $ferros, $tomatos, 'Банк');

        $this->actionResult = array(
            'response' => array(
                'order_id' => $orderId,
            ),
        );

        $this->statData = array(
            'soc_net_id' => $this->userSocNetId,
            'ferros' => $ferros,
            'tomatos' => $tomatos,
        );
    }

    private function _sendError(array $errorParams = array())
    {
        $this->actionResult = array(
            'error' => array(
                'error_code' => Arr::get($errorParams, 'error_code', self::ERROR_CUSTOM),
                'error_msg' => Arr::get($errorParams, 'error_msg', 'Неизвестная ошибка'),
                'critical' => Arr::get($errorParams, 'critical', 1),
            ),
        );
    }

    /**
     * @return array
     */
    private function _getBankRecord()
    {
        $currencyRates = $this->bank->getCurrencyRates();
        $itemId = Arr::req($this->input, 'item');
        $bankRecord = Arr::req($currencyRates, $itemId);
        return $bankRecord;
    }
}
