<?php
namespace action\bank\buy_money;

use krest\Arr;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\Request;
use krest\Response;
use krest\SocNet;
use model\Bank;
use model\User;
use php_rutils\RUtils;

class OkRuBuyer extends Buyer
{
    const ERROR_UNKNOWN = 1;
    const ERROR_SERVICE = 2;
    const ERROR_CALLBACK_INVALID_PAYMENT = 1001;
    const ERROR_SYSTEM = 9999;
    const ERROR_PARAM_SIGNATURE = 104;

    const RESPONSE_OK = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<callbacks_payment_response xmlns="http://api.forticom.com/1.0/">
true
</callbacks_payment_response>
XML;

    const RESPONSE_ERROR_TPL = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<ns2:error_response xmlns:ns2='http://api.forticom.com/1.0/'>
    <error_code>{{ERROR_CODE}}</error_code>
    <error_msg>{{ERROR_MSG}}</error_msg>
</ns2:error_response>
XML;

    /**
     * @var \krest\socnet\OkRu
     */
    private $_socNet;

    /**
     * @var \model\User
     */
    private $_user;

    /**
     * @var \model\Bank
     */
    private $_bank;

    private $_input;
    private $_socNetId;
    private $_actionResult;
    private $_statData;

    public function run()
    {
        try {
            $this->_socNet = SocNet::instance();
            $this->_setInput();
            $this->_execute();
        }
        catch (InvalidDataException $e) {
            $this->_setErrorResponse($e->data);
        }
        catch (\Exception $e) {
            $this->_setErrorResponse();
        }

        return [$this->_actionResult, $this->_statData];
    }

    private function _throwError($errorCode, $message)
    {
        $e = new InvalidDataException($message);
        $e->data = [
            "error_code" => $errorCode,
            "error_msg" => $message,
        ];
        throw $e;
    }

    private function _setErrorResponse(array $errorParams = [])
    {
        $code = Arr::get($errorParams, "error_code", self::ERROR_UNKNOWN);
        $search = ["{{ERROR_CODE}}", "{{ERROR_MSG}}"];
        $replace = [
            $code,
            Arr::get($errorParams, "error_msg", "Unknown error"),
        ];
        $this->_actionResult = str_replace($search, $replace, self::RESPONSE_ERROR_TPL);
        Response::instance()->setHeader("invocation-error", $code);
    }

    private function _setInput()
    {
        $this->_input = Request::instance()->getQueryParams();
        $method = Arr::get($this->_input, "method");
        if ($method !== "callbacks.payment") {
            $this->_throwError(self::ERROR_UNKNOWN, "Invalid method");
        }

        $this->_socNetId = Arr::get($this->_input, "uid");
        if ($this->_socNetId === null) {
            $this->_throwError(self::ERROR_UNKNOWN, "Uid is missed");
        }
    }

    private function _execute()
    {
        $this->_user = new User();
        $this->_bank = new Bank();

        $this->_checkSignature();
        $this->_checkUser();
        $this->_giveTheMoney();
    }

    private function _checkSignature()
    {
        $sig = Arr::get($this->_input, "sig");
        $rightSig = $this->_socNet->makeSignature($this->_input);
        if ($sig !== $rightSig) {
            $this->_throwError(self::ERROR_PARAM_SIGNATURE, "Invalid signature");
        }
    }

    private function _checkUser()
    {
        try {
            $this->_user->getUserInfo($this->_socNetId);
        }
        catch (NotFoundException $e) {
            $this->_throwError(self::ERROR_UNKNOWN, "User not found");
        }
    }

    private function _giveTheMoney()
    {
        $bankRecord = $this->_getBankRecord();
        list($ferros, $tomatos) = \Currency::getPricesByOne($bankRecord['count'], $bankRecord['currency']);
        $userId = $this->_user->getUserIdBySocNetId($this->_socNetId);
        $this->_user->addMoney($userId, $ferros, $tomatos, 'Банк');

        $this->_actionResult = self::RESPONSE_OK;
        $this->_statData = [
            "soc_net_id" => $this->_socNetId,
            "ferros" => $ferros,
            "tomatos" => $tomatos,
        ];
    }

    /**
     * @return array
     */
    private function _getBankRecord()
    {
        $currencyRates = $this->_bank->getCurrencyRates();
        $itemId = Arr::get($this->_input, "product_code");
        if ($itemId === null) {
            $this->_throwError(self::ERROR_CALLBACK_INVALID_PAYMENT, "Invalid product code");
        }
        $bankRecord = Arr::req($currencyRates, $itemId);
        return $bankRecord;
    }
}
