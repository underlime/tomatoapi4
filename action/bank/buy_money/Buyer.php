<?php
namespace action\bank\buy_money;

use krest\SocNet;

abstract class Buyer
{
    /**
     * @return \action\bank\buy_money\Buyer
     * @throws \Exception
     */
    public static function factory()
    {
        switch (SocNet::instance()->getSocNetName()) {
            case SocNet::VK_COM:
                $buyer = new VkComBuyer();
                break;
            case SocNet::OK_RU:
                $buyer = new OkRuBuyer();
                break;
            default:
                throw new \Exception('Soc net epic fail');
        }
        return $buyer;
    }

    abstract public function run();
}
