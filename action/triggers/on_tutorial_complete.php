<?php
namespace action\triggers;

use base\Action;
use model\Achievements;

class on_tutorial_complete extends Action
{
    public function post()
    {
        $achievements = new Achievements();
        $achievements->checkAchievementByCode($this->userInfo['user_id'], 'nerd');
        $this->_addAnswerArr(['trigger_status' => true]);
    }
}
