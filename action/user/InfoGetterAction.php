<?php
namespace action\user;

use base\Action;
use krest\Arr;
use krest\base\Struct;
use krest\EventDispatcher;
use lib\TkEventBroker;
use model\Achievements;
use model\Clans;
use model\ClansClash;
use model\Gains;
use model\Items;
use model\Rivals;
use model\Specials;
use model\UserNews;
use model\UserNewsRead;
use model\UsersJournal;

class InfoGetterAction extends Action
{
    /**
     * @var \model\Items
     */
    protected $items;

    protected
        $userId,
        $fullUserInfo,
        $optInfo,
        $inventory;

    protected function __construct($id)
    {
        parent::__construct($id);
        $this->items = new Items();
        $this->fullUserInfo = array();
    }

    public function checkEverydayBonus($params)
    {
        if (!empty($params->type) && $params->type == 'everyday_bonus') {
            $this->_getMarketItems();
        }
    }

    protected function _getFullUserInfo($socNetId)
    {
        $isCurrentUser = ($socNetId == $this->socNetId && $this->userInfo);
        if ($isCurrentUser) {
            EventDispatcher::instance()->addEventListener(\Events::EVENT_DATA, [$this, 'checkEverydayBonus']);
            $userInfo = $this->userInfo;
        } else {
            $userInfo = $this->user->getUserInfo($socNetId);
        }

        $this->userId = (int)$userInfo['user_id'];
        $userLevelParams = $this->user->getUserLevelParams($this->userId);
        $userFightData = $this->user->getUserFightData($this->userId, $isCurrentUser);
        if ($isCurrentUser) {
            $userStat = $this->user->getUpdatedStat($this->userId);
        } else {
            $userStat = $this->user->getUserStat($this->userId);
        }
        $userBonuses = $this->user->getUserBonuses($this->userId);

        if ($isCurrentUser && Arr::get($_SERVER, 'TEST_EVERYDAY_BONUS')) {
            $this->user->giveEverydayBonus($this->userId, rand(1, 7));
        }

        $this->fullUserInfo += [
            'user_info' => $userInfo,
            'user_level_params' => $userLevelParams,
            'user_fight_data' => $userFightData,
            'user_stat' => $userStat,
            'user_bonuses' => $userBonuses,
        ];

        $clans = new Clans();
        $clansClash = new ClansClash();
        if ($isCurrentUser) {
            $rivals = new Rivals();
            $gains = new Gains();
            $tkEventBroker = TkEventBroker::instance();

            $this->_getOptInfo();
            $this->_addAnswerArr(
                [
                'gains' => $gains->getUserGains($this->userId),
                'config_prices' => [
                    'login_ferros_price' => $this->user->getLoginPrice(),
                    'vegetable_price' => $this->user->getVegetablePrice(),
                    'reset_skills_ferros_price' => $this->user->getResetSkillsPrice(),
                    'ease_enemy_ferros_price' => $this->user->getEaseEnemyPrice(),
                    'item_slot_ferros_price' => $this->user->getItemSlotPrice(),
                    'ring_slot_ferros_price' => $this->user->getRingSlotPrice(),
                    'rival_price' => $rivals->getRivalPrice(),
                    'clan_creation_price' => $clans->getCreationPrice(),
                    'clan_rename_price' => $clans->getRenamePrice(),
                ],
                'game_constants' => [
                    'clan_max_members' => $clans->getMaxMembersCount(),
                    'clans_to_award' => $clansClash->getAwardCount(),
                    'clans_clash_season_duration' => $clansClash->getSeasonDuration(),
                ],
                'tk_event_broker' => [
                    'host' => $tkEventBroker->getHost(),
                    'port' => $tkEventBroker->getPort(),
                    'channel' => $tkEventBroker->getChannel()
                ]
                ]
            );
        }

        $clanId = $clans->getUsersClan($this->userId);
        if ($clanId) {
            $info = Arr::filterFields(
                $clans->getInfo($clanId),
                [
                'id',
                'clan_name',
                'leader_id',
                'glory',
                'previous_place_in_top',
                'victories',
                'picture'
                ]
            );
            $info["members_count"] = $clans->getClanMembersCount($clanId);
            $this->_addAnswerArr(['clan_info' => $info]);
        }
    }

    protected function _getOptInfo()
    {
        $levelExp = $this->user->calcLevelExp($this->fullUserInfo['user_level_params']['max_level']);
        $hpOnLevel = $this->user->getHpOnLevel();
        $energyOnLevel = $this->user->getEnergyOnLevel();
        $energyRecoverPrice = $this->user->getEnergyRecoverFerrosPrice();
        $energyForFight = $this->user->getEnergyForFight();
        $pointsLearningOnLevel = $this->user->getPointsLearningOnLevel();
        $energyPeriod = $this->user->getEnergyPeriod();

        $achievements = new Achievements();
        $specials = new Specials();
        $newsRead = new UserNewsRead();

        $this->optInfo = array(
            'level_exp_current' => $levelExp['current'],
            'level_exp_previous' => $levelExp['previous'],
            'level_exp_next' => $levelExp['next'],
            'hp_on_level' => $hpOnLevel,
            'energy_recover_ferros_price' => $energyRecoverPrice,
            'energy_for_fight' => $energyForFight,
            'energy_on_level' => $energyOnLevel,
            'points_learning_on_level' => $pointsLearningOnLevel,
            'energy_period' => $energyPeriod,
            'day_bonuses_config' => $this->user->getDayBonusesConfig(),
            'start_bonuses_config' => $this->user->getStartBonuses(),
            'achievements_count' => $achievements->getAchievementsCount(),
            'specials_count' => $specials->getSpecialsCount(),
            'unread_news_count' => $newsRead->getUnreadNewsCount($this->userId),
        );

        $this->fullUserInfo['user_opt_info'] = $this->optInfo;
    }

    protected function _getInventory()
    {
        $this->inventory = array(
            'user_inventory' => array(
                '_init' => $this->items->getUsersInventory($this->userId),
            ),
        );
    }

    protected function _getMarketItems()
    {
        $this->fullUserInfo['market_items_list'] = array(
            '_init' => $this->items->getItemsList()
        );
    }
}
