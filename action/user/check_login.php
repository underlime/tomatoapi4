<?php
namespace action\user;

use base\Action;
use krest\Arr;
use krest\Request;
use model\User;

/**
 * Проверить логин на корректность и существование
 * Параметр login
 * Возвращает login_allowable
 */
class check_login extends Action
{
    protected $needUserInfo = false;

    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $login = Arr::req($requestParams, 'login');

        $data = array();
        $data['login_allowable'] = $this->user->checkLoginAllowable($login);
        $this->_addAnswerArr($data);
    }
}
