<?php
namespace action\user;

use base\Action;
use krest\Arr;
use krest\exceptions\InvalidDataException;
use krest\Request;

class set_album_id extends Action
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $albumId = Arr::get($requestParams, 'album_id', '');
        if (!is_numeric($albumId)) {
            throw new InvalidDataException('Wrong album id format');
        }

        $this->user->updateUserInfo($this->socNetId, array('album_id' => $albumId));
        $this->_addAnswerArr(array(
                                  'user_info' => array(
                                      'album_id' => $albumId,
                                  ),
                             ));
    }
}
