<?php
namespace action\user;

use krest\Arr;
use krest\Request;
use model\user\RegisterData;
use model\User;

/**
 * Регистрация пользователя
 * Параметры:
 * - login
 * - vegetable
 * - body
 * - hands
 * - eyes
 * - mouth
 * - hair
 * - [referrer]
 * - [city]
 * Возвращает:
 * - user_id
 * - [user_achievements] (возвращается в случае достижений)
 * Возможно достижение visits_level
 */
class register extends InfoGetterAction
{
    protected $needUserInfo = false;

    /**
     * @var \model\user\RegisterData
     */
    private $registerData;

    public function post()
    {
        $this->registerData = new RegisterData();
        $this->_getParams();

        $user = new User();
        $this->userId = $user->registerUser($this->registerData);

        $this->socNetId = $this->registerData->soc_net_id;
        $this->userInfo = $this->user->getUserInfo($this->socNetId);
        $this->_getFullUserInfo($this->socNetId);
        $this->_getInventory();

        $this->_addAnswerArr($this->fullUserInfo);
        $this->_addAnswerArr($this->optInfo);
        $this->_addAnswerArr($this->inventory);

    }

    private function _getParams()
    {
        $fields = array(
            'login',
            'vegetable',
            'body',
            'hands',
            'eyes',
            'mouth',
            'hair',
            'referrer_type',
            'referrer',
            'city',
        );

        $requestParams = Request::instance()->getRequestBodyParams();

        $this->registerData->soc_net_id = $this->socNetId;
        foreach ($fields as $fieldName) {
            $this->registerData->$fieldName = Arr::get($requestParams, $fieldName);
        }

        $this->registerData->checkRequiredFields();
    }
}
