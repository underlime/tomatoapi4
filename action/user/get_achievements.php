<?php
namespace action\user;

use base\Action;
use model\Achievements;

/**
 * Список достижения пользователя.
 * Параметр [user_id]
 * Возвращает user_achievements
 */
class get_achievements extends Action
{
    public function post()
    {
        $userId = $this->_getUserId();
        $achv = new Achievements();
        $data = array();
        $data['user_achievements_info'] = array(
            'total_count' => sizeof($achv->getAchievementsList())
        );
        $data['user_achievements'] = array(
            '_init' => $achv->getUserAchievements($userId),
        );

        $this->_addAnswerArr($data);
    }
}
