<?php
namespace action\user;

use krest\Arr;
use krest\Request;
use model\User;

/**
 * Извлечь информацию о пользователе и его инвентарь,
 * параметр [soc_net_id]
 * возвращает:
 * - user_info
 * - user_level_params
 * - user_fight_data
 * - user_stat
 * - user_settings
 * - user_bonuses
 * - level_exp
 * - hp_on_level
 * - energy_on_level
 * - points_learning_on_level
 * - energy_period
 * - user_inventory
 * - config_prices [*]
 *
 * Возможны достижения:
 * - visits_level
 * - ferros_payed
 * - tomatos_payed
 *
 * Возможно событие everyday_bonuse
 *
 * [*] Примечание: цена reset_skills_ferros_price дана как базовая ($basePrice).
 *     Для расчета полной стоимости нужно использовать формулу:
 *     +----------------------------------------------------------------------------------+
 *     |  round($basePrice * 487 / 4096 + $levelParams['skill_resets_count'] * 487 / 256) |
 *     +----------------------------------------------------------------------------------+
 */
class get_info extends InfoGetterAction
{

    public function post()
    {
        $socNetId = $this->_getSocNetId();
        $this->_getFullUserInfo($socNetId);
        $this->_getInventory();

        $this->_addAnswerArr($this->fullUserInfo);
        $this->_addAnswerArr($this->inventory);
    }
}
