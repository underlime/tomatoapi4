<?php
namespace action\user;

use base\Action;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;
use krest\Request;
use model\Items;
use model\UserNews;

class give_friends_prizes extends Action
{
    /**
     * @var Items
     */
    private $items;

    public function post()
    {
        if ($this->userInfo['was_friends_prizes_given']) {
            throw new InvalidDataException('Prizes have already given');
        }

        $requestParams = Request::instance()->getRequestBodyParams();
        $bonusType = Arr::req($requestParams, 'bonus_type');

        $bonusesConfig = $this->user->getStartBonuses();
        $allowedBonuses = array_map(
            function ($record) {
                return $record['type'];
            }, $bonusesConfig
        );
        if (!in_array($bonusType, $allowedBonuses)) {
            throw new InvalidDataException('Wrong bonus type');
        }

        $this->user->updateUserInfo($this->socNetId, array('was_friends_prizes_given' => 1));

        $socNetId = $this->userInfo['soc_net_id'];
        $friendsIdsList = $this->user->getFriendsInGameIdsList($socNetId);

        $this->_addAnswerArr(
            array(
                 'user_info' => array(
                     'was_friends_prizes_given' => 1,
                 ),
                 'friends_ids_list' => $friendsIdsList,
            )
        );

        if ($friendsIdsList) {
            $this->_givePrizes($bonusType, $bonusesConfig, $friendsIdsList);
        }
    }

    private function _givePrizes($bonusType, $bonusesConfig, $friendsIdsList)
    {
        $count = 0;
        $itemId = 0;
        foreach ($bonusesConfig as $record) {
            if ($record['type'] == $bonusType) {
                $count = $record['count'];
                $itemId = $record['item_id'];
                break;
            }
        }

        if ($bonusType == 'tomatos' or $bonusType == 'ferros') {
            foreach ($friendsIdsList as $userId) {
                $this->_addMoney($userId, $bonusType, $count);
            }
        }
        else {
            $this->items = new Items();
            foreach ($friendsIdsList as $userId) {
                $this->items->incDecItemCount($userId, $itemId, $count);
            }
        }

        $userNews = new UserNews();
        $userNews->addStartBonusNews($this->userInfo['user_id'], $friendsIdsList, $bonusType, $itemId, $count);
    }

    /**
     * @param $userId
     * @param $currencyName
     * @param $count
     */
    private function _addMoney($userId, $currencyName, $count)
    {
        list($ferros, $tomatos) = \Currency::getPricesByOne($count, $currencyName);
        $this->user->addMoney($userId, $ferros, $tomatos, 'Подарок друга');
    }
}
