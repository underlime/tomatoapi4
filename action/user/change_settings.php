<?php
namespace action\user;

use base\Action;
use krest\Arr;
use krest\exceptions\HttpException;
use krest\exceptions\InvalidDataException;
use krest\Request;
use model\User;

/**
 * Изменить настройки пользователя
 * Параметры:
 * - [graphics_quality] ('low', 'medium', 'high'),
 * - [blood_enabled],
 * - [blood_color] ('0xff3333', '0x009933', '0x990066', '0x0066CC', '0xff9900'),
 * - [cartoon_stroke],
 * - [sounds_enabled]
 * Возвращает user_settings
 * Необходимо указать хотя бы один параметр
 */
class change_settings extends Action
{
    private $newValues = array();

    public function post()
    {
        $this->_getParams();

        try {
            $this->user->updateUserSettings($this->userInfo['user_id'], $this->newValues);
        }
        catch (InvalidDataException $e) {
            throw new HttpException(400, 'Wrong params', 0, $e);
        }

        $data = array();
        $data['user_settings'] = $this->user->getUserSettings($this->userInfo['user_id']);
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $namesList = array(
            'graphics_quality',
            'blood_enabled',
            'blood_color',
            'cartoon_stroke',
            'sounds_enabled'
        );

        foreach ($namesList as $name) {
            $value = Arr::get($requestParams, $name);
            if ($value !== null) {
                $this->newValues[$name] = $value;
            }
        }

        if (!$this->newValues) {
            throw new HttpException(400, 'Need params');
        }
    }
}
