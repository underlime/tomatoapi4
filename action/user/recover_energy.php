<?php
namespace action\user;

use base\Action;

/**
 * Полностью восстановить энергию,
 * возвращает user_fight_data (деньги и энергия)
 */
class recover_energy extends Action
{
    public function post()
    {
        $fightData = $this->user->recoverUserEnergy($this->userInfo['user_id']);
        $this->_addAnswerArr(array(
                                  'user_fight_data' => $fightData
                             ));
    }
}
