<?php
namespace action\user;

use base\Action;
use krest\Arr;
use model\User;

/**
 * Извлечение обновленной энергии персонажа
 * Возвращает user_fight_data (частично, см. код)
 */
class renew_energy extends Action
{
    public function post()
    {
        $data = array();
        $data['user_fight_data'] = Arr::filterFields(
            $this->user->getUserFightData($this->userInfo['user_id'], true),
            array('energy', 'max_energy', 'energy_time')
        );
        $this->_addAnswerArr($data);
    }
}
