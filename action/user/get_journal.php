<?php
namespace action\user;

use base\Action;
use model\UsersJournal;

/**
 * Журнал событий пользователя
 * Возвращает user_journal
 */
class get_journal extends Action
{
    /**
     * @var UsersJournal
     */
    private $userJournal;

    public function post()
    {
        $this->userJournal = new UsersJournal();
        $data = array();
        $data['user_journal'] = array(
            '_init' => $this->userJournal->getUsersMessages($this->userInfo['user_id']),
        );

        $this->_addAnswerArr($data);
    }
}
