<?php
namespace action\user;

use base\Action;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\Request;

class get_list_info extends Action
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $idsList = Arr::req($requestParams, 'soc_net_ids');
        if (!is_array($idsList)) {
            if (is_numeric($idsList)) {
                $idsList = array($idsList);
            }
            else {
                throw new InvalidDataException('Wrong ids format');
            }
        }

        $usersInfo = array();
        foreach ($idsList as $socNetId) {
            try {
                $userInfo = $this->user->getUserInfo($socNetId);
                $userId = $userInfo['user_id'];
                $usersInfo[$socNetId] = array(
                    'user_info' => $userInfo,
                    'user_fight_data' => $this->user->getUserFightData($userId),
                    'user_level_params' => $this->user->getUserLevelParams($userId),
                );
            }
            catch (NotFoundException $e) {
            }
        }

        $this->_addAnswerArr(array(
                                  'users_list_info' => array(
                                      '_init' => $usersInfo
                                  )
                             ));
    }
}
