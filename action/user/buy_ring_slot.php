<?php
namespace action\user;

use base\Action;

/**
 * Купить слот для кольца,
 * возаращает
 * - user_fight_data
 * - user_level_params
 */
class buy_ring_slot extends Action
{
    public function post()
    {
        list($newMoney, $levelParams) = $this->user->addRingSlot($this->userInfo['user_id']);
        $this->_addAnswerArr(array(
                                  'user_fight_data' => $newMoney,
                                  'user_level_params' => $levelParams,
                             ));
    }
}
