<?php
namespace action\user;

use base\Action;

/**
 * Купить доп. слот для боевых предметов
 * возвращает:
 * - user_fight_data (только деньги)
 * - user_level_params (только количество слотов)
 * @package action\user
 */
class buy_item_slot extends Action
{
    public function post()
    {
        list($fightData, $levelParams) = $this->user->addItemsSlot($this->userInfo['user_id']);
        $this->_addAnswerArr(array(
                                  'user_fight_data' => $fightData,
                                  'user_level_params' => $levelParams,
                             ));
    }
}
