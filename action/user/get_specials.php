<?php
namespace action\user;

use base\Action;
use model\Specials;

/**
 * Извлечь навыки пользователя,
 * параметр [user_id]
 * возвращает user_specials
 */
class get_specials extends Action
{
    /**
     * @var Specials
     */
    private $specials;

    public function post()
    {
        $userId = $this->_getUserId();
        $this->specials = new Specials();

        $data = array();
        $data['user_specials_info'] = array(
            'total_count' => sizeof($this->specials->getSpecialsList())
        );
        $data['user_specials'] = array(
            '_init' => $this->specials->getUsersSpecials($userId),
        );
        $this->_addAnswerArr($data);
    }
}
