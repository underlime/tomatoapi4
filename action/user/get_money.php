<?php
namespace action\user;

use base\Action;
use krest\Arr;

class get_money extends Action
{
    public function post()
    {
        $fightData = $this->user->getUserFightData($this->userInfo['user_id']);
        $this->_addAnswerArr(array(
                                  'user_fight_data' => Arr::filterFields($fightData, array('ferros', 'tomatos'))
                             ));
    }
}
