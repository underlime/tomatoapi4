<?php
namespace action\sharpening;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Sharpening;

class sharpen_weapon extends Action
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $itemId = Arr::req($requestParams, 'item_id');
        $currency = Arr::req($requestParams, 'currency');
        $userId = $this->userInfo['user_id'];

        $sharpening = new Sharpening();
        list($newMoney, $success, $level) = $sharpening->sharpenWeapon($userId, $itemId, $currency);

        $this->_addAnswerArr(array(
                                  'sharpen_weapon_result' => array(
                                      'success' => $success,
                                  ),
                                  'user_fight_data' => $newMoney,
                             ));

        if ($success) {
            list($bonuses, , ,) = $sharpening->calculateSharpeningParams($itemId, $level);
            $this->_addAnswerArr(array(
                                      'user_bonuses' => $this->user->getUserBonuses($userId),
                                      'user_inventory' => array(
                                          '_update' => array(
                                              $itemId => array(
                                                  'sharpening_level' => $level,
                                                  'sharpen_damage' => $bonuses->fight->damage,
                                                  'sharpen_armor' => $bonuses->fight->armor,
                                              )
                                          ),
                                      ),
                                 ));
        }
    }
}
