<?php
namespace action\sharpening;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Sharpening;

/**
 * Рассчитать параметры заточки оружия ITEM_ID до уровня LEVEL
 * Class get_sharpening_params
 * @package action\sharpening
 */
class get_sharpening_params extends Action
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $itemId = Arr::req($requestParams, 'item_id');
        $level = Arr::req($requestParams, 'level');

        $sharpening = new Sharpening();
        list($bonuses, $priceFerros,
            $priceTomatos, $successProbability) = $sharpening->calculateSharpeningParams($itemId, $level);

        $this->_addAnswerArr(array(
                                  'sharpening_params' => array(
                                      'damage' => $bonuses->fight->damage,
                                      'armor' => $bonuses->fight->armor,
                                      'price_ferros' => $priceFerros,
                                      'price_tomatos' => $priceTomatos,
                                      'success_probability' => $successProbability,
                                  ),
                             ));
    }
}
