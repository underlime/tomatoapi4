<?php
namespace action\citadel;

use base\Action;
use model\Gains;

class get_gains extends Action
{
    public function post()
    {
        $gains = new Gains();
        $userId = $this->userInfo['user_id'];
        $this->_addAnswerArr(array(
                                  'gains' => $gains->getUserGains($userId),
                             ));
    }
}
