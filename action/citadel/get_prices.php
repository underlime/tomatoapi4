<?php
namespace action\citadel;

use base\Action;
use model\Gains;

class get_prices extends Action
{
    public function post()
    {
        $model = new Gains();
        $prices = $model->getPrices();
        $this->_addAnswerArr(array(
                                  'citadel_prices' => $prices,
                             ));
    }
}
