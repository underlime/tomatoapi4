<?php
namespace action\citadel;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Gains;

class set_gain extends Action
{
    public function post()
    {
        $params = Request::instance()->getRequestBodyParams();
        $hours = Arr::req($params, 'hours');
        $object = Arr::req($params, 'gain_object');
        $userId = $this->userInfo['user_id'];

        $gains = new Gains();
        list($expireDate, $newMoney) = $gains->setGain($userId, $hours, $object);

        $this->_addAnswerArr(array(
                                  'gains' => array(
                                      $object => $expireDate
                                  ),
                                  'user_fight_data' => $newMoney,
                             ));

        if ($object == Gains::DAMAGE || $object == Gains::ARMOR) {
            $this->_addAnswerArr(array(
                                      'user_bonuses' => $this->user->getUserBonuses($this->userInfo['user_id'])
                                                        ->asArray()
                                 ));
        }
    }
}
