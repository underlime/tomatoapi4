<?php
namespace action\items;

use base\Action;
use model\Items;

/**
 * Извлечь предметы для рынка
 * Возвращает market_items_list
 */
class get_market_list extends Action
{
    public function post()
    {
        $items = new Items();
        $data = array(
            'market_items_list' => array(
                '_init' => array_values($items->getItemsList()),
            ),
        );
        $this->_addAnswerArr($data);
    }
}
