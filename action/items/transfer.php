<?php
namespace action\items;

use base\Action;
use krest\Arr;
use model\Items;

class transfer extends Action
{
    public function post()
    {
        $requestData = $this->request->getRequestBodyParams();
        $itemId = Arr::req($requestData, "item_id");
        $toUserId = Arr::req($requestData, "to_user_id");
        $count = Arr::get($requestData, "count", 1);

        $items = new Items();
        $items->transferItem($itemId, $this->userInfo["user_id"], $toUserId, $count);

        $invData = $items->getUsersInventory($this->userInfo["user_id"]);
        $this->_addAnswerArr([
                             'user_inventory' => [
                                 '_update' => [
                                     $itemId => Arr::get($invData, $itemId)
                                 ]
                             ],
                             ]);
    }
}
