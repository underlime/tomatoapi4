<?php
namespace action\items;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Items;

/**
 * Купить предмет
 * Параметры:
 * - item_id
 * - [count]
 * - [currency]
 * Возвращает:
 * - user_fight_data (только деньги)
 * - user_inventory
 */
class buy_item extends Action
{
    private $itemId;
    private $count;
    private $currency;

    public function post()
    {
        $this->_getParams();

        $items = new Items();

        $itemData = $items->buyItem(
            $this->userInfo['user_id'],
            $this->itemId,
            $this->count,
            $this->currency
        );

        if ($itemData['buy_another']) {
            $itemId = $itemData['buy_another'];
            $finalCount = $itemData['buy_another_at_once']*$this->count;
        }
        else {
            $itemId = $itemData['item_id'];
            $finalCount = $this->count;
        }

        $itemInfo = Arr::req(
            $items->getUsersInventory($this->userInfo['user_id']),
            $itemId
        );

        $key = ($itemInfo['count'] == $finalCount) ? '_add' : '_update';

        $data = array(
            'user_fight_data' => Arr::filterFields(
                $this->user->getUserFightData($this->userInfo['user_id'], false),
                array('ferros', 'tomatos')
            ),
            'user_inventory' => array(
                $key => array(
                    $itemId => $itemInfo,
                ),
            ),
        );

        $this->_addAnswerArr($data);

        $items->addMarketStatisticsRecord($this->itemId, $this->userInfo['user_id'], $this->count, $this->currency);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->itemId = Arr::req($requestParams, 'item_id');

        $this->count = Arr::get($requestParams, 'count', 1);
        $this->currency = Arr::get($requestParams, 'currency', \Currency::TOMATOS);
    }
}
