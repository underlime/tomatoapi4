<?php
namespace action\items;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Items;

/**
 * Применить предмет
 * Параметры:
 * - item_id
 * Возвращает:
 * - user_fight_data (частично, см. код)
 * - user_inventory
 */
class apply_item extends Action
{
    private $itemId;

    /**
     * @var Items
     */
    private $items;

    public function post()
    {
        $this->_getParams();

        $this->items = new Items();
        $this->items->applyItem($this->userInfo['user_id'], $this->itemId);

        $itemInfo = Arr::get(
            $this->items->getUsersInventory($this->userInfo['user_id']),
            $this->itemId
        );

        $data = array(
            'user_fight_data' => Arr::filterFields(
                $this->user->getUserFightData($this->userInfo['user_id'], true),
                //Поля user_fight_data
                array('hp', 'energy', 'energy_time')
            ),
            'user_inventory' => array(),
        );

        if ($itemInfo && $itemInfo['count']) {
            $data['user_inventory']['_update'] = array(
                $this->itemId => Arr::filterFields(
                    $itemInfo,
                    array('item_id', 'count')
                )
            );
        }
        else {
            $data['user_inventory']['_delete'] = array(
                $this->itemId => array(
                    'item_id' => $this->itemId,
                ),
            );
        }

        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->itemId = Arr::req($requestParams, 'item_id');
    }
}
