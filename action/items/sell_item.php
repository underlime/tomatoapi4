<?php
namespace action\items;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Items;

/**
 * Продать предмет
 * Параметры:
 * - item_id
 * - [count]
 * Возвращает:
 * - user_inventory
 * - user_fight_data (только деньги)
 */
class sell_item extends Action
{
    /**
     * @var Items
     */
    private $items;

    private $itemId, $count;

    public function post()
    {
        $this->_getParams();

        $this->items = new Items();
        $this->_sellItem();

        $itemInfo = Arr::get(
            $this->items->getUsersInventory($this->userInfo['user_id']),
            $this->itemId
        );

        $inventorySection = array();
        if ($itemInfo) {
            $inventorySection['_update'] = array(
                $this->itemId => Arr::filterFields(
                    $itemInfo,
                    array('item_id', 'count', 'put', 'social_put')
                )
            );
        }
        else {
            $inventorySection['_delete'] = array(
                $this->itemId
            );
        }

        $data = array(
            'user_inventory' => $inventorySection,
            'user_fight_data' => Arr::filterFields(
                $this->user->getUserFightData($this->userInfo['user_id'], false),
                array('ferros', 'tomatos')
            ),
        );
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->itemId = Arr::req($requestParams, 'item_id');

        $this->count = Arr::get($requestParams, 'count', 1);
    }

    private function _sellItem()
    {
        $this->items->sellItem($this->userInfo['user_id'], $this->itemId, $this->count);
    }
}
