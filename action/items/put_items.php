<?php
namespace action\items;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Items;

/**
 * Экипировать предметы
 * Параметры:
 * - [put_list]
 * - [social_put_list]
 * Возвращает:
 * - user_inventory
 * - user_bonuses
 */
class put_items extends Action
{
    private $putList, $socialPutList;

    /**
     * @var Items
     */
    private $items;

    private $newInventory;

    public function post()
    {
        $this->_getParams();

        $this->items = new Items();
        $this->_makePut();

        $data = array(
            'user_inventory' => array(
                '_update' => $this->newInventory,
            ),
            'user_bonuses' => $this->user->getUserBonuses($this->userInfo['user_id'])->asArray(),
        );
        $this->_addAnswerArr($data);
    }

    private function _getParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->putList = Arr::get($requestParams, 'put_list');
        if (!is_array($this->putList) or !$this->putList) {
            $this->putList = array();
        }

        $this->socialPutList = Arr::get($requestParams, 'social_put_list');
        if (!is_array($this->socialPutList) or !$this->socialPutList) {
            $this->socialPutList = array();
        }
    }

    private function _makePut()
    {
        $itemsForUpdate = array();
        $oldInventory = $this->items->getUsersInventory($this->userInfo['user_id']);
        foreach ($oldInventory as $itemInfo) {
            if ($itemInfo['put'] or $itemInfo['social_put']) {
                $itemsForUpdate[] = $itemInfo['item_id'];
            }
        }

        $this->items->equipItems($this->userInfo['user_id'], $this->putList, $this->socialPutList);

        $itemsForUpdate = array_merge(
            $itemsForUpdate,
            array_keys($this->putList),
            array_keys($this->socialPutList)
        );

        $allInventory = $this->items->getUsersInventory($this->userInfo['user_id']);
        $this->newInventory = array();
        foreach ($allInventory as $itemId => $itemInfo) {
            if (in_array($itemId, $itemsForUpdate)) {
                $this->newInventory[$itemId] = Arr::filterFields(
                    $itemInfo,
                    array('item_id', 'put', 'social_put')
                );
            }
        }
    }
}
