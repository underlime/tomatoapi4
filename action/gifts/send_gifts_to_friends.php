<?php
namespace action\gifts;

use base\Action;
use model\Gifts;

/**
 * Подарить подарки друзьям
 * возвращает gifts_sent_count
 */
class send_gifts_to_friends extends Action
{
    public function post()
    {
        $gifts = new Gifts();
        $sentCount = $gifts->sendGiftsToFriends($this->userInfo['user_id']);

        $this->_addAnswerArr(array(
                                  'gifts_sent_count' => $sentCount,
                             ));
    }
}
