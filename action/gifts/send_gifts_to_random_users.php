<?php
namespace action\gifts;

use base\Action;
use model\Gifts;

/**
 * Подарить подарки рандомным пользователям
 * возвращает gifts_sent_count
 */
class send_gifts_to_random_users extends Action
{
    public function post()
    {
        $gifts = new Gifts();
        $sentCount = $gifts->sendGiftsToRandomUsers($this->userInfo['user_id']);

        $this->_addAnswerArr(array(
                                  'gifts_sent_count' => $sentCount,
                             ));
    }
}
