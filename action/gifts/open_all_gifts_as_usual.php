<?php
namespace action\gifts;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Gifts;
use model\Items;

/**
 * Открыть подарок как обычный
 * параметр gift_id
 * возвращает:
 * - gift_items_info_list
 * - gifts_received_count
 * - user_gifts
 * - user_inventory
 */
class open_all_gifts_as_usual extends Action
{
    public function post()
    {
        $gifts = new Gifts();
        $userId = $this->userInfo['user_id'];
        $data = $gifts->openAllGiftsAsUsual($userId);

        $items = new Items();
        $inv = $items->getUsersInventory($userId);
        $invItemsIdsList = array_keys($inv);

        $itemsInvList = array();
        $itemsList = array();
        foreach ($data['item_ids_list'] as $itemId) {
            $invActionKey = (in_array($itemId, $invItemsIdsList)) ? '_update' : '_add';
            if (!isset($itemsInvList[$invActionKey])) {
                $itemsInvList[$invActionKey] = array();
            }

            $itemInfo = Arr::get($inv, $itemId);
            $itemsInvList[$invActionKey][$itemId] = $itemInfo;
            $itemsList[$itemId] = $itemInfo;
        }

        $this->_addAnswerArr(array(
                                  'gift_items_info_list' => $itemsList,
                                  'gifts_received_count' => $gifts->getGiftsReceivedCount($userId),
                                  'user_fight_data' => $data['new_money'],
                                  'user_gifts' => array(
                                      '_delete' => $data['gifts_ids_list'],
                                  ),
                                  'user_inventory' => $itemsInvList,
                             ));
    }
}
