<?php
namespace action\gifts;

use base\Action;
use model\Gifts;

/**
 * Извлечь список подарков пользователя
 * возвращает user_gifts_list
 */
class get_user_gifts_list extends Action
{
    public function post()
    {
        $gifts = new Gifts();
        $giftsList = $gifts->getUserGiftsList($this->userInfo['user_id']);
        $this->_addAnswerArr(array(
                                  'user_gifts_list' => array(
                                      '_init' => $giftsList,
                                  ),
                             ));
    }
}
