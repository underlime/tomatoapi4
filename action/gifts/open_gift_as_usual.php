<?php
namespace action\gifts;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Gifts;
use model\Items;

/**
 * Открыть подарок как обычный
 * параметр gift_id
 * возвращает:
 * - gift_item_info
 * - gifts_received_count
 * - user_gifts
 * - user_inventory
 */
class open_gift_as_usual extends Action
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $giftId = Arr::req($requestParams, 'gift_id');

        $gifts = new Gifts();
        $userId = $this->userInfo['user_id'];
        $itemId = $gifts->openGiftAsUsual($userId, $giftId);

        $items = new Items();
        $inv = $items->getUsersInventory($userId);
        $invItemsIdsList = array_keys($inv);

        $invActionKey = (in_array($itemId, $invItemsIdsList)) ? '_update' : '_add';
        $itemInfo = Arr::get($inv, $itemId);

        $this->_addAnswerArr(array(
                                  'gift_item_info' => $itemInfo,
                                  'gifts_received_count' => $gifts->getGiftsReceivedCount($userId),
                                  'user_gifts' => array(
                                      '_delete' => array($giftId),
                                  ),
                                  'user_inventory' => array(
                                      $invActionKey => array(
                                          $itemId => $itemInfo,
                                      ),
                                  ),
                             ));
    }
}
