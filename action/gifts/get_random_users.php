<?php
namespace action\gifts;

use base\Action;
use model\Gifts;

/**
 * Извлечь список случайных пользователей для подарков.
 * возвращает gifts_users_list
 */
class get_random_users extends Action
{
    public function post()
    {
        $gifts = new Gifts();
        $usersList = $gifts->getRandomUsersForGifts($this->userInfo['user_id']);

        $this->_addAnswerArr(array(
                                  'gifts_users_list' => $usersList,
                             ));
    }
}
