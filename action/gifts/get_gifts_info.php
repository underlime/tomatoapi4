<?php
namespace action\gifts;

/**
 * Извлечь информацию о подарках.
 * (Так же включено в user/get.info).
 * возвращает:
 * - gifts_sent_count
 * - gifts_received_count
 * - max_gifts_to_send
 * - max_gifts_to_receive
 * - gift_improved_ferros_price
 * - gift_open_ferros_price
 *
 * При попытке отправить подарков больше чем max_gifts_to_send
 * вызывает ошибку 400 - нужно обрабатывать ее.
 *
 * При открытии всех подарков общая цена будет
 * (количество подарков) * (gift_open_ferros_price)
 */

use base\Action;
use model\Gifts;

class get_gifts_info extends Action
{
    public function post()
    {
        $gifts = new Gifts();
        $userId = $this->userInfo['user_id'];
        $this->_addAnswerArr(array(
                                  'gifts_sent_count' => $gifts->getGiftsSentCount($userId),
                                  'gifts_received_count' => $gifts->getGiftsReceivedCount($userId),
                                  'max_gifts_to_send' => Gifts::MAX_GIFTS_TO_SEND,
                                  'max_gifts_to_receive' => Gifts::MAX_GIFTS_TO_RECEIVE,
                                  'gift_improved_ferros_price' => $gifts->getImprovedGiftPrice(),
                                  'gift_open_ferros_price' => $gifts->getOpenGiftPrice(),
                             ));
    }
}
