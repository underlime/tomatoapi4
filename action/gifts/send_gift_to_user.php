<?php
namespace action\gifts;

use base\Action;
use krest\Arr;
use krest\Request;
use model\Gifts;

/**
 * Подарить подарок определенному пользователю
 * возвращает gifts_sent_count
 */
class send_gift_to_user extends Action
{
    public function post()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $receiverId = Arr::req($requestParams, 'user_id');

        $gifts = new Gifts();
        $sentCount = $gifts->sendGiftToUser($this->userInfo['user_id'], $receiverId);

        $this->_addAnswerArr(array(
                                  'gifts_sent_count' => $sentCount,
                             ));
    }
}
