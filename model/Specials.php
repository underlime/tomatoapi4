<?php
namespace model;

use base\Model;
use krest\Arr;
use krest\EventDispatcher;
use krest\Db;

class Specials extends Model
{
    /**
     * @var \model\User
     */
    private $user;

    private $specialsList;

    /**
     * Извлечь список навыков,
     * ключ specials_list
     * @return array
     * Массим в формате special_id => данные
     */
    public function getSpecialsList()
    {
        $cacheKey = 'specials_list';
        $specialsList = $this->_getDataFromCache($cacheKey);
        if ($specialsList !== null) {
            return $specialsList;
        }

        $sql = <<<SQL
            SELECT
                special_id,
                code,
                name_ru,
                description_ru,
                picture,
                animation,
                active,
                bonuses,
                k_damage,
                steps,
                additional_data,
                intellect_level,
                level_intellect_step,
                level_k_bonuse
            FROM
                specials
SQL;
        $specialsData = Db::instance()->select($sql);

        $specialsList = array();
        foreach ($specialsData as $item) {
            $specialsList[$item['special_id']] = $item;
        }

        $this->_setDataIntoCache($cacheKey, $specialsList);
        return $specialsList;
    }

    /**
     * Извлечь навыки пользователя,
     * ключ user_specials
     * @param int $userId
     * @return array
     */
    public function getUsersSpecials($userId)
    {
        $specialsList = $this->getSpecialsList();
        $spInUse = $this->_getSpecialsInUse($userId);

        $this->user = new User();
        $usersSpecials = array();
        foreach ($spInUse as $curRow) {
            $special = Arr::req($specialsList, $curRow['special_id']);
            $usersSpecials[$special['special_id']] = array_merge($special, $curRow);
        }

        return $usersSpecials;
    }

    private function _getSpecialsInUse($userId)
    {
        $cacheKey = 'specials_in_use_'.$userId;
        $spInUse = $this->_getDataFromCache($cacheKey);
        if ($spInUse !== null) {
            return $spInUse;
        }

        $sql = 'SELECT special_id, level, k_damage FROM specials_in_use WHERE user_id=:user_id';
        $spInUse = Db::instance()->select($sql, array(':user_id' => $userId));

        $this->_setDataIntoCache($cacheKey, $spInUse);
        return $spInUse;
    }

    /**
     * Проверить и установить навык
     * @param int $userId
     * @param int $intellectLevel
     * @return array
     */
    public function checkAndSetSpecials($userId, $intellectLevel)
    {
        $specialDataList = $this->_getNewUserSpecialsByIntellect($userId, $intellectLevel);
        if ($specialDataList) {
            $this->_setNewSpecialsFromList($userId, $specialDataList);
        }
    }

    private function _getNewUserSpecialsByIntellect($userId, $intellectLevel)
    {
        $sql = <<<SQL
            SELECT
                special_id
            FROM
                specials
            WHERE
                intellect_level <= :intellect_level
                AND special_id NOT IN (
                    SELECT special_id FROM specials_in_use WHERE user_id=:user_id
                )
            ORDER BY
                intellect_level ASC
SQL;

        $params = array(
            ':intellect_level' => $intellectLevel,
            ':user_id' => $userId
        );

        $specialDataList = Db::instance()->select($sql, $params);
        return $specialDataList;
    }

    private function _setNewSpecialsFromList($userId, $specialDataList)
    {
        $this->specialsList = $this->getSpecialsList();
        $sql = 'INSERT INTO specials_in_use SET user_id=:user_id, special_id=:special_id';

        foreach ($specialDataList as $specialData) {
            $this->_setAndDispatchSpecialData($userId, $specialData, $sql);
        }

        $cacheKey = 'specials_in_use_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
    }

    private function _setAndDispatchSpecialData($userId, $specialData, $sql)
    {
        $fullSpecialData = Arr::req($this->specialsList, $specialData['special_id']);

        $params = array(
            ':user_id' => $userId,
            ':special_id' => $specialData['special_id']
        );

        Db::instance()->exec($sql, $params);

        $eventData = new specials\EventData();
        $eventData->active = $fullSpecialData['active'];
        $eventData->code = $fullSpecialData['code'];
        $eventData->name_ru = $fullSpecialData['name_ru'];
        $eventData->text_ru = $fullSpecialData['description_ru'];
        $eventData->picture = $fullSpecialData['picture'];

        if ($fullSpecialData['bonuses']) {
            $bonuses = new user\Bonuses();
            $bonuses->importXml($fullSpecialData['bonuses']);
            $eventData->bonuses = $bonuses->asArray();
        }

        EventDispatcher::instance()->dispatchEvent(\Events::EVENT_DATA, $eventData);
    }

    /**
     * Извлечь навыки для уровня интеллекта
     * @param int $intellectLevel
     * @return array
     */
    public function getSpecialsListByIntellect($intellectLevel)
    {
        $cacheKey = 'specials_by_intellect_'.$intellectLevel;
        $specialsList = $this->_getDataFromCache($cacheKey);
        if ($specialsList !== null) {
            return $specialsList;
        }

        $sql = <<<SQL
          SELECT
            special_id,
            code,
            name_ru,
            description_ru,
            picture,
            animation,
            active,
            bonuses,
            k_damage,
            steps,
            additional_data,
            intellect_level,
            level_intellect_step,
            level_k_bonuse
          FROM
            specials
          WHERE
            intellect_level <= :intellect_level
          ORDER BY
            intellect_level ASC
SQL;

        $specialsList = Db::instance()->select($sql, array(':intellect_level' => $intellectLevel));

        $specialsListById = array();
        foreach ($specialsList as $curSpecial) {
            $specialsListById[$curSpecial['special_id']] = $curSpecial;
        }

        $this->_setDataIntoCache($cacheKey, $specialsListById);
        return $specialsListById;
    }

    /**
     * Установить уровни навыков
     * @param $userId
     * @return array
     */
    public function setUserSpecialsLevels($userId)
    {
        list($specialsList, $userSpecialsList, $intellect) = $this->_getSpecialsDataForSetLevels($userId);
        $newSpecials = $this->_getNewSpecials($userSpecialsList, $intellect, $specialsList);
        if ($newSpecials) {
            $this->_setNewSpecialsLevels($userId, $newSpecials);
        }
        return $newSpecials;
    }

    private function _getSpecialsDataForSetLevels($userId)
    {
        $specialsList = $this->getSpecialsList();

        $this->disableCache();
        $userSpecialsList = $this->getUsersSpecials($userId);
        $this->enableCache();

        $this->user = new User();
        $this->user->disableCache();
        $levelParams = $this->user->getUserLevelParams($userId);
        $this->user->enableCache();

        $intellect = $levelParams['intellect'];
        return array($specialsList, $userSpecialsList, $intellect);
    }

    private function _getNewSpecials($userSpecialsList, $intellect, $specialsList)
    {
        $newSpecials = array();
        foreach ($userSpecialsList as $specialId => $specialData) {
            if (!$specialData['active']) {
                continue;
            }

            $newLevel = $this->_getSpecialLevel($intellect, $specialData);
            if ($specialData['level'] != $newLevel) {
                $originalKDamage = $specialsList[$specialId]['k_damage'];
                $newKDamage = $originalKDamage + $newLevel*$specialsList[$specialId]['level_k_bonuse'];
                $specialData['level'] = $newLevel;
                $specialData['k_damage'] = $newKDamage;
                $newSpecials[$specialId] = $specialData;
            }
        }

        return $newSpecials;
    }

    private function _getSpecialLevel($intellect, array $specialData)
    {
        $dIntel = $intellect - $specialData['intellect_level'];
        if ($dIntel > -1) {
            $level = 1 + floor($dIntel/$specialData['level_intellect_step']);
        }
        else {
            $level = 0;
        }

        return $level;
    }

    private function _setNewSpecialsLevels($userId, array $newSpecials)
    {
        $db = Db::instance();
        $sql = <<<SQL
            UPDATE
                specials_in_use
            SET
                level=:level,
                k_damage=:k_damage
            WHERE
                user_id=:user_id
                AND special_id=:special_id
SQL;

        $params = array(
            ':user_id' => $userId,
        );
        foreach ($newSpecials as $specialId => $specialData) {
            $params[':special_id'] = $specialId;
            $params[':level'] = $specialData['level'];
            $params[':k_damage'] = $specialData['k_damage'];
            $db->exec($sql, $params);
        }

        $cacheKey = 'specials_in_use_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
    }

    /**
     * Сбросить навыки пользователя
     * @param $userId
     */
    public function reserUserSpecials($userId)
    {
        $sql = 'DELETE FROM specials_in_use WHERE user_id=:id';
        Db::instance()->exec($sql, array(':id' => $userId));

        $cacheKey = 'specials_in_use_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
    }

    /**
     * Извлечь количество навыков
     * @return int
     */
    public function getSpecialsCount()
    {
        $cacheKey = 'specials_count';
        $count = $this->_getDataFromCache($cacheKey);
        if ($count !== null) {
            return $count;
        }

        $sql = 'SELECT COUNT(*) AS `count` FROM specials';
        $data = Db::instance()->select($sql);
        $count = $data[0]['count'];

        $this->_setDataIntoCache($cacheKey, $count);
        return $count;
    }
}
