<?php
namespace model;

use base\Model;
use krest\Arr;
use krest\Db;
use krest\EventDispatcher;
use krest\exceptions\InvalidDataException;

class Achievements extends Model
{
    const OP_AND = 'AND';
    const OP_OR = 'OR';

    /**
     * @var \model\User
     */
    private $user;

    /**
     * @var \krest\Db
     */
    private $db;

    /**
     * @var \krest\EventDispatcher
     */
    private $eventDispatcher;

    /**
     * Извлечь список достижений,
     * ключ achievements_list
     * @return array
     * Массив в формате achievement_id => данные
     */
    public function getAchievementsList()
    {
        $cacheKey = 'achievements_list';
        $achievementsList = $this->_getDataFromCache($cacheKey);
        if ($achievementsList !== null) {
            return $achievementsList;
        }

        $sql = <<<SQL
          SELECT
            achievement_id,
            code,
            name_ru,
            text_ru,
            pic,
            glory,
            wins_level,
            fails_level,
            ferros_payed,
            tomatos_payed,
            visits_level,
            level_reached,
            strength_level,
            agility_level,
            intellect_level
          FROM
            achievements
SQL;

        $achievementsData = Db::instance()->select($sql);
        $achievementsList = $this->_fetchAchievements($achievementsData);

        $this->_setDataIntoCache($cacheKey, $achievementsList);
        return $achievementsList;
    }

    private function _fetchAchievements(array $achievementsData)
    {
        $achievementsList = array();
        foreach ($achievementsData as $curAchv) {
            $achievementsList[$curAchv['achievement_id']] = $curAchv;
        }

        return $achievementsList;
    }

    /**
     * Извлечь достижения пользователя,
     * ключ user_achievements
     * @param int $userId
     * @return array
     */
    public function getUserAchievements($userId)
    {
        $achievementsList = $this->getAchievementsList();
        $achvGottenData = $this->_getAchvGottenData($userId);

        $usersAchv = array();
        foreach ($achvGottenData as $curRow) {
            $achv = Arr::req($achievementsList, $curRow['achievement_id']);
            $usersAchv[] = $achv;
        }

        return $usersAchv;
    }

    private function _getAchvGottenData($userId)
    {
        $cacheKey = 'achievements_gotten_'.$userId;
        $achievementsData = $this->_getDataFromCache($cacheKey);
        if ($achievementsData !== null) {
            return $achievementsData;
        }

        $sql = <<<SQL
          SELECT
            achievement_id
          FROM
            achievements_gotten
          WHERE
            user_id=:user_id
SQL;

        $achievementsData = Db::instance()->select($sql, array(':user_id' => $userId));
        $this->_setDataIntoCache($cacheKey, $achievementsData);
        return $achievementsData;
    }

    /**
     * Проверить условия достижений и назначить их
     * @param int $userId
     * @param array $params
     * @param string $op AND или OR
     * @throws \krest\exceptions\InvalidDataException
     * @return void
     */
    public function checkAchievementByFields($userId, array $params, $op = self::OP_AND)
    {
        if (empty($params)) {
            throw new InvalidDataException('Empty condition');
        }

        if ($op != self::OP_AND && $op != self::OP_OR) {
            throw new InvalidDataException('Wrong operator: '.$op);
        }

        $this->db = Db::instance();

        $achievementsList = $this->getAchievementsList();
        $newAchievementsList = $this->_getNewAchievementsByFields($userId, $params, $op);

        $this->user = new User();
        $this->eventDispatcher = EventDispatcher::instance();
        foreach ($newAchievementsList as $achievementId) {
            $curAchv = Arr::req($achievementsList, $achievementId);
            $this->_dispatchAchievementEvent($curAchv);
            $this->_giveAchievementToUser($userId, $curAchv);
        }

        if ($newAchievementsList) {
            $cacheKey = 'achievements_gotten_'.$userId;
            $this->_deleteDataFromCache($cacheKey);
        }
    }

    private function _getNewAchievementsByFields($userId, array $params, $op)
    {
        $sql = 'SELECT achievement_id FROM achievements WHERE ';

        $termsList = array();
        $queryParams = array(':user_id' => $userId);

        $fieldsList = array();
        foreach ($params as $name => $value) {
            $termsList[] = "`$name` <= :$name";
            $queryParams[':'.$name] = $value;
            $fieldsList[] = "`$name`";
        }

        $terms = implode(" $op ", $termsList);
        $sql .= "($terms)";

        $orderFields = implode(' ASC, ', $fieldsList);
        $sql .= " AND achievement_id NOT IN( SELECT achievement_id FROM achievements_gotten WHERE user_id=:user_id ) ORDER BY $orderFields ASC";

        $achvData = $this->db->select($sql, $queryParams);

        $achievementsIdsList = array();
        foreach ($achvData as $curRow) {
            $achievementsIdsList[] = $curRow['achievement_id'];
        }

        return $achievementsIdsList;
    }

    private function _dispatchAchievementEvent($curAchv)
    {
        $eventData = new achievements\AchievementEventData();
        $eventData->achievement_id = $curAchv['achievement_id'];
        $eventData->achievement_code = $curAchv['code'];
        $eventData->name_ru = $curAchv['name_ru'];
        $eventData->text_ru = $curAchv['text_ru'];
        $eventData->pic = $curAchv['pic'];
        $this->eventDispatcher->dispatchEvent(\Events::EVENT_DATA, $eventData);
    }

    private function _giveAchievementToUser($userId, $curAchv)
    {
        $sql = 'INSERT INTO achievements_gotten SET user_id=:user_id, achievement_id=:achievementId';
        $params = array(
            ':user_id' => $userId,
            ':achievementId' => $curAchv['achievement_id']
        );

        $this->db->exec($sql, $params);
    }

    /**
     * Извлечь количество достижений
     * @return int
     */
    public function getAchievementsCount()
    {
        $cacheKey = 'achievements_count';
        $count = $this->_getDataFromCache($cacheKey);
        if ($count !== null) {
            return $count;
        }

        $sql = 'SELECT COUNT(*) AS `count` FROM achievements';
        $data = Db::instance()->select($sql);
        $count = $data[0]['count'];

        $this->_setDataIntoCache($cacheKey, $count);
        return $count;
    }

    /**
     * Проверить достижение по коду и назначить, если еще не назначено
     * @param int $userId
     * @param string $code
     * @throws \RuntimeException
     */
    public function checkAchievementByCode($userId, $code)
    {
        $this->db = Db::instance();

        $sql = <<<SQL
            SELECT
                ag.user_id
            FROM
                achievements_gotten ag
                INNER JOIN achievements a
                USING(achievement_id)
            WHERE
                ag.user_id=:userId
                AND a.code=:dbCode
SQL;
        $res = $this->db->select($sql, [':userId' => $userId, ':dbCode' => $code]);
        if (empty($res)) {
            $data = array_filter(
                $this->getAchievementsList(),
                function ($rec) use ($code) {
                    return ($rec['code'] === $code);
                }
            );
            if (empty($data)) {
                throw new \RuntimeException("Achievement {$code} is not found");
            }
            $achievement = reset($data);

            $this->user = new User();
            $this->eventDispatcher = EventDispatcher::instance();
            $this->_dispatchAchievementEvent($achievement);
            $this->_giveAchievementToUser($userId, $achievement);

            if ($data) {
                $cacheKey = 'achievements_gotten_'.$userId;
                $this->_deleteDataFromCache($cacheKey);
            }
        }
    }
}
