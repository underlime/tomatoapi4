<?php
namespace model\spy;

use krest\base\MagicAccessors;
use model\SpyData;

/**
 * Class Spy
 * @package model\spy
 * @property string $userId
 * @property array $actions
 */
class Spy
{
    use MagicAccessors;

    /**
     * @var Spy
     */
    private static $_instance;

    private $_userId;
    private $_isTarget = true;
    private $_actions = [];

    public static function instance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function __construct()
    {
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     * Id пользователя в социальной сети
     * @param string $socNetId
     */
    public function setUserId($socNetId)
    {
        $this->_userId = $socNetId;
        $this->setIsTarget((new SpyData())->isUserTarget($socNetId));
    }

    /**
     * Добавить действие
     * @param $action
     */
    public function pushAction(SpyAction $action)
    {
        if ($this->_isTarget) {
            $this->_actions[] = $action;
        }
    }

    /**
     * Добавить действие из скалярных переменных
     * @param $file
     * @param $method
     * @param $line
     * @param $description
     * @param $data
     */
    public function pushActionData($file, $method, $line, $description, $data=null)
    {
        if ($this->_isTarget) {
            $action = new SpyAction();
            $action->file = $file;
            $action->method = $method;
            $action->line = $line;
            $action->description = $description;
            $action->data = $data;
            $this->pushAction($action);
        }
    }

    /**
     * Количество записей
     * @return int
     */
    public function getActionsCount()
    {
        return sizeof($this->_actions);
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->_actions;
    }

    /**
     * Является ли пользователь целью шпионажа
     * @return boolean
     */
    public function getIsTarget()
    {
        return $this->_isTarget;
    }

    /**
     * @param boolean $isTarget
     */
    public function setIsTarget($isTarget)
    {
        $this->_isTarget = $isTarget;
        if ($this->_isTarget == false) {
            $this->_actions = [];
        }
    }

    private function __clone()
    {
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function __wakeup()
    {
    }
}
