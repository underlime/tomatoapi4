<?php
namespace model\spy;

use krest\base\Struct;

class SpyAction extends Struct
{
    public $file, $method, $line;
    public $description;
    public $data;
}
