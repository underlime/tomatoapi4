<?php
namespace model;

use base\Model;
use Exception;
use krest\api\Config;
use krest\Arr;
use krest\Db;
use krest\EventDispatcher;
use krest\exceptions\AlreadyExistsException;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\SocNet;
use krest\strings\SqlQuery;
use model\chardetails\DetailsList;
use model\user\RegisterData;

class User extends Model
{
    const LOGIN_PATTERN = '/^[\w\.-]{3,20}$/u';

    const INTERVAL_DAY = 'day';

    const INTERVAL_WEEK = 'week';

    const INTERVAL_GLOBAL = 'global';

    const LEVEL_PARAM_STRENGTH = 'strength';

    const LEVEL_PARAM_AGILITY = 'agility';

    const LEVEL_PARAM_INTELLECT = 'intellect';

    const FIGHT_DATA_EXPERIENCE = 'experience';

    const MAX_ITEMS_SLOTS_AVAIBLE = 5;

    const MAX_RINGS_SLOTS_AVAIBLE = 2;

    /**
     * @var array
     */
    private static $config;

    /**
     * @var \model\Items
     */
    private $items;

    /**
     * @var \model\CharDetails
     */
    private $charDetails;

    /**
     * @var \model\Achievements
     */
    private $achievements;

    /**
     * @var \model\Specials
     */
    private $specials;

    function __construct()
    {
        parent::__construct();

        if (self::$config === null) {
            self::$config = Config::instance()->getForClass(__CLASS__);
        }
    }

    /**
     * Извлечь базовую информацию,
     * ключ user_info
     * @param string $socNetId (!)Это ID в социальной сети
     * @throws \krest\exceptions\NotFoundException
     * @return array
     */
    public function getUserInfo($socNetId)
    {
        $cacheKey = 'user_info_' . $socNetId;
        $userInfo = $this->_getDataFromCache($cacheKey);
        if ($userInfo !== null) {
            return $userInfo;
        }

        $sql = '
            SELECT
                soc_net_id,
                user_id,
                city,
                login,
                ban_level,
                ban_reason,
                vegetable,
                body,
                hands,
                eyes,
                mouth,
                hair,
                reg_time,
                was_friends_prizes_given,
                album_id
            FROM
                users
            WHERE
                soc_net_id=:soc_net_id
       ';

        $userInfoData = Db::instance()->select($sql, array(':soc_net_id' => $socNetId));
        $userInfo = Arr::get($userInfoData, 0);
        if ($userInfo === null) {
            throw new NotFoundException("User $socNetId not found");
        }

        $this->_setDataIntoCache($cacheKey, $userInfo);
        return $userInfo;
    }

    /**
     * Извлечь Id социальной сети по id пользователя
     * @param int $userId
     * @return string
     * @throws \krest\exceptions\NotFoundException
     */
    public function getSocNetIdByUserId($userId)
    {
        $cacheKey = 'soc_net_id-of-user-' . $userId;
        $socNetId = $this->_getDataFromCache($cacheKey);
        if ($socNetId !== null) {
            return $socNetId;
        }

        $sql = 'SELECT soc_net_id FROM users WHERE user_id=:user_id';
        $data = Db::instance()->select($sql, array(':user_id' => $userId));
        if (!$data) {
            throw new NotFoundException("User $userId not found");
        }

        $socNetId = $data[0]['soc_net_id'];
        $this->_setDataIntoCache($cacheKey, $socNetId);

        return $socNetId;
    }

    /**
     * Извлечь Id социальной сети по id пользователя
     * @param string $socNetId
     * @return int
     * @throws \krest\exceptions\NotFoundException
     */
    public function getUserIdBySocNetId($socNetId)
    {
        $cacheKey = 'user_id-of-soc_net_id-' . $socNetId;
        $userId = $this->_getDataFromCache($cacheKey);
        if ($userId !== null) {
            return $userId;
        }

        $sql = 'SELECT user_id FROM users WHERE soc_net_id=:soc_net_id';
        $data = Db::instance()->select($sql, array(':soc_net_id' => $socNetId));
        if (!$data) {
            throw new NotFoundException("User $socNetId not found");
        }

        $userId = $data[0]['user_id'];
        $this->_setDataIntoCache($cacheKey, $userId);

        return $userId;
    }

    /**
     * Извлечь базовую информацию, используя user_id
     * ключ user_info
     * @param $userId
     * @throws \krest\exceptions\NotFoundException
     * @return array
     */
    public function getUserInfoByUserId($userId)
    {
        $socNetId = $this->getSocNetIdByUserId($userId);
        $userInfo = $this->getUserInfo($socNetId);
        return $userInfo;
    }

    /**
     * Обновить информацию о пользователе
     * @param string $socNetId (!)Это ID в социальной сети
     * @param array $newValues
     */
    public function updateUserInfo($socNetId, array $newValues)
    {
        $cacheKey = 'user_info_' . $socNetId;
        $this->_updateCachedData($cacheKey, $newValues);

        $table = 'users';
        $idField = 'soc_net_id';
        $this->_updateUsersTable($table, $socNetId, $newValues, $idField);
    }

    private function _updateUsersTable($table, $userId, array $newValues, $idFiels = 'user_id')
    {
        if (empty($newValues)) {
            throw new InvalidDataException('newValues is empty');
        }

        $fields = array_keys($newValues);

        $set = SqlQuery::buildSetSection($fields);
        $params = SqlQuery::buildQueryParams($newValues);
        $params[":$idFiels"] = $userId;

        $sql = "UPDATE `$table` SET $set WHERE `$idFiels`=:$idFiels";
        Db::instance()->exec($sql, $params);
    }

    /**
     * Изменить логин,
     * ключ result
     * @param $socNetId
     * @param $newLogin
     * @return bool Был ли изменен логин
     * @throws \Exception
     * @throws \krest\exceptions\InvalidDataException
     * @throws \krest\exceptions\AlreadyExistsException
     */
    public function changeLogin($socNetId, $newLogin)
    {
        try {
            $this->_checkLoginBrutal($newLogin);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->disableCache();
        $userInfo = $this->getUserInfo($socNetId);
        $this->enableCache();

        $newMoney = $this->_setNewLogin($newLogin, $socNetId, $userInfo['user_id']);
        return $newMoney;
    }

    private function _checkLoginBrutal($login)
    {
        $check = $this->checkLoginAllowable($login);
        if (!$check['correct']) {
            throw new InvalidDataException('Login has wrong format');
        }

        if ($check['exists']) {
            throw new AlreadyExistsException('Login already exists');
        }
    }

    private function _setNewLogin($newLogin, $socNetId, $userId)
    {
        $loginPrice = $this->getLoginPrice();
        $newMoney = $this->pay($userId, $loginPrice, 0, 'Логин');

        $this->updateUserInfo($socNetId, array('login' => $newLogin));

        return $newMoney;
    }

    /**
     * Изменить части персонажа
     * @param $socNetId
     * @param $currency
     * @param DetailsList $detailsList
     * @return array
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     */
    public function changeCharDetails($socNetId, $currency, DetailsList $detailsList)
    {
        $detailsList->hands = null;
        if ($detailsList->isEmpty()) {
            throw new InvalidDataException('Details list is empty');
        }

        $this->charDetails = new CharDetails();

        $this->disableCache();
        $userInfo = $this->getUserInfo($socNetId);
        $this->enableCache();

        $needToChange = $this->_getPartsNeedsToBeChanged($detailsList, $userInfo);
        if ($needToChange->isEmpty()) {
            throw new InvalidDataException('Nothing to change');
        }

        $actualNeedToChange = $needToChange->asArrayNotEmpty();
        try {
            $price = $this->_getPartsPrice($currency, $userInfo, $actualNeedToChange);
        } catch (Exception $e) {
            throw $e;
        }

        list($ferros, $tomatos) = \Currency::getPricesByOne($price, $currency);
        $newMoney = $this->pay($userInfo['user_id'], $ferros, $tomatos, 'Части персонажа');
        $this->updateUserInfo($socNetId, $actualNeedToChange);

        return $newMoney;
    }

    /**
     * @param $detailsList
     * @param $userInfo
     * @return chardetails\DetailsList
     */
    private function _getPartsNeedsToBeChanged(DetailsList $detailsList, $userInfo)
    {
        $existsPartsList = $this->charDetails->getDetailsGroupByMc($userInfo['vegetable']);
        $needToChange = new DetailsList();
        $actualDetailsList = $detailsList->asArrayNotEmpty();

        foreach ($actualDetailsList as $partType => $partName) {
            $partData = Arr::req($existsPartsList, $partName);
            if ($partName != $userInfo[$partType]) {
                $this->charDetails->appendDetailRowToList($partData, $needToChange);
            }
        }

        return $needToChange;
    }

    private function _getPartsPrice($currency, array $userInfo, array $actualNeedToChange)
    {
        $detailsList = $this->charDetails->getDetailsGroupByMc($userInfo['vegetable']);
        $fullCurrency = \Currency::getFullCurrencyName($currency);

        if (isset($actualNeedToChange['hands'])) {
            unset($actualNeedToChange['hands']);
        }

        $price = 0;
        foreach ($actualNeedToChange as $partName) {
            $curDetailData = Arr::req($detailsList, $partName);
            $curPrice = $curDetailData["price_{$fullCurrency}"];
            if (!$curPrice) {
                throw new InvalidDataException('Wrong currency');
            }

            $price += $curPrice;
        }

        return $price;
    }

    /**
     * Изменить тип овоща
     * @param $socNetId
     * @param $vegetable
     * @param $currency
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     * @return array
     */
    public function changeVegetable($socNetId, $vegetable, $currency)
    {
        $allowedVegetables = array('beta', 'carrot', 'cucumber', 'onion', 'pepper', 'tomato');
        if (!in_array($vegetable, $allowedVegetables)) {
            throw new InvalidDataException('Wrong vegetable type');
        }

        $this->disableCache();
        $userInfo = $this->getUserInfo($socNetId);
        $this->enableCache();

        if ($userInfo['vegetable'] == $vegetable) {
            throw new InvalidDataException('Nothing to change');
        }

        $this->charDetails = new CharDetails();
        $priceData = $this->getVegetablePrice();
        $price = Arr::req($priceData, $currency);
        list($ferros, $tomatos) = \Currency::getPricesByOne($price, $currency);
        $newMoney = $this->pay($userInfo['user_id'], $ferros, $tomatos, 'Смена овоща');

        $defaultParts = $this->charDetails->getDefaultPartsSet($vegetable)
            ->asArray();

        $aVeg = array('vegetable' => $vegetable);
        $newValues = array_merge($aVeg, $defaultParts);
        $this->updateUserInfo($socNetId, $newValues);
        return $newMoney;
    }

    /**
     * Извлечь боевую информацию пользователя,
     * ключ user_fight_data
     * @param int $userId
     * @param bool $updateEnergy
     * @throws NotFoundException
     * @return array
     */
    public function getUserFightData($userId, $updateEnergy = false)
    {
        $cacheKey = 'user_fight_data_' . $userId;
        if (!$updateEnergy) {
            $userFightData = $this->_getDataFromCache($cacheKey);
            if ($userFightData !== null) {
                return $userFightData;
            }
        }

        $sql = <<<SQL
            SELECT
                hp,
                glory,
                day_glory,
                week_glory,
                season_glory,
                energy,
                max_energy,
                energy_time,
                experience,
                ferros,
                tomatos,
                battles_count,
                wins_count,
                items_given
            FROM
                users_fight_data
            WHERE
                user_id=:user_id
SQL;

        $userInfoData = Db::instance()->select($sql, array(':user_id' => $userId));
        $userFightData = Arr::get($userInfoData, 0);
        if ($userFightData === null) {
            throw new NotFoundException("User $userId not found");
        }

        if ($updateEnergy) {
            $newEnergy = $this->updateUsersEnergy(
                $userId,
                $userFightData['energy'],
                $userFightData['energy_time'],
                $userFightData['max_energy']
            );
            $userFightData = array_merge($userFightData, $newEnergy);
        }

        $this->_setDataIntoCache($cacheKey, $userFightData);
        return $userFightData;
    }

    /**
     * Обновить энергию пользователя с готовыми параметрами
     * @param int $userId
     * @param int $energyAtNow
     * @param string $energyMysqlDate
     * @param int $maxEnergy
     * @return array
     */
    public function updateUsersEnergy($userId, $energyAtNow, $energyMysqlDate, $maxEnergy)
    {
        $energyTime = strtotime($energyMysqlDate);
        $nowTime = time();
        $dTime = $nowTime - $energyTime;

        $energyPeriod = $this->getEnergyPeriod();
        $dEnergy = round($dTime / $energyPeriod);
        if ($dEnergy < 1) {
            $oldValues = array('energy' => $energyAtNow, 'energy_time' => $energyMysqlDate);
            return $oldValues;
        }

        $newEnergy = min($maxEnergy, $energyAtNow + $dEnergy);
        $nowMysqlDate = date('Y-m-d H:i:s', $nowTime);

        $newValues = array('energy' => $newEnergy, 'energy_time' => $nowMysqlDate);
        $this->updateUserFightData($userId, $newValues);

        return $newValues;
    }

    /**
     * Полностью восстановить энергию пользователя
     * @param $userId
     * @return array fight_data
     */
    public function recoverUserEnergy($userId)
    {
        $price = $this->getEnergyRecoverFerrosPrice();
        $newMoney = $this->pay($userId, $price, 0, 'Энергия');

        $this->disableCache();
        $fightData = $this->getUserFightData($userId);
        $this->enableCache();

        $data = array(
            'energy' => $fightData['max_energy'],
            'energy_time' => date('Y-m-d H:i:s'),
        );
        $this->updateUserFightData($userId, $data);

        $newMoney += $data;
        return $newMoney;
    }

    /**
     * Обновить боевые данные пользователя,
     * Для обновления поля glory используйте метод User::updateGlory
     * @param int $userId
     * @param array $newValues
     */
    public function updateUserFightData($userId, array $newValues)
    {
        if (isset($newValues['energy']) or isset($newValues['hp'])) {
            $this->_makeUserFightDataCorrections($userId, $newValues);
        }

        $cacheKey = 'user_fight_data_' . $userId;
        $this->_updateCachedData($cacheKey, $newValues);

        $table = 'users_fight_data';
        $this->_updateUsersTable($table, $userId, $newValues);

        $this->_checkUserFightDataAchievements($userId, $newValues);
    }

    private function _makeUserFightDataCorrections($userId, array &$newValues)
    {
        if (isset($newValues['energy'])) {
            $this->_makeCorrectionOfEnergy($userId, $newValues);
        }

        if (isset($newValues['hp'])) {
            $levelParams = $this->getUserLevelParams($userId);
            $newValues['hp'] = min($levelParams['max_hp'], $newValues['hp']);
        }
    }

    private function _makeCorrectionOfEnergy($userId, array &$newValues)
    {
        if (isset($newValues['max_energy'])) {
            $maxEnergy = $newValues['max_energy'];
        } else {
            $fightData = $this->getUserFightData($userId, false);
            $maxEnergy = $fightData['max_energy'];
        }

        $newValues['energy'] = min($maxEnergy, $newValues['energy']);
    }

    private function _checkUserFightDataAchievements($userId, array $newValues)
    {
        if (!empty($newValues['wins_count'])) {
            $this->achievements = new Achievements();
            $this->achievements->checkAchievementByFields($userId, array('wins_level' => $newValues['wins_count']));
            if (!empty($newValues['battles_count'])) {
                $failsLevel = $newValues['battles_count'] - $newValues['wins_count'];
                $this->achievements->checkAchievementByFields($userId, array('fails_level' => $failsLevel));
            }
        }
    }

    /**
     * Обновить славу
     * @param int $userId
     * @param int $newGlory новая глобальная слава
     * @throws \krest\exceptions\NotFoundException
     */
    public function updateGlory($userId, $newGlory)
    {
        $db = Db::instance();

        $clans = new Clans();
        $clanId = $clans->getUsersClan($userId);

        $sql = 'SELECT glory, day_glory, week_glory, season_glory FROM users_fight_data WHERE user_id=:userId';
        $data = $db->select($sql, array(':userId' => $userId));
        if (!$data) {
            throw new NotFoundException("User $userId not found");
        }
        $gloryData = $data[0];

        $dGlory = $newGlory - $gloryData['glory'];
        $dayGlory = max(0, $gloryData['day_glory'] + $dGlory);
        $weekGlory = max(0, $gloryData['week_glory'] + $dGlory);
        if ($clanId) {
            $seasonGlory = max(0, $gloryData['season_glory'] + $dGlory);
        } else {
            $seasonGlory = 0;
        }

        $this->updateUserFightData(
            $userId,
            array(
                 'glory' => $newGlory,
                 'day_glory' => $dayGlory,
                 'week_glory' => $weekGlory,
                 'season_glory' => $seasonGlory,
            )
        );
        if ($clanId !== null) {
            $clans->addGlory($clanId, $dGlory);
        }
    }

    /**
     * Сбросить сезонную славу
     */
    public function resetSeasonGlory()
    {
        Db::instance()->exec('UPDATE users_fight_data SET season_glory=0');
    }

    /**
     * Добавить денег пользователю
     * @param $userId
     * @param $ferros
     * @param $tomatos
     * @param string $comment
     * @throws InvalidDataException
     * @return array
     */
    public function addMoney($userId, $ferros, $tomatos, $comment = '')
    {
        $this->_castMoneyToInt($ferros, $tomatos);
        $this->_checkMoneyForTransaction($ferros, $tomatos);

        $this->disableCache();
        $fightData = $this->getUserFightData($userId);
        $this->enableCache();

        $newValues = array(
            'ferros' => $fightData['ferros'] + $ferros,
            'tomatos' => $fightData['tomatos'] + $tomatos,
        );
        $this->updateUserFightData($userId, $newValues);

        $this->_logBuyMoney($userId, $ferros, $tomatos, $comment);
        return $newValues;
    }

    private function _castMoneyToInt(&$ferros, &$tomatos)
    {
        $ferros = (int)$ferros;
        $tomatos = (int)$tomatos;
    }

    private function _checkMoneyForTransaction($ferros, $tomatos)
    {
        if ($tomatos < 0 || $ferros < 0) {
            throw new InvalidDataException('Trying to give a negative money');
        }

        if ($tomatos == 0 && $ferros == 0) {
            throw new InvalidDataException('Try to add no money');
        }
    }

    private function _logBuyMoney($userId, $ferros, $tomatos, $comment)
    {
        $sql = <<<SQL
INSERT DELAYED INTO
    users_buy_money_log
SET
    user_id=:user_id,
    ferros=:ferros,
    tomatos=:tomatos,
    `comment`=:comment_field,
    time=:time
SQL;

        $params = array(
            ':user_id' => $userId,
            ':ferros' => $ferros,
            ':tomatos' => $tomatos,
            ':comment_field' => $comment,
            ':time' => date('Y-m-d H:i:s'),
        );
        Db::instance()->exec($sql, $params);
    }

    /**
     * Заплатить
     * @param $userId
     * @param $ferros
     * @param $tomatos
     * @param string $comment
     * @throws InvalidDataException
     * @return array
     */
    public function pay($userId, $ferros, $tomatos, $comment = '')
    {
        $this->_castMoneyToInt($ferros, $tomatos);
        $this->_checkMoneyForTransaction($ferros, $tomatos);

        $this->disableCache();
        $fightData = $this->getUserFightData($userId);
        $this->enableCache();

        $newFerros = $fightData['ferros'] - $ferros;
        $newTomatos = $fightData['tomatos'] - $tomatos;

        if ($newFerros < 0 || $newTomatos < 0) {
            throw new InvalidDataException('You have not so much money');
        }

        $newValues = array(
            'ferros' => $newFerros,
            'tomatos' => $newTomatos,
        );
        $this->updateUserFightData($userId, $newValues);

        $this->_logPayMoney($userId, $ferros, $tomatos, $comment);
        return $newValues;
    }

    private function _logPayMoney($userId, $ferros, $tomatos, $comment)
    {
        $sql = <<<SQL
INSERT DELAYED INTO
    users_pay_log
SET
    user_id=:user_id,
    ferros=:ferros,
    tomatos=:tomatos,
    `comment`=:comment_field,
    time=:time
SQL;

        $params = array(
            ':user_id' => $userId,
            ':ferros' => $ferros,
            ':tomatos' => $tomatos,
            ':comment_field' => $comment,
            ':time' => date('Y-m-d H:i:s'),
        );

        Db::instance()->exec($sql, $params);

        $this->disableCache();
        $userStat = $this->getUserStat($userId);
        $this->enableCache();

        $newValues = array(
            'ferros_payed' => $userStat['ferros_payed'] + $ferros,
            'tomatos_payed' => $userStat['tomatos_payed'] + $tomatos,
        );
        $this->updateUserStat($userId, $newValues);
    }

    /**
     * Извлечь параметры уровня пользователя,
     * ключ user_level_params
     * @param int $userId
     * @throws \krest\exceptions\NotFoundException
     * @return array
     */
    public function getUserLevelParams($userId)
    {
        $cacheKey = 'user_level_params_' . $userId;
        $userLevelParams = $this->_getDataFromCache($cacheKey);
        if ($userLevelParams !== null) {
            return $userLevelParams;
        }

        $sql = <<<SQL
            SELECT
                strength,
                agility,
                intellect,
                max_hp,
                `level`,
                max_level,
                points_learning,
                items_slots_avaible,
                ring_slots_avaible,
                skill_resets_count
            FROM
                users_level_params
            WHERE
                user_id=:user_id
SQL;

        $userInfoData = Db::instance()->select($sql, array(':user_id' => $userId));
        $userLevelParams = Arr::get($userInfoData, 0);
        if ($userLevelParams === null) {
            throw new NotFoundException("User $userId not found");
        }

        $this->_setDataIntoCache($cacheKey, $userLevelParams);
        return $userLevelParams;
    }

    /**
     * Обновить параметры уровня пользователя
     * @param int $userId
     * @param array $newValues
     */
    public function updateUserLevelParams($userId, array $newValues)
    {
        $cacheKey = 'user_level_params_' . $userId;
        $this->_updateCachedData($cacheKey, $newValues);

        $table = 'users_level_params';
        $this->_updateUsersTable($table, $userId, $newValues);

        $this->_checkUserLevelParamsAchievements($userId, $newValues);
        $this->_checkNewSpecials($userId, $newValues);
    }

    private function _checkUserLevelParamsAchievements($userId, array $newValues)
    {
        $this->achievements = new Achievements();
        if (!empty($newValues['level'])) {
            $this->achievements->checkAchievementByFields($userId, array('level_reached' => $newValues['level']));
        }

        if (!empty($newValues['strength'])) {
            $this->achievements->checkAchievementByFields($userId, array('strength_level' => $newValues['strength']));
        }

        if (!empty($newValues['agility'])) {
            $this->achievements->checkAchievementByFields($userId, array('agility_level' => $newValues['agility']));
        }

        if (!empty($newValues['intellect'])) {
            $this->achievements->checkAchievementByFields($userId, array('intellect_level' => $newValues['intellect']));
        }
    }

    private function _checkNewSpecials($userId, array $newValues)
    {
        if (!empty($newValues['intellect'])) {
            $this->specials = new Specials();
            $this->specials->checkAndSetSpecials($userId, $newValues['intellect']);
        }
    }

    /**
     * Тренировать навыки
     * @param int $userId
     * @param int $strength
     * @param int $agility
     * @param $intellect
     * @throws \krest\exceptions\InvalidDataException
     * @param int $intellect
     * @return void
     */
    public function trainSkills($userId, $strength, $agility, $intellect)
    {
        $strength = (int)$strength;
        $agility = (int)$agility;
        $intellect = (int)$intellect;
        if ($strength < 0 or $agility < 0 or $intellect < 0) {
            throw new InvalidDataException('Trying to degrade');
        }

        $this->disableCache();
        $levelParams = $this->getUserLevelParams($userId);
        $this->enableCache();

        $pointsLearningNeed = $strength + $agility + $intellect;
        if ($pointsLearningNeed > $levelParams['points_learning']) {
            throw new InvalidDataException('Too little learning points');
        }

        $newStrength = $levelParams['strength'] + $strength;
        $newAgility = $levelParams['agility'] + $agility;
        $newIntellect = $levelParams['intellect'] + $intellect;
        $newPointsLearning = $levelParams['points_learning'] - $pointsLearningNeed;

        $this->_setNewSkills($userId, $newStrength, $newAgility, $newIntellect, $newPointsLearning);
    }

    private function _setNewSkills($userId, $newStrength, $newAgility, $newIntellect, $newPointsLearning)
    {
        $newLevel = $this->_calcLevelForStats($newStrength, $newAgility, $newIntellect);
        list($newMaxHp, $newMaxEnergy) = $this->_calcHpAndEnergyForLevel($newLevel);

        $newValues = array(
            'strength' => $newStrength,
            'agility' => $newAgility,
            'intellect' => $newIntellect,
            'points_learning' => $newPointsLearning,
            'level' => $newLevel,
            'max_hp' => $newMaxHp
        );
        $this->updateUserLevelParams($userId, $newValues);

        $newValues = array(
            'max_energy' => $newMaxEnergy,
            'hp' => $newMaxHp,
        );
        $this->updateUserFightData($userId, $newValues);

        return $newLevel;
    }

    private function _calcLevelForStats($strength, $agility, $intellect)
    {
        $pointsSpent = ($strength + $agility + $intellect) - 300;
        $targLevel = floor($pointsSpent / 10);

        return $targLevel;
    }

    private function _calcHpAndEnergyForLevel($level)
    {
        $hpOnLevel = $this->getHpOnLevel();
        $energyOnLevel = $this->getEnergyOnLevel();

        $hp = 1000 + $level * $hpOnLevel;
        $energy = 100 + $level * $energyOnLevel;
        return array($hp, $energy);
    }

    /**
     * Сбросить навыки пользователя.
     * Так же сбрасывает навыки!
     * @param $userId
     * @return array list($userLevelParams, $userFightData)
     */
    public function resetUserSkills($userId)
    {
        $this->specials = new Specials();

        $this->disableCache();
        $levelParams = $this->getUserLevelParams($userId);
        $this->enableCache();

        $fullPrice = $this->calculateResetSkillsFerrosPrice($levelParams);
        $this->pay($userId, $fullPrice, 0, 'Сброс очков');

        $strength = 100;
        $agility = 100;
        $intellect = 100;
        $pointsLearning = $levelParams['max_level'] * 10;

        $this->specials->reserUserSpecials($userId);
        $this->_setNewSkills($userId, $strength, $agility, $intellect, $pointsLearning);

        $newLevelParams = array(
            'skill_resets_count' => $levelParams['skill_resets_count'] + 1
        );
        $this->updateUserLevelParams($userId, $newLevelParams);

        $this->items = new Items();
        $this->items->resetUserInventory($userId);

        return array(
            $this->getUserLevelParams($userId),
            $this->getUserFightData($userId),
            $this->items->getUsersInventory($userId),
        );
    }

    /**
     * Проверить и, если надо, обновить уровень
     * @param int $userId
     * @throws \Exception
     * @return int Текущее значение уровня
     */
    public function levelUpIfNeed($userId)
    {
        $this->disableCache();
        $levelParams = $this->getUserLevelParams($userId);

        $fightData = $this->getUserFightData($userId);
        $this->enableCache();

        $targExp = $this->calcLevelExp($levelParams['max_level']);
        if ($fightData['experience'] >= $targExp['current']) {
            $maxLevel = $this->_incUserMaxLevel($userId, $levelParams, $fightData, $targExp);
        } else {
            $maxLevel = $levelParams['max_level'];
        }
        return $maxLevel;
    }

    private function _incUserMaxLevel($userId, array $levelParams, array $fightData, array $targExp)
    {
        $maxLevel = $this->_getTargMaxLevel($levelParams, $fightData, $targExp);
        $dLevel = $maxLevel - $levelParams['max_level'];

        $pointsLearningOnLevel = $this->getPointsLearningOnLevel();
        $pointsLearning = $levelParams['points_learning'] + $pointsLearningOnLevel * $dLevel;

        $energyRegenOnLevel = Arr::req(self::$config, 'energy_regen_on_level');
        $energy = $fightData['energy'] + $energyRegenOnLevel * $dLevel;

        $newValues = array(
            'max_level' => $maxLevel,
            'points_learning' => $pointsLearning,
        );
        $this->updateUserLevelParams($userId, $newValues);

        $newValues = array(
            'energy' => $energy,
        );
        $this->updateUserFightData($userId, $newValues);

        return $maxLevel;
    }

    private function _getTargMaxLevel(array $levelParams, array $fightData, array $targExp)
    {
        $targLevel = $levelParams['max_level'];
        while ($targExp['current'] <= $fightData['experience']) {
            ++$targLevel;
            $targExp = $this->calcLevelExp($targLevel);
        }

        return $targLevel;
    }

    /**
     * Извлечь настройки пользователя,
     * ключ user_settings
     * @param int $userId
     * @throws NotFoundException
     * @return array
     */
    public function getUserSettings($userId)
    {
        $cacheKey = 'user_settings_' . $userId;
        $userLevelParams = $this->_getDataFromCache($cacheKey);
        if ($userLevelParams !== null) {
            return $userLevelParams;
        }

        $sql = '
            SELECT
                blood_enabled,
                blood_color,
                cartoon_stroke,
                sounds_enabled,
                graphics_quality
            FROM
                users_settings
            WHERE
                user_id=:user_id
       ';

        $userInfoData = Db::instance()->select($sql, array(':user_id' => $userId));
        $userLevelParams = Arr::get($userInfoData, 0);
        if ($userLevelParams === null) {
            throw new NotFoundException("User $userId not found");
        }

        $this->_setDataIntoCache($cacheKey, $userLevelParams);
        return $userLevelParams;
    }

    /**
     * Обновить настройки пользователя
     * @param int $userId
     * @param array $newValues
     * @throws InvalidDataException
     */
    public function updateUserSettings($userId, array $newValues)
    {
        $this->_checkUserSettingsValues($newValues);

        $cacheKey = 'user_settings_' . $userId;
        $this->_updateCachedData($cacheKey, $newValues);

        $table = 'users_settings';
        $this->_updateUsersTable($table, $userId, $newValues);
    }

    private function _checkUserSettingsValues(array $newValues)
    {
        $bloodColor = Arr::get($newValues, 'blood_color');
        if ($bloodColor !== null
            && !in_array($bloodColor, array('0xff3333', '0x009933', '0x990066', '0x0066CC', '0xff9900'))
        ) {
            throw new InvalidDataException('Wrong value of blood color');
        }

        $graphicsQuality = Arr::get($newValues, 'graphics_quality');
        if ($graphicsQuality !== null && !in_array($graphicsQuality, array('low', 'medium', 'high'))) {
            throw new InvalidDataException('Wrong value of graphics quality');
        }
    }

    /**
     * Извлечь статистику пользователя,
     * ключ user_stat
     * @param int $userId
     * @throws NotFoundException
     * @return array
     */
    public function getUserStat($userId)
    {
        $cacheKey = 'user_stat_' . $userId;
        $userStat = $this->_getDataFromCache($cacheKey);
        if ($userStat !== null) {
            return $userStat;
        }

        $db = Db::instance();
        $sql = <<<SQL
            SELECT
                visits,
                last_visit,
                ferros_payed,
                tomatos_payed
            FROM
                users_stat
            WHERE
                user_id=:user_id
SQL;


        $userInfoData = $db->select($sql, [':user_id' => $userId]);
        $userStat = Arr::get($userInfoData, 0);
        if ($userStat === null) {
            throw new NotFoundException("User $userId not found");
        }

        $this->_setDataIntoCache($cacheKey, $userStat);
        return $userStat;
    }

    /**
     * Извлечь статистику пользователя с обновлением данных о визитах,
     * ключ user_stat
     * @param int $userId
     * @throws \krest\exceptions\NotFoundException
     * @return array
     */
    public function getUpdatedStat($userId)
    {
        $this->disableCache();
        $userStat = $this->getUserStat($userId);
        $this->enableCache();
        $newValues = $this->_updateVisits($userId, $userStat);
        $userStat = array_merge($userStat, $newValues);
        return $userStat;
    }

    private function _updateVisits($userId, array $userStat)
    {
        $visitsCount = $userStat['visits'];

        $lastVisitTime = strtotime($userStat['last_visit']);
        $todayMidnight = strtotime('today midnight');

        if ($lastVisitTime >= $todayMidnight) {
            $newValues = array(
                'visits' => $visitsCount,
                'last_visit' => $userStat['last_visit'],
            );
            return $newValues;
        }

        $nowTime = time();
        $nowMysqlDate = date('Y-m-d H:i:s');

        $dTime = $nowTime - $lastVisitTime;
        $twoDays = 172798;
        if ($dTime > $twoDays) {
            $visitsCount = 1;
        } else {
            ++$visitsCount;
        }

        $newValues = array(
            'visits' => $visitsCount,
            'last_visit' => $nowMysqlDate,
        );
        $this->updateUserStat($userId, $newValues);

        $this->giveEverydayBonus($userId, $visitsCount);

        return $newValues;
    }

    /**
     * Выдать пользователю ежедневный бонус
     * @param int $userId
     * @param int $visitsCount
     */
    public function giveEverydayBonus($userId, $visitsCount)
    {
        $dayScope = $visitsCount % 7;
        if ($dayScope == 0) {
            $dayScope = 7;
        }

        if ($dayScope != 3 && $dayScope != 7) {
            $this->_giveEverydayBonusTomatos($userId, $dayScope);
        } else {
            $this->_giveEverydayBonusItems($userId, $dayScope);
        }
    }

    private function _giveEverydayBonusTomatos($userId, $dayScope)
    {
        $config = $this->getDayBonusesConfig();
        $tomatos = $config['day_tomatos'] * $dayScope;
        $this->addMoney($userId, 0, $tomatos, 'Ежедневный бонус');

        $eventData = new user\EverydayBonusData();
        $eventData->day = $dayScope;
        $eventData->tomatos = $tomatos;

        EventDispatcher::instance()->dispatchEvent(\Events::EVENT_DATA, $eventData);
    }

    private function _giveEverydayBonusItems($userId, $dayScope)
    {
        $config = $this->getDayBonusesConfig();
        $dayConfig = Arr::req($config['item_days'], $dayScope);
        $arrangementVariant = $dayConfig['variant'];
        $count = $dayConfig['count'];

        $this->items = new Items();
        $itemsIdsList = $this->items->getItemsIdsByArrangement($arrangementVariant);
        $ind = array_rand($itemsIdsList);
        $itemId = $itemsIdsList[$ind];

        $this->items->incDecItemCount($userId, $itemId, $count);

        $eventData = new user\EverydayBonusData();
        $eventData->day = $dayScope;
        $eventData->item_id = $itemId;
        $eventData->count = $count;

        EventDispatcher::instance()->dispatchEvent(\Events::EVENT_DATA, $eventData);
    }

    /**
     * Обновить статистику пользователя
     * @param int $userId
     * @param array $newValues
     */
    public function updateUserStat($userId, array $newValues)
    {
        $cacheKey = 'user_stat_' . $userId;
        $this->_updateCachedData($cacheKey, $newValues);

        $table = 'users_stat';
        $this->_updateUsersTable($table, $userId, $newValues);

        $this->achievements = new Achievements();
        if (!empty($newValues['visits'])) {
            $this->achievements->checkAchievementByFields($userId, array('visits_level' => $newValues['visits']));
        }

        if (!empty($newValues['ferros_payed']) || !empty($newValues['tomatos_payed'])) {
            $params = [];
            if (!empty($newValues['ferros_payed'])) {
                $params['ferros_payed'] = $newValues['ferros_payed'];
            }
            if (!empty($newValues['tomatos_payed'])) {
                $params['tomatos_payed'] = $newValues['tomatos_payed'];
            }
            $this->achievements->checkAchievementByFields($userId, $params, Achievements::OP_OR);
        }
    }

    /**
     * Извлечь бонусы от предметов, навыков и т. д.,
     * ключ user_bonuses
     * @param $userId
     * @return user\Bonuses
     */
    public function getUserBonuses($userId)
    {
        $this->items = new Items();
        $this->specials = new Specials();
        $bonuses = new user\Bonuses();

        $itemsList = $this->items->getUsersInventoryGroupByPlace($userId);
        $specialsList = $this->specials->getUsersSpecials($userId);

        $allowedPlaces = array('hat', 'mask', 'cloak', 'shield', 'weapon', 'armor', 'ring', 'amulet', 'boots');
        foreach ($itemsList as $place => $inventoryPart) {
            if (in_array($place, $allowedPlaces)) {
                $this->_getBonusesFromInventoryPart($inventoryPart, $bonuses);
            }
        }

        foreach ($specialsList as $curRecord) {
            if ($curRecord['bonuses']) {
                $bonuses->importXml($curRecord['bonuses']);
            }
        }

        //Применение ГМО к бонусам
        $gains = new Gains();
        if ($gains->hasUserGain($userId, Gains::ARMOR)) {
            $newArmor = round($bonuses->fight->armor * 0.15);
            if ($newArmor < 100) {
                $newArmor = 100;
            }
            $bonuses->fight->armor += $newArmor;
        }

        if ($gains->hasUserGain($userId, Gains::DAMAGE)) {
            $newDamage = round($bonuses->fight->damage * 0.15);
            if ($newDamage < 100) {
                $newDamage = 100;
            }
            $bonuses->fight->damage += $newDamage;
        }

        return $bonuses;
    }

    private function _getBonusesFromInventoryPart(array $inventoryPart, user\Bonuses $bonuses)
    {
        foreach ($inventoryPart as $itemInfo) {
            if ($itemInfo['put'] && $itemInfo['bonuses']) {
                $baseDamage = $bonuses->fight->damage;
                $baseArmor = $bonuses->fight->armor;
                $bonuses->importXml($itemInfo['bonuses'], $itemInfo['put']);
                if ($itemInfo['sharpening_level']) {
                    $bonuses->fight->damage = $baseDamage + Arr::get($itemInfo, 'sharpen_damage', 0);
                    $bonuses->fight->armor = $baseArmor + Arr::get($itemInfo, 'sharpen_armor', 0);
                }
            }
        }
    }

    /**
     * Извлечь список пользователей,
     * которые не заходили какое-то время
     * @param int $lastVisitMysqlDate YY-MM-DD
     * @throws \krest\exceptions\InvalidDataException
     * @return array
     */
    public function getUserIdsByLastVisit($lastVisitMysqlDate)
    {
        $date = $this->_parseBorderDate($lastVisitMysqlDate);

        $params = array();
        $params[':startDate'] = $date . ' 00:00:00';
        $params[':stopDate'] = $date . ' 23:59:59';

        $sql = 'SELECT `user_id` FROM `users_stat` WHERE `last_visit` BETWEEN :startDate AND :stopDate';
        $idsData = Db::instance()->select($sql, $params);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    private function _parseBorderDate($rowDate)
    {
        $datePattern = '#^(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})#';
        $res = preg_match_all($datePattern, $rowDate, $matches);
        if (!$res) {
            throw new InvalidDataException('Wrong date format');
        }

        $date = "{$matches['year'][0]}-{$matches['month'][0]}-{$matches['day'][0]}";
        return $date;
    }

    /**
     * Извлечь список пользователей,
     * которые _не заходили давно_
     * @param int $borderDate YYYY-MM-DD
     * @throws \krest\exceptions\InvalidDataException
     * @return array
     */
    public function getUserIdsWhoHaveNotCome($borderDate)
    {
        $date = $this->_parseBorderDate($borderDate);

        $params = array();
        $params[':borderDate'] = $date . ' 23:59:59';

        $sql = 'SELECT `user_id` FROM `users_stat` WHERE `last_visit` <= :borderDate';
        $idsData = Db::instance()->select($sql, $params);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    /**
     * Извлечь список пользователей,
     * которые _заходили недавно_
     * @param int $borderDate YYYY-MM-DD
     * @throws \krest\exceptions\InvalidDataException
     * @return array
     */
    public function getUserIdsWhoHaveCome($borderDate)
    {
        $date = $this->_parseBorderDate($borderDate);

        $params = array();
        $params[':borderDate'] = $date . ' 00:00:00';

        $sql = 'SELECT `user_id` FROM `users_stat` WHERE `last_visit` >= :borderDate';
        $idsData = Db::instance()->select($sql, $params);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    /**
     * Извлечь список пользователей,
     * которые _заходили недавно_
     * @param int $borderDate YYYY-MM-DD
     * @throws \krest\exceptions\InvalidDataException
     * @return array
     */
    public function getSocNetIdsWhoHaveCome($borderDate)
    {
        $date = $this->_parseBorderDate($borderDate);

        $params = array();
        $params[':borderDate'] = $date . ' 00:00:00';

        $sql = <<<SQL
            SELECT
                u.soc_net_id
            FROM
                users_stat us
                INNER JOIN
                users u
                USING(user_id)
            WHERE
                us.last_visit >= :borderDate
SQL;

        $idsData = Db::instance()->select($sql, $params);
        $idsList = $this->_fetchSocNetIds($idsData);

        return $idsList;
    }

    private function _fetchSocNetIds($idsData)
    {
        $idsList = array();
        foreach ($idsData as $row) {
            $idsList[] = $row['soc_net_id'];
        }

        return $idsList;
    }

    /**
     * Извлечь максимальную славу,
     * ключ max_glory
     * @return int
     */
    public function getMaxGlory()
    {
        $cacheKey = 'max_glory';
        $maxGlory = $this->_getDataFromCache($cacheKey);
        if ($maxGlory !== null) {
            return $maxGlory;
        }

        $sql = 'SELECT MAX(`glory`) AS max_glory FROM `users_fight_data`';
        $data = Db::instance()->select($sql);
        $maxGlory = Arr::req(Arr::req($data, 0), 'max_glory');

        $this->_setDataIntoCache($cacheKey, $maxGlory, 600);
        return $maxGlory;
    }

    /**
     * Рассчитать опыт, необходимый
     * для перехода на следующий уровень,
     * ключ level_exp
     * @param int $level
     * @return array array(current=>$current, previous=>$previous, next=>$next)
     */
    public function calcLevelExp($level)
    {
        $levelExp = array();
        $levelExp['current'] = $this->_calcCurLevelExp($level);

        if ($level > 0) {
            $levelExp['previous'] = $this->_calcCurLevelExp($level - 1);
        } else {
            $levelExp['previous'] = 0;
        }


        $levelExp['next'] = $this->_calcCurLevelExp($level + 1);

        return $levelExp;
    }

    private function _calcCurLevelExp($level)
    {
        if ($level > 0 && $level < 120) {
            $targExp = 100;
            for ($i = 1; $i < $level; ++$i) {
                $targExp += (100 + 5 * $i);
            }
        } elseif ($level) {
            $targExp = 48000;
            for ($i = 120; $i < $level; ++$i) {
                $targExp += (100 + 10 * $i);
            }
        } else {
            $targExp = 1;
        }

        return $targExp;
    }

    /**
     * Проверить корректность и существование логина,
     * ключ login_allowable
     * @param string $login
     * @return array
     */
    public function checkLoginAllowable($login)
    {
        $result = array();
        $result['correct'] = preg_match(self::LOGIN_PATTERN, $login);
        if ($result['correct']) {
            $result['exists'] = $this->_checkLoginExists($login);
        } else {
            $result['exists'] = 0;
        }

        return $result;
    }

    private function _checkLoginExists($login)
    {
        $sql = 'SELECT COUNT(*) AS count FROM `users` WHERE `login`=:login';
        $res = Db::instance()->select($sql, array(':login' => $login));

        $count = Arr::req(Arr::req($res, 0), 'count');
        $exists = ($count) ? 1 : 0;
        return $exists;
    }

    /**
     * Проверить, соответствует ли
     * пользователь требованиям
     * @param int $userId
     * @param $requiredLevel
     * @throws \krest\exceptions\InvalidDataException
     * @return bool
     */
    public function checkUserRequirements($userId, $requiredLevel)
    {
        $levelParams = $this->getUserLevelParams($userId);
        if ($levelParams['level'] < $requiredLevel) {
            throw new InvalidDataException("Too little level");
        }
        return true;
    }

    /**
     * Извлечь набор user_id для глобального топа
     * @param int|string $interval
     * @return array
     */
    public function getGlobalGloryTopIds($interval = self::INTERVAL_GLOBAL)
    {
        $limit = $this->_getTopLimit();
        $field = $this->_getGloryTopField($interval);
        $params = array();

        $sql = "SELECT user_id FROM users_fight_data WHERE `$field` ORDER BY `$field` DESC LIMIT $limit";
        $idsData = Db::instance()->select($sql, $params);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    private function _getTopLimit()
    {
        $limit = (int)Arr::req(self::$config, 'top_limit');
        return $limit;
    }

    private function _getGloryTopField($interval)
    {
        switch ($interval) {
            case self::INTERVAL_GLOBAL:
                $field = 'glory';
                break;
            case self::INTERVAL_WEEK:
                $field = 'week_glory';
                break;
            case self::INTERVAL_DAY:
                $field = 'day_glory';
                break;
            default:
                throw new Exception('Incorrect interval');
        }

        return $field;
    }

    private function _fetchUserIds(array $userData)
    {
        $idsList = array();
        foreach ($userData as $curIdData) {
            $idsList[] = $curIdData['user_id'];
        }

        return $idsList;
    }

    /**
     * Извлечь набор user_id для глобального топа
     * @param $param
     * @throws \Exception
     * @return array
     */
    public function getGlobalLevelParamsTopIds($param)
    {
        if (!in_array(
            $param,
            array(
                 self::LEVEL_PARAM_STRENGTH,
                 self::LEVEL_PARAM_AGILITY,
                 self::LEVEL_PARAM_INTELLECT,
            )
        )
        ) {
            throw new Exception('Wrong level param');
        }

        $limit = $this->_getTopLimit();

        $sql = "SELECT user_id FROM users_level_params WHERE `$param` ORDER BY `$param` DESC LIMIT $limit";
        $idsData = Db::instance()->select($sql);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    /**
     * Извлечь набор user_id для глобального топа
     * @param $param
     * @throws \Exception
     * @return array
     */
    public function getGlobalFightDataTopIds($param)
    {
        if ($param != self::FIGHT_DATA_EXPERIENCE) {
            throw new Exception('Wrong fight data param');
        }

        $limit = $this->_getTopLimit();

        $sql = "SELECT user_id FROM users_fight_data ORDER BY `$param` DESC LIMIT $limit";
        $idsData = Db::instance()->select($sql);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    /**
     * Извлечь vk_id для топа по городу
     * @param int $cityCode
     * @param string $interval
     * @return array
     */
    public function getCityGloryTopIds($cityCode, $interval = self::INTERVAL_GLOBAL)
    {
        $limit = $this->_getTopLimit();
        $field = $this->_getGloryTopField($interval);
        $params = array(
            ':city' => $cityCode,
        );

        $sql = <<<SQL
SELECT
    user_id
FROM
    users_fight_data
WHERE
    user_id IN (SELECT user_id FROM users WHERE city=:city)
    AND `$field`
ORDER BY
    `$field` DESC
LIMIT $limit
SQL;

        $idsData = Db::instance()->select($sql, $params);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    /**
     * Извлечь user_id для топа по заданным пользователям
     * @param array $userIdsList
     * @param string $interval
     * @return array
     */
    public function getChosenGloryTopIds(array $userIdsList, $interval = self::INTERVAL_GLOBAL)
    {
        if (empty($userIdsList)) {
            return array();
        }

        $limit = $this->_getTopLimit();
        $field = $this->_getGloryTopField($interval);
        $params = array(
            ':userIdsList' => $userIdsList,
        );

        $sql = <<<SQL
SELECT
    user_id
FROM
    users_fight_data
WHERE
    user_id IN :userIdsList
    AND `$field`
ORDER BY
    `$field` DESC
LIMIT $limit
SQL;

        $idsData = Db::instance()->select($sql, $params);

        $idsList = $this->_fetchUserIds($idsData);
        return $idsList;
    }

    /**
     * Извлечь список user_id по логину или user_id
     * @param string $identity Рассматривается как user_id или логин
     * @param int $userId id пользователя, который запращивает (он убирается из множества)
     * @return array
     */
    public function getUsersByIdentity($identity, $userId)
    {
        $sql = 'SELECT `user_id` FROM `users` WHERE `soc_net_id`=:identity OR `login`=:identity';
        $data = Db::instance()->select($sql, array(':identity' => $identity));

        $idsList = $this->_fetchUserIds($data);
        $this->_excludeSameUserFromSet($userId, $idsList);
        return $idsList;
    }

    private function _excludeSameUserFromSet($userId, array &$idsList)
    {
        $pos = array_search($userId, $idsList);
        while ($pos !== false) {
            array_splice($idsList, $pos, 1);
            $pos = array_search($userId, $idsList);
        }
    }

    /**
     * Извлекает большой набор user_id по уровню
     * @param int $level
     * @param int $userId id пользователя, который запрашивает
     * @return array
     */
    public function getUsersByLevel($level, $userId)
    {
        $idsList = $this->_getUsersByLevelExactly($level);
        $this->_excludeSameUserFromSet($userId, $idsList);
        if (empty($idsList)) {
            $idsList = $this->_getUsersByLevelAppr($level);
            $this->_excludeSameUserFromSet($userId, $idsList);
        }

        return $idsList;
    }

    private function _getUsersByLevelExactly($level)
    {
        $cacheKey = 'users_by_level_exactly_' . $level;
        $idsList = $this->_getDataFromCache($cacheKey);
        if ($idsList !== null) {
            return $idsList;
        }

        $sql = 'SELECT `user_id` FROM `users_level_params` WHERE `level`=:level ORDER BY RAND() LIMIT 60';
        $idsData = Db::instance()->select($sql, array(':level' => $level));

        $idsList = $this->_fetchUserIds($idsData);
        $this->_setDataIntoCache($cacheKey, $idsList, 120);

        return $idsList;
    }

    private function _getUsersByLevelAppr($level)
    {
        $cacheKey = 'users_by_level_appr_' . $level;
        $idsList = $this->_getDataFromCache($cacheKey);
        if ($idsList !== null) {
            return $idsList;
        }

        $sql = <<<SQL
            (SELECT
                `user_id`
            FROM
                `users_level_params`
            WHERE
                `level`<=:level
            ORDER BY
                `level` DESC
            LIMIT 60
            )
            UNION ALL (
                SELECT
                    `user_id`
                FROM
                    `users_level_params`
                WHERE
                    `level`>:level
                ORDER BY
                    `level` ASC
                LIMIT 60
            )
            ORDER BY
                RAND()
            LIMIT 60
SQL;

        $idsData = Db::instance()->select($sql, array(':level' => $level));

        $idsList = $this->_fetchUserIds($idsData);
        $this->_setDataInToCache($cacheKey, $idsList, 120);

        return $idsList;
    }

    /**
     * Зарегистрировать нового пользователя,
     * ключ user_id
     * @param RegisterData $data
     */
    public function registerUser(RegisterData $data)
    {
        $data->checkRequiredFields();

        $this->_checkRegisterData($data);
        $userId = $this->_insertUser($data);

        $this->_setDefaultUserFightData($userId);
        $this->_setDefaultUserLevelParams($userId);
        $this->_setDefaultUserSettings($userId);
        $this->_setDefaultUserStat($userId);
        $this->_setDefaultGiftsReceived($userId);

        if (self::$config['friends_news']) {
            try {
                $this->_makeFriendsKnowAboutRegister($data);
            } catch (Exception $e) {
            }
        }
        return $userId;
    }

    private function _checkRegisterData(RegisterData $data)
    {
        $this->_checkSocNetIdBrutal($data->soc_net_id);
        $this->_checkLoginBrutal($data->login);

        if ($data->referrer === null || $data->referrer == $data->soc_net_id) {
            $data->referrer = "0";
        }

        if ($data->city === null) {
            $data->city = "0";
        }
    }

    private function _checkSocNetIdBrutal($socNetId)
    {
        $sql = 'SELECT COUNT(*) AS count FROM users WHERE soc_net_id=:soc_net_id';
        $data = Db::instance()->select($sql, array(':soc_net_id' => $socNetId));
        if ($data[0]['count']) {
            throw new AlreadyExistsException('Id already exists');
        }
    }

    private function _insertUser(RegisterData $data)
    {
        $vars = $data->asArray();
        $fields = array_keys($vars);
        $fields[] = 'reg_time';

        $registerSet = SqlQuery::buildSetSection($fields);
        $registerParams = SqlQuery::buildQueryParams($vars);
        $registerParams[':reg_time'] = date('Y-m-d H:i:s');

        $sql = "INSERT INTO `users` SET $registerSet";
        $result = Db::instance()->insert($sql, $registerParams);
        $userId = $result['insert_id'];

        return $userId;
    }

    private function _setDefaultUserFightData($userId)
    {
        $sql = '
            INSERT INTO
                `users_fight_data`
            SET
                `user_id`=:user_id,
                `hp`=1000,
                `glory`=1,
                `energy`=100,
                `max_energy`=100,
                `ferros`=5,
                `tomatos`=100
        ';

        Db::instance()->exec($sql, array(':user_id' => $userId));
    }

    private function _setDefaultUserLevelParams($userId)
    {
        $sql = '
            INSERT INTO
                `users_level_params`
            SET
                `user_id`=:user_id,
                `strength`=100,
                `agility`=100,
                `intellect`=100,
                `max_hp`=1000,
                `level`=0,
                `max_level`=0,
                `points_learning`=0,
                `items_slots_avaible`=4
        ';

        Db::instance()->exec($sql, array(':user_id' => $userId));
    }

    private function _setDefaultUserSettings($userId)
    {
        $sql = "
            INSERT INTO
                `users_settings`
            SET
                `user_id`=:user_id,
                `blood_enabled`=1,
                `blood_color`='0xff3333',
                `cartoon_stroke`=0,
                `sounds_enabled`=1,
                `graphics_quality`='high'
        ";

        Db::instance()->exec($sql, array(':user_id' => $userId));
    }

    private function _setDefaultUserStat($userId)
    {
        $sql = "
            INSERT INTO
                `users_stat`
            SET
                `user_id`=:user_id,
                `visits`=1
        ";

        Db::instance()->exec($sql, array(':user_id' => $userId));
    }

    private function _setDefaultGiftsReceived($userId)
    {
        $sql = 'INSERT INTO gifts_received_count SET user_id=:user_id';
        Db::instance()->exec($sql, array(':user_id' => $userId));
    }

    private function _makeFriendsKnowAboutRegister(RegisterData $data)
    {
        $friendsSocNetIdsList = $this->getFriendsInGameIdsList($data->soc_net_id);
        if ($friendsSocNetIdsList) {
            $news = new UserNews();
            $news->addNewFriendNews($friendsSocNetIdsList, $data->soc_net_id, $data->login);
        }
    }

    /**
     * Добавить слот для боевых предметов
     * @param $userId
     * @return array list($fightData, $levelParams)
     * @throws \krest\exceptions\InvalidDataException
     */
    public function addItemsSlot($userId)
    {
        $this->disableCache();
        $levelParams = $this->getUserLevelParams($userId);
        $this->enableCache();

        if ($levelParams['items_slots_avaible'] >= self::MAX_ITEMS_SLOTS_AVAIBLE) {
            throw new InvalidDataException('Max items slots exceeded');
        }

        $price = $this->getItemSlotPrice();
        $newMoney = $this->pay($userId, $price, 0, 'Дополнительный слот');

        $newLevelParams = array(
            'items_slots_avaible' => $levelParams['items_slots_avaible'] + 1
        );
        $this->updateUserLevelParams($userId, $newLevelParams);

        return array(
            $newMoney,
            $newLevelParams
        );
    }

    /**
     * Добавить слот для колец
     * @param $userId
     * @return array list($fightData, $levelParams)
     * @throws \krest\exceptions\InvalidDataException
     */
    public function addRingSlot($userId)
    {
        $this->disableCache();
        $levelParams = $this->getUserLevelParams($userId);
        $this->enableCache();

        if ($levelParams['ring_slots_avaible'] >= self::MAX_RINGS_SLOTS_AVAIBLE) {
            throw new InvalidDataException('Max ring slots exceeded');
        }

        $price = $this->getRingSlotPrice();
        $newMoney = $this->pay($userId, $price, 0, 'Дополнительный слот колец');

        $newLevelParams = array(
            'ring_slots_avaible' => $levelParams['ring_slots_avaible'] + 1
        );
        $this->updateUserLevelParams($userId, $newLevelParams);
        return array(
            $newMoney,
            $newLevelParams
        );
    }

    /**
     * Извлечь список друзей в игру
     * (на вход soc_net_id, на выход - user_id!)
     * @param string $socNetId
     * @return array
     */
    public function getFriendsInGameIdsList($socNetId)
    {
        $cacheKey = 'friends_in_game_ids_list_' . $socNetId;
        $idsList = $this->_getDataFromCache($cacheKey);
        if ($idsList !== null) {
            return $idsList;
        }

        $allFriendsList = SocNet::instance()->getFriendsList($socNetId);
        if (empty($allFriendsList)) {
            return array();
        }

        $sql = 'SELECT user_id FROM users WHERE soc_net_id IN :allFriendsList';
        $data = Db::instance()->select($sql, array(':allFriendsList' => $allFriendsList));

        $idsList = $this->_fetchUserIds($data);
        $this->_setDataIntoCache($cacheKey, $idsList, 86400);
        return $idsList;
    }

    /**
     * Максимальный id пользователя
     * @return int
     */
    public function getMaxUserId()
    {
        $cacheKey = 'max_user_id';
        $maxUserId = $this->_getDataFromCache($cacheKey);
        if ($maxUserId !== null) {
            return $maxUserId;
        }

        $sql = 'SELECT MAX(user_id) AS max_user_id FROM users';
        $data = Db::instance()->select($sql);

        $maxUserId = (int)Arr::get(Arr::get($data, 0), 'max_user_id');
        $this->_setDataIntoCache($cacheKey, $maxUserId);

        return $maxUserId;
    }

    /**
     * @param array $levelParams
     * @return float
     */
    public function calculateResetSkillsFerrosPrice(array $levelParams)
    {
        $basePrice = $this->getResetSkillsPrice();
        return round($basePrice * 487 / 4096 + $levelParams['skill_resets_count'] * 487 / 256);
    }


    /**
     * Извлечь перод восстановления 1 единицы энергии
     * в секундах,
     * ключ energy_period
     * @return int
     */
    public function getEnergyPeriod()
    {
        return Arr::req(self::$config, 'energy_period');
    }

    /**
     * Извлечь жизни, добавляемые при новом уровне,
     * ключ hp_on_level
     * @return int
     */
    public function getHpOnLevel()
    {
        return Arr::req(self::$config, 'hp_on_level');
    }

    /**
     * Извлечь энергию, добавляемую при уровне,
     * ключ energy_on_level
     * @return int
     */
    public function getEnergyOnLevel()
    {
        return Arr::req(self::$config, 'energy_on_level');
    }

    /**
     * Извлечь энергию, необходимую для боя
     * ключ energy_for_fight
     * @return int
     */
    public function getEnergyForFight()
    {
        return Arr::req(self::$config, 'energy_for_fight');
    }

    /**
     * Извлечь стоимость полного восстановления энергии
     * ключ energy_recover_ferros_price
     * @return int
     */
    public function getEnergyRecoverFerrosPrice()
    {
        return Arr::req(self::$config, 'energy_recover_ferros_price');
    }

    /**
     * Извлечь количество очков обучения на уровень,
     * ключ points_learning_on_level
     * @return int
     */
    public function getPointsLearningOnLevel()
    {
        return Arr::req(self::$config, 'points_learning_on_level');
    }

    /**
     * Извлечь стоимость смены персонажа,
     * ключ vegetable_price
     * @return array
     */
    public function getVegetablePrice()
    {
        return Arr::req(self::$config, 'vegetable_price');
    }

    /**
     * Извлечь стоимость смены персонажа,
     * ключ login_ferros_price
     * @return int
     */
    public function getLoginPrice()
    {
        return Arr::req(self::$config, 'login_price');
    }

    /**
     * Извлечь стоимость сброса очков,
     * ключ reset_skills_ferros_price
     * @return int
     */
    public function getResetSkillsPrice()
    {
        return Arr::req(self::$config, 'reset_skills_price');
    }

    /**
     * Извлечь стоимость ослабления врага,
     * ключ ease_enemy_ferros_price
     * @return int
     */
    public function getEaseEnemyPrice()
    {
        return Arr::req(self::$config, 'ease_enemy_price');
    }

    /**
     * Извлечь цену разблокировки дополнительного слота,
     * ключ item_slot_ferros_price
     * @return int
     */
    public function getItemSlotPrice()
    {
        return Arr::req(self::$config, 'item_slot_price');
    }

    /**
     * Извлечь цену разблокировки дополнительного слота для колец,
     * ключ ring_slot_ferros_price
     * @return int
     */
    public function getRingSlotPrice()
    {
        return Arr::req(self::$config, 'ring_slot_price');
    }

    /**
     * Извлечь конфиг ежедневных бонусов
     * @return array
     */
    public function getDayBonusesConfig()
    {
        return Arr::req(self::$config, 'day_bonuses');
    }

    /**
     * Извлечь стартовые подарки друзьям
     * @return array
     */
    public function getStartBonuses()
    {
        return Arr::req(self::$config, 'start_bonuses');
    }
}
