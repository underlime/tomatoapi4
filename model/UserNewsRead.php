<?php
namespace model;

use base\Model;
use krest\Db;
use krest\exceptions\InvalidDataException;

class UserNewsRead extends Model
{
    /**
     * Извлечь id прочитанных новостей
     * @param $userId
     * @return array
     */
    public function getNewsReadIds($userId)
    {
        $cacheKey = "news_read_ids_$userId";
        $idsList = $this->_getDataFromCache($cacheKey);
        if ($idsList !== null) {
            return $idsList;
        }

        $sql = 'SELECT news_id FROM user_news_read WHERE user_id=:userId';
        $data = Db::instance()->select($sql, array(':userId' => $userId));

        $idsList = array();
        foreach ($data as $record) {
            $idsList[] = $record['news_id'];
        }

        $this->_setDataIntoCache($cacheKey, $idsList);
        return $idsList;
    }

    /**
     * Извлечь количество непрочинанных новостей
     * @param int $userId
     * @return int
     */
    public function getUnreadNewsCount($userId)
    {
        $cacheKey = "unread_news_count_$userId";
        $count = $this->_getDataFromCache($cacheKey);
        if ($count !== null) {
            return $count;
        }

        $sql = <<<SQL
            SELECT
                COUNT(*) AS count
            FROM
                user_news un
            WHERE
                (user_id=:userId
                OR user_id IS NULL)
                AND NOT EXISTS(
                    SELECT news_id FROM user_news_read unr WHERE unr.news_id=un.id
                )
SQL;

        $data = Db::instance()->select($sql, array(':userId' => $userId));
        $count = $data[0]['count'];

        $this->_setDataIntoCache($cacheKey, $count);
        return $count;
    }

    /**
     * Проверить, прочитана ли новость
     * @param int $userId
     * @param int $newsId
     * @return bool
     */
    public function checkNewsRead($userId, $newsId)
    {
        $sql = 'SELECT COUNT(*) AS count FROM user_news_read WHERE user_id=:userId AND news_id=:newsId';
        $data = Db::instance()->select($sql, array(
                                                     ':userId' => $userId,
                                                     ':newsId' => $newsId,
                                                ));
        return (bool)$data[0]['count'];
    }

    /**
     * Отметить новость как прочитанную
     * @param $userId
     * @param $newsId
     * @param $isShared
     * @throws \krest\exceptions\InvalidDataException
     */
    public function setNewsRead($userId, $newsId, $isShared)
    {
        $news = new UserNews();

        $newsData = $news->getNewsRecord($newsId);
        if ($newsData['user_id'] != $userId && $newsData['user_id'] !== null) {
            throw new InvalidDataException('Wrong news user');
        }

        if ($this->checkNewsRead($userId, $newsId)) {
            throw new InvalidDataException('News is already read');
        }

        if ($newsData['like_reward_type'] !== null) {
            $this->_rewardForNewsLike($userId, $newsData);
        }

        $sql = 'INSERT INTO user_news_read SET user_id=:userId, news_id=:newsId, shared=:isShared';
        Db::instance()->exec($sql, array(
                                           ':userId' => $userId,
                                           ':newsId' => $newsId,
                                           ':isShared' => $isShared,
                                      ));

        $this->_deleteDataFromCache("unread_news_count_$userId");
        $this->_deleteDataFromCache("news_read_ids_$userId");
    }

    private function _rewardForNewsLike($userId, array $newsData)
    {
        switch ($newsData['like_reward_type']) {
            case 'tomatos':
                $currency = \Currency::TOMATOS;
                break;
            case 'ferros':
                $currency = \Currency::FERROS;
                break;
            default:
                throw new \Exception('Wrong news reward type');
        }

        $user = new User();
        list($ferros, $tomatos) = \Currency::getPricesByOne($newsData['like_reward_count'], $currency);
        $user->addMoney($userId, $ferros, $tomatos, 'Новости');
    }
}
