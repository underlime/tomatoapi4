<?php
namespace model;

use krest\Db;
use lib\RediskaFactory;
use model\spy\Spy;
use model\spy\SpyAction;

/**
 * Class SpyData
 * @package model
 * Реализует наблюдение за ответами системы определенным пользователям
 */
class SpyData
{
    /**
     * @var \Rediska
     */
    private $_redis;

    function __construct()
    {
        $this->_redis = RediskaFactory::getInstance();
    }

    /**
     * Установить список ID из строки,
     * где ID разделены символами [\s\t\r\n,;]
     * @param $idsString
     */
    public function setIdsFromString($idsString)
    {
        $idsList = preg_split('#[\s\t\r\n,;]+#', $idsString);
        if (empty($idsList)) {
            $idsList = [];
        }
        $this->setIdsFromList($idsList);
    }

    /**
     * Установить список ID из массива
     * @param array $idsList
     */
    public function setIdsFromList(array $idsList)
    {
        $pipeline = $this->_redis->pipeline();
        $pipeline->delete('spy:ids');
        foreach ($idsList as $id) {
            $id = trim($id);
            if ($id) {
                $pipeline->addToSet('spy:ids', $id);
            }
        }
        $pipeline->execute();
    }

    /**
     * Извлечь сохаренные ID пользователей
     * @return array
     */
    public function getSavedIds()
    {
        return $this->_redis->getSet('spy:ids');
    }

    /**
     * Является ли пользователь целью шпионажа?
     * @param $socNetId
     * @return bool
     */
    public function isUserTarget($socNetId)
    {
        return $this->_redis->existsInSet('spy:ids', $socNetId);
    }

    /**
     * Сохранить в базу действия пользователя
     * @param $userId
     * @param $actions
     */
    public function saveSpyingData($userId, $actions)
    {
        $time = date('Y-m-d H:i:s');
        $db = Db::instance();
        $values = array_map(function (SpyAction $action) use ($db, $userId, $time) {
            $tuple = [
                $db->quote($userId),
                $db->quote($action->file),
                $db->quote($action->method),
                intval($action->line),
                $db->quote($action->description),
                $db->quote(json_encode($action->data)),
                $db->quote($time),
            ];
            return '(' . implode(',', $tuple) . ')';
        }, $actions);

        if ($values) {
            $sql = <<<QUERY
              INSERT DELAYED INTO
                spy_data (user_id, `file`, method, line, description, data, `time`)
              VALUES
QUERY;
            $sql .= "\n" . implode(',', $values);
            $db->exec($sql);
        }
    }
}
