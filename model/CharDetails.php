<?php
namespace model;

use base\Model;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;

class CharDetails extends Model
{
    /**
     * Извлечь детали персонажа,
     * ключ char_details
     * @param string|null $vegetable
     * @throws \krest\exceptions\InvalidDataException
     * @return array
     */
    public function getDetails($vegetable = null)
    {
        $this->_checkVegetable($vegetable);

        $cacheKey = 'char_details_'.$vegetable;
        $charDetails = $this->_getDataFromCache($cacheKey);
        if ($charDetails !== null) {
            return $charDetails;
        }

        if ($vegetable && $vegetable !== 'all') {
            $qParam = 'WHERE vegetable IN (:vegetable, \'all\')';
            $params = array(':vegetable' => $vegetable);
        }
        else {
            $qParam = '';
            $params = array();
        }

        $sql = <<<SQL
          SELECT
            char_detail_id,
            mc_1_name,
            mc_2_name,
            vegetable,
            name_ru,
            `group`,
            `is_default`,
            price_ferros,
            price_tomatos
          FROM
            char_details
          {$qParam}
SQL;

        $rawDetails = Db::instance()->select($sql, $params);

        $charDetails = array();
        foreach ($rawDetails as $curDetail) {
            $charDetails[$curDetail['char_detail_id']] = $curDetail;
        }

        $this->_setDataIntoCache($cacheKey, $charDetails);
        return $charDetails;
    }

    private function _checkVegetable($vegetable)
    {
        static $allowedTypes = array(null, 'all', 'beta', 'carrot', 'cucumber', 'onion', 'pepper', 'tomato');
        if (!in_array($vegetable, $allowedTypes)) {
            throw new InvalidDataException('Wrong vegetable type');
        }
    }

    /**
     * Извлечь детали персонажа, сгруппированные по мувиклипу
     * @param string|null $vegetable
     * @throws \krest\exceptions\InvalidDataException
     * @return array
     */
    public function getDetailsGroupByMc($vegetable = null)
    {
        $rawDetails = $this->getDetails($vegetable);

        $charDetails = array();
        foreach ($rawDetails as $curDetail) {
            $charDetails[$curDetail['mc_1_name']] = $curDetail;
        }

        return $charDetails;
    }

    /**
     * Извлечь детали по умолчанию для овоща
     * @param string $vegetable
     * @throws \krest\exceptions\NotFoundException
     * @return \model\chardetails\DetailsList
     */
    public function getDefaultPartsSet($vegetable)
    {
        $this->_checkVegetable($vegetable);

        $cacheKey = 'char_default_parts_'.$vegetable;
        $sortedParts = $this->_getDataFromCache($cacheKey);
        if ($sortedParts !== null) {
            return $sortedParts;
        }

        $sql = <<<SQL
          SELECT
            mc_1_name,
            mc_2_name,
            `group`
          FROM
            char_details
          WHERE
            vegetable IN (:vegetable, 'all')
            AND `is_default`=1
SQL;
        $params = array(':vegetable' => $vegetable);
        $defaultParts = Db::instance()->select($sql, $params);
        if (sizeof($defaultParts) < 4) {
            throw new NotFoundException('Parts not found');
        }

        $sortedParts = new chardetails\DetailsList();
        foreach ($defaultParts as $row) {
            $this->appendDetailRowToList($row, $sortedParts);
        }

        $this->_setDataIntoCache($cacheKey, $sortedParts);
        return $sortedParts;
    }

    /**
     * Добавить строку детали в список деталей
     * @param array $detailRow
     * @param \model\chardetails\DetailsList $detailsList
     * @throws \krest\exceptions\InvalidDataException
     * @return void
     */
    public function appendDetailRowToList(array $detailRow, chardetails\DetailsList $detailsList)
    {
        switch ($detailRow['group']) {
            case 'bodies':
                $detailsList->body = $detailRow['mc_1_name'];
                $detailsList->hands = $detailRow['mc_2_name'];
                break;
            case 'hairs':
                $detailsList->hair = $detailRow['mc_1_name'];
                break;
            case 'mouths':
                $detailsList->mouth = $detailRow['mc_1_name'];
                break;
            case 'eyes':
                $detailsList->eyes = $detailRow['mc_1_name'];
                break;
            default:
                throw new InvalidDataException("Wrong group {$detailRow['group']}");
        }
    }
}
