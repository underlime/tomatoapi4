<?php
namespace model;

use base\Model;
use krest\Db;
use krest\exceptions\NotFoundException;
use krest\SocNet;

class UserNews extends Model
{
    const TYPE_COMMON = 0;

    const TYPE_START_BONUS = 1;

    const TYPE_FRIEND = 2;

    /**
     * Извлечь новости пользователя
     * @param $userId
     * @return array
     */
    public function getNews($userId)
    {
        $cacheKey = 'user_news'.$userId;
        $data = $this->_getDataFromCache($cacheKey);
        if ($data !== null) {
            return $data;
        }

        $sql = <<<SQL
            SELECT
                id,
                time,
                news_type,
                additional_data,
                like_reward_type,
                like_reward_count,
                like_reward_data
            FROM
                user_news
            WHERE
                user_id=:user_id OR user_id IS NULL
            ORDER BY
                time DESC
SQL;

        $data = Db::instance()->select($sql, array(':user_id' => $userId));

        $this->_setDataIntoCache($cacheKey, $data);
        return $data;
    }

    /**
     * Извлечь запись новости по id
     * @param int $newsId
     * @throws \krest\exceptions\NotFoundException
     * @return array
     */
    public function getNewsRecord($newsId)
    {
        $cacheKey = "news_$newsId";
        $data = $this->_getDataFromCache($cacheKey);
        if ($data !== null) {
            return $data;
        }

        $sql = <<<SQL
            SELECT
                user_id,
                time,
                news_type,
                additional_data,
                like_reward_type,
                like_reward_count,
                like_reward_data
            FROM
                user_news
            WHERE
                id=:newsId
SQL;

        $rows = Db::instance()->select($sql, array(':newsId' => $newsId));
        if (!$rows) {
            throw new NotFoundException('News not fount');
        }

        $data = $rows[0];
        $this->_setDataIntoCache($cacheKey, $data, 600);
        return $data;
    }

    /**
     * Добавить новость-сообщение
     * @param $userId
     * @param $header
     * @param $message
     */
    public function addMessageNews($userId, $header, $message)
    {
        $data = [
            'h_ru' => $header,
            'h_en' => $header,
            'ru' => $message,
            'en' => $message,
        ];

        $timeZone = new \DateTimeZone('UTC');
        $time = new \DateTime('now', $timeZone);
        $expiresTime = clone $time;
        $expiresTime->add(new \DateInterval('P1M'));

        $timeFormat = 'Y-m-d H:i:s';
        $sql = <<<SQL
            INSERT INTO
                user_news
            SET
                user_id=:userId,
                `time`=:nowTime,
                expires_time=:expiresTime,
                news_type=:newsType,
                additional_data=:jsonData,
                like_reward_type=NULL,
                like_reward_count=0
SQL;
        Db::instance()->exec($sql, [
                                      ':userId' => $userId,
                                      ':nowTime' => $time->format($timeFormat),
                                      ':expiresTime' => $expiresTime->format($timeFormat),
                                      ':newsType' => self::TYPE_COMMON,
                                      ':jsonData' => json_encode($data, JSON_UNESCAPED_UNICODE),
                                      ]);
    }

    /**
     * Добавить новость общего назначения
     * @param $headerRu
     * @param $textRu
     * @param $headerEn
     * @param $textEn
     * @param \DateTime $time
     * @param \DateTime $expiresTime
     * @param int|array|null $userIds
     */
    public function addCommonNews(
        $headerRu, $textRu, $headerEn, $textEn, $time = null, $expiresTime = null, $userIds = null
    ) {
        $data = array(
            'h_ru' => $headerRu,
            'h_en' => $headerEn,
            'ru' => $textRu,
            'en' => $textEn,
        );
        $this->insertNewsRecords($userIds, $time, $expiresTime, self::TYPE_COMMON, $data);
    }

    /**
     * Добавить новость о стартовом бонусе
     * @param int $whoGiveId
     * @param int|array $toWhomGiveIds
     * @param int $bonusType
     * @param int $itemId
     * @param int $count
     * @throws \Exception
     */
    public function addStartBonusNews($whoGiveId, $toWhomGiveIds, $bonusType, $itemId, $count)
    {
        $user = new User();
        $userInfo = $user->getUserInfoByUserId($whoGiveId);

        $data = array(
            'soc_net_id' => $userInfo['soc_net_id'],
            'login' => $userInfo['login'],
            'bonus_type' => $bonusType,
            'item_id' => $itemId,
            'count' => $count,
        );
        $this->insertNewsRecords($toWhomGiveIds, null, null, self::TYPE_START_BONUS, $data);
    }

    /**
     * Добавить запись о новом друге
     * @param int|array $userIds
     * @param $friendSocNetId
     * @param $friendLogin
     * @throws \Exception
     */
    public function addNewFriendNews($userIds, $friendSocNetId, $friendLogin)
    {
        if ($userIds === null) {
            throw new \Exception('userIds is NULL');
        }

        $data = array(
            'soc_net_id' => $friendSocNetId,
            'login' => $friendLogin,
        );
        $this->insertNewsRecords($userIds, null, null, self::TYPE_FRIEND, $data);
    }

    /**
     * @param int|array|null $userIds
     * @param \DateTime $time
     * @param \DateTime $expiresTime
     * @param $newsType
     * @param $additionalData
     * @throws \Exception
     */
    public function insertNewsRecords($userIds, $time, $expiresTime, $newsType, $additionalData)
    {
        if ($time == null or $expiresTime == null) {
            list($time, $expiresTime) = $this->_getNewsTimes($time, $expiresTime);
        }

        $jsonData = json_encode($additionalData, JSON_UNESCAPED_UNICODE);
        $format = 'Y-m-d H:i:s';
        $strTime = $time->format($format);
        $strExpires = $expiresTime->format($format);

        $sql = 'INSERT INTO user_news (user_id, time, expires_time, news_type, additional_data) VALUES ';
        if (is_array($userIds) && $userIds) {
            $this->_insertMultipleRecords($sql, $userIds, $newsType, $strTime, $strExpires, $jsonData);
        }
        elseif (is_numeric($userIds) or $userIds === null) {
            $this->_insertSingleRecord($sql, $userIds, $newsType, $strTime, $strExpires, $jsonData);
        }
        else {
            throw new \Exception('Wrong userIds type');
        }
    }

    private function _getNewsTimes($time, $expiresTime)
    {
        $timeZone = new \DateTimeZone('UTC');
        if ($time == null) {
            $time = new \DateTime('now', $timeZone);
        }

        if ($expiresTime == null) {
            $interval = new \DateInterval('P1M');
            $expiresTime = new \DateTime('now', $timeZone);
            $expiresTime->add($interval);
        }

        return array($time, $expiresTime);
    }

    private function _createNewsTuple($userId, $strTime, $strExpires, $newsType, $jsonData)
    {
        $db = Db::instance();

        $values = array();
        if ($userId === null) {
            $values[] = 'NULL';
        }
        else {
            $values[] = $db->quote($userId, \PDO::PARAM_INT);
        }
        $values[] = $db->quote($strTime);
        $values[] = $db->quote($strExpires);
        $values[] = $db->quote($newsType, \PDO::PARAM_INT);
        $values[] = $db->quote($jsonData);

        return "(".implode(',', $values).")";
    }

    private function _insertMultipleRecords($sql, $userIds, $newsType, $strTime, $strExpires, $jsonData)
    {
        $tuples = array();
        foreach ($userIds as $id) {
            $tuples[] = $this->_createNewsTuple($id, $strTime, $strExpires, $newsType, $jsonData);
        }

        $sql .= implode(',', $tuples);
        Db::instance()->exec($sql);

        foreach ($userIds as $id) {
            $this->_deleteDataFromCache('user_news'.$id);
        }
    }

    private function _insertSingleRecord($sql, $userIds, $newsType, $strTime, $strExpires, $jsonData)
    {
        $sql .= $this->_createNewsTuple($userIds, $strTime, $strExpires, $newsType, $jsonData);
        Db::instance()->exec($sql);
        if ($userIds) {
            $this->_deleteDataFromCache('user_news'.$userIds);
        }
    }

    /**
     * Удалить устаревшие новости
     * @return int Количество удаленных записей
     */
    public function deleteExpired()
    {
        $date = date("Y-m-d 00:00:00");
        $res = Db::instance()->delete("DELETE FROM user_news WHERE expires_time < :borderDate",
                                         [":borderDate" => $date]);
        return $res["affected_rows"];
    }
}
