<?php
namespace model;

use base\Model;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;
use model\user\Bonuses;

class Sharpening extends Model
{
    /**
     * Извлечь уровень заточки предмета
     * @param int $userId
     * @param int $itemId
     * @return int
     */
    public function getSharpeningLevel($userId, $itemId)
    {
        $sql = 'SELECT sharpening_level FROM sharpening WHERE user_id=:userId AND item_id=:itemId';
        $data = Db::instance()->select($sql, array(
                                                     ':userId' => $userId,
                                                     ':itemId' => $itemId,
                                                ));
        $levelData = Arr::get($data, 0, array());
        $level = Arr::get($levelData, 'sharpening_level', 0);
        return $level;
    }

    /**
     * Извлечь информацию о предмете для заточки
     * @param int $itemId
     * @return array
     * @throws \krest\exceptions\InvalidDataException
     */
    public function getItemInfo($itemId)
    {
        $items = new Items();
        $itemInfo = $items->getItemInfo($itemId);
        if ($itemInfo['rubric_code'] != 'weapons' && $itemInfo['rubric_code'] != 'armor') {
            throw new InvalidDataException('Item is not a weapon and not an armor');
        }
        return $itemInfo;
    }

    /**
     * Рассчитать бонусы от заточки до уровня LEVEL,
     * а так же стоимость заточки
     * @param int $itemId
     * @param int $level
     * @throws \krest\exceptions\InvalidDataException
     * @return array($bonuses, $priceFerros, $priceTomatos, $successProbability)
     */
    public function calculateSharpeningParams($itemId, $level)
    {
        if ($level < 0) {
            throw new InvalidDataException('Level is negative');
        }

        $itemInfo = $this->getItemInfo($itemId);
        $bonuses = new Bonuses();
        $bonuses->importXml($itemInfo['bonuses']);

        $damage = $bonuses->fight->damage;
        $armor = $bonuses->fight->armor;
        $priceFerros = $itemInfo['price_ferros'];
        $priceTomatos = $itemInfo['price_tomatos'];
        $successProbability = 100;

        for ($i = 0; $i < $level; ++$i) {
            $damage *= 1.0625;
            $armor *= 1.0625;
            $priceFerros = ($priceFerros*1.210526 + 2)/1.2;
            $priceTomatos *= 1.015625;
            $successProbability = $successProbability*0.875;
        }

        if ($itemInfo['rubric_code'] == 'weapons') {
            $bonuses->fight->damage = round($damage) + 10;
            $bonuses->fight->armor = 0;
        }
        else {
            $bonuses->fight->damage = 0;
            $bonuses->fight->armor = round($armor) + 10;
        }
        $priceFerros = round($priceFerros);
        $priceTomatos = round($priceTomatos);
        $successProbability = round($successProbability);

        return array(
            $bonuses, $priceFerros,
            $priceTomatos, $successProbability
        );
    }

    /**
     * Заточить оружие
     * @param $userId
     * @param $itemId
     * @param $currency
     * @return array($newMoney, $success, $level)
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     */
    public function sharpenWeapon($userId, $itemId, $currency)
    {
        $currentLevel = $this->getSharpeningLevel($userId, $itemId);
        $nextLevel = $currentLevel + 1;
        if ($nextLevel > 17) {
            throw new InvalidDataException('Level <= 17');
        }

        try {
            list(, $priceFerros, $priceTomatos, $successProbability) = $this->calculateSharpeningParams($itemId,
                                                                                                        $nextLevel);
        }
        catch (\Exception $e) {
            throw $e;
        }

        list($ferros, $tomatos) = \Currency::getFinalPriceByItemParams($priceFerros, $priceTomatos, $currency);
        $user = new User();
        $newMoney = $user->pay($userId, $ferros, $tomatos, "Заточка {$itemId} до {$nextLevel}");

        $success = false;
        $level = $currentLevel;
        if ($currency === \Currency::FERROS || mt_rand(0, 100) < $successProbability) {
            $sql = <<<SQL
INSERT INTO
    sharpening
SET
    user_id=:userId, item_id=:itemId, sharpening_level=:level
ON DUPLICATE KEY UPDATE
    sharpening_level=:level
SQL;
            Db::instance()->insert($sql, array(
                                                 ':userId' => $userId,
                                                 ':itemId' => $itemId,
                                                 ':level' => $nextLevel,
                                            ));
            $success = true;
            $level = $nextLevel;
        }

        $this->_deleteDataFromCache('user_inventory_data_'.$userId);
        return array($newMoney, $success, $level);
    }
}
