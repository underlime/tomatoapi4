<?php
namespace model\specials;

class EventData extends \base\EventData
{
    /**
     * Название спецприема
     * @var string
     */
    public $name_ru;

    /**
     * Символьный код спецприема
     * @var string
     */
    public $code;

    /**
     * Картинка спецприема
     * @var string
     */
    public $picture;

    /**
     * Активный?
     * @var int
     */
    public $active;

    /**
     * Бонусы
     * @var array
     */
    public $bonuses;

    public function __construct()
    {
        $this->type = 'new_special';
    }
}
