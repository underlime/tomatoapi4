<?php
namespace model;

use base\Model;
use krest\api\Config;
use krest\Arr;
use krest\Db;
use krest\exceptions\AlreadyExistsException;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\SocNet;

/**
 * Class Clans
 * Модель для работы с кланами
 * Сущности: clans, clan_members
 * @package model
 * @property int $creationPrice
 * @property int $renamePrice
 */
class Clans extends Model
{
    const NAME_PATTERN = '/^[\w\s\.-]{3,32}$/u';
    const LEVELS_COUNT = 39;

    private $_config;

    public function __construct()
    {
        parent::__construct();
        $this->_config = Config::instance()->getForClass(__CLASS__);
    }

    /**
     * Цена создания клана
     * @return int
     */
    public function getCreationPrice()
    {
        return Arr::req($this->_config, 'creation_price');
    }

    /**
     * Цена переименования клана
     * @return int
     */
    public function getRenamePrice()
    {
        return Arr::req($this->_config, 'rename_price');
    }

    /**
     * Максимальное количество членов клана
     * @return int
     */
    public function getMaxMembersCount()
    {
        return Arr::req($this->_config, 'max_members_count');
    }

    /**
     * Очистить имя от лишних символов
     * @param string $name
     * @return string
     */
    public function cleanName($name)
    {
        $search = array(
            '#(?:^[\s_-]+)|(?:[\s_-]+$)#',
            '#([\s_-])\1+#'
        );
        $replace = array('', '$1');
        $name = preg_replace($search, $replace, $name);
        return $name;
    }

    /**
     * Проверить корректность названия клана
     * @param string $clanName
     * @return array(correct => bool, exists => bool)
     */
    public function validateName($clanName)
    {
        $clanName = $this->cleanName($clanName);
        $result['correct'] = (bool)preg_match(self::NAME_PATTERN, $clanName);
        if ($result['correct']) {
            $result['exists'] = $this->_checkNameExists($clanName);
        }
        else {
            $result['exists'] = false;
        }
        return $result;
    }

    private function _checkNameExists($name)
    {
        $res = Db::instance()->select('SELECT COUNT(*) AS `count` FROM clans WHERE `name`=:clanName', array(
                                                                                                           ':clanName' => $name
                                                                                                      ));

        $count = Arr::req(Arr::req($res, 0), 'count');
        $exists = (bool)$count;
        return $exists;
    }

    /**
     * Создать клан
     * @param int $leaderId
     * @param string $clanName
     * @throws \krest\exceptions\InvalidDataException
     * @throws \krest\exceptions\AlreadyExistsException
     * @throws \Exception
     * @return int Id нового клана
     */
    public function create($leaderId, $clanName)
    {
        $db = Db::instance();

        $validation = $this->validateName($clanName);
        if (!$validation['correct']) {
            throw new InvalidDataException('Invalid clan name');
        }
        if ($validation['exists']) {
            throw new AlreadyExistsException("Clan with the name '$clanName' is already exists");
        }

        $data = $db->select('SELECT COUNT(*) AS `count` FROM clans WHERE leader_id=:leaderId', array(
                                                                                                    ':leaderId' => $leaderId,
                                                                                               ));
        if (!empty($data[0]['count'])) {
            throw new AlreadyExistsException("User $leaderId has already created a clan");
        }

        $price = $this->getCreationPrice();
        $user = new User();
        try {
            $user->pay($leaderId, $price, 0, 'Создание клана');
        }
        catch (\Exception $e) {
            throw $e;
        }

        $result = $db->insert('INSERT INTO clans SET `name`=:clanName, leader_id=:leaderId', array(
                                                                                                  ':clanName' => $clanName,
                                                                                                  ':leaderId' => $leaderId,
                                                                                             ));
        $clanId = intval($result['insert_id']);
        $this->addClanMember($clanId, $leaderId);

        return $clanId;
    }

    /**
     * Изменить название клана
     * @param int $leaderId
     * @param int $clanId
     * @param string $newName
     * @return array Новые деньги
     * @throws \krest\exceptions\InvalidDataException
     * @throws \krest\exceptions\AlreadyExistsException
     * @throws \Exception
     */
    public function rename($leaderId, $clanId, $newName)
    {
        $db = Db::instance();

        $validation = $this->validateName($newName);
        if (!$validation['correct']) {
            throw new InvalidDataException('Invalid clan name');
        }

        if ($validation['exists']) {
            throw new AlreadyExistsException("Clan with the name '$newName' is already exists");
        }

        $data = $db->select('SELECT COUNT(*) AS `count` FROM clans WHERE id=:clanId AND leader_id=:userId', array(
                                                                                                                 ':clanId' => $clanId,
                                                                                                                 ':userId' => $leaderId,
                                                                                                            ));
        if (empty($data[0]['count'])) {
            throw new InvalidDataException("User $leaderId is not a clan leader");
        }

        $price = $this->getRenamePrice();
        $user = new User();
        $newMoney = $user->pay($leaderId, $price, 0, 'Создание клана');

        $db->exec('UPDATE clans SET `name`=:newName WHERE id=:clanId', array(
                                                                            ':clanId' => $clanId,
                                                                            ':newName' => $newName,
                                                                       ));

        $this->_deleteDataFromCache("clan_info_$clanId");
        $this->_deleteDataFromCache("clans_top");
        return $newMoney;
    }

    /**
     * Удалить клан
     * @param $userId
     * @param $clanId
     * @throws \krest\exceptions\InvalidDataException
     */
    public function delete($userId, $clanId)
    {
        $db = Db::instance();
        $data = $db->select('SELECT COUNT(*) AS `count` FROM clans WHERE id=:clanId AND leader_id=:userId', array(
                                                                                                                 ':clanId' => $clanId,
                                                                                                                 ':userId' => $userId,
                                                                                                            ));
        if (empty($data[0]['count'])) {
            throw new InvalidDataException("User $userId is not a clan leader");
        }

        $result = $db->delete('DELETE FROM clans WHERE id=:clanId', array(':clanId' => $clanId));
        if ($result['affected_rows'] == 0) {
            throw new InvalidDataException('Something goes wrong');
        }

        $this->_deleteDataFromCache("clan_info_$clanId");
        $this->_deleteDataFromCache("clan_members_$clanId");
        $this->_deleteDataFromCache("clan_members_count_$clanId");
        $this->_deleteDataFromCache("clan_id_by_leader_id_$userId");
        $this->_deleteDataFromCache("clans_top");
    }

    /**
     * Извлечь информацию о клане
     * @param $clanId
     * @return mixed
     * @throws \krest\exceptions\NotFoundException
     */
    public function getInfo($clanId)
    {
        $key = "clan_info_$clanId";
        $getter = function () use ($clanId) {
            $sql = <<<SQL
                SELECT
                    id, `name`, `name` AS clan_name,
                    leader_id, glory, previous_place_in_top,
                    victories, picture
                FROM
                    clans
                WHERE
                    id=:clanId
SQL;
            $data = Db::instance()->select($sql, array(':clanId' => $clanId));
            if (!$data) {
                throw new NotFoundException("Clan $clanId is not found");
            }
            return $data[0];
        };
        return $this->getCached($getter, $key, 600);
    }

    /**
     * Извлечь id клана по id лидера
     * @param int $leaderId
     * @return int
     * @throws \krest\exceptions\NotFoundException
     */
    public function getClanIdByLeaderId($leaderId)
    {
        $key = "clan_id_by_leader_id_$leaderId";
        $getter = function () use ($leaderId) {
            $data = Db::instance()->select('SELECT id FROM clans WHERE leader_id=:leaderId',
                                           [':leaderId' => $leaderId]);
            if (!$data) {
                throw new NotFoundException("Clan with leader $leaderId is not found");
            }
            $id = (int)$data[0]['id'];
            return $id;
        };
        return $this->getCached($getter, $key);
    }

    /**
     * Проверить существование клана
     * @param int $clanId
     * @throws \krest\exceptions\NotFoundException
     */
    public function checkClanExists($clanId)
    {
        $data = Db::instance()->select('SELECT id FROM clans WHERE id=:clanId', array(':clanId' => $clanId));
        if (empty($data)) {
            throw new NotFoundException("Clan $clanId is not found");
        }
    }

    /**
     * Извлечь массив членов слана
     * @param int $clanId
     * @return array
     */
    public function getClanMembers($clanId)
    {
        $key = "clan_members_$clanId";
        $getter = function () use ($clanId) {
            $data = Db::instance()->select('SELECT user_id FROM clan_members WHERE clan_id=:clanId',
                                           [':clanId' => $clanId]);
            $data = array_map(function ($row) {
                return $row['user_id'];
            }, $data);
            return $data;
        };
        return $this->getCached($getter, $key);
    }

    /**
     * Добавить члена клана
     * @param int $clanId
     * @param int $userId
     * @throws \krest\exceptions\InvalidDataException
     */
    public function addClanMember($clanId, $userId)
    {
        if ($this->isSomeClanMember($userId)) {
            throw new InvalidDataException('User is already some clan member');
        }

        if ($this->getClanMembersCount($clanId) >= $this->getMaxMembersCount()) {
            throw new InvalidDataException('Max members count is reached');
        }

        Db::instance()->exec('INSERT INTO clan_members SET clan_id=:clanId, user_id=:userId', array(
                                                                                                   ':clanId' => $clanId,
                                                                                                   ':userId' => $userId,

                                                                                              ));
        $this->_deleteDataFromCache("clan_members_$clanId");
        $this->_deleteDataFromCache("clan_members_count_$clanId");

        $user = new User();
        $clanInfo = $this->getInfo($clanId);
        $socNetId = $user->getSocNetIdByUserId($userId);
        $header = \Messages::factory()['add_to_clan_header'];
        $message = \Messages::factory()['add_to_clan_notification'];
        $message = str_replace('{clan_name}', $clanInfo['name'], $message);

        SocNet::instance()->sendNotification([$socNetId], $message);

        $news = new UserNews();
        $news->addCommonNews($header, $message, $header, $message, null, null, $userId);
    }

    /**
     * Удалить пользователя из клана
     * @param int $clanId
     * @param int $userId
     * @throws \krest\exceptions\InvalidDataException
     */
    public function delClanMember($clanId, $userId)
    {
        if (!$this->isClanMember($clanId, $userId)) {
            throw new InvalidDataException("User is not the clan member");
        }

        $clanInfo = $this->getInfo($clanId);
        if ($userId == $clanInfo["leader_id"]) {
            throw new InvalidDataException("User is the clan leader");
        }

        Db::instance()->exec('DELETE FROM clan_members WHERE clan_id=:clanId AND user_id=:userId', array(
                                                                                                        ':clanId' => $clanId,
                                                                                                        ':userId' => $userId,
                                                                                                   ));
        $this->_deleteDataFromCache("clan_members_$clanId");
        $this->_deleteDataFromCache("clan_members_count_$clanId");

        $user = new User();
        $clanInfo = $this->getInfo($clanId);
        $socNetId = $user->getSocNetIdByUserId($userId);
        $header = \Messages::factory()['exile_from_clan_header'];
        $message = \Messages::factory()['exile_from_clan_notification'];
        $message = str_replace('{clan_name}', $clanInfo['name'], $message);

        SocNet::instance()->sendNotification([$socNetId], $message);

        $news = new UserNews();
        $news->addCommonNews($header, $message, $header, $message, null, null, $userId);
    }

    /**
     * Является ли пользователь членом конкретного клана
     * @param int $clanId
     * @param int $userId
     * @return bool
     */
    public function isClanMember($clanId, $userId)
    {
        $data = Db::instance()->select('SELECT clan_id FROM clan_members WHERE clan_id=:clanId AND user_id=:userId',
                                       array(
                                            ':clanId' => $clanId,
                                            ':userId' => $userId,
                                       ));
        return (bool)$data;
    }

    /**
     * Является ли пользователь членом любого клана
     * @param int $userId
     * @return bool
     */
    public function isSomeClanMember($userId)
    {
        $data = Db::instance()->select('SELECT user_id FROM clan_members WHERE user_id=:userId',
                                       array(':userId' => $userId));
        return (bool)$data;
    }

    /**
     * Извлечь клан пользователя
     * @param int $userId
     * @return int|null
     */
    public function getUsersClan($userId)
    {
        $data = Db::instance()->select('SELECT clan_id FROM clan_members WHERE user_id=:userId',
                                       array(':userId' => $userId));
        if ($data) {
            return $data[0]['clan_id'];
        }
        return null;
    }

    /**
     * Извлечь количество участников клана
     * @param int $clanId
     * @return int
     */
    public function getClanMembersCount($clanId)
    {
        $key = "clan_members_count_$clanId";
        $getter = function () use ($clanId) {
            $data = Db::instance()->select('SELECT COUNT(*) AS `count` FROM clan_members WHERE clan_id=:clanId',
                                           [':clanId' => $clanId]);

            $count = (int)Arr::get(Arr::get($data, 0), 'count');
            return $count;
        };
        return $this->getCached($getter, $key);
    }

    /**
     * Увеличить количество побед клана
     * @param int $clanId
     * @return int новое количество побед
     * @throws \krest\exceptions\NotFoundException
     */
    public function incVictories($clanId)
    {
        $db = Db::instance();

        $data = $db->select('SELECT victories FROM clans WHERE id=:id', array(':id' => $clanId));
        if (!$data) {
            throw new NotFoundException("Clan $clanId not found");
        }

        $victories = $data[0]['victories'] + 1;
        $db->exec('UPDATE clans SET victories=:victories WHERE id=:id', array(
                                                                             ':victories' => $victories,
                                                                             ':id' => $clanId,
                                                                        ));

        $this->_deleteDataFromCache("clan_info_$clanId");
        return $victories;
    }

    /**
     * Прибавить клану славы
     * @param int $clanId
     * @param int $dGlory
     * @return int
     * @throws \krest\exceptions\NotFoundException
     */
    public function addGlory($clanId, $dGlory)
    {
        $db = Db::instance();

        $data = $db->select('SELECT glory FROM clans WHERE id=:id', array(':id' => $clanId));
        if (!$data) {
            throw new NotFoundException("Clan $clanId not found");
        }

        $newGlory = max(0, $data[0]['glory'] + $dGlory);
        $db->exec('UPDATE clans SET glory=:glory WHERE id=:id', array(
                                                                     ':glory' => $newGlory,
                                                                     ':id' => $clanId,
                                                                ));

        $this->_deleteDataFromCache("clan_info_$clanId");
        return $newGlory;
    }

    /**
     * Сбросить славу
     */
    public function resetGlory()
    {
        Db::instance()->exec('UPDATE clans SET glory=0');
        $this->_deleteDataFromCache("clans_top");
    }

    /**
     * Извлечь топ кланов
     * @return array
     */
    public function getTop()
    {
        $key = "clans_top";
        $ttl = 900;
        $getter = function () {
            $data = Db::instance()->select('SELECT id FROM clans ORDER BY glory DESC LIMIT 50');
            $data = array_map(function ($row) {
                return $row['id'];
            }, $data);

            return $data;
        };
        return $this->getCached($getter, $key, $ttl);
    }

    /**
     * Установить картинки кланам
     */
    public function setPictures()
    {
        $this->disableCache();
        $topIds = $this->getTop();
        $this->enableCache();

        foreach ($topIds as $c => $clanId) {
            if ($c > 2) {
                $this->_incPicture($clanId, 1);
            }
            elseif ($c === 1 || $c === 2) {
                $this->_incPicture($clanId, 2);
            }
            elseif ($c === 0) {
                $this->_incPicture($clanId, 3);
            }
        }
    }

    private function _incPicture($clanId, $count)
    {
        $info = $this->getInfo($clanId);
        $currentPicture = intval($info['picture']);
        $picture = min(self::LEVELS_COUNT, $currentPicture + $count + 2);
        Db::instance()->exec('UPDATE clans SET picture=:picture WHERE id=:id',
                             [':id' => $clanId, ':picture' => $picture]);
        $this->_deleteDataFromCache("clan_info_$clanId");
    }

    /**
     * Записать места в топе для всех кланов
     */
    public function writePreviousPlaces()
    {
        $db = Db::instance();
        $db->exec('UPDATE clans SET previous_place_in_top=0');

        $this->disableCache();
        $topIds = $this->getTop();
        $this->enableCache();

        $count = sizeof($topIds);
        for ($i = 0; $i < $count; ++$i) {
            $db->exec('UPDATE clans SET previous_place_in_top=:place WHERE id=:id', array(
                                                                                         ':place' => $i + 1,
                                                                                         ':id' => $topIds[$i],
                                                                                    ));
        }
    }
}
