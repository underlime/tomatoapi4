<?php
namespace model;

use base\Model;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;

class SpecDay extends Model
{
    /**
     * @var User
     */
    private $user;

    private $probability = 10;
    private $forWhoR;
    private $bonusesCountR;

    /**
     * Установить вероятность назначения спец. дня,
     * ТОЛЬКО ДЛЯ ТЕСТОВ
     * @param $probability
     */
    public function setProbability($probability)
    {
        $this->probability = $probability;
    }

    /**
     * Установить "случайное" значение для определения объекта бонусов,
     * ТОЛЬКО ДЛЯ ТЕСТОВ
     * @param float $forWhoR
     */
    public function setForWhoR($forWhoR)
    {
        $this->forWhoR = $forWhoR;
    }

    /**
     * Установить "случайное" значение для определения количества бонусов,
     * ТОЛЬКО ДЛЯ ТЕСТОВ
     * @param float $bonusesCountR
     * @return void
     */
    public function setBonusesCountR($bonusesCountR)
    {
        $this->bonusesCountR = $bonusesCountR;
    }

    /**
     * Извлечь специальные дни
     * @param $socNetId
     * @return array
     */
    public function checkoutSpecialDay($socNetId)
    {
        $this->_setSpecialDay();

        $bonusesList = $this->_selectBonuses();
        if ($bonusesList) {
            //$this->_giveBonusesNames($bonusesList);
            $this->_applyMoneyBonuse($socNetId, $bonusesList);
        }
        return $bonusesList;
    }

    private function _selectBonuses()
    {
        $cacheKey = 'day_bonuses';
        $bonusesList = $this->_getDataFromCache($cacheKey);
        if ($bonusesList !== null) {
            return $bonusesList;
        }

        $bonusesList = $this->_getAllBonusesRecords();
        $this->_clearOldBonuses($bonusesList);

        $tomorrowMidnight = strtotime('tomorrow midnight');
        $ttl = $tomorrowMidnight - time();

        $this->_setDataIntoCache($cacheKey, $bonusesList, $ttl);
        return $bonusesList;
    }

    private function _getAllBonusesRecords()
    {
        $sql = <<<SQL
          SELECT
             day_bonuse_id,
             `date`,
             for_who,
             tomatos,
             ferros,
             damage,
             armor,
             speed
          FROM
            day_bonuses
SQL;

        $bonusesList = Db::instance()->select($sql);
        return $bonusesList;
    }

    private function _clearOldBonuses(array &$bonusesList)
    {
        $todayMidnight = strtotime('today midnight');

        $bonusesForDel = array();
        foreach ($bonusesList as $c => $curBonuse) {
            $bonTime = strtotime($curBonuse['date']);
            if ($bonTime < $todayMidnight) {
                $bonusesForDel[] = $curBonuse['day_bonuse_id'];
                unset($bonusesList[$c]);
            }
        }

        if ($bonusesForDel) {
            $this->_deleteBonuses($bonusesForDel);
        }
    }

    private function _deleteBonuses(array $bonusesIds)
    {
        $sql = 'DELETE FROM day_bonuses WHERE day_bonuse_id IN :bonusesIds';
        Db::instance()->exec($sql, array(':bonusesIds' => $bonusesIds));
    }

    private function _giveBonusesNames(array &$bonusesList)
    {
        foreach ($bonusesList as &$curBonuse) {
            $forWhoName = $this->_getNameForWho($curBonuse['for_who']);
            $theyWord = ($curBonuse['for_who'] == 'all') ? 'все' : 'они';
            $bonuseName = $this->_getBonuseName($curBonuse);
            $curBonuse['name'] = "День $forWhoName";
            $curBonuse['text'] = "Объявляется день $forWhoName! Сегодня $theyWord получают $bonuseName.";
        }
    }

    private function _getNameForWho($forWho)
    {
        switch ($forWho) {
            case 'all':
                $name = 'добра';
                break;
            case 'tomato':
                $name = 'томатов';
                break;
            case 'onion':
                $name = 'луков';
                break;
            case 'pepper':
                $name = 'перцев';
                break;
            case 'carrot':
                $name = 'морковей';
                break;
            case 'cucumber':
                $name = 'огурцов';
                break;
            case 'beta':
                $name = 'свекол';
                break;
            case 'short':
                $name = 'круглых';
                break;
            case 'long':
                $name = 'длинных';
                break;
            default:
                throw new InvalidDataException("Wrong type: $forWho");
        }

        return $name;
    }

    private function _getBonuseName(array $curBonuse)
    {
        $bonuses = array();
        if ($curBonuse['tomatos']) {
            $bonuses[] = "{$curBonuse['tomatos']} томатосов";
        }

        if ($curBonuse['ferros']) {
            $bonuses[] = "{$curBonuse['ferros']} ферросов";
        }

        if ($curBonuse['damage']) {
            $bonuses[] = "{$curBonuse['damage']} урона";
        }

        if ($curBonuse['armor']) {
            $bonuses[] = "{$curBonuse['armor']} защиты";
        }

        if ($curBonuse['speed']) {
            $bonuses[] = "{$curBonuse['speed']} скорости";
        }

        $len = sizeof($bonuses);
        if ($len == 1) {
            $bonuseName = $bonuses[0];
        }
        elseif ($len == 2) {
            $bonuseName = $bonuses[0].' и '.$bonuses[1];
        }
        else {
            $bonuseName = '';
            for ($i = 0; $i < $len - 2; ++$i) {
                $bonuseName .= $bonuses[$i].', ';
            }
            $bonuseName .= $bonuses[$len - 2].' и '.$bonuses[$len - 1];
        }

        return $bonuseName;
    }

    /**
     * Установить специальный день
     * @return void
     */
    private function _setSpecialDay()
    {
        $cacheKey = 'special_day_set';
        $specDaySet = $this->_getDataFromCache($cacheKey);
        if ($specDaySet !== null) {
            return;
        }

        $dateInfo = Db::instance()->select("SELECT `date` FROM misc_dates WHERE `key`='special_day_set'");
        $setDateData = Arr::req($dateInfo, 0);
        $setDate = Arr::req($setDateData, 'date');

        $tomorrowMidnight = strtotime('tomorrow midnight');
        $cacheTtl = $tomorrowMidnight - time();

        $nowDate = date('Y-m-d');
        if ($nowDate == $setDate) {
            $this->_setDataIntoCache($cacheKey, 1, $cacheTtl);
            return;
        }

        if ($this->_needSpecialDay()) {
            $forWho = $this->_getForWho();
            $bonusesCount = $this->_getBonusesCount();
            $bonusesValues = $this->_getBonusesByCount($bonusesCount);
            $this->_sendBonusesQuery($forWho, $bonusesValues);
        }

        $sql = "UPDATE `misc_dates` SET `date`=:date WHERE `key`='special_day_set'";
        Db::instance()->exec($sql, array(':date' => $nowDate));
        $this->_setDataIntoCache($cacheKey, 1, $cacheTtl);
    }

    private function _needSpecialDay()
    {
        return (mt_rand(1, 100) <= $this->probability);
    }

    private function _getForWho()
    {
        $r = ($this->forWhoR) ? $this->forWhoR : (mt_rand()/mt_getrandmax());
        if ($r < 0.16) {
            $forWho = 'tomato';
        }
        elseif ($r >= 0.16 && $r < 0.32) {
            $forWho = 'onion';
        }
        elseif ($r >= 0.32 && $r < 0.48) {
            $forWho = 'beta';
        }
        elseif ($r >= 0.48 && $r < 0.64) {
            $forWho = 'pepper';
        }
        elseif ($r >= 0.64 && $r < 0.80) {
            $forWho = 'carrot';
        }
        elseif ($r >= 0.80 && $r < 0.96) {
            $forWho = 'cucumber';
        }
        elseif ($r >= 0.96 && $r < 0.975) {
            $forWho = 'short';
        }
        elseif ($r >= 0.975 && $r < 0.99) {
            $forWho = 'long';
        }
        else {
            $forWho = 'all';
        }

        return $forWho;
    }

    private function _getBonusesCount()
    {
        $r = ($this->bonusesCountR) ? $this->bonusesCountR : (mt_rand()/mt_getrandmax());
        if ($r < 0.72) {
            $count = 1;
        }
        elseif ($r >= 0.72 && $r < 0.82) {
            $count = 2;
        }
        elseif ($r >= 0.82 && $r < 0.88) {
            $count = 3;
        }
        elseif ($r >= 0.94 && $r < 0.98) {
            $count = 4;
        }
        else {
            $count = 5;
        }

        return $count;
    }

    private function _getBonusesByCount($bonusesCount)
    {
        $workFields = array('tomatos', 'ferros', 'damage', 'armor', 'speed');
        $selectedBonuses = array();
        for ($i = 0; $i < $bonusesCount; ++$i) {
            $selectedBonuses[] = $this->_getFieldFromArray($workFields);
        }

        $bonusesWithValues = array();
        foreach ($selectedBonuses as $curBonuse) {
            $bonusesWithValues[$curBonuse] = $this->_getBonusesValues($curBonuse);
        }

        return $bonusesWithValues;
    }

    private function _getFieldFromArray(array &$fields)
    {
        $len = sizeof($fields);
        $ind = mt_rand(0, $len - 1);

        $curField = $fields[$ind];
        array_splice($fields, $ind, 1);

        return $curField;
    }

    private function _getBonusesValues($fieldName)
    {
        if ($fieldName == 'tomatos') {
            $toMin = 50;
            $max = 150;
        }
        elseif ($fieldName == 'ferros') {
            $toMin = 2;
            $max = 5;
        }
        else {
            $toMin = 5;
            $max = 15;
        }

        $kff = mt_rand()/mt_getrandmax();
        $rawValue = $max - $toMin*$kff;
        $value = round($rawValue);

        return $value;
    }

    private function _sendBonusesQuery($forWho, array $bonusesValues)
    {
        $sql = 'INSERT INTO day_bonuses SET `date`=:date, for_who=:forWho, ';
        $params = array(
            ':date' => date('Y-m-d'),
            ':forWho' => $forWho
        );

        $values = array();
        foreach ($bonusesValues as $name => $val) {
            $values[] = " `$name`=:$name";
            $params[':'.$name] = $val;
        }

        $sql .= implode(', ', $values);

        Db::instance()->exec($sql, $params);
    }

    /**
     * Применить бонус денег
     * @param string $socNetId
     * @param $bonusesList
     * @return void
     */
    private function _applyMoneyBonuse($socNetId, $bonusesList)
    {
        $cacheKey = 'apply_money_bonuse_'.$socNetId;
        $applied = $this->_getDataFromCache($cacheKey);
        if ($applied) {
            return;
        }

        $midNightTime = strtotime('tomorrow midnight');
        $ttl = $midNightTime - time();
        $this->_setDataIntoCache($cacheKey, 1, $ttl);

        $this->user = new User();
        $this->user->disableCache();
        $userInfo = $this->user->getUserInfo($socNetId);
        $this->user->enableCache();

        if (in_array($userInfo['vegetable'], array('tomato', 'onion', 'beta'))) {
            $type = 'short';
        }
        else {
            $type = 'long';
        }

        $ferros = 0;
        $tomatos = 0;
        $bonuses = array();

        foreach ($bonusesList as $curBonuse) {
            if (in_array($curBonuse['for_who'], array($userInfo['vegetable'], $type, 'all'))) {
                $ferros += $curBonuse['ferros'];
                $tomatos += $curBonuse['tomatos'];
                $bonuses[] = $curBonuse['day_bonuse_id'];
            }
        }

        if ($bonuses) {
            $shown = $this->_getBonusesSown($socNetId, $bonuses);
            if ($shown) {
                return;
            }
        }

        if ($ferros or $tomatos) {
            $this->user->addMoney($userInfo['user_id'], $ferros, $tomatos, 'Спец. день');
        }

        if ($bonuses) {
            $this->_terminateBonuses($userInfo['user_id'], $bonuses);
        }
    }

    private function _getBonusesSown($userId, array $bonuses)
    {
        $sql = 'SELECT COUNT(*) AS count FROM day_bonuses_shown WHERE user_id=:user_id AND day_bonuse_id IN :bonusesList';
        $params = array(
            ':user_id' => $userId,
            ':bonusesList' => $bonuses
        );

        $res = Db::instance()->select($sql, $params);
        $ans = (bool)$res[0]['count'];

        return $ans;
    }

    private function _terminateBonuses($userId, array $bonuses)
    {
        $values = array();
        $params = array(
            ':user_id' => $userId
        );
        foreach ($bonuses as $c => $curBonuse) {
            $key = ":bonuse$c";
            $values[] = "(:user_id, $key)";
            $params[$key] = $curBonuse;
        }

        $strValues = implode(', ', $values);
        $sql = 'INSERT IGNORE INTO day_bonuses_shown (user_id, day_bonuse_id) VALUES '.$strValues;

        Db::instance()->exec($sql, $params);
    }
}
