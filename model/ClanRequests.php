<?php
namespace model;

use base\Model;
use krest\exceptions\InvalidDataException;
use lib\RediskaFactory;

class ClanRequests extends Model
{
    /**
     * @var \model\Clans
     */
    private $_clans;

    /**
     * @var \Rediska
     */
    private $_rediska;

    public function __construct()
    {
        parent::__construct();
        $this->_rediska = RediskaFactory::getInstance();
        $this->_clans = new Clans();
    }

    /**
     * Добавить заявку в клан
     * @param int $clanId
     * @param int $userId
     * @throws \krest\exceptions\InvalidDataException
     */
    public function create($clanId, $userId)
    {
        $this->_clans->checkClanExists($clanId);
        if ($this->_clans->isSomeClanMember($userId)) {
            throw new InvalidDataException('User is already clan member');
        }

        if ($this->_clans->getClanMembersCount($clanId) >= $this->_clans->getMaxMembersCount()) {
            throw new InvalidDataException('Max members count is reached');
        }

        $userRequestKey = "clan_requester:{$userId}";
        if ($this->_rediska->exists($userRequestKey)) {
            throw new InvalidDataException("User $userId has already added request");
        }

        $usersKey = "clan:{$clanId}:requests:users";
        $timesKey = "clan:{$clanId}:requests:times";

        $pipeline = $this->_rediska->pipeline();
        $pipeline->addToSet($usersKey, $userId);
        $pipeline->setToHash($timesKey, [$userId => date("Y-m-d H:i:s")]);
        $pipeline->append($userRequestKey, true);
        $pipeline->execute();
    }

    /**
     * Извлечь заявки в клан
     * @param $clanId
     * @return array id пользователей
     */
    public function get($clanId)
    {
        $usersKey = "clan:{$clanId}:requests:users";
        $timesKey = "clan:{$clanId}:requests:times";

        $userIds = $this->_rediska->getSet($usersKey);
        $userIds = array_reverse($userIds);

        $data = [];
        foreach ($userIds as $id) {
            $data[] = [
                "user_id" => $id,
                "time" => $this->_rediska->getFromHash($timesKey, $id),
            ];
        }
        return $data;
    }

    /**
     * Принять заявку в клан
     * @param int $clanId
     * @param int $userId
     * @throws \krest\exceptions\InvalidDataException
     */
    public function accept($clanId, $userId)
    {
        $userRequestKey = "clan_requester:{$userId}";
        $usersKey = "clan:{$clanId}:requests:users";
        $timesKey = "clan:{$clanId}:requests:times";
        if (!$this->_rediska->existsInSet($usersKey, $userId)) {
            throw new InvalidDataException('Request not found');
        }

        $this->_clans->addClanMember($clanId, $userId);

        $pipeline = $this->_rediska->pipeline();
        $pipeline->deleteFromSet($usersKey, $userId);
        $pipeline->deleteFromHash($timesKey, $userId);
        $pipeline->delete($userRequestKey);
        $pipeline->execute();
    }

    /**
     * Отклонить заявку в клан
     * @param int $clanId
     * @param int $userId
     * @throws \krest\exceptions\InvalidDataException
     */
    public function reject($clanId, $userId)
    {
        $userRequestKey = "clan_requester:{$userId}";
        $clanKey = "clan:{$clanId}:requests:users";
        $timesKey = "clan:{$clanId}:requests:times";

        $pipeline = $this->_rediska->pipeline();
        $pipeline->deleteFromSet($clanKey, $userId);
        $pipeline->deleteFromHash($timesKey, $userId);
        $pipeline->delete($userRequestKey);
        $pipeline->execute();
    }

    /**
     * Очистить устаревшие заявки
     * @return int
     */
    public function clearOutdated()
    {
        $now = time();
        $ttl = 604800;  // 7 дней
        $expired = [];
        $timesKeys = $this->_rediska->getKeysByPattern('clan:*:requests:times');
        foreach ($timesKeys as $key) {
            $hash = $this->_rediska->getHash($key);
            foreach ($hash as $userId => $strTime) {
                $time = strtotime($strTime);
                if ($now - $time > $ttl) {
                    $clanId = preg_filter('/^clan:(\d+):requests:times$/', '$1', $key);
                    $expired[$clanId][] = $userId;
                }
            }
        }

        $affectedRows = 0;
        foreach ($expired as $clanId => $users) {
            foreach ($users as $userId) {
                $this->reject($clanId, $userId);
                $affectedRows++;
            }
        }
        return $affectedRows;
    }
}
