<?php
namespace model;

use base\Model;
use krest\api\Config;
use krest\Arr;
use krest\Db;
use krest\exceptions\AlreadyExistsException;
use krest\strings\Text;
use lib\TkEventBroker;

class Rivals extends Model
{
    /**
     * @var array
     */
    private static $config;

    /**
     * @var \model\User
     */
    private $user;

    function __construct()
    {
        parent::__construct();

        if (self::$config === null) {
            self::$config = Config::instance()->getForClass(__CLASS__);
        }
    }

    /**
     * Извлечь цену добавления в соперники
     * ключ rivals_ferros_price
     * @return int
     */
    public function getRivalPrice()
    {
        return Arr::req(self::$config, 'rivals_ferros_price');
    }

    /**
     * Извлечь соперников
     * @return array
     */
    public function getRivalsList()
    {
        $cacheKey = 'rivals_list';
        $rivals = $this->_getDataFromCache($cacheKey);
        if ($rivals !== null) {
            return $rivals;
        }

        $sql = 'SELECT r.user_id, r.message, u.soc_net_id FROM rivals r INNER JOIN users u USING(user_id) ORDER BY r.rival_id DESC LIMIT 7';
        $rivals = Db::instance()->select($sql);

        $this->_setDataIntoCache($cacheKey, $rivals);
        return $rivals;
    }

    /**
     * Добавить соперника
     * @param $userId
     * @param $message
     * @throws \krest\exceptions\AlreadyExistsException
     * @return void
     */
    public function addRival($userId, $message)
    {
        if ($this->getLastUserId() == $userId) {
            throw new AlreadyExistsException("The last record is already belongs to User #{$userId}");
        }

        $this->user = new User();
        $comment = 'Соперники';

        $ferros = $this->getRivalPrice();
        $this->user->pay($userId, $ferros, 0, $comment);
        $this->_addRivalMessage($userId, $message);

        $cacheKey = 'rivals_list';
        $this->_deleteDataFromCache($cacheKey);
        TkEventBroker::instance()->dispatchBroadcastEvent('rival_added');
    }

    private function _addRivalMessage($userId, $message)
    {
        $cleanMessage = Text::clear($message);
        $cleanMessage = Text::reduce($cleanMessage, 100);

        $sql = 'INSERT INTO rivals SET user_id=:user_id, message=:message, time=:time';
        $params = array(
            ':user_id' => $userId,
            ':message' => $cleanMessage,
            ':time' => date('Y-m-d H:i:s'),
        );

        Db::instance()->exec($sql, $params);
    }

    /**
     * Извлечь id последнего пользователя
     * @return int|null
     */
    public function getLastUserId()
    {
        $data = Db::instance()->select('SELECT user_id FROM rivals ORDER BY rival_id DESC LIMIT 1');
        return Arr::getByPath($data, '0.user_id');
    }

    /**
     * Удалить устаревшие записи
     * @return int Количество удаленных записей
     */
    public function cleanOutdated()
    {
        $actual = Db::instance()->select('SELECT rival_id FROM rivals ORDER BY rival_id DESC LIMIT 7');
        $ids = array_map(function ($rec) {
            return $rec['rival_id'];
        }, $actual);

        if ($ids) {
            $result = Db::instance()->delete('DELETE FROM rivals WHERE rival_id NOT IN :ids', [':ids' => $ids]);
            $affectedRows = $result['affected_rows'];
        }
        else {
            $affectedRows = 0;
        }
        return $affectedRows;
    }
}
