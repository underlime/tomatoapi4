<?php
namespace model;

use base\Model;
use krest\api\Config;
use krest\Arr;
use krest\Db;
use krest\exceptions\DbException;
use krest\exceptions\InvalidDataException;
use krest\Random;

class Gifts extends Model
{
    const USERS_LIMIT = 9;
    const MAX_GIFTS_TO_RECEIVE = 3;
    const MAX_GIFTS_TO_SEND = 27;

    /**
     * Извлечь цену прокачки подарка
     * ключ gift_improved_ferros_price
     * @return int
     */
    public function getImprovedGiftPrice()
    {
        $config = Config::instance()->getForClass(__CLASS__);
        return Arr::req($config, 'improved_ferros_price');
    }

    /**
     * Извлечь цену прокачки подарка
     * ключ gift_open_ferros_price
     * @return int
     */
    public function getOpenGiftPrice()
    {
        $config = Config::instance()->getForClass(__CLASS__);
        return Arr::req($config, 'open_gift_ferros_price');
    }

    /**
     * Извлечь список пользователей для подарков
     * ключ gifts_users_list
     * @param $userId
     * @return array|mixed
     */
    public function getRandomUsersForGifts($userId)
    {
        $limit = self::USERS_LIMIT;

        $cacheKey = "random_users_for_gifts_{$limit}_{$userId}";
        $usersList = $this->_getDataFromCache($cacheKey);
        if ($usersList !== null) {
            return $usersList;
        }

        $user = new User();
        $maxUserId = $user->getMaxUserId();
        $userIdsList = Random::getRandomList(1, $maxUserId, $limit*3);

        $sql = <<<SQL
            SELECT
                u.soc_net_id,
                u.user_id,
                u.login,
                ulp.level,
                ufd.glory
            FROM
                users AS u
                INNER JOIN users_level_params ulp USING(user_id)
                INNER JOIN users_fight_data ufd USING(user_id)
            WHERE
                u.user_id IN :userIdsList
                AND NOT EXISTS(
                    SELECT receiver_id FROM gifts WHERE receiver_id=u.user_id AND DATE=:DATE
                )
            LIMIT $limit
SQL;

        $params = array(
            ':userIdsList' => $userIdsList,
            ':date' => date('Y-m-d'),
        );
        $usersList = Db::instance()
                     ->select($sql, $params);

        $lifeTime = strtotime('tomorrow midnight') - time();
        $this->_setDataIntoCache($cacheKey, $usersList, $lifeTime);
        return $usersList;
    }

    /**
     * Раздать подарки случайным чувакам
     * @param $userId
     * @throws \krest\exceptions\InvalidDataException
     * @return int Новое количество отправленных подарков
     */
    public function sendGiftsToRandomUsers($userId)
    {
        $usersData = $this->getRandomUsersForGifts($userId);

        $this->disableCache();
        $sentCount = $this->getGiftsSentCount($userId);
        $this->enableCache();

        if (empty($usersData)) {
            return $sentCount;
        }

        $newCountToSend = sizeof($usersData);
        $sendCountAfterOperation = $sentCount + $newCountToSend;
        if ($sendCountAfterOperation > self::MAX_GIFTS_TO_SEND) {
            throw new InvalidDataException('Too many gifts to send');
        }

        $usersList = Arr::fetchField($usersData, 'user_id');
        $this->_sendGiftsToUsers($userId, $usersList);

        $limit = self::USERS_LIMIT;
        $cacheKey = "random_users_for_gifts_{$limit}_{$userId}";
        $this->_deleteDataFromCache($cacheKey);

        return $sendCountAfterOperation;
    }

    private function _sendGiftsToUsers($userId, array $usersList)
    {
        $params = array(
            ':sender_id' => $userId,
            ':date' => date('Y-m-d'),
        );
        $c = 0;
        $queryParts = array();
        foreach ($usersList as $receiverId) {
            $key = ":{$c}_receiver_id";
            $params[$key] = $receiverId;
            $queryParts[] = "(:sender_id, $key, :date)";
            ++$c;
        }

        $sql = 'INSERT IGNORE INTO gifts (sender_id, receiver_id, date) VALUES '.implode(',', $queryParts);
        try {
            Db::instance()->exec($sql, $params);
        }
        catch (DbException $e) {
            throw new InvalidDataException('Wrong receivers', $e->getCode(), $e);
        }

        $cacheKey = "gifts_sent_count_$userId";
        $this->_deleteDataFromCache($cacheKey);
    }

    /**
     * Раздать подарки друзьям
     * @param $userId
     * @return int Новое количество отправленных подарков
     */
    public function sendGiftsToFriends($userId)
    {
        $user = new User();
        $socNetId = $user->getSocNetIdByUserId($userId);
        $usersList = $user->getFriendsInGameIdsList($socNetId);

        $this->disableCache();
        $sentCount = $this->getGiftsSentCount($userId);
        $this->enableCache();

        if (empty($usersList)) {
            return $sentCount;
        }

        $sendCountAfterOperation = $sentCount + sizeof($usersList);
        $this->_sendGiftsToUsers($userId, $usersList);

        return $sendCountAfterOperation;
    }

    /**
     * Раздать подарки друзьям
     * @param $userId
     * @param $receiverId
     * @throws \krest\exceptions\InvalidDataException
     * @return int Новое количество отправленных подарков
     */
    public function sendGiftToUser($userId, $receiverId)
    {
        if ($userId == $receiverId) {
            throw new InvalidDataException('You can\'t compliment yourself');
        }

        $this->disableCache();
        $sentCount = $this->getGiftsSentCount($userId);
        $this->enableCache();

        $usersList = array($receiverId);
        $sendCountAfterOperation = $sentCount + 1;
        if ($sendCountAfterOperation > self::MAX_GIFTS_TO_SEND) {
            throw new InvalidDataException('Too many gifts to send');
        }

        $this->_sendGiftsToUsers($userId, $usersList);

        return $sendCountAfterOperation;
    }

    /**
     * Извлечь список подарков пользователей
     * ключ user_gifts_list
     * @param int $userId
     * @return array
     */
    public function getUserGiftsList($userId)
    {
        $cacheKey = "get_user_gifts_list_$userId";
        $giftsList = $this->_getDataFromCache($cacheKey);
        if ($giftsList !== null) {
            return $giftsList;
        }

        $sql = <<<SQL
            SELECT
                g.gift_id,
                g.sender_id,
                g.receiver_id,
                g.date,
                u.soc_net_id AS sender_soc_net_id,
                u.login AS sender_login
            FROM
                gifts g
                INNER JOIN users u ON u.user_id=g.sender_id
            WHERE
                g.receiver_id=:user_id
SQL;

        $data = Db::instance()
                ->select($sql, array(':user_id' => $userId));

        $giftsList = array();
        foreach ($data as $row) {
            $giftsList[$row['gift_id']] = $row;
        }

        $this->_setDataIntoCache($cacheKey, $giftsList);
        return $giftsList;
    }

    /**
     * Извлечь количество подаренных подарков
     * ключ gifts_sent_count
     * @param $userId
     * @return int
     */
    public function getGiftsSentCount($userId)
    {
        $cacheKey = "gifts_sent_count_$userId";
        $count = $this->_getDataFromCache($cacheKey);
        if ($count !== null) {
            return $count;
        }

        $sql = 'SELECT COUNT(*) AS count FROM gifts WHERE sender_id=:user_id AND date=:date';
        $params = array(
            ':user_id' => $userId,
            ':date' => date('Y-m-d'),
        );
        $data = Db::instance()->select($sql, $params);
        $count = Arr::req($data[0], 'count');

        $lifeTime = strtotime('tomorrow midnight') - time();
        $this->_setDataIntoCache($cacheKey, $count, $lifeTime);
        return $count;
    }

    /**
     * Извлечь количество подаренных подарков
     * ключ gifts_received_count
     * @param $userId
     * @return int
     */
    public function getGiftsReceivedCount($userId)
    {
        $cacheKey = "gifts_received_count_$userId";
        $count = $this->_getDataFromCache($cacheKey);
        if ($count !== null) {
            return $count;
        }

        $sql = 'SELECT gifts_received, last_date FROM gifts_received_count WHERE user_id=:user_id';
        $params = array(':user_id' => $userId);

        $rawData = Db::instance()->select($sql, $params);
        $data = Arr::req($rawData, 0);

        $currentDate = date('Y-m-d');
        if ($data['last_date'] == $currentDate) {
            $count = $data['gifts_received'];
        }
        else {
            $count = 0;
        }

        $lifeTime = strtotime('tomorrow midnight') - time();
        $this->_setDataIntoCache($cacheKey, $count, $lifeTime);
        return $count;
    }

    /**
     * Увеличить количетсво полученных подарков
     * @param int $userId
     * @param int $delta
     * @return void
     */
    public function incGiftsReceivedCount($userId, $delta = 1)
    {
        $db = Db::instance();

        $this->disableCache();
        $count = $this->getGiftsReceivedCount($userId);
        $this->enableCache();

        $newCount = $count + $delta;
        $sql = 'UPDATE gifts_received_count SET gifts_received=:count, last_date=:date WHERE user_id=:user_id';
        $params = array(
            ':user_id' => $userId,
            ':count' => $newCount,
            ':date' => date('Y-m-d'),
        );

        $db->exec($sql, $params);

        $cacheKey = "gifts_received_count_$userId";
        $lifeTime = strtotime('tomorrow midnight') - time();
        $this->_setDataIntoCache($cacheKey, $newCount, $lifeTime);
    }

    /**
     * Извлечь информацию о подарке
     * @param $giftId
     * @return array|null
     */
    public function getGiftInfo($giftId)
    {
        $sql = 'SELECT sender_id, receiver_id, date FROM gifts WHERE gift_id=:gift_id';
        $data = Db::instance()->select($sql, array(':gift_id' => $giftId));
        return Arr::get($data, 0);
    }

    /**
     * Открыть подарок как обычный
     * @param int $userId
     * @param int $giftId
     * @param bool $checkReceivedCount
     * @return int
     */
    public function openGiftAsUsual($userId, $giftId, $checkReceivedCount = true)
    {
        if ($checkReceivedCount) {
            $this->_checkReceivedCount($userId);
        }
        $this->_checkGiftInfo($userId, $giftId);
        $itemId = $this->_giftItemExchange($userId, $giftId, Items::GIFT_USUAL);
        return $itemId;
    }

    private function _checkReceivedCount($userId)
    {
        $receivedCount = $this->getGiftsReceivedCount($userId);
        if ($receivedCount >= self::MAX_GIFTS_TO_RECEIVE) {
            throw new InvalidDataException('Too many gifts to receive');
        }
    }

    private function _checkGiftInfo($userId, $giftId)
    {
        $giftInfo = $this->getGiftInfo($giftId);
        if ($giftInfo === null or $giftInfo['receiver_id'] != $userId) {
            throw new InvalidDataException('Wrong gift id');
        }
    }

    private function _giftItemExchange($userId, $giftId, $giftType)
    {
        $sql = 'DELETE FROM gifts WHERE gift_id=:gift_id';
        Db::instance()->exec($sql, array(':gift_id' => $giftId));

        $items = new Items();
        $itemId = $items->getGiftItemId($giftType);

        $items->incDecItemCount($userId, $itemId);
        $this->incGiftsReceivedCount($userId);

        $cacheKey = "get_user_gifts_list_$userId";
        $this->_deleteDataFromCache($cacheKey);

        return $itemId;
    }

    /**
     * Открыть подарок как прокачанный
     * @param $userId
     * @param $giftId
     * @throws \krest\exceptions\InvalidDataException
     * @return int
     */
    public function openGiftAsImproved($userId, $giftId)
    {
        $this->_checkReceivedCount($userId);
        $this->_checkGiftInfo($userId, $giftId);

        $price = $this->getImprovedGiftPrice();
        $user = new User();
        $comment = 'Прокачать подарок';
        $newMoney = $user->pay($userId, $price, 0, $comment);

        $itemId = $this->_giftItemExchange($userId, $giftId, Items::GIFT_IMPROVED);

        return array(
            'item_id' => $itemId,
            'new_money' => $newMoney,
        );
    }

    /**
     * Открыть все подарки как обычные
     * @param $userId
     * @return array
     */
    public function openAllGiftsAsUsual($userId)
    {
        $this->_checkReceivedCount($userId);

        $giftsList = $this->getUserGiftsList($userId);
        $giftsCount = sizeof($giftsList);
        $price = $giftsCount*$this->getOpenGiftPrice();
        $giftsIdsList = Arr::fetchField($giftsList, 'gift_id');

        $user = new User();
        $comment = 'Открыть все подарки';
        $newMoney = $user->pay($userId, $price, 0, $comment);

        $itemIdsList = array();
        foreach ($giftsList as $giftData) {
            $itemId = $this->openGiftAsUsual($userId, $giftData['gift_id'], false);
            $itemIdsList[] = $itemId;
        }

        return array(
            'new_money' => $newMoney,
            'item_ids_list' => array_unique($itemIdsList),
            'gifts_ids_list' => $giftsIdsList,
        );
    }
}
