<?php
namespace model\achievements;

use base\EventData;

class AchievementEventData extends EventData
{
    /**
     * ID достижения
     * @var string
     */
    public $achievement_id;

    /**
     * Код достижения
     * @var string
     */
    public $achievement_code;

    /**
     * Имя достижения
     * @var string
     */
    public $name_ru;

    /**
     * Количество добавляемой славы
     * @var int
     */
    public $glory = 0;

    /**
     * Картинка достижения
     * @var string
     */
    public $pic;

    public function __construct()
    {
        $this->type = 'achievment';
    }

}
