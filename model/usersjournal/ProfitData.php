<?php
namespace model\usersjournal;

use krest\base\Struct;

class ProfitData extends Struct
{
    public
        $experience = 0,
        $glory = 0,
        $ferros = 0,
        $tomatos = 0,
        $level = 0;
}
