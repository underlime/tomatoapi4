<?php
namespace model\user;

use krest\base\Struct;

class Bonuses extends Struct
{
    /**
     * Боевые бонусы
     * @var bonuses\Fight
     */
    public $fight;

    /**
     * Бонусы шкал
     * @var bonuses\Scale
     */
    public $scale;

    /**
     * Бонусы применения
     * @var bonuses\Apply
     */
    public $apply;

    public function __construct()
    {
        $this->fight = new bonuses\Fight();
        $this->scale = new bonuses\Scale();
        $this->apply = new bonuses\Apply();
    }

    /**
     * Импортировать XML
     * @param string $data
     * @param int $factor Множитель для характеристик
     */
    public function importXml($data, $factor = 1)
    {
        $xml = simplexml_load_string($data);

        $fight = $xml->fight;
        if ($fight) {
            $fields = array(
                'damage',
                'armor',
                'act_again',
                'dodge',
                'crit',
                'block',
                'special',
                'speed'
            );
            foreach ($fields as $fieldName) {
                $val = (float)($fight->{$fieldName});
                $this->fight->{$fieldName} += $val*$factor;
            }
        }

        $hpBon = (int)($xml->scale->hp);
        $this->scale->hp += $hpBon*$factor;

        $apply = $xml->apply;
        if ($apply) {
            $enBon = (int)($apply->energy);
            $this->apply->energy += $enBon*$factor;

            $hpBon = (int)($apply->hp);
            $this->apply->hp += $hpBon*$factor;
        }
    }

    /**
     * Добавить бонусы из структуры Bonuses
     * @param Bonuses $bonuses
     * @param int $factor
     */
    public function add(Bonuses $bonuses, $factor = 1)
    {
        $fight = $bonuses->fight;
        if ($fight) {
            $fields = array(
                'damage',
                'armor',
                'act_again',
                'dodge',
                'crit',
                'block',
                'special',
                'speed'
            );
            foreach ($fields as $fieldName) {
                $val = (float)($fight->{$fieldName});
                $this->fight->{$fieldName} += $val*$factor;
            }
        }

        $hpBon = (int)($bonuses->scale->hp);
        $this->scale->hp += $hpBon*$factor;

        $apply = $bonuses->apply;
        if ($apply) {
            $enBon = (int)($apply->energy);
            $this->apply->energy += $enBon*$factor;

            $hpBon = (int)($apply->hp);
            $this->apply->hp += $hpBon*$factor;
        }
    }
}
