<?php
namespace model\user;

use base\EventData;

class EverydayBonusData extends EventData
{
    /**
     * Количество посещений
     * @var int
     */
    public $day;

    /**
     * @var int
     */
    public $item_id = 0;

    /**
     * @var int
     */
    public $count = 0;

    /**
     * @var int
     */
    public $tomatos = 0;

    public function __construct()
    {
        $this->type = 'everyday_bonus';
    }
}
