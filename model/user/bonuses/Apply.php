<?php
namespace model\user\bonuses;

use krest\base\Struct;

class Apply extends Struct
{
    /**
     * Восстановить энергию, уд. энергии
     * @var int
     */
    public $energy = 0;

    /**
     * Восстановить здоровье, ед. здоровья
     * @var int
     */
    public $hp = 0;
}
