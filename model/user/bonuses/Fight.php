<?php
namespace model\user\bonuses;

use krest\base\Struct;

class Fight extends Struct
{
    /**
     * Урон, ед. урона
     * @var int
     */
    public $damage = 0;

    /**
     * Броня, ед. брони
     * @var int
     */
    public $armor = 0;

    /**
     * Скорость, ед. скорости
     * @var int
     */
    public $speed = 0;

    /**
     * Вероятность повторного хода, %
     * @var int
     */
    public $act_again = 0;

    /**
     * Вероятность уворота, %
     * @var int
     */
    public $dodge = 0;

    /**
     * Вероятность критического удара, %
     * @var int
     */
    public $crit = 0;

    /**
     * Вероятность блока, %
     * @var int
     */
    public $block = 0;

    /**
     * Вероятность спецприема, %
     * @var int
     */
    public $special = 0;
}
