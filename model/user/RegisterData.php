<?php
namespace model\user;

use krest\base\Struct;

class RegisterData extends Struct
{
    public
        $soc_net_id,
        $login,
        $vegetable,
        $body,
        $hands,
        $eyes,
        $mouth,
        $hair,
        $referrer_type,
        $referrer,
        $city;

    protected function _requiredFields()
    {
        return array(
            'soc_net_id',
            'login',
            'vegetable',
            'body',
            'hands',
            'eyes',
            'mouth',
            'hair',
        );
    }
}
