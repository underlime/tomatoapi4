<?php
namespace model;

use base\Model;
use Currency;
use krest\Arr;
use krest\Db;
use krest\exceptions\DbException;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\SocNet;

class Items extends Model
{
    const GIFT_USUAL = 0;

    const GIFT_IMPROVED = 1;

    /**
     * @var User
     */
    private $user;

    private $putEquipmentTargets;

    private $itemsSlotsAvaible;

    private $ringSlotsAvaible;


    /**
     * Извлечь полный список предметов
     * @param bool $useShowInMarket
     * @return array|mixed
     */
    public function getItemsList($useShowInMarket = true)
    {
        $cacheKey = 'items_list_'.intval($useShowInMarket);
        $itemsList = $this->_getDataFromCache($cacheKey);
        if ($itemsList !== null) {
            return $itemsList;
        }

        $whereSection = ($useShowInMarket ? ' WHERE `show_in_market`=1 ' : '');

        $sql = "
            SELECT
                `item_id`,
                `arrangement_variant`,
                `rubric_code`,
                `name_ru`,
                `description_ru`,
                `picture`,
                `price_ferros`,
                `price_tomatos`,
                `sell_price_tomatos`,
                `buy_another`,
                `buy_another_at_once`,
                `required_level`,
                `bonuses`,
                `apply_in_fight`,
                `fight_damage`,
                `heal_percents`,
                `show_in_market`,
                `sort`
            FROM
                `items`
            $whereSection
            ORDER BY
                `sort` ASC,
                `item_id` ASC
        ";

        $itemsData = Db::instance()->select($sql);
        $itemsList = $this->_fetchItemsList($itemsData);

        $this->_setDataIntoCache($cacheKey, $itemsList);
        return $itemsList;
    }

    private function _fetchItemsList(array $itemsData)
    {
        $itemsList = array();
        foreach ($itemsData as $curItemData) {
            $itemsList[$curItemData['item_id']] = $curItemData;
        }

        return $itemsList;
    }

    /**
     * Извлечь информацию о предмете,
     * ключ item_info
     * @param int $itemId
     * @throws \krest\exceptions\NotFoundException
     * @return array
     */
    public function getItemInfo($itemId)
    {
        $itemsList = $this->getItemsList(false);
        $itemInfo = Arr::get($itemsList, $itemId);
        if ($itemInfo === null) {
            throw new NotFoundException("Item $itemId is not found");
        }

        return $itemInfo;
    }

    /**
     * Извлечь предметы по варианту экипировки
     * ссылки на покупку других предметов исключаются
     * @param string $arrangementVariant
     * @return array
     */
    public function getItemsIdsByArrangement($arrangementVariant)
    {
        $cacheKey = 'items_ids_list_by_arrangement_'.$arrangementVariant;
        $itemsIdsList = $this->_getDataFromCache($cacheKey);
        if ($itemsIdsList !== null) {
            return $itemsIdsList;
        }

        $sql = '
            SELECT
                `item_id`
            FROM
                `items`
            WHERE
                `arrangement_variant`=:arrangementVariant
                AND
                `buy_another`=0
        ';

        $params = array(':arrangementVariant' => $arrangementVariant);
        $itemsData = Db::instance()->select($sql, $params);
        $itemsIdsList = $this->_fetchItemsIdsList($itemsData);

        $this->_setDataIntoCache($cacheKey, $itemsIdsList);
        return $itemsIdsList;
    }

    private function _fetchItemsIdsList(array $itemsData)
    {
        $itemsIdsList = array();
        foreach ($itemsData as $curItemData) {
            $itemsIdsList[] = $curItemData['item_id'];
        }

        return $itemsIdsList;
    }

    /**
     * Извлечь инвентарь пользователя,
     * ключ user_inventory,
     * @param int $userId
     * @return array
     */
    public function getUsersInventory($userId)
    {
        $itemsList = $this->getItemsList(false);
        $invData = $this->_getUserInventoryData($userId);

        $inventory = array();
        foreach ($invData as $itemData) {
            $itemInfo = Arr::req($itemsList, $itemData['item_id']);
            $inventory[$itemData['item_id']] = array_merge($itemInfo, $itemData);
        }

        return $inventory;
    }

    private function _getUserInventoryData($userId)
    {
        $cacheKey = 'user_inventory_data_'.$userId;
        $invData = $this->_getDataFromCache($cacheKey);
        if ($invData !== null) {
            return $invData;
        }

        $sql = <<<SQL
SELECT
    i.item_id,
    i.count,
    i.broken_count,
    i.put,
    i.social_put,
    s.sharpening_level
FROM
    items_inventory i
    LEFT JOIN
        sharpening s
        ON s.user_id=:user_id AND s.item_id=i.item_id
WHERE
    i.user_id=:user_id
SQL;

        $sharpening = new Sharpening();
        $invRawData = Db::instance()->select($sql, array(':user_id' => $userId));
        $invData = array();
        foreach ($invRawData as $itemData) {
            if ($itemData['sharpening_level']) {
                list($sharpeningBonuses, , ,) = $sharpening->calculateSharpeningParams($itemData['item_id'],
                                                                                       $itemData['sharpening_level']);
                $itemData['sharpen_damage'] = $sharpeningBonuses->fight->damage;
                $itemData['sharpen_armor'] = $sharpeningBonuses->fight->armor;
            }
            $invData[$itemData['item_id']] = $itemData;
        }

        $this->_setDataIntoCache($cacheKey, $invData);
        return $invData;
    }

    /**
     * Извлечь инвентарь пользователя, сгруппированный
     * по месту в инвентаре
     * @param $userId
     * @return array
     */
    public function getUsersInventoryGroupByPlace($userId)
    {
        $usersInventory = $this->getUsersInventory($userId);

        $inventoryByPlace = array();
        foreach ($usersInventory as $itemId => $itemInfo) {
            $place = $this->_getItemInventoryPlace($itemInfo);
            if (!isset($inventoryByPlace[$place])) {
                $inventoryByPlace[$place] = array();
            }

            $inventoryByPlace[$place][$itemId] = $itemInfo;
        }

        return $inventoryByPlace;
    }

    private function _getItemInventoryPlace(array $itemInfo)
    {
        switch ($itemInfo['arrangement_variant']) {
            case 'drink':
            case 'grenade':
            case 'slot':
                $place = 'slot';
                break;
            case 'hat':
                $place = 'hat';
                break;
            case 'mask':
            case 'glasses':
                $place = 'mask';
                break;
            case 'cloak':
                $place = 'cloak';
                break;
            case 'left_hand':
                $place = 'shield';
                break;
            case 'right_hand':
            case 'rod':
            case 'both_hands':
            case 'heavy_weapon':
                $place = 'weapon';
                break;
            case 'armor':
                $place = 'armor';
                break;
            case 'ring':
                $place = 'ring';
                break;
            case 'amulet':
                $place = 'amulet';
                break;
            case 'boots':
                $place = 'boots';
                break;
            case 'apply':
                $place = 'apply';
                break;
            default:
                throw new InvalidDataException("Wrong arrangement variant: {$itemInfo['arrangement_variant']}");
        }

        return $place;
    }

    /**
     * Купить предмет
     * @param $userId
     * @param $itemId
     * @param int $count
     * @param string $currency
     * @return array
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     */
    public function buyItem($userId, $itemId, $count = 1, $currency = Currency::TOMATOS)
    {
        $count = (int)$count;
        if ($count < 1) {
            throw new InvalidDataException("Wrong count $count");
        }

        $itemInfo = $this->getItemInfo($itemId);
        if ($itemInfo === null) {
            throw new InvalidDataException("Wrong $itemId");
        }

        $this->user = new User();
        list($ferros, $tomatos) = Currency::getFinalPriceByItemParams($itemInfo['price_ferros'],
                                                                      $itemInfo['price_tomatos'], $currency);
        $ferros *= $count;
        $tomatos *= $count;

        $comment = 'Покупка предмета';
        $this->user->pay($userId, $ferros, $tomatos, $comment);
        $this->_giveItemToUser($userId, $count, $itemInfo);
        return $itemInfo;
    }

    private function _giveItemToUser($userId, $count, array $itemInfo)
    {
        if ($itemInfo['buy_another']) {
            $itemId = $itemInfo['buy_another'];
            $finalCount = $itemInfo['buy_another_at_once']*$count;
        }
        else {
            $itemId = $itemInfo['item_id'];
            $finalCount = $count;
        }

        $this->incDecItemCount($userId, $itemId, $finalCount);
    }

    /**
     * Увеличить/уменьшить количество предмето в инвентаре
     * @param $userId
     * @param $itemId
     * @param int $delta
     * @throws \Exception|\krest\exceptions\DbException
     * @return mixed
     */
    public function incDecItemCount($userId, $itemId, $delta = 1)
    {
        $db = Db::instance();

        $this->disableCache();
        $invData = $this->_getUserInventoryData($userId);
        $this->enableCache();

        $itemData = Arr::get($invData, $itemId, array());
        $currentCount = Arr::get($itemData, 'count', 0);
        $currentPut = Arr::get($itemData, 'put', 0);
        $currentSocPut = Arr::get($itemData, 'social_put', 0);
        $newCount = max(0, $currentCount + $delta);

        $itemInfo = $this->getItemInfo($itemId);

        // TODO: убрать после исправления TK-185
        try {
            list($sql, $params) = $this->_getIncDecQuery($currentCount, $currentPut, $currentSocPut, $newCount, $delta,
                                                         $itemInfo['apply_in_fight']);
            $params[':user_id'] = $userId;
            $params[':item_id'] = $itemId;

            $db->exec($sql, $params);
        }
        catch (DbException $e) {
            ob_start();
            echo date('Y-m-d H:i:s'), "\n";
            echo "Items::incDecItemCount\n", __FILE__, ' ', __LINE__, "\n";
            var_dump(get_defined_vars());
            \Logger::getLogger('watch')->info(ob_get_clean());
            throw $e;
        }

        $cacheKey = 'user_inventory_data_'.$userId;
        $this->_deleteDataFromCache($cacheKey);

        return $newCount;
    }

    private function _getIncDecQuery($currentCount, $currentPut, $currentSocPut, $newCount, $delta, $applyInFight)
    {
        static $DEC_PUT_TYPES = array('med_low', 'med_mid', 'med_high', 'grenade');

        $params = array();
        if ($currentCount && $newCount) {
            $sql = <<<SQL
                UPDATE
                    items_inventory
                SET
                    count=:count,
                    put=:put,
                    social_put=:social_put
                WHERE
                    user_id=:user_id
                    AND
                    item_id=:item_id
SQL;

            if ($delta < 0 && in_array($applyInFight, $DEC_PUT_TYPES)) {
                $basePut = max(0, $currentPut + $delta);
            }
            else {
                $basePut = $currentPut;
            }

            $params[':count'] = $newCount;
            $params[':put'] = min($newCount, $basePut);
            $params[':social_put'] = min($newCount, $currentSocPut);
        }
        elseif ($newCount) {
            $sql = '
                INSERT INTO
                    `items_inventory`
                SET
                    `user_id`=:user_id,
                    `item_id`=:item_id,
                    `count`=:count
            ';

            $params[':count'] = $newCount;
        }
        else {
            $sql = '
                DELETE FROM
                    `items_inventory`
                WHERE
                    `user_id`=:user_id
                    AND
                    `item_id`=:item_id
            ';
        }

        return array($sql, $params);
    }

    /**
     * Увеличить/уменьшить количество надетых предметов в инвентаре
     * @param $userId
     * @param $itemId
     * @param int $delta
     * @return int
     */
    public function incDecItemPut($userId, $itemId, $delta = 1)
    {
        $putField = 'put';
        $newPut = $this->_incDecItemSomePut($userId, $itemId, $delta, $putField);
        return $newPut;
    }

    private function _incDecItemSomePut($userId, $itemId, $delta, $putField)
    {
        $db = Db::instance();

        $this->disableCache();
        $invData = $this->_getUserInventoryData($userId);
        $this->enableCache();

        $itemData = Arr::get($invData, $itemId, array());
        $currentPut = Arr::get($itemData, $putField, 0);
        $newPut = max(0, $currentPut + $delta);

        $params = array(
            ':user_id' => $userId,
            ':item_id' => $itemId,
            ':put' => $newPut,
        );

        $sql = "UPDATE `items_inventory` SET `$putField`=:put WHERE `user_id`=:user_id AND `item_id`=:item_id";
        $db->exec($sql, $params);

        $cacheKey = 'user_inventory_data_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
        return $newPut;
    }

    /**
     * Увеличить/уменьшить количество надетых предметов в социальном слоте
     * @param $userId
     * @param $itemId
     * @param int $delta
     * @return int
     */
    public function incDecItemSocPut($userId, $itemId, $delta = 1)
    {
        $putField = 'social_put';
        $newPut = $this->_incDecItemSomePut($userId, $itemId, $delta, $putField);
        return $newPut;
    }

    /**
     * Продать предмет за томатосы
     * @param $userId
     * @param $itemId
     * @param int $count
     * @return array
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     */
    public function sellItem($userId, $itemId, $count = 1)
    {
        $count = (int)$count;
        if ($count < 1) {
            throw new InvalidDataException("Wrong count $count");
        }

        $inv = $this->getUsersInventory($userId);
        $itemInfo = Arr::req($inv, $itemId);
        if ($itemInfo['count'] < $count) {
            throw new InvalidDataException('Too little item count');
        }

        $this->user = new User();

        $ferros = 0;
        $tomatos = $itemInfo['sell_price_tomatos']*$count;
        $delta = -$count;

        $comment = 'Продажа предмета';
        $newMoney = $this->user->addMoney($userId, $ferros, $tomatos, $comment);
        $this->incDecItemCount($userId, $itemId, $delta);

        return $newMoney;
    }

    /**
     * Уменьшить количество расходников
     * @param $userId
     * @param array $decList
     * @throws \krest\exceptions\InvalidDataException
     */
    public function decUsedConsumables($userId, array $decList)
    {
        if (empty($decList)) {
            throw new InvalidDataException('Empty decList');
        }

        $this->disableCache();
        $invData = $this->_getUserInventoryData($userId);
        $this->enableCache();

        foreach ($decList as $itemId => $delta) {
            $this->_makeDecConsumable($userId, $itemId, $delta, $invData);
        }

        $cacheKey = 'user_inventory_data_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
    }

    private function _makeDecConsumable($userId, $itemId, $delta, array $invData)
    {
        $itemInfo = Arr::req($invData, $itemId);
        $newCount = max(0, $itemInfo['count'] - $delta);
        $newPut = max(0, $itemInfo['put'] - $delta);

        list($sql, $params) = $this->_getDecConsumableQuery($newCount, $newPut);
        $params[':user_id'] = $userId;
        $params[':item_id'] = $itemId;

        Db::instance()->exec($sql, $params);
    }

    private function _getDecConsumableQuery($newCount, $newPut)
    {
        $params = array();
        if ($newCount) {
            $sql = '
                UPDATE
                    `items_inventory`
                SET
                    `count`=:count,
                    `put`=:put
                WHERE
                    `user_id`=:user_id
                    AND
                    `item_id`=:item_id
            ';

            $params[':count'] = $newCount;
            $params[':put'] = $newPut;
        }
        else {
            $sql = '
                DELETE FROM
                    `items_inventory`
                WHERE
                    `user_id`=:user_id
                    AND
                    `item_id`=:item_id
            ';
        }

        return array($sql, $params);
    }

    /**
     * Применить предмет
     * @param $userId
     * @param $itemId
     * @return int Новое количество предмета
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     */
    public function applyItem($userId, $itemId)
    {
        $itemInfo = $this->_checkoutItemInfo($userId, $itemId);
        $newCount = $this->incDecItemCount($userId, $itemId, -1);
        $bonuses = trim($itemInfo['bonuses']);
        if ($bonuses) {
            $this->_applyItemData($userId, $bonuses);
        }
        return $newCount;
    }

    private function _checkoutItemInfo($userId, $itemId)
    {
        $this->disableCache();
        $invData = $this->_getUserInventoryData($userId);
        $this->enableCache();
        if (!(isset($invData[$itemId]) && $invData[$itemId]['count'] > 0)) {
            throw new InvalidDataException("Item $itemId not found in the inventory");
        }

        $itemInfo = $this->getItemInfo($itemId);
        if ($itemInfo['arrangement_variant'] != 'apply') {
            throw new InvalidDataException('Apply is not allowed');
        }

        return $itemInfo;
    }

    private function _applyItemData($userId, $xmlBonuses)
    {
        $bonuses = new user\Bonuses();
        $bonuses->importXml($xmlBonuses);

        $this->user = new User();
        $this->user->disableCache();
        $fightData = $this->user->getUserFightData($userId);
        $this->user->enableCache();

        $newValues = array();
        if ($bonuses->apply->energy) {
            $newValues['energy'] = $fightData['energy'] + $bonuses->apply->energy;
        }

        if ($bonuses->apply->hp) {
            $newValues['hp'] = $fightData['hp'] + $bonuses->apply->hp;
        }

        if ($newValues) {
            $this->user->updateUserFightData($userId, $newValues);
        }
    }

    /**
     * Экипировать снаряжение
     * @param $userId
     * @param array $putList
     * @param array $socialPutList
     * @throws \krest\exceptions\InvalidDataException
     * @throws \Exception
     */
    public function equipItems($userId, array $putList, array $socialPutList = array())
    {
        $this->user = new User();
        $levelParams = $this->user->getUserLevelParams($userId);
        $this->itemsSlotsAvaible = $levelParams['items_slots_avaible'];
        $this->ringSlotsAvaible = $levelParams['ring_slots_avaible'];

        $itemIdsList = array();
        $allItemsData = $putList;
        foreach ($allItemsData as $itemId => $count) {
            $itemIdsList[] = $itemId;
        }
        $itemIdsList = array_unique($itemIdsList);

        foreach ($itemIdsList as $itemId) {
            $itemInfo = $this->getItemInfo($itemId);
            if ($levelParams['max_level'] < $itemInfo['required_level']) {
                throw new InvalidDataException('Too little max_level to equip');
            }
        }

        $this->resetUserInventory($userId);
        $this->_makePut($userId, $putList, $socialPutList);

        $cacheKey = 'user_inventory_data_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
    }

    private function _makePut($userId, array $putList, array $socialPutList)
    {
        $this->putEquipmentTargets = $this->_createPutEquipmentTargets();
        foreach ($putList as $itemId => $count) {
            $this->_makeEquipmentPutTargetCheck($itemId, $count);
        }

        $this->putEquipmentTargets = $this->_createSpcialPutEquipmentTargets();
        foreach ($socialPutList as $itemId => $count) {
            $this->_makeEquipmentSocialPutTargetCheck($itemId, $count);
        }

        foreach ($putList as $itemId => $count) {
            $this->_makeEquipmentQuery($userId, $itemId, $count, 'put');
        }

        foreach ($socialPutList as $itemId => $count) {
            $this->_makeEquipmentQuery($userId, $itemId, $count, 'social_put');
        }
    }

    private function _createPutEquipmentTargets()
    {
        return array(
            'slot' => 0,
            'hat' => 0,
            'mask' => 0,
            'cloak' => 0,
            'weapon' => 0,
            'shield' => 0,
            'armor' => 0,
            'ring' => 0,
            'amulet' => 0,
            'boots' => 0,
        );
    }

    private function _makeEquipmentPutTargetCheck($itemId, $count)
    {
        $itemInfo = $this->getItemInfo($itemId);
        switch ($itemInfo['arrangement_variant']) {
            case 'drink':
            case 'grenade':
            case 'slot':
                $this->putEquipmentTargets['slot'] += $count;
                $error = ($this->putEquipmentTargets['slot'] > $this->itemsSlotsAvaible);
                break;
            case 'hat':
                $this->putEquipmentTargets['hat'] += $count;
                $error = ($this->putEquipmentTargets['hat'] > 1);
                break;
            case 'mask':
            case 'glasses':
                $this->putEquipmentTargets['mask'] += $count;
                $error = ($this->putEquipmentTargets['mask'] > 1);
                break;
            case 'cloak':
                $this->putEquipmentTargets['cloak'] += $count;
                $error = ($this->putEquipmentTargets['cloak'] > 1);
                break;
            case 'left_hand':
                $this->putEquipmentTargets['shield'] += $count;
                $error = ($this->putEquipmentTargets['shield'] > 1);
                break;
            case 'right_hand':
            case 'rod':
                $this->putEquipmentTargets['weapon'] += $count;
                $error = ($this->putEquipmentTargets['weapon'] > 1);
                break;
            case 'both_hands':
            case 'heavy_weapon':
                $this->putEquipmentTargets['weapon'] += $count;
                $this->putEquipmentTargets['shield'] += $count;
                $error = ($this->putEquipmentTargets['weapon'] > 1 or $this->putEquipmentTargets['shield'] > 1);
                break;
            case 'armor':
                $this->putEquipmentTargets['armor'] += $count;
                $error = ($this->putEquipmentTargets['armor'] > 1);
                break;
            case 'ring':
                $this->putEquipmentTargets['ring'] += $count;
                $error = ($this->putEquipmentTargets['ring'] > $this->ringSlotsAvaible);
                break;
            case 'amulet':
                $this->putEquipmentTargets['amulet'] += $count;
                $error = ($this->putEquipmentTargets['amulet'] > 1);
                break;
            case 'boots':
                $this->putEquipmentTargets['boots'] += $count;
                $error = ($this->putEquipmentTargets['boots'] > 1);
                break;
            default:
                $error = true;
        }

        if ($error) {
            throw new InvalidDataException('Equipment error');
        }
    }

    private function _createSpcialPutEquipmentTargets()
    {
        return array(
            'hat' => 0,
            'mask' => 0,
            'cloak' => 0,
            'weapon' => 0,
            'shield' => 0,
        );
    }

    private function _makeEquipmentSocialPutTargetCheck($itemId, $count)
    {
        $itemInfo = $this->getItemInfo($itemId);
        switch ($itemInfo['arrangement_variant']) {
            case 'hat':
                $this->putEquipmentTargets['hat'] += $count;
                $error = ($this->putEquipmentTargets['hat'] > 1);
                break;
            case 'mask':
            case 'glasses':
                $this->putEquipmentTargets['mask'] += $count;
                $error = ($this->putEquipmentTargets['mask'] > 1);
                break;
            case 'cloak':
                $this->putEquipmentTargets['cloak'] += $count;
                $error = ($this->putEquipmentTargets['cloak'] > 1);
                break;
            case 'left_hand':
                $this->putEquipmentTargets['shield'] += $count;
                $error = ($this->putEquipmentTargets['shield'] > 1);
                break;
            case 'right_hand':
            case 'rod':
                $this->putEquipmentTargets['weapon'] += $count;
                $error = ($this->putEquipmentTargets['weapon'] > 1);
                break;
            case 'both_hands':
            case 'heavy_weapon':
                $this->putEquipmentTargets['weapon'] += $count;
                $this->putEquipmentTargets['shield'] += $count;
                $error = ($this->putEquipmentTargets['weapon'] > 1 or $this->putEquipmentTargets['shield'] > 1);
                break;
            default:
                $error = true;
        }

        if ($error) {
            throw new InvalidDataException('Social equipment error');
        }
    }

    private function _makeEquipmentQuery($userId, $itemId, $count, $slot)
    {
        $sql = "UPDATE `items_inventory` SET `$slot`=:count WHERE `user_id`=:user_id AND `item_id`=:item_id";
        $params = array(
            ':count' => $count,
            ':user_id' => $userId,
            ':item_id' => $itemId,
        );

        Db::instance()->exec($sql, $params);
    }


    /**
     * Сбросить инвентарь пользователя
     * @param $userId
     */
    public function resetUserInventory($userId)
    {
        $sql = 'UPDATE `items_inventory` SET `put`=0, `social_put`=0 WHERE `user_id`=:user_id';
        Db::instance()->exec($sql, array(':user_id' => $userId));

        $cacheKey = 'user_inventory_data_'.$userId;
        $this->_deleteDataFromCache($cacheKey);
    }

    /**
     * Извлечь id предмета для подарка
     * @param int $giftType
     * @return int
     */
    public function getGiftItemId($giftType = self::GIFT_USUAL)
    {
        $sql = 'SELECT item_id FROM items WHERE gift_type=:gift_type ORDER BY RAND() LIMIT 1';
        $params = array(
            ':gift_type' => ($giftType == self::GIFT_IMPROVED) ? 'improved' : 'usual',
        );

        $data = Db::instance()
                ->select($sql, $params);

        $itemData = Arr::req($data, 0);
        return Arr::req($itemData, 'item_id');
    }

    /**
     * Добавить запись в статистику покупок
     * @param int $itemId
     * @param int $userId
     * @param int $count
     * @param string $currency f или t
     */
    public function addMarketStatisticsRecord($itemId, $userId, $count, $currency)
    {
        $itemInfo = $this->getItemInfo($itemId);
        $date = date('Y-m-d');

        $sql = <<<SQL
            INSERT DELAYED
            INTO
                market_statistics
            SET
                item_id=:itemId,
                name_ru=:nameRu,
                user_id=:userId,
                count=:count,
                date=:date,
                currency=:currency
SQL;

        Db::instance()->exec($sql, array(
                                           ':itemId' => $itemId,
                                           ':nameRu' => $itemInfo['name_ru'],
                                           ':userId' => $userId,
                                           ':count' => $count,
                                           ':date' => $date,
                                           ':currency' => $currency,
                                      ));
    }

    /**
     * Передать предмет
     * @param $itemId
     * @param $fromUserId
     * @param $toUserId
     * @param int $count
     * @throws \krest\exceptions\InvalidDataException
     */
    public function transferItem($itemId, $fromUserId, $toUserId, $count = 1)
    {
        $count = (int)$count;
        if ($count < 1) {
            throw new InvalidDataException("Invalid count: $count");
        }

        $clans = new Clans();
        $db = Db::instance();

        $clanId1 = $clans->getUsersClan($fromUserId);
        if ($clanId1 === null) {
            throw new InvalidDataException("User $fromUserId is not an any clan member");
        }

        $clanId2 = $clans->getUsersClan($toUserId);
        if ($clanId2 === null) {
            throw new InvalidDataException("User $toUserId is not an any clan member");
        }
        elseif ($clanId2 !== $clanId1) {
            throw new InvalidDataException("Users are not the same clan members");
        }

        $fromItemData = $db->select("SELECT `count` FROM items_inventory WHERE item_id=:itemId AND user_id=:fromId",
                                    [":itemId" => $itemId, ":fromId" => $fromUserId]);
        if (!$fromItemData) {
            throw new InvalidDataException("User $fromUserId has not item $itemId");
        }

        if ($fromItemData[0]["count"] < $count) {
            throw new InvalidDataException("User $fromItemData has not enough of $itemId");
        }

        $newCount = $fromItemData[0]["count"] - $count;
        $db->exec("UPDATE items_inventory SET `count`=:newCount WHERE item_id=:itemId AND user_id=:fromId",
                  [":newCount" => $newCount, ":itemId" => $itemId, ":fromId" => $fromUserId]);


        $toItemData = $db->select("SELECT `count` FROM items_inventory WHERE item_id=:itemId AND user_id=:toId",
                                  [":itemId" => $itemId, ":toId" => $toUserId]);
        if (!$toItemData || $toItemData[0]["count"] == 0) {
            $db->exec("INSERT INTO items_inventory SET item_id=:itemId, user_id=:toId, `count`=:newCount",
                      [":itemId" => $itemId, ":toId" => $toUserId, ":newCount" => $count]);
        }
        else {
            $newCount = $toItemData[0]["count"] + $count;
            $db->exec("UPDATE items_inventory SET `count`=:newCount WHERE item_id=:itemId AND user_id=:toId",
                      [":itemId" => $itemId, ":toId" => $toUserId, ":newCount" => $newCount]);
        }

        $this->_deleteDataFromCache("user_inventory_data_$fromUserId");
        $this->_deleteDataFromCache("user_inventory_data_$toUserId");

        $user = new User();
        $user->disableCache();
        $fromFightData = $user->getUserFightData($fromUserId);
        $user->enableCache();
        $user->updateUserFightData($fromUserId, ['items_given' => $fromFightData['items_given'] + 1]);

        $toSocNetId = $user->getSocNetIdByUserId($toUserId);
        $fromUserInfo = $user->getUserInfoByUserId($fromUserId);
        $itemInfo = $this->getItemInfo($itemId);

        $header = \Messages::factory()['transfer_item_header'];
        $message = \Messages::factory()['transfer_item_notification'];
        $message = str_replace(
            ['{user_name}', '{item_name}'],
            [$fromUserInfo['login'], $itemInfo['name_ru']], // TODO: i18n
            $message
        );
        SocNet::instance()->sendNotification([$toSocNetId], $message);

        $news = new UserNews();
        $news->addMessageNews($toUserId, $header, $message);
    }
}
