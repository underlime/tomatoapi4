<?php
namespace model;

use base\Model;
use krest\api\Config;
use krest\Arr;
use krest\exceptions\InvalidDataException;
use lib\RediskaFactory;

/**
 * Модель для работы с усилениями (Цитадель ГМО)
 * Class Gains
 * @package model
 */
class Gains extends Model
{
    const EXPERIENCE = 'experience';
    const GLORY = 'glory';
    const ENERGY = 'energy';
    const DAMAGE = 'damage';
    const ARMOR = 'armor';
    const FIGHT_TOMATOS = 'fight_tomatos';

    /**
     * @var \Rediska
     */
    private $_rediska;

    private $_config;

    public function __construct()
    {
        parent::__construct();
        $this->_config = Config::instance()->getForClass(__CLASS__);
        $this->_rediska = RediskaFactory::getInstance();
    }

    /**
     * Извлечь цены усилений
     * @return mixed
     */
    public function getPrices()
    {
        return Arr::req($this->_config, 'prices');
    }

    /**
     * Установить увеличение "объекта"
     * @param int $userId
     * @param int $hours
     * @param string $gainObject (название улучшаемого параметра: experience, glory и т. д.)
     * @throws \krest\exceptions\InvalidDataException
     * @return array array($expireDate, $newMoney)
     */
    public function setGain($userId, $hours, $gainObject)
    {
        $this->_validateGainObject($gainObject);

        $userId = (int)$userId;
        $hours = (int)$hours;


        $price = Arr::req(Arr::req($this->getPrices(), $gainObject), $hours);
        $userModel = new User();
        $newMoney = $userModel->pay($userId, $price, 0, "Увеличение $gainObject на время $hours");


        $keyName = "gains:$userId:$gainObject";

        $time = new \DateTime();
        $time->add(new \DateInterval("PT{$hours}H"));
        $expireDate = $time->format('Y-m-d H:i:s');
        $expireSeconds = $time->getTimestamp() - time();

        $this->_rediska->set($keyName, $expireDate, $expireSeconds);
        $this->_rediska->expire($keyName, $expireSeconds);
        return array($expireDate, $newMoney);
    }

    private function _validateGainObject($gainObject)
    {
        if (!in_array($gainObject, array(
                                        self::EXPERIENCE, self::GLORY, self::ENERGY, self::DAMAGE,
                                        self::ARMOR, self::FIGHT_TOMATOS
                                   ))
        ) {
            throw new InvalidDataException('Wrong gain object name');
        }
    }

    /**
     * Извлечь все увеличения пользователя
     * @param int $userId
     * @return array
     */
    public function getUserGains($userId)
    {
        $userId = (int)$userId;

        $keysPattern = "gains:$userId:*";
        $keys = $this->_rediska->getKeysByPattern($keysPattern);

        if (empty($keys)) {
            return array();
        }

        $redisData = $this->_rediska->get($keys);
        $gainsData = array();

        $pattern = '#^gains:\d+:(?P<gain_object>\w+)$#';
        foreach ($redisData as $key => $val) {
            if (preg_match($pattern, $key, $matches)) {
                $gainsData[$matches['gain_object']] = $val;
            }
        }

        return $gainsData;
    }

    /**
     * Проверить наличие того или иного усиления
     * @param int $userId
     * @param string $gainObject
     * @return bool
     */
    public function hasUserGain($userId, $gainObject)
    {
        $this->_validateGainObject($gainObject);
        $keyName = "gains:$userId:$gainObject";
        return $this->_rediska->exists($keyName);
    }
}
