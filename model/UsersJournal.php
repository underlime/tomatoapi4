<?php
namespace model;

use base\Model;
use krest\Arr;
use krest\Db;
use krest\exceptions\InvalidDataException;
use krest\strings\SqlQuery;

/**
 * Модель журнала пользователя.
 * В данный момент используется только для журнала боевой сводки
 * Class UsersJournal
 * @package model
 */
class UsersJournal extends Model
{
    const WIN = 'win';
    const FAIL = 'fail';

    private static $allowedEventTypes = array(
        self::WIN,
        self::FAIL
    );

    /**
     * Извлечь последние сообщения журнала,
     * ключ user_journal
     * @param int $userId
     * @param int $limit
     * @return array
     */
    public function getUsersMessages($userId, $limit = 50)
    {
        $cacheKey = 'users_journal_'.$userId.'_'.$limit;
        $journal = $this->_getDataFromCache($cacheKey);
        if ($journal !== null) {
            return $journal;
        }

        $limit = (int)$limit;
        $sql = <<<SQL
          SELECT
            event_id,
            subject_user_id,
            event_type,
            `time`,
            description,
            experience,
            glory,
            tomatos,
            ferros,
            `level`
          FROM
            users_journal
          WHERE
            user_id=:user_id
          ORDER BY
            event_id DESC
          LIMIT {$limit}
SQL;

        $journal = Db::instance()->select($sql, array(':user_id' => $userId));

        $this->_setDataIntoCache($cacheKey, $journal, 300);
        return $journal;
    }

    /**
     * Добавить сообщение в журнал
     * @param int $userId
     * @param $eventType
     * @param $subjectUserId
     * @param \model\usersjournal\ProfitData $profit
     * @param string $description
     * @throws \krest\exceptions\InvalidDataException
     * @return void
     */
    public function addMessage(
        $userId, $eventType, $subjectUserId, usersjournal\ProfitData $profit = null, $description = ''
    ) {
        if (!in_array($eventType, self::$allowedEventTypes)) {
            throw new InvalidDataException('Wrong event type');
        }

        if ($profit === null) {
            $profit = new usersjournal\ProfitData();
        }

        $sql = <<<SQL
            INSERT INTO
                users_journal
            SET
                user_id=:user_id,
                event_type=:event_type,
                subject_user_id=:subject_user_id,
                description=:description,
                time=:time
SQL;

        $params = array(
            ':user_id' => $userId,
            ':event_type' => $eventType,
            ':subject_user_id' => $subjectUserId,
            ':description' => $description,
            ':time' => date('Y-m-d H:i:s'),
        );


        $profitData = $profit->asArrayNotEmpty();
        if ($profitData) {
            $fields = array_keys($profitData);
            $sql .= ', '.SqlQuery::buildSetSection($fields);
            $params = SqlQuery::buildQueryParams($profitData, $params);
        }

        Db::instance()->exec($sql, $params);
    }

    /**
     * Удалить сообщения, которые старше времени
     * @param int $time
     * @return int
     */
    public function deleteMessagesEarlierThanTime($time)
    {
        $sql = 'DELETE FROM users_journal WHERE time <= :date';
        $params = array(
            ':date' => date('Y-m-d H:i:s', $time)
        );

        $result = Db::instance()->delete($sql, $params);
        return $result['affected_rows'];
    }
}