<?php
namespace model;

use base\Model;
use Currency;
use krest\Arr;
use krest\SocNet;
use krest\Db;
use krest\api\Config;

class Bank extends Model
{
    static private $config;

    public function __construct()
    {
        parent::__construct();
        if (self::$config === null) {
            self::$config = Config::instance()->getForClass(__CLASS__);
        }
    }

    /**
     * Извлечь курс валют
     * ключ currency_rates
     * @return array
     */
    public function getCurrencyRates()
    {
        $allRates = Arr::req(self::$config, "currency_rates");
        $rates = Arr::req($allRates, SocNet::getSocNetName());
        if (Arr::get(self::$config, "payments_testing") === true) {
            foreach ($rates as &$record) {
                $record["price"] = 1;
            }
        }
        return $rates;
    }

    /**
     * Добавить запись в статистику
     * @param string $socNetId
     * @param int $ferros
     * @param int $tomatos
     */
    public function addStatisticsRecord($socNetId, $ferros, $tomatos)
    {
        $date = date('Y-m-d');

        $sql = 'INSERT DELAYED INTO bank_statistics SET soc_net_id=:socNetId, ferros=:ferros, tomatos=:tomatos, date=:date';
        Db::instance()->exec($sql, array(
                                           ':socNetId' => $socNetId,
                                           ':ferros' => $ferros,
                                           ':tomatos' => $tomatos,
                                           ':date' => $date,
                                      ));
    }
}
