<?php
namespace model;

use base\Model;
use krest\api\Config;
use krest\Arr;
use krest\Db;
use lib\RediskaFactory;
use php_rutils\RUtils;

class ClansClash extends Model
{
    /**
     * @var \Rediska
     */
    private $_rediska;

    /**
     * @var \model\Clans
     */
    private $_clans;

    /**
     * @var \model\User
     */
    private $_user;

    private $_config;
    private $_dbInstId;

    public function __construct($dbInstId = null)
    {
        parent::__construct();
        $this->_dbInstId = $dbInstId;
        $this->_config = Config::instance()->getForClass(__CLASS__);
        $namespace = $dbInstId ? "tomato_kombat:$dbInstId:" : null;
        $this->_rediska = RediskaFactory::getInstance($namespace);
        $this->_clans = new Clans();
        $this->_user = new User();
    }

    /**
     * Длительность сезона
     * @return string
     */
    public function getSeasonDuration()
    {
        return Arr::req($this->_config, 'season_duration');
    }

    /**
     * Количество кланов,
     * среди которых распределяется награда
     * @return int
     */
    function getAwardCount()
    {
        return 3;
    }

    /**
     * Длительность сезона
     * @return string
     */
    public function getConfigSeasonPrize()
    {
        return Arr::req($this->_config, 'season_prize');
    }

    /**
     * Максимальный приз
     * @return int
     */
    public function getConfigMaxUserPrize()
    {
        return Arr::req($this->_config, 'max_user_prize');
    }

    /**
     * Начать новый сезон
     */
    public function startSeason()
    {
        $this->_awardClans();
        $this->_clans->setPictures();
        $this->_clans->writePreviousPlaces();
        $this->_clans->resetGlory();
        $this->_user->resetSeasonGlory();

        $seasonEnd = new \DateTime();
        $seasonEnd->add(new \DateInterval('P' . $this->getSeasonDuration()));

        $pipeline = $this->_rediska->pipeline();
        $pipeline->increment('clans_clash:season_id');
        $pipeline->setToHash('clans_clash:current_season', array(
                                                                'season_end' => $seasonEnd->format('Y-m-d H:i:s'),
                                                                'season_prize' => $this->getConfigSeasonPrize(),
                                                                'max_user_prize' => $this->getConfigMaxUserPrize(),
                                                           ));
        $pipeline->execute();
        Model::resetCache();
    }

    private function _awardClans()
    {
        $seasonInfo = $this->getSeasonInfo();
        $ids = array_slice($this->_clans->getTop(), 0, 3);

        $prize = $this->getConfigSeasonPrize();
        $maxPrize = $this->getConfigMaxUserPrize();
        if ($seasonInfo) {
            $prize = Arr::get($seasonInfo, 'season_prize', $prize);
            $maxPrize = Arr::get($seasonInfo, 'max_user_prize', $maxPrize);
        }
        $prizesProportions = [0.5, 0.3, 0.2];

        $prizesTable = [];
        foreach ($ids as $c => $clanId) {
            $clanPrize = $prize * $prizesProportions[$c];
            $users = $this->_clans->getClanMembers($clanId);
            $usersData = [];
            $clanGlory = 0;
            foreach ($users as $userId) {
                $fightData = $this->_user->getUserFightData($userId);
                $usersData[$userId] = $fightData['season_glory'];
                $clanGlory += $fightData['season_glory'];
            }

            if ($clanGlory) {
                foreach ($usersData as $userId => $glory) {
                    $k = $glory / $clanGlory;
                    $userPrize = min($maxPrize, floor($k * $clanPrize));
                    if ($userPrize) {
                        $this->_user->addMoney($userId, $userPrize, 0, 'Турнир кланов');
                    }
                    $prizesTable[$userId] = $userPrize;
                }
            }
        }

        $news = new UserNews();
        $header = \Messages::factory()['clans_clash_prize_header'];
        $message = \Messages::factory()['clans_clash_prize_message'];
        foreach ($prizesTable as $userId => $prizeVal) {
            $prize = RUtils::numeral()->getPlural($prizeVal, ['феррос', 'ферроса', 'ферросов']); // TODO: i18n
            $userMessage = str_replace('{prize}', $prize, $message);
            $news->addMessageNews($userId, $header, $userMessage);
        }
    }

    /**
     * Извлечь информацию о текущем сезоне
     * @return array
     */
    public function getSeasonInfo()
    {
        $data = $this->_rediska->getHash('clans_clash:current_season');
        if ($data) {
            $data['id'] = $this->_rediska->get('clans_clash:season_id');
        }
        return $data;
    }
}
