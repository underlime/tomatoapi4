<?php
namespace model\chardetails;

use krest\base\Struct;

class DetailsList extends Struct
{
    public
        $body,
        $hands,
        $hair,
        $mouth,
        $eyes;
}
