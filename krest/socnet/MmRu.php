<?php
namespace krest\socnet;

use krest\api\Config;
use krest\Arr;
use krest\exceptions\HttpException;
use krest\exceptions\InvalidDataException;
use krest\Request;
use krest\SocNet;

class MmRu extends SocNet
{
    private $_localConfig;

    private $_queryParamsJson;
    private $_queryParams;
    private $_sig;
    private $_sessionKey;

    protected function __construct()
    {
        parent::__construct();
        $this->_localConfig = Config::instance()->getForClass(__CLASS__);
    }

    /**
     * @throws \krest\exceptions\HttpException
     */
    public function checkAuth()
    {
        $this->_setAuthParams();
        $rightSig = $this->makeSignature($this->_queryParams);
        if (!$this->_socNetId || $this->_sig !== $rightSig) {
            throw new HttpException(400, "Auth params are wrong");
        }
    }

    private function _setAuthParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->_queryParamsJson = Arr::req($requestParams, "query_params");
        $this->_queryParams = json_decode($this->_queryParamsJson, true);
        if (!is_array($this->_queryParams)) {
            throw new InvalidDataException("Wrong query params format");
        }
        $this->_socNetId = Arr::req($this->_queryParams, "vid");
        $this->_sig = Arr::req($this->_queryParams, "sig");
        $this->_sessionKey = Arr::req($this->_queryParams, "session_key");
    }

    /**
     * Создать верную подпись запроса
     * @param array $queryParams
     * @return string
     */
    public function makeSignature(array $queryParams)
    {
        ksort($queryParams);
        $base = "";
        foreach ($queryParams as $name => $value) {
            if ($name != "sig") {
                $base .= "{$name}={$value}";
            }
        }
        $base .= Arr::req($this->_localConfig, "api_secret");
        return md5($base);
    }

    /**
     * Создать токен для проверки
     * @return string
     */
    public function makeToken()
    {
        if ($this->_sig === null) {
            $this->_setAuthParams();
        }
        $requestUri = Request::instance()->getRequestUri();
        if (substr($requestUri, -1) !== "/") {
            $requestUri .= "/";
        }
        $salt = Arr::req($this->_config, "token_salt");
        $base = "{$this->_queryParamsJson}{$requestUri}{$salt}";
        return md5($base);
    }

    /**
     * Извлечь список друзей
     * @param string $socNetId
     * @return array
     */
    public function getFriendsList($socNetId)
    {
        //TODO: реализовать
        return [];
    }

    /**
     * @param array $socNetIdsList
     * @param $message
     * @return array
     */
    public function sendNotification(array $socNetIdsList, $message)
    {
        //TODO: реализовать
    }

    /**
     * Извлечь API_SECRET
     * @return string
     */
    public function getApiSecret()
    {
        return Arr::req($this->_localConfig, "api_secret");
    }
}
