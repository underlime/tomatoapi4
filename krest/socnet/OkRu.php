<?php
namespace krest\socnet;

use krest\api\Config;
use krest\Arr;
use krest\exceptions\ExternalRequestException;
use krest\exceptions\HttpException;
use krest\exceptions\InvalidDataException;
use krest\Request;
use krest\SocNet;
use krest\UrlRequest;

class OkRu extends SocNet
{
    private $_localConfig;

    private $_queryParamsJson;
    private $_queryParams;
    private $_sig;
    private $_apiServer;
    private $_applicationKey;
    private $_sessionKey;


    protected function __construct()
    {
        parent::__construct();
        $this->_localConfig = Config::instance()->getForClass(__CLASS__);
    }

    /**
     * @throws \krest\exceptions\HttpException
     */
    public function checkAuth()
    {
        $this->_setAuthParams();
        $rightSig = $this->makeSignature($this->_queryParams);
        if (!$this->_socNetId || $this->_sig !== $rightSig) {
            throw new HttpException(400, "Auth params are wrong");
        }
    }

    private function _setAuthParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->_queryParamsJson = Arr::req($requestParams, "query_params");
        $this->_queryParams = json_decode($this->_queryParamsJson, true);
        if (!is_array($this->_queryParams)) {
            throw new InvalidDataException("Wrong query params format");
        }
        $this->_socNetId = Arr::req($this->_queryParams, "logged_user_id");
        $this->_sig = Arr::req($this->_queryParams, "sig");
        $this->_applicationKey = Arr::req($this->_queryParams, "application_key");
        $this->_sessionKey = Arr::req($this->_queryParams, "session_key");
        $this->_apiServer = Arr::get($this->_queryParams, "api_server", "http://api.odnoklassniki.ru/");
    }

    /**
     * Создать верную подпись запроса
     * @param array $queryParams
     * @return string
     */
    public function makeSignature(array $queryParams)
    {
        ksort($queryParams);
        $base = "";
        foreach ($queryParams as $name => $value) {
            if ($name != "sig") {
                $base .= "{$name}={$value}";
            }
        }
        $base .= Arr::req($this->_localConfig, "api_secret");
        return md5($base);
    }

    /**
     * Создать токен для проверки
     * @return string
     */
    public function makeToken()
    {
        if ($this->_apiServer === null) {
            $this->_setAuthParams();
        }
        $requestUri = Request::instance()->getRequestUri();
        if (substr($requestUri, -1) !== "/") {
            $requestUri .= "/";
        }
        $salt = Arr::req($this->_config, "token_salt");
        $base = "{$this->_queryParamsJson}{$requestUri}{$salt}";
        return md5($base);
    }

    /**
     * Извлечь список друзей
     * @param string $socNetId
     * @return array
     */
    public function getFriendsList($socNetId)
    {
        return $this->_makeApiCall('friends.get', ['uid' => $socNetId]);
    }

    private function _makeApiCall($method, $params = [])
    {
        $params['method'] = $method;
        $params['application_key'] = Arr::req($this->_localConfig, 'api_key');
        $params['format'] = 'JSON';
        $params['sig'] = $this->makeSignature($params);

        $url = $this->_apiServer.'/fb.do?'.http_build_query($params);

        $urlRequest = new UrlRequest();
        $result = $urlRequest
                  ->setUrl($url)
                  ->execute();

        if ($result->httpCode < 200 or $result->httpCode > 299) {
            throw new ExternalRequestException("Social network error {$result->httpCode}");
        }

        $answer = json_decode($result->body, true);
        if (!empty($answer['error_msg'])) {
            throw new ExternalRequestException($answer['error_msg']);
        }
        return $answer;
    }

    /**
     * @param array $socNetIdsList
     * @param $message
     * @return array
     */
    public function sendNotification(array $socNetIdsList, $message)
    {
        $params = ['text' => $message];
        $results = [];
        $needSleep = sizeof($socNetIdsList) > 1;
        foreach($socNetIdsList as $socNetId) {
            $params['uid'] = $socNetId;
            try {
                $results[] = $this->_makeApiCall('notifications.sendSimple', $params);
                if ($needSleep) {
                    sleep(0.35);
                }
            }
            catch (\Exception $e) {
            }
        }
        return $results;
    }

    /**
     * Извлечь API_SECRET
     * @return string
     */
    public function getApiSecret()
    {
        return Arr::req($this->_localConfig, "api_secret");
    }
}
