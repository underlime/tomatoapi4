<?php
namespace krest\socnet;

use Exception;
use krest\api\Config;
use krest\Arr;
use krest\exceptions\ExternalRequestException;
use krest\exceptions\HttpException;
use krest\Obj;
use krest\Request;
use krest\SocNet;
use krest\UrlRequest;

class VkCom extends SocNet
{
    protected $authKey, $sid;
    private $vkConfig;

    protected function __construct()
    {
        parent::__construct();
        $this->vkConfig = Config::instance()->getForClass(__CLASS__);
    }

    /**
     * @throws \krest\exceptions\HttpException
     */
    public function checkAuth()
    {
        $this->_setAuthParams();
        $rightAuthKey = $this->makeAuthKey();
        if (strcmp($this->authKey, $rightAuthKey)) {
            throw new HttpException(400, 'Auth params are wrong');
        }
    }

    private function _setAuthParams()
    {
        $requestParams = Request::instance()->getRequestBodyParams();

        $this->authKey = Arr::get($requestParams, 'auth_key');
        $this->sid = Arr::get($requestParams, 'sid');
        $this->_socNetId = Arr::get($requestParams, 'viewer_id');

        if (empty($this->authKey) or empty($this->sid) or empty($this->_socNetId)) {
            throw new HttpException(400, 'Auth params not set');
        }
    }

    /**
     * Создать токен для проверки
     * @return string
     */
    public function makeToken()
    {
        if (!$this->authKey) {
            $this->_setAuthParams();
        }

        $salt = Arr::req($this->_config, 'token_salt');
        $queryString = Request::instance()->getRequestUri();

        $baseString = $this->authKey.$this->sid.$this->_socNetId.$queryString.$salt;
        $token = md5($baseString);

        return $token;
    }

    /**
     * Создать auth_key
     * @return string
     */
    public function makeAuthKey()
    {
        $apiId = Arr::req($this->vkConfig, 'api_id');
        $apiSecret = Arr::req($this->vkConfig, 'api_secret');

        $baseString = "{$apiId}_{$this->_socNetId}_{$apiSecret}";
        $authKey = md5($baseString);

        return $authKey;
    }

    /**
     * Извлечь список друзей
     * @param string $socNetId
     * @return array
     */
    public function getFriendsList($socNetId)
    {
        $result = $this->_makeApiCall('friends.get', array('uid' => $socNetId));

        $idsList = array();
        foreach ($result->response as $record) {
            $idsList[] = $record->uid;
        }

        return $idsList;
    }

    private function _makeApiCall($method, array $params = array())
    {
        $rand = mt_rand();
        $time = time();

        $params['api_id'] = Arr::req($this->vkConfig, 'api_id');
        $params['format'] = 'json';
        $params['method'] = $method;
        $params['rand'] = $rand;
        $params['timestamp'] = $time;

        if ($this->sid) {
            $params['sid'] = $this->sid;
        }

        ksort($params, SORT_STRING);

        $secretString = '';
        foreach ($params as $name => $value) {
            $secretString .= $name.'='.$value;
        }

        $secretString .= Arr::req($this->vkConfig, 'api_secret');
        $sig = md5($secretString);

        $requestUrl = "http://api.vk.com/api.php?sig={$sig}";
        $urlRequest = new UrlRequest();
        $result = $urlRequest
                  ->setUrl($requestUrl)
                  ->setQueryParams($params)
                  ->execute();

        if ($result->httpCode < 200 or $result->httpCode > 299) {
            throw new ExternalRequestException("Social network error {$result->httpCode}");
        }

        $answer = json_decode($result->body);
        $error = Obj::get($answer, 'error');
        if ($error) {
            throw new ExternalRequestException($error);
        }

        return $answer;
    }

    /**
     * @param array $socNetIdsList
     * @param $message
     * @return array
     */
    public function sendNotification(array $socNetIdsList, $message)
    {
        if (defined('PHPUNIT_TESTING')) {
            return [];
        }

        $params = array(
            'uids' => null,
            'message' => $message,
        );

        $length = sizeof($socNetIdsList);
        $sendAtOnce = Arr::get($this->vkConfig, 'send_at_once_notifications', 1);
        $timeout = Arr::get($this->vkConfig, 'notifications_timeout', 1);
        $sentList = array();

        while ($length > 0) {
            $localIdsList = array();
            $localLength = min($length, $sendAtOnce);

            for ($i = 0; $i < $localLength; ++$i) {
                $localIdsList[] = $socNetIdsList[$i];
            }
            array_splice($socNetIdsList, 0, $localLength);

            $params['uids'] = implode(',', $localIdsList);
            try {
                $localResult = $this->_makeApiCall('secure.sendNotification', $params);
                $localSentList = explode(',', $localResult->response);
                $sentList = array_merge($sentList, $localSentList);
            }
            catch (\Exception $e) {
            }

            $length = sizeof($socNetIdsList);
            sleep($timeout);
        }

        return $sentList;
    }

    /**
     * Извлечь API_SECRET
     * @return string
     */
    public function getApiSecret()
    {
        return Arr::req($this->vkConfig, 'api_secret');
    }
}
