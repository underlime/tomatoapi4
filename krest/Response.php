<?php
namespace krest;

class Response
{
    private static $instance;

    private $headers = array();
    private $cookies = array();
    private $responseBody = '';

    /**
     * Вернуть инстанс объекта
     * @return \krest\Response
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    final protected function __construct()
    {
    }

    public function __clone()
    {
        throw new \Exception('Clone not avaible');
    }

    public function __wakeup()
    {
        throw new \Exception('Clone not wakeup');
    }

    /**
     * Установить заголовок
     * @param string $name Имя заголовка
     * @param string $value Значение заголовка
     * @return Response
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * Установить новое тело ответа
     * @param string $content
     * @return Response
     */
    public function setNewBody($content)
    {
        $this->responseBody = $content;
        return $this;
    }

    /**
     * Добавить данные к телу ответа
     * перед существующими данными
     * @param string $content
     * @return Response
     */
    public function prependDataToBody($content)
    {
        $this->responseBody = $content.$this->responseBody;
        return $this;
    }

    /**
     * Добавить данные к телу ответа
     * после существующих данных
     * @param string $content
     * @return Response
     */
    public function appendDataToBody($content)
    {
        $this->responseBody .= $content;
        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @param mixed $lifeTime
     * @param string $path
     * @param string $domain
     */
    public function setCookie($name, $value, $lifeTime = null, $path = '/', $domain = null)
    {
        if ($lifeTime === null) {
            $lifeTime = 3600;
        }

        if ($domain === null) {
            $domain = $_SERVER['HTTP_HOST'];
        }

        $expire = time() + $lifeTime;
        $this->cookies[] = array($name, $value, $expire, $path, $domain);
    }

    /**
     * Отправить заголовки
     * @return Response
     */
    public function sendHeaders()
    {
        foreach ($this->cookies as $cookieParams) {
            $this->_setCookie($cookieParams);
        }

        foreach ($this->headers as $name => $value) {
            header("$name: $value");
        }

        $this->cookies = array();
        $this->headers = array();
        return $this;
    }

    private function _setCookie(array $cookieParams)
    {
        $name = Arr::req($cookieParams, 0);
        $value = Arr::req($cookieParams, 1);
        $expire = Arr::req($cookieParams, 2);
        $path = Arr::req($cookieParams, 3);
        $domain = Arr::req($cookieParams, 4);

        setcookie($name, $value, $expire, $path, $domain, null, true);
    }

    /**
     * Отправить тело ответа
     * @return Response
     */
    public function echoBody()
    {
        echo $this->responseBody;
        $this->responseBody = '';
        return $this;
    }

}
