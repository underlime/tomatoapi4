<?php
namespace krest\mock;

use krest\exceptions\InvalidDataException;
use krest\Request;

class ClientRequest
{
    private static $fieldsToSaveState = array(
        'REQUEST_METHOD',
        'CONTENT_TYPE',
        'REQUEST_URI',
        'HTTP_X_HTTP_METHOD_OVERRIDE',
    );

    private static $defaultValues = array(
        'REQUEST_METHOD' => Request::GET,
        'CONTENT_TYPE' => null,
        'REQUEST_URI' => '/',
        'HTTP_X_HTTP_METHOD_OVERRIDE' => null,
    );

    private
        $serverState,
        $startGet,
        $startPost,
        $startRequest,
        $rawBody;

    public function __construct()
    {
        $this->_setDefaultValues();
        $this->_saveState();
        Request::clear();
    }

    private function _setDefaultValues()
    {
        foreach (self::$defaultValues as $name => $val) {
            if (!isset($_SERVER[$name])) {
                $_SERVER[$name] = $val;
            }
        }
    }

    private function _saveState()
    {
        $this->serverState = array();
        foreach (self::$fieldsToSaveState as $fieldName) {
            if (empty($_SERVER[$fieldName])) {
                $this->serverState[$fieldName] = $_SERVER[$fieldName];
            }
        }

        $this->startGet = $_GET;
        $this->startPost = $_POST;
        $this->startRequest = $_REQUEST;

        $this->rawBody = Request::instance()->getRawRequestBody();
    }

    public function __destruct()
    {
        $this->_restoreState();
    }

    private function _restoreState()
    {
        foreach ($this->serverState as $name => $val) {
            $_SERVER[$name] = $val;
        }

        $_GET = $this->startGet;
        $_POST = $this->startPost;
        $_REQUEST = $this->startRequest;

        Request::instance()->setRawBody($this->rawBody);
    }

    /**
     * Установить мутод запроса
     * @param $method
     * @return \krest\mock\ClientRequest
     * @throws \krest\exceptions\InvalidDataException
     */
    public function setMethod($method)
    {
        $allowedMethods = array(
            Request::GET,
            Request::POST,
            Request::PUT,
            Request::DELETE
        );
        if (!in_array($method, $allowedMethods)) {
            throw new InvalidDataException('Wrong method');
        }

        $_SERVER['REQUEST_METHOD'] = $method;
        return $this;
    }

    /**
     * Установить метод с помощью X-HTTP-Header-Override,
     * допустимы методы PUT или DELETE
     * @param $method
     * @throws \krest\exceptions\InvalidDataException
     * @return \krest\mock\ClientRequest
     */
    public function setMethodByOverride($method)
    {
        $allowedMethods = array(
            Request::PUT,
            Request::DELETE
        );
        if (!in_array($method, $allowedMethods)) {
            throw new InvalidDataException('Wrong method');
        }

        $_SERVER['REQUEST_METHOD'] = Request::POST;
        $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'] = $method;

        return $this;
    }

    /**
     * Установить URI
     * @param $uri
     * @return \krest\mock\ClientRequest
     */
    public function setUri($uri)
    {
        $_SERVER['REQUEST_URI'] = $uri;
        return $this;
    }

    /**
     * Установить тип содержимого
     * @param $contentType
     * @return \krest\mock\ClientRequest
     */
    public function setContentType($contentType)
    {
        $_SERVER['CONTENT_TYPE'] = $contentType;
        return $this;
    }

    /**
     * Установить массив $_GET
     * @param array $get
     * @return \krest\mock\ClientRequest
     */
    public function setGetArray(array $get)
    {
        $_GET = $get;
        foreach ($get as $name => $val) {
            $_REQUEST[$name] = $get[$name];
        }

        return $this;
    }

    /**
     * Установить массив $_POST
     * @param array $post
     * @return \krest\mock\ClientRequest
     */
    public function setPostArray(array $post)
    {
        $_POST = $post;
        foreach ($post as $name => $val) {
            $_REQUEST[$name] = $post[$name];
        }

        return $this;
    }

    /**
     * Установить "сырое" тело запроса
     * @param $body
     * @return \krest\mock\ClientRequest
     */
    public function setRawRequestBody($body)
    {
        Request::instance()->setRawBody($body);
        return $this;
    }
}
