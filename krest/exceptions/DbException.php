<?php
namespace krest\exceptions;

class DbException extends \Exception
{
    private $query;

    public function __construct($message = '', $query = '', $code = 0, \Exception $previous = null)
    {
        $this->query = $query;
        parent::__construct($message, $code, $previous);
    }

    public function getQuery()
    {
        return $this->query;
    }
}
