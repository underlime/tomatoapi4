<?php
namespace krest\exceptions;

class ErrorException extends \ErrorException
{
    private $errno, $errstr, $errfile, $errline, $errcontext;

    public function __construct($errno, $errstr, $errfile, $errline, array $errcontext)
    {
        $this->errno = $errno;
        $this->errstr = $errstr;
        $this->errfile = $errfile;
        $this->errline = $errline;
        $this->errcontext = $errcontext;

        parent::__construct($errstr, E_ERROR, 1, $errfile, $errline);
    }

    public function getErrcontext()
    {
        return $this->errcontext;
    }

    public function getErrfile()
    {
        return $this->errfile;
    }

    public function getErrline()
    {
        return $this->errline;
    }

    public function getErrno()
    {
        return $this->errno;
    }

    public function getErrstr()
    {
        return $this->errstr;
    }
}
