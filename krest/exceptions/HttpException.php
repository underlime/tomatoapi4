<?php
namespace krest\exceptions;

class HttpException extends \Exception
{
    private $httpCode;

    /**
     * @param string $httpCode
     * @param string $message
     * @param int $code
     * @param \Exception|NULL $previous
     */
    public function __construct($httpCode, $message = '', $code = 0, $previous = null)
    {
        $this->httpCode = $httpCode;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return string
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }


}
