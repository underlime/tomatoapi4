<?php
namespace krest\exceptions;

/**
 * Class InvalidDataException
 * @package krest
 */
class InvalidDataException extends \RuntimeException
{
    public $data = [];
}
