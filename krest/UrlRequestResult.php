<?php
namespace krest;

class UrlRequestResult
{
    public
        $body,
        $headers,
        $httpCode,
        $contentType;
}
