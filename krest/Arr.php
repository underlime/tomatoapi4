<?php
namespace krest;

use krest\exceptions\InvalidDataException;

class Arr
{
    /**
     * @static
     * @param array $array
     * @param $key
     * @param $default
     * @return mixed
     * @assert (array(), 1) == NULL
     * @assert (array(1), 0) == 1
     * @assert (array(), 1, 2) == 2
     */
    public static function get($array, $key, $default = null)
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }

    /**
     * @static
     * @param array $array
     * @param $key
     * @throws InvalidDataException
     * @return mixed
     * @assert (array(1), 0) == 1
     */
    public static function req($array, $key)
    {
        if (isset($array[$key])) {
            return $array[$key];
        }
        throw new InvalidDataException("Array[{$key}] is not set");
    }

    /**
     * @static
     * @param array $array
     * @param $path
     * @param string $delimiter
     * @return array|mixed|null
     */
    public static function getByPath($array, $path, $delimiter = '.')
    {
        $pathParts = explode($delimiter, $path);
        $arrPart = $array;
        $result = null;

        $hasElements = false;
        foreach ($pathParts as $key) {
            if (strlen($key)) {
                if (!is_array($arrPart)) {
                    $hasElements = false;
                    break;
                }
                $arrPart = self::get($arrPart, $key);
                if ($arrPart === null) {
                    break;
                }
                $hasElements = true;
            }
        }

        if ($hasElements) {
            $result = $arrPart;
        }
        return $result;
    }

    /**
     * @param array $array
     * @param string $path
     * @param mixed $value
     * @param string $delimiter
     */
    public static function setByPath(&$array, $path, $value, $delimiter = '.')
    {
        $pathParts = array_filter(explode($delimiter, $path), function ($key) {
            return strlen($key);
        });
        $pathParts = array_values($pathParts);

        $element = &$array;
        $length = sizeof($pathParts);
        for ($i = 0; $i < $length; ++$i) {
            $key = $pathParts[$i];
            if (!(isset($element[$key]) && is_array($element[$key]))) {
                $element[$key] = [];
            }
            $element = &$element[$key];
        }
        $element = $value;
    }

    /**
     * Отфильтровать поля массива
     * @param array $array
     * @param array $namesList
     * @return array
     */
    public static function filterFields($array, array $namesList)
    {
        $newArray = array();
        foreach ($namesList as $name) {
            $newArray[$name] = self::get($array, $name);
        }
        return $newArray;
    }

    /**
     * Создать список значений из хеша
     * @static
     * @param array $array
     * @param $field
     * @return array
     */
    public static function fetchField(array $array, $field)
    {
        $list = array();
        foreach ($array as $row) {
            $list[] = self::req($row, $field);
        }
        return $list;
    }

    /**
     * Извлечь HTML-представление массива в виде списка
     * @param array|object $arr
     * @return string
     */
    public static function getArrayAsList($arr)
    {
        if (empty($arr)) {
            return '';
        }
        $out = "<ul class='array'>\n";
        foreach ($arr as $key => $val) {
            $isStruct = (is_array($val) || is_object($val));
            $className = $isStruct ? 'struct' : 'scalar';
            $out .= "<li class='$className'>\n";
            $out .= '<span class="key">'.$key."</span>\n";
            if ($isStruct) {
                $out .= self::getArrayAsList($val);
            }
            else {
                $out .= "<span class='eq'>=</span>\n<span class='value'>".$val."</span>\n";
            }
            $out .= "</li>\n";
        }
        $out .= "</ul>\n";
        return $out;
    }

    /**
     * Рекурсивное ("глубокое") слияние массивов
     * @param array $arr1
     * @param array $arr2
     * @param array ...
     * @return array
     */
    public static function deepMerge($arr1, $arr2)
    {
        $result = array();
        for ($i = 0, $total = func_num_args(); $i < $total; $i++) {
            // Get the next array
            $arr = func_get_arg($i);

            // Is the array associative?
            $assoc = self::isAssoc($arr);

            foreach ($arr as $key => $val) {
                if (isset($result[$key])) {
                    if (is_array($val) && is_array($result[$key])) {
                        if (Arr::isAssoc($val)) {
                            // Associative arrays are merged recursively
                            $result[$key] = Arr::deepMerge($result[$key], $val);
                        }
                        else {
                            // Find the values that are not already present
                            $diff = array_diff($val, $result[$key]);

                            // Indexed arrays are merged to prevent duplicates
                            $result[$key] = array_merge($result[$key], $diff);
                        }
                    }
                    else {
                        if ($assoc) {
                            // Associative values are replaced
                            $result[$key] = $val;
                        }
                        elseif (!in_array($val, $result, true)) {
                            // Indexed values are added only if they do not yet exist
                            $result[] = $val;
                        }
                    }
                }
                else {
                    // New values are added
                    $result[$key] = $val;
                }
            }
        }
        return $result;
    }

    /**
     * Tests if an array is associative or not.
     *
     *     // Returns TRUE
     *     Arr::isAssoc(array('username' => 'john.doe'));
     *
     *     // Returns FALSE
     *     Arr::isAssoc('foo', 'bar');
     *
     * @param   array   array to check
     * @return  boolean
     */
    public static function isAssoc(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }
}
