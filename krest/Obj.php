<?php
namespace krest;

class Obj
{
    /**
     * Извлечь значение из объекта
     * @param \stdClass $obj
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get(\stdClass $obj, $key, $default = null)
    {
        return (isset($obj->{$key}) ? $obj->{$key} : $default);
    }

    /**
     * Извлечь значение из массива, при неудаче - исключение
     * @param \stdClass $obj
     * @param string $key
     * @throws \Exception
     * @return mixed
     */
    public static function req(\stdClass $obj, $key)
    {
        if (isset($obj->{$key})) {
            return $obj->{$key};
        }

        throw new \Exception("Object->$key is not set");
    }
}
