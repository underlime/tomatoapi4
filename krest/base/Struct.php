<?php
namespace krest\base;

use krest\exceptions\InvalidDataException;

abstract class Struct
{
    public function __set($name, $value)
    {
        throw new \Exception("Cannot set $name");
    }

    /**
     * Возвращает список имен обязательных полей
     * @return array
     */
    protected function _requiredFields()
    {
        return array();
    }

    /**
     * Импортировать данные из массива
     * @param array $data
     */
    public function importArray(array $data)
    {
        foreach ($data as $name => $value) {
            $this->$name = $value;
        }
    }

    /**
     * Вернуть структуру как массив
     * @return array
     */
    public function asArray()
    {
        $data = self::_asArray($this, true);
        return $data;
    }

    private static function _asArray($obj, $includeEmpty)
    {
        $data = array();
        if (is_object($obj)) {
            $vars = get_object_vars($obj);
        }
        else {
            $vars = $obj;
        }

        foreach ($vars as $name => $val) {
            if ($includeEmpty or (!$includeEmpty && !empty($val))) {
                if (is_object($val) || is_array($val)) {
                    $data[$name] = self::_asArray($val, $includeEmpty);
                }
                else {
                    $data[$name] = $val;
                }
            }
        }

        return $data;
    }

    /**
     * Вернуть структуру как массив,
     * только непустые значения
     * @return array
     */
    public function asArrayNotEmpty()
    {
        $data = self::_asArray($this, false);
        return $data;
    }

    /**
     * Проверить, установлены ли обязательные поля
     * @throws \krest\exceptions\InvalidDataException
     */
    public function checkRequiredFields()
    {
        $requiredFields = $this->_requiredFields();
        foreach ($requiredFields as $name) {
            if (!(isset($this->$name) && !empty($this->$name))) {
                throw new InvalidDataException("Required field $name is empty");
            }
        }
    }

    /**
     * Являются ли все переменные пустыми
     * @return bool
     */
    public function isEmpty()
    {
        $empty = true;
        foreach ($this as $var) {
            $empty = empty($var);
            if (!$empty) {
                break;
            }
        }

        return $empty;
    }

    /**
     * Создать простой массив из массива структур
     * @param array $data
     * @return array
     */
    public static function createArray(array $data)
    {
        $result = array();
        foreach ($data as $key => $value) {
            $result[$key] = self::_asArray($value, true);
        }
        return $result;
    }
}
