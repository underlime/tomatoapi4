<?php
namespace krest\base;

/**
 * Class MagicAccessors
 * @package krest\base
 * Реализует логику "Магических аксессоров" __get и __set
 */
trait MagicAccessors
{
    public function __get($name)
    {
        $getter = "get{$name}";
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }
        throw new \InvalidArgumentException("$name is not defined");
    }

    public function __set($name, $value)
    {
        $setter = "set{$name}";
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        }
        else {
            throw new \InvalidArgumentException("$name is not defined");
        }
    }
}
