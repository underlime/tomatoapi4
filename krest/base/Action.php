<?php
namespace krest\base;

use krest\Arr;
use krest\exceptions\HttpException;
use krest\Request;

abstract class Action
{
    private static $_wasInvoked = false;

    private static $_reqClassName;
    private static $_reqId;

    private $_id;
    private $_answer = array();

    /**
     * @static
     * @param string $queryString
     * @throws \krest\exceptions\HttpException
     * @throws \Exception
     * @return \krest\base\Action
     */
    public static function factory($queryString)
    {
        if (self::$_wasInvoked) {
            throw new \Exception('Trying to invoke second action');
        }

        self::_setActionParts($queryString);

        if (class_exists(self::$_reqClassName)) {
            $action = new self::$_reqClassName(self::$_reqId);
        }
        else {
            throw new HttpException(404, 'Action not found');
        }

        //Флаг, показывающий, что один экшен уже был запущен,
        //в случае тестирования всегда должен быть FALSE
        self::$_wasInvoked = !defined('PHPUNIT_TESTING');
        return $action;
    }

    private static function _setActionParts($queryString)
    {
        $resParts = self::_getResourceParts($queryString);
        self::_setActionPartsValues($resParts);
    }

    private static function _getResourceParts($queryString)
    {
        $pattern = <<<PATTERN
#
^(?:
    (?:.*?/api)?
    (?P<soc_net>
        /(?:vk\.com|my\.mail\.ru|odnoklassniki\.ru)
    )
   (?:
        (?P<resource>
            (?:/[\w\.]+)+?
        )
        /?
    )
    (?:
        /
        (?P<id>\d+)
        /?
    )?
    (?P<get_params>\?.*)?
)$
#x
PATTERN;

        $res = preg_match_all($pattern, $queryString, $matches);
        if (!$res) {
            throw new HttpException(400, "Invalid URL: $queryString");
        }

        $resParts = array(
            'resource' => $matches['resource'][0],
            'id' => $matches['id'][0],
        );

        return $resParts;
    }

    private static function _setActionPartsValues(array $resParts)
    {
        self::$_reqId = (is_numeric($resParts['id'])) ? $resParts['id'] : null;

        $search = array('/', '.');
        $replace = array('\\', '_');
        self::$_reqClassName = '\action'.str_replace($search, $replace, $resParts['resource']);
    }

    protected function __construct($id)
    {
        $this->_id = $id;
    }


    /**
     * @return mixed
     * @throws \krest\exceptions\HttpException
     */
    public function get()
    {
        throw new HttpException(405, 'Method not allowed');
    }

    /**
     * @return mixed
     * @throws \krest\exceptions\HttpException
     */
    public function post()
    {
        throw new HttpException(405, 'Method not allowed');
    }

    /**
     * @return mixed
     * @throws \krest\exceptions\HttpException
     */
    public function put()
    {
        throw new HttpException(405, 'Method not allowed');
    }

    /**
     * @return mixed
     * @throws \krest\exceptions\HttpException
     */
    public function delete()
    {
        throw new HttpException(405, 'Method not allowed');
    }

    /**
     * Добавить массив к ответу
     * @param array $answer
     */
    protected function _addAnswerArr(array $answer)
    {
        if (!is_array($this->_answer)) {
            $this->_answer = [];
        }
        $this->_answer = array_merge($this->_answer, $answer);
    }

    /**
     * Установить строковое значение ответа
     * @param string $answer
     */
    protected function _setStringAnswer($answer)
    {
        $this->_answer = $answer;
    }

    /**
     * @return array|string
     */
    public function getAnswer()
    {
        return $this->_answer;
    }
}
