<?php
namespace krest\base;

use krest\CliParams;
use krest\Db;

/**
 * Class CliCommand
 * @package krest\base
 * @property string $output
 */
abstract class CliCommand
{
    use MagicAccessors;

    protected static $_cliParams = [];
    private static $_wasInvoked = false;
    private static $_reqClassName;

    private $_output = '';

    /**
     * @static
     * @throws \Exception
     * @return \krest\base\CliCommand
     */
    public static function factory()
    {
        if (self::$_wasInvoked) {
            throw new \Exception('Trying to invoke second command');
        }

        self::_detectCommandClass();

        if (class_exists(self::$_reqClassName)) {
            $command = new self::$_reqClassName();
        }
        else {
            throw new \Exception('Command not found');
        }

        //Флаг, показывающий, что один команда уже был запущена,
        //в случае тестирования всегда должен быть FALSE
        self::$_wasInvoked = !defined('PHPUNIT_TESTING');
        return $command;
    }

    private static function _detectCommandClass()
    {
        global $argc;

        if ($argc < 2) {
            self::_echoUsageMessage();
        }

        self::$_cliParams = CliParams::instance()
                            ->getCliParams();

        if (isset(self::$_cliParams['command'])) {
            self::$_reqClassName = "\\cli\\".self::$_cliParams['command'];
        }
        else {
            self::_echoUsageMessage();
        }
    }

    private static function _echoUsageMessage()
    {
        $message = PHP_EOL."Need parameter --command=<command>".PHP_EOL;
        throw new \Exception($message);
    }

    protected function __construct()
    {
    }

    /**
     * Вывести строку
     * @param string $string
     * @param string [$string]
     * @param string ...
     */
    protected function _print()
    {
        $output = implode(' ', func_get_args())."\n";
        $this->_output .= $output;
        echo $output;
    }

    abstract public function execute();

    /**
     * @return string
     */
    public function getOutput()
    {
        return $this->_output;
    }
}
