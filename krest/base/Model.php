<?php
namespace krest\base;

use krest\Arr;
use krest\Cache;
use krest\Db;

abstract class Model
{
    /**
     * Быстрый кеш
     * @var array
     */
    private static $cache = [];

    private $cacheEnabled = true;

    /**
     * Включить извлечение данных из кэша
     */
    public function enableCache()
    {
        $this->cacheEnabled = true;
    }

    /**
     * Отключить извлечение данных из кэша
     */
    public function disableCache()
    {
        $this->cacheEnabled = false;
    }

    /**
     * Извлечь состояние извлечения из кэша
     * @return bool
     */
    public function getCacheState()
    {
        return $this->cacheEnabled;
    }

    /**
     * Сбросить быстрый кеш
     * @static
     */
    public static function resetCache()
    {
        self::$cache = [];
    }

    /**
     * Извлечь данные из кеша
     * @param string $cacheKey
     * @return mixed
     */
    protected function _getDataFromCache($cacheKey)
    {
        $data = $this->_getDataFromFastCache($cacheKey);
        if ($data !== null) {
            return $data;
        }

        $data = $this->_getDataFromSlowCache($cacheKey);
        $this->_setIntoFastCache($cacheKey, $data);

        return $data;
    }

    private function _getDataFromFastCache($cacheKey)
    {
        if ($this->cacheEnabled) {
            $data = Arr::get(self::$cache, $cacheKey);
        }
        else {
            $data = null;
        }

        return $data;
    }

    private function _getDataFromSlowCache($cacheKey)
    {
        if ($this->cacheEnabled) {
            $data = Cache::instance()->get($cacheKey);
        }
        else {
            $data = null;
        }

        return $data;
    }

    private function _setIntoFastCache($cacheKey, $data, $time = null)
    {
        if ($data !== null && ($time === null or $time > 0)) {
            self::$cache[$cacheKey] = $data;
        }
        else {
            $this->_deleteFromFastCache($cacheKey);
        }
    }

    private function _deleteFromFastCache($cacheKey)
    {
        if (isset(self::$cache[$cacheKey])) {
            unset(self::$cache[$cacheKey]);
        }
    }

    /**
     * Занести данные в кеш
     * @param string $cacheKey
     * @param mixed $data
     * @param int|null $time
     * @return bool
     */
    protected function _setDataIntoCache($cacheKey, $data, $time = null)
    {
        $this->_setIntoFastCache($cacheKey, $data, $time);
        $res = Cache::instance()->set($cacheKey, $data, $time);
        return $res;
    }

    /**
     * Изменить данные в кеше
     * @param string $cacheKey
     * @param mixed $data
     * @param int|null $time
     * @return bool|null
     */
    protected function _updateCachedData($cacheKey, $data, $time = null)
    {
        $slowData = Cache::instance()->get($cacheKey);
        if ($slowData !== null) {
            $res = $this->_updateAndSetCacheItems($slowData, $data, $cacheKey, $time);
        }
        else {
            $res = $this->_handleNoDataToUpdate($cacheKey);
        }

        return $res;
    }

    private function _updateAndSetCacheItems($slowData, $data, $cacheKey, $time)
    {
        $newSlowData = $this->_updateCachedItems($slowData, $data);
        $res = Cache::instance()->set($cacheKey, $newSlowData, $time);
        $this->_setIntoFastCache($cacheKey, $newSlowData, $time);

        return $res;
    }

    private function _handleNoDataToUpdate($cacheKey)
    {
        $this->_deleteFromFastCache($cacheKey);
        return null;
    }

    private function _updateCachedItems($dataToChange, $newData)
    {
        if (is_array($dataToChange) && is_array($newData)) {
            foreach ($newData as $name => $val) {
                $dataToChange[$name] = $val;
            }
        }
        else {
            $dataToChange = $newData;
        }

        return $dataToChange;
    }

    /**
     * Удалить данные из кеша
     * @param string $cacheKey
     * @return bool
     */
    protected function _deleteDataFromCache($cacheKey)
    {
        $res = Cache::instance()->delete($cacheKey);
        $this->_deleteFromFastCache($cacheKey);
        return $res;
    }
}
