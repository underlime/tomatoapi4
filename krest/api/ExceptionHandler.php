<?php
namespace krest\api;

use krest\api\exceptionhandler\DbExceptionHandler;
use krest\api\exceptionhandler\ErrorExceptionHandler;
use krest\api\exceptionhandler\ExternalRequestHandler;
use krest\api\exceptionhandler\HttpExceptionHandler;
use krest\Arr;
use krest\exceptions\DbException;
use krest\exceptions\ErrorException;
use krest\exceptions\ExternalRequestException;
use krest\exceptions\HttpException;
use krest\Request;
use krest\strings\XmlRender;
use Logger;

class ExceptionHandler
{
    protected static $_always200 = false;
    protected static $_config;

    /**
     * @var \Exception
     */
    protected $_e;
    protected $_httpCode = 500;
    protected $_time;
    protected $_userMessage;

    /**
     * @var Logger
     */
    protected $_log;
    protected $_logName = 'error.common';

    /**
     * Установить эмуляцию кодов ошибок
     * @static
     * @param bool $always200
     */
    public static function setAlways200($always200)
    {
        self::$_always200 = $always200;
    }

    /**
     * Сделать запись в журнале
     */
    public function writeLog()
    {
        $this->_log->fatal($this->_getExceptionLogText());
    }

    /**
     * @static
     * @param \Exception $e
     * @return \krest\api\ExceptionHandler
     */
    public static function factory(\Exception $e)
    {
        if ($e instanceof ErrorException) {
            $handler = new ErrorExceptionHandler($e);
        }
        elseif ($e instanceof HttpException) {
            $handler = new HttpExceptionHandler($e);
        }
        elseif ($e instanceof DbException) {
            $handler = new DbExceptionHandler($e);
        }
        elseif ($e instanceof ExternalRequestException) {
            $handler = new ExternalRequestHandler($e);
        }
        else {
            $handler = new self($e);
        }
        return $handler;
    }

    protected function __construct(\Exception $e)
    {
        if (self::$_config === null) {
            self::$_config = Config::instance()->getForClass(__CLASS__);
        }
        $this->_e = $e;
        $this->_log = Logger::getLogger($this->_logName);
        $this->_time = date('Y-m-d H:i:s');
        $this->_userMessage = "General error at {$this->_time}!";
    }

    protected function _getExceptionLogText()
    {
        $code = $this->_e->getCode();
        $message = $this->_e->getMessage();
        $file = $this->_e->getFile();
        $line = $this->_e->getLine();
        $trace = $this->_e->getTraceAsString();

        $text = date('[Y-m-d H:i:s] ');
        $text .= "Exception #{$code}: ";
        $text .= "{$message}\n";
        $text .= "{$file} ";
        $text .= "({$line})\n";
        $text .= "{$trace}\n";
        return $text;
    }

    public function echoMessage()
    {
        if (php_sapi_name() == 'cli') {
            $message = $this->_e->getMessage() . PHP_EOL;
            $message .= $this->_e->getTraceAsString();
            echo $message, PHP_EOL;
        }
        else {
            $this->_echoHttpMessage();
        }
    }

    protected function _echoHttpMessage()
    {
        $always200 = Arr::get(self::$_config, 'always_code_200');
        if ($always200 || self::$_always200) {
            $this->_echoFormatMessageWithoutCode();
        }
        else {
            $this->_echoTextMessageWithCode();
        }
    }

    protected function _echoFormatMessageWithoutCode()
    {
        $queryParams = Request::instance()->getQueryParams();
        $format = Arr::get($queryParams, 'format', 'JSON');
        if ($format == 'JSON') {
            $this->_echoJsonMessageWithoutCode();
        }
        else {
            $this->_echoXmlMessageWithoutCode();
        }
    }

    private function _echoJsonMessageWithoutCode()
    {
        header('Content-type: application/json; charset=UTF-8');
        $data = array(
            'error' => array(
                'code' => $this->_httpCode,
                'message' => $this->_userMessage,
            ),
        );
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    private function _echoXmlMessageWithoutCode()
    {
        header('Content-type: application/xml; charset=UTF-8');
        $data = array(
            'error' => array(
                'code' => $this->_httpCode,
                'message' => $this->_userMessage,
            ),
        );
        echo XmlRender::render($data);
    }

    protected function _echoTextMessageWithCode()
    {
        header("{$_SERVER['SERVER_PROTOCOL']} {$this->_httpCode} Error");
        header('Content-type: text/plain; charset=UTF-8');
        echo $this->_userMessage, PHP_EOL;
    }
}
