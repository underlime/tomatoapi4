<?php
namespace krest\api;

class AutoLoader
{
    private $_baseDir;
    private $_libDir;

    public function __construct($baseDir)
    {
        $this->_baseDir = realpath($baseDir);
        $this->_libDir = $this->_baseDir.DIRECTORY_SEPARATOR.'lib';
    }

    public function load($className)
    {
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $className).'.php';
        if ($this->_loadClass($classPath) == false) {
            $classPath = str_replace('_', DIRECTORY_SEPARATOR, $className).'.php';
            $this->_loadClass($classPath);
        }
    }

    /**
     * @param $classPath
     * @return bool
     */
    private function _loadClass($classPath)
    {
        $loaded = true;
        $fullPath = $this->_baseDir.DIRECTORY_SEPARATOR.$classPath;
        if (file_exists($fullPath)) {
            require $fullPath;
        }
        else {
            $fullPath = $this->_libDir.DIRECTORY_SEPARATOR.$classPath;
            if (file_exists($fullPath)) {
                require $fullPath;
            }
            else {
                $loaded = false;
            }
        }
        return $loaded;
    }
}
