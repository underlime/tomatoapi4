<?php
namespace krest\api;

use model\spy\Spy;
use model\SpyData;

class SpySaver
{
    public function saveData()
    {
        $spyData = new SpyData();
        $spy = Spy::instance();
        if ($spy->getActionsCount()) {
            $spy->pushActionData(__FILE__, __METHOD__, __LINE__, 'Завершение работы', connection_status());
            $spyData->saveSpyingData($spy->getUserId(), $spy->getActions());
        }
    }
}
