<?php
namespace krest\api;

use krest\exceptions\ErrorException;

class FailsHandler
{
    private static $wasHandling = false;

    public function errorHandler($errno, $errstr, $errfile, $errline, array $errcontext)
    {
        throw new ErrorException($errno, $errstr, $errfile, $errline, $errcontext);
    }

    public function exceptionHandler(\Exception $e)
    {
        $handler = ExceptionHandler::factory($e);
        $handler->writeLog();
        if (!self::$wasHandling) {
            $handler->echoMessage();
        }
        self::$wasHandling = true;
    }
}
