<?php
namespace krest\api;

use krest\Arr;
use krest\base\Action;
use krest\Db;
use krest\exceptions\AlreadyExistsException;
use krest\exceptions\HttpException;
use krest\exceptions\InvalidDataException;
use krest\exceptions\NotFoundException;
use krest\Request;

class Application
{
    /**
     * @var \krest\Request
     */
    private $_request;

    /**
     * @var \krest\base\Action
     */
    private $_action;

    private $_inTransaction;

    /**
     * @static
     * @param $baseDir директория, в которой лежит index.php
     * @return \krest\api\Application
     */
    public static function factory($baseDir)
    {
        $application = new self($baseDir);
        return $application;
    }

    public function __construct($baseDir)
    {
        $this->systemInit();
    }

    private function systemInit()
    {
        mb_internal_encoding('UTF-8');
    }


    /**
     * Выполнить алгоритм
     * @throws \krest\exceptions\HttpException
     * @throws \Exception
     * @return \krest\base\Action
     */
    public function execute()
    {
        $this->_request = Request::instance();
        $this->_createAction();
        $this->_inTransaction = Db::instance()->beginTransaction();
        try {
            $this->_applyMethod();
        }
        catch (InvalidDataException $e) {
            $this->_rollBack();
            throw new HttpException(400, $e->getMessage(), 0, $e);
        }
        catch (NotFoundException $e) {
            $this->_rollBack();
            throw new HttpException(404, $e->getMessage(), 0, $e);
        }
        catch (AlreadyExistsException $e) {
            $this->_rollBack();
            throw new HttpException(409, $e->getMessage(), 0, $e);
        }
        catch (\Exception $e) {
            $this->_rollBack();
            throw $e;
        }

        if ($this->_inTransaction) {
            Db::instance()->commit();
        }
        return $this->_action;
    }

    private function _createAction()
    {
        $queryString = $this->_request->getRequestUri();
        $this->_action = Action::factory($queryString);
    }

    private function _applyMethod()
    {
        $method = $this->_request->getMethod();
        switch ($method) {
            case Request::GET:
                $this->_action->get();
                break;
            case Request::POST:
                $this->_action->post();
                break;
            case Request::PUT:
                $this->_action->put();
                break;
            case Request::DELETE:
                $this->_action->delete();
                break;
            default:
                throw new HttpException(405, 'Method not allowed');
        }
    }

    private function _rollBack()
    {
        if ($this->_inTransaction) {
            Db::instance()->rollBack();
        }
    }
}
