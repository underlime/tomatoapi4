<?php
namespace krest\api\exceptionhandler;

use krest\api\Config;
use krest\api\ExceptionHandler;
use krest\exceptions\ErrorException;
use Logger;

class ErrorExceptionHandler extends ExceptionHandler
{
    protected $_logName = 'error.program';

    /**
     * @var \krest\exceptions\ErrorException
     */
    protected $_e;

    protected function __construct(\Exception $e)
    {
        parent::__construct($e);
        $this->_userMessage = "Runtime error at {$this->_time}!";
    }

    protected function _getExceptionLogText()
    {
        $message = $this->_e->getErrstr();
        $errno = $this->_e->getErrno();
        $file = $this->_e->getErrfile();
        $line = $this->_e->getErrline();
        $trace = $this->_e->getTraceAsString();

        $text = date('[Y-m-d H:i:s] ');
        $text .= "$errno: $message\n$file ($line)\n";
        $text .= "{$trace}\n";

        return $text;
    }
}
