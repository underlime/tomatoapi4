<?php
namespace krest\api\exceptionhandler;

use krest\api\ExceptionHandler;
use krest\Arr;
use krest\exceptions\HttpException;

class HttpExceptionHandler extends ExceptionHandler
{
    protected $_logName = 'error.http';

    /**
     * @var \krest\exceptions\HttpException
     */
    protected $_e;

    protected function __construct(HttpException $e)
    {
        parent::__construct($e);
        $this->_httpCode = $this->_e->getHttpCode();
        $this->_userMessage = $this->_e->getMessage();
    }

    public function writeLog()
    {
        if ($this->_httpCode < 400 || $this->_httpCode >= 500) {
            parent::writeLog();
        }
        elseif (!in_array($this->_httpCode, [401, 403])) {
            $logger = \Logger::getLogger('error.http.4xx');
            $logger->warn($this->_getExceptionLogText());
        }
    }

    protected function _getExceptionLogText()
    {
        $code = $this->_e->getCode();
        $message = $this->_e->getMessage();
        $file = $this->_e->getFile();
        $line = $this->_e->getLine();
        $trace = $this->_e->getTraceAsString();

        $text = date('[Y-m-d H:i:s] ');
        $text .= "HttpException #{$code}: HttpStatus {$this->_httpCode}:";
        $text .= "{$message}\n";
        $text .= "{$file} ";
        $text .= "({$line})\n";
        $text .= "{$trace}\n";

        return $text;
    }
}
