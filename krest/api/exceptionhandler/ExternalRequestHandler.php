<?php
namespace krest\api\exceptionhandler;

use krest\api\ExceptionHandler;
use krest\Arr;
use krest\exceptions\ExternalRequestException;

class ExternalRequestHandler extends ExceptionHandler
{
    protected $_logName = 'error.external_request';

    /**
     * @var \krest\exceptions\ExternalRequestException
     */
    protected $_e;

    protected function __construct(ExternalRequestException $e)
    {
        parent::__construct($e);
        $this->_httpCode = 503;
        $this->_userMessage = 'Temporary unavailable. Please try later';
    }

    protected function _getExceptionLogText()
    {
        $code = $this->_e->getCode();
        $message = $this->_e->getMessage();
        $file = $this->_e->getFile();
        $line = $this->_e->getLine();
        $trace = $this->_e->getTraceAsString();

        $text = date('[Y-m-d H:i:s] ');
        $text .= "ExternalRequestException #{$code}: {$message}\n";
        $text .= "{$file} ";
        $text .= "({$line})\n";
        $text .= "{$trace}\n";

        return $text;
    }
}
