<?php
namespace krest\api\exceptionhandler;

use krest\api\Config;
use krest\api\ExceptionHandler;
use krest\exceptions\DbException;
use Logger;

class DbExceptionHandler extends ExceptionHandler
{
    protected $_logName = 'error.sql';

    /**
     * @var \krest\exceptions\DbException
     */
    protected $_e;

    protected function __construct(\Exception $e)
    {
        parent::__construct($e);
        $this->_userMessage = "Database error at {$this->_time}!";
    }

    protected function _getExceptionLogText()
    {
        $code = $this->_e->getCode();
        $message = $this->_e->getMessage();
        $file = $this->_e->getFile();
        $line = $this->_e->getLine();
        $trace = $this->_e->getTraceAsString();
        $query = $this->_e->getQuery();

        $text = date('[Y-m-d H:i:s] ');
        $text .= "DbException #{$code}: ";
        $text .= "{$message}\n";
        $text .= "{$query}\n";
        $text .= "{$file} ";
        $text .= "({$line})\n";
        $text .= "{$trace}\n";

        return $text;
    }
}
