<?php
namespace krest\api;

use krest\Arr;

class Config
{
    private static $_defaultFile = 'config.php';
    private static $_instances = [];

    /**
     * @var \Rediska
     */
    private static $_redisClient;

    private $_file;
    private $_key;
    private $_defaults;
    private $_config;

    /**
     * @static
     * @param $configFile
     * @return \krest\api\Config
     */
    public static function instance($configFile = null)
    {
        if ($configFile === null) {
            $configFile = self::$_defaultFile;
        }

        $configFilePath = realpath($configFile);
        if (!isset(self::$_instances[$configFilePath])) {
            self::$_instances[$configFilePath] = new self($configFilePath);
        }

        return self::$_instances[$configFilePath];
    }

    public function __construct($configFilePath)
    {
        if (!($configFilePath !== false && file_exists($configFilePath))) {
            throw new \Exception("Config file $configFilePath doesn't exists");
        }

        $config = require($configFilePath);
        $this->_defaults = $config;
        $this->_file = $configFilePath;
        $this->_key = 'krest_config:'.mb_strtolower(preg_replace('#[\\/:]+#', '_', $configFilePath));

        $this->_config = $this->getConfig();
    }

    /**
     * @param string $defaultConfigFile
     */
    public static function setDefaultConfigFile($defaultConfigFile)
    {
        self::$_defaultFile = $defaultConfigFile;
    }

    /**
     * @param \Rediska $redisClient
     */
    public static function setRedisClient(\Rediska $redisClient)
    {
        self::$_redisClient = $redisClient;
    }

    public function __clone()
    {
        throw new \Exception('Clone not available');
    }

    public function __wakeup()
    {
        throw new \Exception('Wakeup not available');
    }

    /**
     * Извлечь конфигурацию
     * @return array
     */
    public function getConfig()
    {
        if (self::$_redisClient === null) {
            $config = $this->_defaults;
        }
        else {
            $userConfig = $this->getUserConfig();
            if (empty($userConfig)) {
                $config = $this->_defaults;
            }
            else {
                $config = Arr::deepMerge($this->_defaults, $userConfig);
            }
        }
        return $config;
    }

    /**
     * Извлечь измененные пользователем параметры
     * @return array|null
     */
    public function getUserConfig()
    {
        $userConfig = self::$_redisClient->get($this->_key);
        if (empty($userConfig)) {
            $userConfig = [];
        }
        return $userConfig;
    }

    /**
     * Установить измененные пользователем параметры
     * @param array $userConfig
     * @throws \Exception
     * @return mixed
     */
    public function setUserConfig(array $userConfig)
    {
        if (self::$_redisClient !== null) {
            return self::$_redisClient->set($this->_key, $userConfig);
        }
        throw new \Exception('Redis client is not set');
    }

    public function getForClass($className)
    {
        return (array)(Arr::getByPath($this->_config, $className, '\\'));
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->_file;
    }
}
