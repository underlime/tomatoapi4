<?php
namespace krest;

use krest\exceptions\HttpException;
use krest\strings\XmlRender;

class View
{
    const XML = "XML";

    const JSON = "JSON";

    private $format;

    private $data;

    /**
     * @static
     * @param array|string $data
     * @return \krest\View
     */
    public static function factory($data)
    {
        $view = new self($data);
        return $view;
    }

    public function __construct($data)
    {
        $this->data = $data;
        $this->_getFormat();
    }

    private function _getFormat()
    {
        $requestParams = Request::instance()->getRequestBodyParams();
        $this->format = Arr::get($requestParams, 'format', self::JSON);

        if ($this->format != self::JSON && $this->format != self::XML) {
            throw new HttpException(400, 'Wrong format');
        }
    }

    /**
     * @return string
     */
    public function render()
    {
        if (is_array($this->data)) {
            if ($this->format == self::XML) {
                $resp = $this->_renderXml();
            }
            else {
                $resp = $this->_renderJson();
            }
        }
        else {
            $resp = $this->data;
        }
        return $resp;
    }

    private function _renderXml()
    {
        Response::instance()->setHeader('Content-type', 'application/xml; charset=utf-8');
        return XmlRender::render($this->data);
    }

    private function _renderJson()
    {
        Response::instance()->setHeader('Content-type', 'application/json; charset=utf-8');
        return json_encode($this->data, JSON_UNESCAPED_UNICODE);
    }
}
