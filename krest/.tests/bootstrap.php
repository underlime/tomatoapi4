<?php
use krest\api\AutoLoader;
use krest\api\Config;

define('PHPUNIT_TESTING', true);
define('ROOT_PATH', realpath(__DIR__.'/../..'));

ini_set('display_errors', 1);
error_reporting(-1);
mb_internal_encoding('UTF-8');

require ROOT_PATH.'/krest/api/AutoLoader.php';
$autoLoader = new AutoLoader(ROOT_PATH);
spl_autoload_register([$autoLoader, 'load']);
if (file_exists(ROOT_PATH.'/vendor/autoload.php')) {
    require ROOT_PATH.'/vendor/autoload.php';
}

Config::setDefaultConfigFile(__DIR__.'/krest_config.php');
\Logger::configure(Config::instance()->getConfig()['_logger']);
