<?php

use krest\SocNet;

require_once __DIR__.'/TestCase.php';

class SocNetTest extends TestCase
{
    /**
     * @var \krest\SocNet
     */
    private $object;

    protected function setUp()
    {
        parent::setUp();

        $requestMock = new \krest\mock\ClientRequest();
        $requestMock->setPostArray(array(
                                        'sid' => '013102ac2abc812005eea3671ebfe20815ec77dbed2973cb5bc70fca2f070f',
                                        'auth_key' => 'b46ef6bdbd397d587a872682d54bf0d9',
                                        'viewer_id' => 3081515,
                                   ));
        SocNet::setSocNetName(SocNet::VK_COM);
        $this->object = SocNet::instance();
        try {
            $this->object->checkAuth();
        }
        catch (Exception $e) {
        }
    }

    protected function tearDown()
    {
        parent::tearDown();
        \krest\SocNet::clear();
    }

    /**
     * @covers \krest\socnet\VkCom::getFriends
     */
    public function test_getFriends()
    {
        $idsList = $this->object->getFriendsList(3081515);
        $this->assertTrue(!empty($idsList));
        $this->_echoTestPassed();
    }
}
