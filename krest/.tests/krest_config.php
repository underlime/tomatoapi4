<?php
return array(
    '_logger' => array(
        'logs_dir' => __DIR__.'/test_data/logs',
        'loggers' => array(
            'error.program' => array(
                'appenders' => array('program_errors'),
                'level' => 'ERROR',
            ),
            'error.common' => array(
                'appenders' => array('common_errors'),
                'level' => 'ERROR',
            ),
            'error.sql' => array(
                'appenders' => array('sql_errors'),
                'level' => 'ERROR',
            ),
            'error.http' => array(
                'appenders' => array('http_errors'),
                'level' => 'ERROR',
            ),
            'error.http.4xx' => array(
                'appenders' => array('http_warnings'),
                'level' => 'WARN',
            ),
            'error.external_request' => array(
                'appenders' => array('external_requests'),
                'level' => 'ERROR',
            ),
        ),
        /*'rootLogger' => array(
            'appenders' => array('default'),
            'level' => 'ERROR',
        ),*/
        'appenders' => array(
            'default' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/default.log',
                    'append' => true
                )
            ),
            'program_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/error.log',
                    'append' => true
                )
            ),
            'common_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/common_exceptions.log',
                    'append' => true
                )
            ),
            'sql_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/sql_db_exceptions.log',
                    'append' => true
                )
            ),
            'http_errors' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/http_exceptions.log',
                    'append' => true
                )
            ),
            'http_warnings' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/http_warnings.log',
                    'append' => true
                )
            ),
            'external_requests' => array(
                'class' => 'LoggerAppenderFile',
                'layout' => array(
                    'class' => 'LoggerLayoutSimple'
                ),
                'params' => array(
                    'file' => __DIR__.'/test_data/logs/external_request_exceptions.log',
                    'append' => true
                )
            ),
        )
    ),
    'krest' => array(
        'socnet' => array(
            'VkCom' => array(
                'api_id' => 2893571,
                'api_secret' => 'TmmJet3LZ0WnNItJpWic',
                'send_at_once_notifications' => 100,
                'notifications_timeout' => 1,
            ),
        ),
        'api' => array(
            'ExceptionHandler' => array(
                'always_code_200' => true,
            ),
        ),
        'Cache' => array(
            'default_provider' => \krest\Cache::MEMCACHE,
            'default_ttl' => 86400,
            'emulate' => false,
        ),
        'Db' => array(
            'default' => array(
                //'unix_socket' => '/var/run/mysql.sock',
                'host' => '192.168.2.10',
                'db_name' => 'tomato_test',
                'user' => 'root',
                'password' => 'password',
                'soc_net' => 'vk.com',
            ),
        ),
        'SocNet' => array(
            'token_salt' => 'ded_maksim',
        ),
    ),
);
