<?php

use krest\EventDispatcher;
use lib\RediskaFactory;

abstract class DbTestCase extends \PHPUnit_Extensions_Database_TestCase
{
    /**
     * @var \PDO
     */
    protected static $_pdoCache;

    /**
     * @var PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection
     */
    protected static $_con;

    /**
     * @var \PDO
     */
    protected $_pdo;

    /**
     * @var \krest\EventDispatcher
     */
    protected $_eventDispatcher;

    protected $_localTesting;
    protected $_dbHost;
    protected $_dbName = "tomato_test";
    protected $_dbUser = "root";
    protected $_dbPassword;

    function __construct()
    {
        $this->_localTesting = isset($_SERVER["DEVELOPMENT_MACHINE"]);
        $this->_dbHost = $this->_localTesting ? "192.168.2.10" : "localhost";
        $this->_dbPassword = $this->_localTesting ? "password" : "1qaz3wasql";

        if (self::$_pdoCache === null || self::$_con === null) {
            $this->_createNewConnection();
        }
        $this->_pdo = self::$_pdoCache;
    }

    private function _createNewConnection()
    {
        $dsn = "mysql:host={$this->_dbHost};dbname={$this->_dbName};charset=utf8";
        self::$_pdoCache = new PDO($dsn, $this->_dbUser, $this->_dbPassword);
        self::$_con = $this->createDefaultDBConnection(self::$_pdoCache, $this->_dbName);
    }


    /**
     * @return \PHPUnit_Extensions_Database_Operation_Composite
     */
    public function getSetUpOperation()
    {
        $operations = array(
            \PHPUnit_Extensions_Database_Operation_Factory::DELETE_ALL(),
            \PHPUnit_Extensions_Database_Operation_Factory::INSERT(),
        );
        return new \PHPUnit_Extensions_Database_Operation_Composite($operations);
    }

    /**
     * Получить PDO
     * @return \PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection|\PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    protected function getConnection()
    {
        return self::$_con;
    }

    /**
     * Настройки перед тестами
     */
    protected function setUp()
    {
        parent::setUp();
        \krest\base\Model::resetCache();

        $this->_eventDispatcher = EventDispatcher::instance();
        $this->_resetEventDispatcher();
    }

    /**
     * Очистка после тестов
     */
    protected function tearDown()
    {
        if ($this->_pdo->inTransaction()) {
            $this->_pdo->rollBack();
        }
        parent::tearDown();
    }

    /**
     * Очистить Redis
     */
    protected function _flushRedis()
    {
        $rediska = RediskaFactory::getInstance();
        $rediska->flushDb();
    }

    /**
     * Сбросить события
     */
    protected function _resetEventDispatcher()
    {
        $this->_eventDispatcher->clearDispatchQueue();
        $this->_eventDispatcher->clearEventsList();
    }
}
