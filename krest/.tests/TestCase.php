<?php

use krest\api\Config;

class TestCase extends PHPUnit_Framework_TestCase
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        Config::setDefaultConfigFile(__DIR__.'/krest_config.php');
    }

    /**
     * @deprecated
     */
    protected function _echoTestPassed()
    {
    }
}
