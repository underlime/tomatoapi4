<?php

require_once dirname(__FILE__).'/TestCase.php';

class UrlRequestTest extends TestCase
{
    /**
     * @var \krest\UrlRequest
     */
    private $object;

    protected function setUp()
    {
        parent::setUp();
        $this->object = new \krest\UrlRequest();

    }

    /**
     * @covers \krest\UrlRequest::execute
     */
    public function test_works()
    {
        $result = $this->object
                      ->setUrl('http://127.0.0.1/')
                  ->setMethod(\krest\Request::GET)
                  ->setRequestBodyAsArray(array('test' => 1))
                  ->setQueryParams(array('test' => 1))
                  ->setNeedHeaders(true)
                  ->execute();

        $this->assertInstanceOf('\krest\UrlRequestResult', $result);

        $this->_echoTestPassed();
    }
}
