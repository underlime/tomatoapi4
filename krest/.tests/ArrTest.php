<?php
use krest\Arr;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2012-05-22 at 21:23:20.
 */
class ArrTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Arr
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Arr;
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * Generated from @assert (array(), 1) == NULL.
     *
     * @covers krest\Arr::get
     */
    public function testGet()
    {
        $this->assertEquals(
            null,
            krest\Arr::get(array(), 1)
        );
    }

    /**
     * Generated from @assert (array(1), 0) == 1.
     *
     * @covers krest\Arr::get
     */
    public function testGet2()
    {
        $this->assertEquals(
            1,
            krest\Arr::get(array(1), 0)
        );
    }

    /**
     * Generated from @assert (array(), 1, 2) == 2.
     *
     * @covers krest\Arr::get
     */
    public function testGet3()
    {
        $this->assertEquals(
            2,
            krest\Arr::get(array(), 1, 2)
        );
    }

    /**
     * Generated from @assert (array(1), 0) == 1.
     * @covers krest\Arr::req
     */
    public function testReq()
    {
        $this->assertEquals(
            1,
            krest\Arr::req(array(1), 0)
        );
    }

    /**
     * @covers krest\Arr::req
     */
    public function testReqException()
    {
        try {
            Arr::req(array(), 'test');
            $this->fail('getted');
        }
        catch (Exception $e) {
        }
    }

    /**
     * @covers krest\Arr::getByPath
     */
    public function testGetByPath1()
    {
        $arr = array(
            'key1' => array(
                'key2' => 2
            )
        );

        $res = Arr::getByPath($arr, '.key1.key2.');
        $this->assertEquals($res, 2);
    }

    /**
     * @covers krest\Arr::getByPath
     */
    public function testGetByPath2()
    {
        $arr = array(
            'key1' => array(
                'key2' => 2
            )
        );

        $res = Arr::getByPath($arr, '.key1.key2.key3');
        $this->assertNull($res);
    }

    /**
     * @covers krest\Arr::setByPath
     */
    public function testSetByPath1()
    {
        $arr = array(
            'key1' => array(
                'key2' => 2
            )
        );

        Arr::setByPath($arr, '.key1.key2.key3', 'test');

        $expected = array(
            'key1' => array(
                'key2' => array(
                    'key3' => 'test'
                )
            )
        );
        $this->assertEquals($expected, $arr);
    }

    /**
     * @covers krest\Arr::setByPath
     */
    public function testSetByPath2()
    {
        $arr = array(
            'key1' => array(
                'key2' => array(
                    '1' => 'test'
                )
            )
        );

        Arr::setByPath($arr, '.key1.key2.key3', 'test');

        $expected = array(
            'key1' => array(
                'key2' => array(
                    '1' => 'test',
                    'key3' => 'test'
                )
            )
        );
        $this->assertEquals($expected, $arr);
    }

    /**
     * @covers krest\Arr::getByPath
     */
    public function testGetByPath3()
    {
        $arr = array(
            'key1' => array(
                'key2' => 2
            )
        );

        $res = Arr::getByPath($arr, '');
        $this->assertNull($res);
    }

    /**
     * @covers krest\Arr::deepMerge
     */
    public function testDeepMerge()
    {
        $arr1 = [
            'test1' => [
                'a' => 'b',
                'c' => 'd',
            ]
        ];
        $arr2 = [
            'test1' => [
                'a' => '1',
                'b' => '2',
            ],
            'test2' => [1, 2, 3]
        ];
        $arr3 = [
            'test1' => [
                'd' => 'yellow',
            ],
            'test2' => [4]
        ];
        $result = $this->object->deepMerge($arr1, $arr2, $arr3);

        $expected = [
            'test1' => [
                'a' => '1',
                'b' => '2',
                'c' => 'd',
                'd' => 'yellow',
            ],
            'test2' => [1, 2, 3, 4]
        ];
        $this->assertEquals($expected, $result);
    }
}
