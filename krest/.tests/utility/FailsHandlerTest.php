<?php

use krest\api\FailsHandler;

class FailsHandlerTest extends PHPUnit_Framework_TestCase
{
    private $failsHandler;

    public function setUp()
    {
        $this->failsHandler = new FailsHandler();
    }

    /**
     * @covers krest\api\ErrorException::__construct
     * @covers krest\api\FailsHandler::errorHandler
     */
    public function testErrorThrowsException()
    {
        set_error_handler(array($this->failsHandler, 'errorHandler'), E_ALL | E_STRICT);
        try {
            /** @noinspection PhpUndefinedVariableInspection */
            /** @noinspection PhpExpressionResultUnusedInspection */
            @$notSetVariable;
            $this->fail();
        }
        catch (\krest\exceptions\ErrorException $e) {
        }
    }
}
