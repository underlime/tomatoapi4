<?php
namespace krest\utility;


class XHProf
{
    /**
     * @var string
     */
    private $_namespace;

    function __construct($namespace="krest")
    {
        if (!extension_loaded('xhprof')) {
            throw new \ErrorException('XHprof isn\'t enabled');
        }
        $this->_namespace = $namespace;
    }

    public function start()
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        /** @noinspection PhpUndefinedConstantInspection */
        xhprof_enable(XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY);
    }

    public function stop()
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        $data = xhprof_disable();

        $libPath = realpath(__DIR__.'/../xhprof_lib');
        require_once "$libPath/utils/xhprof_lib.php";
        require_once "$libPath/utils/xhprof_runs.php";

        $runs = new \XHProfRuns_Default();
        $id = $runs->save_run($data, $this->_namespace);
        return $id;
    }
}
