<?php
namespace krest;

class Prob
{
    public static function probably($p)
    {
        $r = mt_rand()/mt_getrandmax();
        return ($r <= $p);
    }
}
