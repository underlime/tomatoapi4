<?php
namespace krest;

class CliParams
{
    private static $instance;
    private $cliParams = array();

    /**
     * @static
     * @return \krest\CliParams
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return array
     */
    public function getCliParams()
    {
        return $this->cliParams;
    }

    protected function __construct()
    {
        global $argc, $argv;
        $this->_setCliParams($argc, $argv);
    }

    private function _setCliParams($argc, array $argv)
    {
        $this->cliParams['@flags'] = array();

        $longPattern = '#^--(?<name>[\w-]+?)=(?<value>.+)$#';
        $shortPattern = '#^-(?<flags>[a-z]+)$#i';

        for ($i = 1; $i < $argc; ++$i) {
            if (preg_match($longPattern, $argv[$i], $matches)) {
                $this->cliParams[$matches['name']] = $matches['value'];
            }
            elseif (preg_match($shortPattern, $argv[$i], $matches)) {
                $this->cliParams['@flags'] = array_merge(
                    $this->cliParams['@flags'],
                    str_split($matches['flags'])
                );
            }
        }
    }
}
