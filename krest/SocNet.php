<?php
namespace krest;

use krest\api\Config;
use krest\exceptions\InvalidDataException;
use krest\Request;
use krest\socnet\MmRu;
use krest\socnet\OkRu;
use krest\socnet\VkCom;

abstract class SocNet
{
    const VK_COM = "vk.com";
    const OK_RU = "odnoklassniki.ru";
    const MM_RU = "my.mail.ru";

    const LANG_RU = 'ru';
    const LANG_EN = 'en';

    private static $_instance;
    protected static $_socNetName;

    protected $_socNetId;
    protected $_config = [];

    /**
     * @return mixed
     */
    public function getSocNetId()
    {
        return $this->_socNetId;
    }

    /**
     * Очистка, только для тестов
     * @return void
     */
    public static function clear()
    {
        self::$_instance = null;
    }

    /**
     * @param string $socNet
     * @throws InvalidDataException
     */
    public static function setSocNetName($socNet)
    {
        if (!in_array($socNet, [self::VK_COM, self::OK_RU, self::MM_RU])) {
            throw new InvalidDataException("Wrong social network code: $socNet");
        }
        self::$_socNetName = $socNet;
    }

    /**
     * @return string
     */
    public static function getSocNetName()
    {
        return self::$_socNetName;
    }

    /**
     * @static
     * @return \krest\SocNet
     */
    public static function instance()
    {
        if (self::$_instance === null) {
            self::$_instance = self::_createInstance();
        }
        return self::$_instance;
    }

    private static function _createInstance()
    {
        self::_setSocNetNameFromEnvironment();
        switch (self::$_socNetName) {
            case self::VK_COM:
                $instance = new VkCom();
                break;
            case self::OK_RU:
                $instance = new OkRu();
                break;
            case self::MM_RU:
                $instance = new MmRu();
                break;
            default:
                throw new InvalidDataException('Wrong social network id');
        }

        return $instance;
    }

    private static function _setSocNetNameFromEnvironment()
    {
        if (self::$_socNetName === null) {
            $cliParams = CliParams::instance()->getCliParams();
            self::$_socNetName = Arr::get($cliParams, 'soc_net');
        }
    }

    protected function __construct()
    {
        $this->_config = Config::instance()->getForClass(__CLASS__);
    }

    /**
     * Язык пользователя
     * @return string
     */
    public function getLanguage()
    {
        return self::LANG_RU;
    }

    /**
     * Подставить инстанс (только для тестов)
     * @param SocNet $instance
     * @throws \Exception
     */
    public static function setMockInstance(SocNet $instance)
    {
        if (!defined('PHPUNIT_TESTING')) {
            throw new \Exception('Функция недоступна вне тестирования');
        }
        self::$_instance = $instance;
        self::$_instance->__construct();
    }

    /**
     * Проверить авторизацию
     * @abstract
     * @throws \krest\exceptions\HttpException
     * @return void
     */
    abstract public function checkAuth();

    /**
     * Создать токен для проверки
     * @abstract
     * @return string
     */
    abstract public function makeToken();

    /**
     * Извлечь список друзей
     * @abstract
     * @param string $socNetId
     * @return array
     */
    abstract public function getFriendsList($socNetId);

    /**
     * @abstract
     * @param array $socNetIdsList
     * @param $message
     * @return array
     */
    abstract public function sendNotification(array $socNetIdsList, $message);
}
