<?php
namespace krest;

abstract class Cache
{
    const APC = "apc";
    const MEMCACHE = "memcache";
    const FAKE = "fake";

    private static $_instances = array();
    private static $_config;

    protected static $_prefix = '';

    /**
     * Возвращает экземпляр класса
     * @param string $cacheType
     * @return Cache
     */
    public static function instance($cacheType = null)
    {
        $config = self::_loadConfig();
        $emulate = Arr::get($config, "emulate");

        if ($emulate || defined("KREST_DISABLE_CACHE") && KREST_DISABLE_CACHE) {
            $cacheType = self::FAKE;
        }
        elseif ($cacheType === null) {
            $defaultProvider = Arr::req($config, "default_provider");
            $cacheType = $defaultProvider;
        }

        if (!isset(self::$_instances[$cacheType])) {
            self::$_instances[$cacheType] = self::_getInstance($cacheType);
        }

        return self::$_instances[$cacheType];
    }

    protected static function _loadConfig()
    {
        if (self::$_config === null) {
            self::$_config = api\Config::instance()->getForClass(__CLASS__);
        }
        return self::$_config;
    }

    private static function _getInstance($cacheType)
    {
        switch ($cacheType) {
            case self::MEMCACHE:
                $cache = new cache\Memcache();
                break;
            case self::APC:
                $cache = new cache\Apc();
                break;
            case self::FAKE:
                $cache = new cache\Fake();
                break;
            default:
                throw new \Exception("Wrong cache type");
        }

        return $cache;
    }

    protected function __construct()
    {
    }

    /**
     * @return string
     */
    public static function getPrefix()
    {
        return self::$_prefix;
    }

    /**
     * @param string $prefix
     */
    public static function setPrefix($prefix)
    {
        self::$_prefix = $prefix;
    }

    public function __clone()
    {
        throw new \Exception("Clone is not available");
    }

    public function __wakeup()
    {
        throw new \Exception("Wake up is not available");
    }

    /**
     * @abstract
     * @param string $name
     * @param mixed $value
     * @param (int|null) $ttl
     * @return mixed
     */
    abstract public function set($name, $value, $ttl = null);

    /**
     * @abstract
     * @param string $name
     * @return mixed
     */
    abstract public function get($name);

    /**
     * @abstract
     * @param string $name
     * @return mixed
     */
    abstract public function delete($name);

    /**
     * Flush all data
     * @return mixed
     */
    abstract public function flushAll();
}
