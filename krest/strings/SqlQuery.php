<?php
namespace krest\strings;

class SqlQuery
{
    /**
     * Построить секцию SET для INSERT или UPDATE запроса по именам полей
     * @param array $setFieldsNames Массив имен полей
     * @return string Секция SET
     */
    public static function buildSetSection(array $setFieldsNames)
    {
        $setParts = array();
        foreach ($setFieldsNames as $name) {
            $setParts[] = "`$name`=:$name";
        }

        $set = implode(', ', $setParts);

        return $set;
    }

    /**
     * Построить массив параметров для запроса по массиву значений
     * @param array $setFields Массив значений, где ключи - имена полей
     * @param array $simpleParams Массив "обычных" параметров запроса
     * @return array Массив всех параметров
     */
    public static function buildQueryParams(array $setFields, array $simpleParams = array())
    {
        $params = array();
        foreach ($setFields as $name => $val) {
            $params[':'.$name] = $val;
        }

        $params = array_merge($params, $simpleParams);
        return $params;
    }

}
