<?php
namespace krest\strings;

class Text
{
    /**
     * Сделать произвольный текст чистым простым текстом
     * @param string $dirtyText
     * @return string
     */
    public static function clear($dirtyText)
    {
        $cleanText = preg_replace('/[^\p{Latin}\p{Cyrillic}\pN\pP\pS\pZ]+/u', '', $dirtyText);
        $cleanText = strip_tags($cleanText);
        $cleanText = preg_replace('/\s{2,}/', ' ', $cleanText);
        $cleanText = preg_replace('/((\r)?\n){2,}/', "\n", $cleanText);
        $cleanText = trim($cleanText);

        return $cleanText;
    }

    /**
     * Сократить текст
     * @param string $text
     * @param int $limit
     * @return string
     */
    public static function reduce($text, $limit)
    {
        if (mb_strlen($text) > $limit) {
            $text = mb_substr($text, 0, $limit - 3).'...';
        }

        return $text;
    }

    /**
     * Проверяет две строки на равенстко без учета регистра
     * @param $str1
     * @param $str2
     * @return bool
     */
    public static function isEquals($str1, $str2)
    {
        $str1 = mb_strtolower($str1);
        $str2 = mb_strtolower($str2);

        $res = (strcmp($str1, $str2) === 0);
        return $res;
    }

    /**
     * Подготовить строку к выводу
     * @param string $str
     * @param string $strEncoding
     * @return string
     */
    public static function prepareToPrint($str, $strEncoding = "cp1251")
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === "WIN") {
            if (php_sapi_name() === "cli") {
                $str = iconv($strEncoding, "cp866", $str);
            }
            else {
                $str = iconv($strEncoding, "UTF-8", $str);
            }
        }
        return $str;
    }
}
