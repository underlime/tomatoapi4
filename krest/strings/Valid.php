<?php
namespace krest\strings;

class Valid
{
    /**
     * Проверить идентификатор на валидность
     * @param string $name
     * @return bool
     */
    public static function ident($name)
    {
        return (bool)preg_match('/^[\w]+$/', $name);
    }

    /**
     * Проверка на допустимость поля данных
     * @param string $value
     * @return bool
     */
    public static function dataField($value)
    {
        return (bool)preg_match('/^[a-z0-9_]{1,32}$/i', $value);
    }

    /**
     * Проверка корректности имени класса
     * @param $className
     * @return bool
     */
    public static function className($className)
    {
        return (bool)preg_match('/^[\w\\\]+$/', $className);
    }

}
