<?php
namespace krest\strings;

use krest\base\Struct;

class XmlRender
{
    /**
     * @var \DOMDocument
     */
    private static $domDocument;

    /**
     * Создать xml-документ из ассоциативного массива
     * @param array $data
     * @param string $rootName
     * @return string
     */
    public static function render(array $data, $rootName = 'data')
    {
        self::$domDocument = new \DOMDocument('1.0', 'UTF-8');
        $rootNode = self::_createXmlSubnode($rootName, $data);
        self::$domDocument->appendChild($rootNode);

        self::$domDocument->formatOutput = true;
        $text = self::$domDocument->saveXML();

        self::$domDocument = null;
        return $text;
    }

    /**
     * Создать дочерний узел
     * @param string $name
     * @param mixed $data
     * @return \DOMElement
     */
    private static function _createXmlSubnode($name, $data)
    {
        if (is_numeric($name)) {
            $name = 'item';
        }

        $node = self::$domDocument->createElement($name);
        if (is_array($data)) {
            self::_handleArrayData($node, $data);
        }
        elseif (is_object($data) && $data instanceof Struct) {
            $arData = $data->asArray();
            self::_handleArrayData($node, $arData);
        }
        elseif (is_object($data)) {
            $arData = get_object_vars($data);
            self::_handleArrayData($node, $arData);
        }
        else {
            self::_handleStringData($node, $data);
        }

        return $node;
    }

    private static function _handleArrayData(\DOMElement $node, array $data)
    {
        foreach ($data as $name => $subData) {
            $child = self::_createXmlSubnode($name, $subData);
            $node->appendChild($child);
        }
    }

    private static function _handleStringData(\DOMElement $node, $data)
    {
        $strData = (string)$data;
        $child = self::$domDocument->createCDATASection($strData);
        $node->appendChild($child);
    }
}
