<?php
namespace krest;

use krest\exceptions\ExternalRequestException;

class UrlRequest
{
    private $url;
    private $port = 80;
    private $method = Request::GET;
    private $contentType = 'application/x-www-form-urlencoded';
    private $queryParams = array();
    private $requestBody = '';
    private $needSetHeaders = false;

    /**
     * @param string $url
     * @return \krest\UrlRequest
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param int $port
     * @return \krest\UrlRequest
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @param string $method
     * @return \krest\UrlRequest
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param string $contentType
     * @return \krest\UrlRequest
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @param array $queryParams
     * @return \krest\UrlRequest
     */
    public function setQueryParams(array $queryParams)
    {
        $this->queryParams = $queryParams;
        return $this;
    }

    /**
     * @param string $requestBody
     * @return \krest\UrlRequest
     */
    public function setRequestBodyAsString($requestBody)
    {
        $this->requestBody = $requestBody;
        return $this;
    }

    /**
     * @param array $requestBodyParams
     * @return \krest\UrlRequest
     */
    public function setRequestBodyAsArray(array $requestBodyParams)
    {
        $this->requestBody = http_build_query($requestBodyParams);
        return $this;
    }

    /**
     * @param bool $needSetHeaders
     * @return \krest\UrlRequest
     */
    public function setNeedHeaders($needSetHeaders)
    {
        $this->needSetHeaders = $needSetHeaders;
        return $this;
    }

    /**
     * @return UrlRequestResult
     */
    public function execute()
    {
        if (extension_loaded('curl')) {
            $ch = $this->_createCurlHandler();
            return $this->_executeCurlRequest($ch);
        }
        else {
            return $this->_executeStream();
        }
    }

    private function _createCurlHandler()
    {
        $fullUrl = $this->_buildUrl();

        $ch = curl_init($fullUrl);
        $this->_setOptions($ch);

        return $ch;
    }

    private function _buildUrl()
    {
        if ($this->queryParams) {
            $urlDelimiter = (strpos($this->url, '?') ? '&' : '?');
            $fullUrl = $this->url.$urlDelimiter.http_build_query($this->queryParams);
        }
        else {
            $fullUrl = $this->url;
        }

        return $fullUrl;
    }

    private function _setOptions($ch)
    {
        $params = array(
            CURLOPT_PORT => $this->port,
            CURLOPT_CUSTOMREQUEST => $this->method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HEADER => $this->needSetHeaders,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Kraken cURL)',
        );

        if ($this->method == Request::POST or $this->method == Request::PUT) {
            $params[CURLOPT_HTTPHEADER] = array("Content-type: {$this->contentType}");
            $params[CURLOPT_POSTFIELDS] = $this->requestBody;
        }

        curl_setopt_array($ch, $params);
    }

    private function _executeCurlRequest($ch)
    {
        $result = new UrlRequestResult();
        $result->body = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch));
        }

        $result->httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $result->contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

        if ($this->needSetHeaders) {
            $headersSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $result->headers = substr($result->body, 0, $headersSize);
            $result->body = substr($result->body, $headersSize);
        }

        curl_close($ch);
        return $result;
    }

    private function _executeStream()
    {
        $result = new UrlRequestResult();
        $fullUrl = $this->_buildUrl();

        $opts = array(
            'http' => array(
                'method' => $this->method,
                'user_agent' => 'Mozilla/5.0 (Kraken stream)'
            )
        );

        if ($this->method == Request::POST or $this->method == Request::PUT) {
            $opts['http']['header'] = "Content-type: {$this->contentType}";
            $opts['http']['content'] = $this->requestBody;
        }
        $context = stream_context_create($opts);
        $stream = @fopen($fullUrl, 'r', false, $context);
        if ($stream === false) {
            $errorData = error_get_last();
            throw new ExternalRequestException(Arr::get($errorData, 'message'));
        }

        $metaData = stream_get_meta_data($stream);
        $result->body = stream_get_contents($stream);
        fclose($stream);

        $codeData = preg_filter('#^http/1\.\d\s+(\d+).*#i', '$1', $metaData['wrapper_data']);
        $typeData = preg_filter('#Content-Type:(.+?)#i', '$1', $metaData['wrapper_data']);
        $result->httpCode = (int)($codeData ? end($codeData) : 500);
        $result->contentType = trim($typeData ? end($typeData) : 'application/octet-stream');

        return $result;
    }
}
