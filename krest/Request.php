<?php
namespace krest;

use krest\exceptions\HttpException;

class Request
{
    const GET = 'GET';

    const PUT = 'PUT';

    const POST = 'POST';

    const DELETE = 'DELETE';

    private static $instance;

    private $rawBody;

    //Инициализируются при создании объекта
    private $method;

    private $contentType;

    private $charset;

    //Лениво инициализируются
    private $queryString;

    private $queryParams;

    private $requestBodyParams;

    private $rawRequestBody;

    private $parsedQueryString;

    /**
     * Вернуть инстанцированный объект
     * @return \krest\Request Единственный объект запроса
     * @throws \Exception Исключение при неверном HTTP методе
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        $this->_getQueryParams();
    }

    public function __clone()
    {
        throw new \Exception('Clone not available');
    }

    public function __wakeup()
    {
        throw new \Exception('Wakeup not available');
    }

    private function _getQueryParams()
    {
        //X-HTTP-Method-Override
        $this->method = $this->_getMethod();
        if (!in_array($this->method, array(self::GET, self::POST, self::PUT, self::DELETE))) {
            if (php_sapi_name() != 'cli') {
                throw new HttpException(405, 'Method Not Allowed');
            }
        }

        $this->_setContentParams();
    }

    private function _setContentParams()
    {
        $contentType = Arr::get($_SERVER, 'CONTENT_TYPE');
        if (!$contentType) {
            return;
        }

        $regExp = '#^([\w-]+/[\w-]+)(;.*?(charset=([\w-]+)).*)?$#i';
        preg_match_all($regExp, $contentType, $contentParams);

        $this->contentType = $contentParams[1][0];
        $this->charset = $contentParams[4][0];
    }

    private function _getMethod()
    {
        $override = Arr::get($_SERVER, 'HTTP_X_HTTP_METHOD_OVERRIDE');
        $defaultRequestMethod = Arr::get($_SERVER, 'REQUEST_METHOD');

        if ($override && $defaultRequestMethod == self::POST) {
            $finalRequestMethod = $override;
        }
        else {
            $finalRequestMethod = $defaultRequestMethod;
        }

        return $finalRequestMethod;
    }

    /**
     * Извлечь метод запроса
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * Извлечь значение Content-type
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Извлечь имя ресурса
     * @return string
     */
    public function getRequestUri()
    {
        if ($this->queryString === null) {
            $this->queryString = Arr::get($_SERVER, 'REQUEST_URI');
        }

        return $this->queryString;
    }

    /**
     * Извлечь данные строки запроса
     * @return array
     */
    public function getQueryParams()
    {
        if ($this->queryParams === null) {
            $this->queryParams = $_GET;
        }

        return $this->queryParams;
    }

    /**
     * Извлечь части строки запроса
     * @return array
     */
    public function getParsedQueryString()
    {
        if ($this->parsedQueryString === null) {
            $queryString = $this->getRequestUri();
            $this->parsedQueryString = parse_url($queryString);
        }

        return $this->parsedQueryString;
    }

    /**
     * Извлечь необработанные данные тела запроса
     * @return string
     */
    public function getRawRequestBody()
    {
        if ($this->rawRequestBody === null) {
            $this->_getRawRequestBody();
        }

        return $this->rawRequestBody;
    }

    private function _getRawRequestBody()
    {
        switch ($this->method) {
            case self::POST:
            case self::PUT:
                $this->rawRequestBody = $this->_getRawBody();
                break;
            default:
                $this->rawRequestBody = '';
        }
    }

    private function _getRawBody()
    {
        if ($this->rawBody) {
            $rawBody = $this->rawBody;
        }
        else {
            $rawBody = file_get_contents('php://input');
        }

        return $rawBody;
    }

    /**
     * Извлечь данные тела запроса
     * @return array
     */
    public function getRequestBodyParams()
    {
        if ($this->requestBodyParams === null) {
            $this->_getRawRequestBody();
            $this->_getRequestBodyParams();
        }

        return $this->requestBodyParams;
    }

    private function _getRequestBodyParams()
    {
        switch ($this->method) {
            case self::POST:
                $this->requestBodyParams = $_POST;
                break;
            case self::PUT:
                $this->requestBodyParams = $this->_getPutData();
                break;
            default:
                $this->requestBodyParams = array();
        }
    }

    private function _getPutData()
    {
        if (stripos($this->contentType, 'application/x-www-form-urlencoded') === 0) {
            $putData = $this->_getDataFromInput();
        }
        else {
            $putData = array();
        }

        return $putData;
    }

    private function _getDataFromInput()
    {
        parse_str($this->rawRequestBody, $requestVars);
        return $requestVars;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getCookie($name)
    {
        return Arr::get($_COOKIE, $name);
    }

    /**
     * Вызов данной функции допустим только в тестах
     * @static
     * @throws \Exception
     */
    public static function clear()
    {
        self::$instance = null;
    }

    /**
     * @param string $rawBody
     * @throws \Exception
     */
    public function setRawBody($rawBody)
    {
        $this->rawBody = $rawBody;
    }
}
