<?php
namespace krest;

use krest\api\Config;
use krest\base\MagicAccessors;
use krest\exceptions\DbException;
use krest\strings\Text;
use PDO;
use PDOException;
use PDOStatement;

/**
 * Class Db
 * @package krest
 * @property \PDO $pdo
 * @property string $instId
 * @property string $dsn
 * @property string $user
 * @property string $password
 */
class Db
{
    use MagicAccessors;

    protected static $_instances = [];
    protected static $_config;

    private static $_defaultName = "default";
    private $_arrayParamsExists = false;
    private $_instId;
    private $_socNetName;

    /**
     * @var \PDO
     */
    private $_pdo;

    private $_dsn;
    private $_user;
    private $_password;

    /**
     * Создание объекта
     * @param string $instId
     * @return Db
     */
    public static function instance($instId = null)
    {
        if ($instId === null) {
            $instId = self::$_defaultName;
        }

        if (!isset(self::$_instances[$instId])) {
            self::$_instances[$instId] = new self($instId);
        }

        return self::$_instances[$instId];
    }

    protected function __construct($instId)
    {
        $this->_instId = $instId;
        try {
            $this->_initNewConnection($instId);
        }
        catch (PDOException $e) {
            $origMessage = Text::prepareToPrint($e->getMessage());
            $message = "Unable to establish the '{$this->_instId}' connection: {$origMessage}";
            throw new DbException($message, null, 0, $e);
        }
    }

    protected function _initNewConnection($instId)
    {
        $config = self::_loadConfig();
        $instConf = Arr::req($config, $instId);

        $this->_dsn = "mysql:";
        if (isset($instConf["unix_socket"])) {
            $this->_dsn .= "unix_socket={$instConf["unix_socket"]}";
        }
        elseif (isset($instConf["host"])) {
            $this->_dsn .= "host={$instConf['host']}";
        }
        else {
            throw new \Exception("No host or socket param for DB");
        }
        $this->_dsn .= ";dbname={$instConf['db_name']};charset=utf8";

        $this->_pdo = new PDO($this->_dsn, $instConf["user"], $instConf["password"]);
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_user = $instConf["user"];
        $this->_password = $instConf["password"];

        $this->_socNetName = $instConf['soc_net'];
    }

    private static function _loadConfig()
    {
        if (self::$_config === null) {
            self::$_config = Config::instance()->getForClass(__CLASS__);
        }

        return self::$_config;
    }

    final public function __clone()
    {
        throw new \Exception("Clone not available");
    }

    final public function __wakeup()
    {
        throw new \Exception("Wakeup not available");
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array
     */
    public function select($sql, array $params = [])
    {
        return $this->exec($sql, $params)
               ->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array
     */
    public function insert($sql, array $params = [])
    {
        $query = $this->exec($sql, $params);
        $result = $this->_createInsertResult($query);
        return $result;
    }

    private function _createInsertResult(PDOStatement $query)
    {
        $result = array(
            "affected_rows" => $query->rowCount(),
            "insert_id" => $this->_pdo->lastInsertId()
        );

        return $result;
    }

    /**
     * @param string $sql
     * @param array $params
     * @throws DbException
     * @return PDOStatement
     */
    public function exec($sql, array $params = [])
    {
        $sql = $this->_replaceArraysInQuery($sql, $params);

        $query = $this->_pdo->prepare($sql);
        if ($this->_arrayParamsExists) {
            foreach ($params as $name => $value) {
                $this->_bindValue($query, $name, $value);
            }
        }
        else {
            foreach ($params as $name => $value) {
                $query->bindValue($name, $value);
            }
        }

        try {
            $query->execute();
        }
        catch (PDOException $e) {
            throw new DbException($e->getMessage(), $sql);
        }

        return $query;
    }

    private function _replaceArraysInQuery($sql, array $params)
    {
        $this->_arrayParamsExists = false;

        foreach ($params as $oldName => $value) {
            if (is_array($value)) {
                $newName = $this->_getInSetName($oldName, $value);
                $sql = str_replace($oldName, $newName, $sql);
                $this->_arrayParamsExists = true;
            }
        }

        return $sql;
    }

    private function _getInSetName($name, array $value)
    {
        $length = sizeof($value);
        $nameParts = [];
        for ($i = 0; $i < $length; ++$i) {
            $key = $this->_createInSetKey($name, $i);
            $nameParts[] = $key;
        }

        $newName = "(".implode(",", $nameParts).")";
        return $newName;
    }

    private function _createInSetKey($name, $c)
    {
        $key = str_replace(":", ":{$c}_", $name);
        return $key;
    }

    private function _bindValue(PDOStatement $query, $name, $value)
    {
        if (is_array($value)) {
            $this->_bindArrayValue($query, $name, $value);
        }
        else {
            $query->bindValue($name, $value);
        }
    }

    private function _bindArrayValue(PDOStatement $query, $name, array $value)
    {
        $c = 0;
        foreach ($value as $valuePart) {
            $key = $this->_createInSetKey($name, $c++);
            $query->bindValue($key, $valuePart);
        }
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array
     */
    public function update($sql, array $params = [])
    {
        $query = $this->exec($sql, $params);
        $result = $this->_createUDResult($query);
        return $result;
    }

    private function _createUDResult(PDOStatement $query)
    {
        $result = array(
            "affected_rows" => $query->rowCount()
        );

        return $result;
    }

    /**
     * @param string $sql
     * @param array $params
     * @return array
     */
    public function delete($sql, array $params = [])
    {
        $query = $this->exec($sql, $params);
        $result = $this->_createUDResult($query);
        return $result;
    }

    /**
     * Экранировать и заключить в кавычки параметр запроса
     * @param mixed $value
     * @param int $type
     * @return string
     */
    public function quote($value, $type = PDO::PARAM_STR)
    {
        return $this->_pdo->quote($value, $type);
    }

    /**
     * Начать транзакцию
     * @return bool
     * TRUE если сейчас был старт, FALSE, если старт был раньше
     */
    public function beginTransaction()
    {
        if ($this->_pdo->inTransaction()) {
            return false;
        }
        $this->_pdo->beginTransaction();
        return true;
    }

    /**
     * Фиксация транзакции
     * @return bool
     */
    public function commit()
    {
        return $this->_pdo->commit();
    }

    /**
     * Откат транзакции
     * @return bool
     */
    public function rollBack()
    {
        return $this->_pdo->rollBack();
    }

    /**
     * Находимся ли мы в транзакции?
     * @return bool
     */
    public function inTransaction()
    {
        return $this->_pdo->inTransaction();
    }

    /**
     * @param string $defaultName
     */
    public static function setDefaultName($defaultName)
    {
        self::$_defaultName = $defaultName;
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->_pdo;
    }

    /**
     * @return string
     */
    public function getInstId()
    {
        return $this->_instId;
    }

    /**
     * @return string
     */
    public function getDsn()
    {
        return $this->_dsn;
    }

    /**
     * @return string
     */
    public function getSocNetName()
    {
        return $this->_socNetName;
    }

    /**
     * Извлечь доступные id соединений
     * @return array
     */
    public static function getInstIds()
    {
        return array_keys(self::_loadConfig());
    }

    /**
     * Извлечь доступные id соединений
     * @return array
     */
    public static function getSocNetTable()
    {
        $config = self::_loadConfig();
        $table = [];
        foreach ($config as $instId => $data) {
            $table[$data['soc_net']] = $instId;
        }
        return $table;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->_password;
    }
}
