<?php
namespace krest;

class Random
{
    /**
     * Создать список случайных чисел
     * @static
     * @param $min
     * @param $max
     * @param $count
     * @return array
     */
    public static function getRandomList($min, $max, $count)
    {
        mt_srand();

        $list = array();
        $i = 0;
        while (++$i <= $count) {
            while (
            in_array(
                $r = mt_rand($min, $max),
                $list
            )
            ) {
                ;
            }
            $list[] = $r;
        }
        return $list;
    }
}
