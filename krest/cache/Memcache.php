<?php
namespace krest\cache;

use krest\Cache;

class Memcache extends Cache
{
    /**
     * @var Memcache
     */
    private $_memcache;

    protected function __construct()
    {
        parent::__construct();
        $this->_memcache = new \Memcache();
        $this->_memcache->connect("127.0.0.1", 11211);
    }

    public function __destruct()
    {
        $this->_memcache->close();
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param (int|null) $ttl
     * @return bool
     */
    public function set($name, $value, $ttl = null)
    {
        $name = self::$_prefix.$name;
        if ($ttl === null) {
            $config = $this->_loadConfig();
            $ttl = $config["default_ttl"];
        }
        return $this->_memcache->set($name, $value, 0, $ttl);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        $name = self::$_prefix.$name;
        $value = $this->_memcache->get($name);
        if ($value === false) {
            $value = null;
        }
        return $value;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function delete($name)
    {
        $name = self::$_prefix.$name;
        return $this->_memcache->delete($name);
    }

    /**
     * Flush all data
     * @return mixed
     */
    public function flushAll()
    {
        $this->_memcache->flushAll();
    }
}
