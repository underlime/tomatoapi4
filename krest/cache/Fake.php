<?php
namespace krest\cache;

use krest\Cache;

class Fake extends Cache
{
    /**
     * @param string $name
     * @param mixed $value
     * @param (int|null) $ttl
     * @return mixed
     */
    public function set($name, $value, $ttl = null)
    {
        return true;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return null;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function delete($name)
    {
        return true;
    }

    /**
     * Flush all data
     * @return mixed
     */
    public function flushAll()
    {
        return true;
    }
}
