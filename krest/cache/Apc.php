<?php
namespace krest\cache;

use krest\Cache;

class Apc extends Cache
{
    /**
     * @param string $name
     * @param mixed $value
     * @param null $ttl
     * @return bool|mixed
     */
    public function set($name, $value, $ttl = null)
    {
        $name = self::$_prefix.$name;
        if ($ttl === null) {
            $config = $this->_loadConfig();
            $ttl = $config['default_ttl'];
        }
        return apc_store($name, $value, $ttl);
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function get($name)
    {
        $name = self::$_prefix.$name;
        $value = apc_fetch($name, $success);
        if (!$success) {
            $value = null;
        }
        return $value;
    }

    /**
     * @param string $name
     * @return bool|mixed|string[]
     */
    public function delete($name)
    {
        $name = self::$_prefix.$name;
        return apc_delete($name);
    }

    /**
     * Flush all data
     * @return mixed
     */
    public function flushAll()
    {
        return apc_clear_cache("user");
    }
}
