<?php
namespace krest;

class EventDispatcher
{
    /**
     * @var \krest\EventDispatcher
     */
    private static $instance = null;

    private $eventsList = array();
    private $dispatchQueue = array();
    private $wasDispatched = array();

    /**
     * @return \krest\EventDispatcher
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __clone()
    {
        throw new \Exception('Unable to clone');
    }

    public function __wakeup()
    {
        throw new \Exception('Unable to wakeup');
    }

    /**
     * @param string $event
     * @param callable $handler
     */
    public function addEventListener($event, $handler)
    {
        if (!isset($this->eventsList[$event])) {
            $this->eventsList[$event] = array();
        }

        $key = $this->_getHandlerKey($handler);
        $this->eventsList[$event][$key] = $handler;

        foreach ($this->dispatchQueue as $cnt => $eventData) {
            $curEvent = Arr::get($eventData, 'event');
            if ($curEvent == $event) {
                $params = Arr::get($eventData, 'params');
                call_user_func($handler, $params);
                unset($this->dispatchQueue[$cnt]);
            }
        }
    }

    private function _getHandlerKey($handler)
    {
        if (is_array($handler) && sizeof($handler) == 2) {
            $key = $this->_getObjectCallbackKey($handler[0], $handler[1]);
        }
        elseif ($handler instanceof \Closure) {
            /** @noinspection PhpParamsInspection */
            $key = spl_object_hash($handler);
        }
        elseif (is_string($handler)) {
            $key = $handler;
        }
        else {
            throw new \Exception('Invalid callback');
        }

        return $key;
    }

    private function _getObjectCallbackKey($object, $method)
    {
        if (is_object($object)) {
            $part1 = spl_object_hash($object);
        }
        elseif (is_string($object)) {
            $part1 = $object;
        }
        else {
            throw new \Exception('Invalid callback');
        }

        $key = $part1.'_'.$method;
        return $key;
    }

    /**
     * Удалить обработчик события
     * @param string $event
     * @param callable $handler
     */
    public function removeEventListener($event, $handler)
    {
        $key = $this->_getHandlerKey($handler);
        if (isset($this->eventsList[$event][$key])) {
            unset($this->eventsList[$event][$key]);
        }
    }

    /**
     * Разослать событие
     * @param string $event
     * @param mixed $params
     */
    public function dispatchEvent($event, $params = null)
    {
        $handlers = Arr::get($this->eventsList, $event, array());
        if ($handlers) {
            foreach ($handlers as $function) {
                call_user_func($function, $params);
            }
        }
        else {
            $this->dispatchQueue[] = array(
                'event' => $event,
                'params' => $params
            );
        }
        $this->wasDispatched[] = $event;
    }

    /**
     * @param string $event
     * @param callable $handler
     * @return bool
     */
    public function hasEventListener($event, $handler)
    {
        $key = $this->_getHandlerKey($handler);
        $has = isset($this->eventsList[$event][$key]);

        return $has;
    }

    /**
     * @return array
     */
    public function getEventsList()
    {
        return $this->eventsList;
    }

    /**
     * @return array
     */
    public function getDispatchQueue()
    {
        return $this->dispatchQueue;
    }

    /**
     * Очистить список событий
     */
    public function clearEventsList()
    {
        $this->eventsList = array();
    }

    /**
     * Очистить очередь событий
     */
    public function clearDispatchQueue()
    {
        $this->dispatchQueue = array();
    }

    /**
     * @param string $event
     * @return bool
     */
    public function wasDispatched($event)
    {
        return in_array($event, $this->wasDispatched);
    }
}
