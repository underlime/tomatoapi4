#!/bin/bash

set -e

DIR=$(cd `dirname $0` && pwd)
cd ${DIR}

git reset --hard
git pull
git submodule update
cd "$DIR/_utility/backoffice/protected"

for ID in "db" "db_ok" "db_mm"; do
    echo "Migrate $ID"
    php yiic.php migrate --interactive=0 --connectionID=${ID}
done
