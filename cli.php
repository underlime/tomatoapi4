<?php
namespace cli_file;

use krest\base\CliCommand;
use krest\api\AutoLoader;
use krest\api\Config;
use krest\api\FailsHandler;
use lib\RediskaFactory;

(php_sapi_name() == 'cli') or die('?');

require __DIR__.'/krest/api/AutoLoader.php';
$autoLoader = new AutoLoader(__DIR__);
spl_autoload_register(array($autoLoader, 'load'));
require __DIR__.'/vendor/autoload.php';

$failsHandler = new FailsHandler();
set_error_handler(array($failsHandler, 'errorHandler'), E_ALL | E_STRICT);
set_exception_handler(array($failsHandler, 'exceptionHandler'));


$config = realpath(__DIR__.'/config.php');
Config::setDefaultConfigFile($config);
Config::setRedisClient(RediskaFactory::getInstance()); // TODO: подумать, как заменить это
\Logger::configure(Config::instance()->getConfig()['_logger']);

$command = CliCommand::factory();
$command->execute();

$logFile = Config::instance()->getConfig()['_common']['data_dir'].'/.logs/cli.log';
$message = date('Y-m-d H:i:s')."\n$command->output\n";
file_put_contents($logFile, $message, FILE_APPEND);
